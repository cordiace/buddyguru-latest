<?php

namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;

class PublitoService
{
  protected $apiUrl = 'https://api.publit.io/v1/';

  public function uploadFile($file)
  {
      $apiKey = `DsVyLPT4b6P0jAd3w9e8`;
      $apiSecret =`5dHahN62MngehxAaz2Rx3TQvPAziXRz5`;

      $httpClient = new Client();

      $response = $httpClient->request('POST', $this->apiUrl . 'files/create', [
          'multipart' => [
              [
                  'name' => 'file',
                  'contents' => file_get_contents($file->path()),
                  'filename' => $file->getClientOriginalName(),
              ],
          ],
          'headers' => [
              'Authorization' => 'Bearer ' . $apiKey . ':' . $apiSecret,
          ],
      ]);

      $responseData = json_decode($response->getBody(), true);

      return $responseData['url'];
  }
}
