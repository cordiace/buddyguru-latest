<?php

namespace App\Http\Controllers\pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TutorSession;
use App\Models\User;
use App\Models\CategorySkills;
use App\Models\TrendingVideos;
use App\Models\TutorSkills;
use Carbon\Carbon;
class HomePage extends Controller
{
  public function index()
  {

    $now = Carbon::now();

    $sessions = TutorSession::all();
    $Skills = CategorySkills::all();
    $tutors = User::where('role','tutor')->with('tutorSkills')->get();
    // dd($tutors);
    $TutorSkill = TutorSkills::all();
    $TrendingVideos = TrendingVideos::all();
    $users = User::where('role','student')->get();
    $latest_tutors = User::where('role','tutor')->orderBy('created_at', 'desc')->take(5)->get();
    $latest_students = User::where('role','student')->orderBy('created_at', 'desc')->take(5)->get();
    $last_tutor= collect($tutors)->last();
    $last_student= collect($users)->last();
    $tutors_monthly = User::select('id', 'created_at')->where('role','tutor')->whereYear('created_at',$now->year)->get()->groupBy(function($date) {
        return Carbon::parse($date->created_at)->format('m');
    });
    $student_monthly = User::select('id', 'created_at')->where('role','student')->whereYear('created_at',$now->year)->get()->groupBy(function($date) {
        return Carbon::parse($date->created_at)->format('m');
    });
    $grapghTwo = $this->getMonthData($tutors_monthly);
    $grapghOne = $this->getMonthData($student_monthly);


    return view('content.pages.pages-home',\compact('TrendingVideos','Skills','sessions','users','tutors', 'last_tutor', 'last_student', 'latest_tutors', 'latest_students','grapghTwo','grapghOne'));
  }

  public function getMonthData($data)
  {
      $datamcount = [];
      $dataArr = [];
      foreach ($data as $key => $value) {
          $datamcount[(int)$key] = count($value);
      }
      for($i = 1; $i <= 12; $i++){
          if(!empty($datamcount[$i])){
              $dataArr[$i] = $datamcount[$i];
          }else{
              $dataArr[$i] = 0;
          }
      }

      return $dataArr;
  }
}
