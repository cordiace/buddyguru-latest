<?php

namespace App\Http\Controllers;

use App\Banner;
use App\Models\Region;
use App\Batch;
use App\Models\Category;
use App\CmsField;
use App\Models\Country;
use App\Models\Generalsetting;
use App\Grade;
use App\Models\Package;
use App\PackageBatch;
use App\Models\Payment;
use App\Review;
use App\Sessions;
use App\Models\CategorySkills;
use App\Models\StudentSkills;
use App\Models\StudentDetail;
use App\SubCategory;
use App\Models\Subscriber;
use App\Token;
use App\Traits\VideoFFMpeg;
use App\Traits\ZoomJWT;
use App\Models\TutorDetail;
use App\TutorPortfolio;
use App\TutorSession;
use App\Models\User;
use App\Models\UserSubscription;
use DB;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Mail;
use Validator;

class ApiController extends Controller
{
    // use VideoFFMpeg;
    // use ZoomJWT;

    const MEETING_TYPE_INSTANT = 1;
    const MEETING_TYPE_SCHEDULE = 2;
    const MEETING_TYPE_RECURRING = 3;
    const MEETING_TYPE_FIXED_RECURRING_FIXED = 8;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('AuthCheck');
    }

    public function getTermsAndPolicy()
    {
        $response['status'] = "true";
        $response['data']['terms'] = "http://3.131.58.88/uploads/terms.pdf";
        $response['data']['privacy_policy'] = "http://3.131.58.88/uploads/privacy_policy.pdf";

        return response()->json($response, 200);

    }

    public function refresh(Request $request)
    {
        $bearer = $request->bearerToken();
        if ($token = DB::table('personal_access_tokens')->where('token', hash('sha256', $bearer))->first()) {
            if ($user = \App\User::find($token->tokenable_id)) {
                $token = $user->createRefreshToken('web', 20)->plainTextToken;
                $response['status'] = "true";
                $response['data']['refresh_token'] = $token;

                return response()->json($response, 200);
            }
        }

        $response['errors'] = 'error getting data';
        return response()->json($response, 400);
    }

    public function userLogin(Request $request)
    {
//        $user=User::findOrFail($request->id);
//        return response()->json(['data' => $request->password], 404);
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();
            // $token = $user->createToken('auth_token')->plainTextToken;
            // $token = $user->createAuthToken('auth_token', env('ACCESS_TOKEN_TIME', 10))->plainTextToken;
            $token =  Str::random(60);
            // $refresh_token = $user->createRefreshToken('refresh_token', 20)->plainTextToken;
            $user->remember_token = $token;
            $user->save();

            $response['status'] = "true";
            $response['data']['token'] = $token;
            $response['data']['type'] = $user->role;
            try {
                // do {
                // $token_key = Str::random(24);
                // } while (User::where("token", "=", $token_key)->first() instanceof User);
                // $user->token = $token_key;
                // $user->save();

                if ($user->role == 'student') {
                    // $this->getSubscription($user);
                    
                    $student = User::where('id', $user->id)->first();
                 
                    if ($student) {
                       
                        $country = $student->location;

                        // $grade = Grade::find($student->grade);
                        $skills = StudentSkills::where('student_id',$student->id)->pluck('skill_id');
                     
$categorySkill =CategorySkills::whereIn('id',$skills)->get();
                        $response['data']['detail'] = [
                            'token' => $student->token,
                            // 'refresh_token' => $refresh_token,
                            'type' => $student->role,
                            'id' => $student->id,
                            'name' => $student->name,
                            'email' => $student->email,
                            'phone_number' => $student->phone_number,
                            'dob' => date('Y-m-d', strtotime($student->dob)),
                            'location' => [
                                'country_id' => $country,
                                'country_name' => $student->country->country_name,
                            ],
                            'profile_image' => $student->profile_image != "" ? url('/' . $student->profile_image) : "",
                            // 'parent_name' => $student->parent_name,
                            // 'parent_email' => $student->parent_email,
                            // 'parent_mobile' => $student->parent_mobile,
                            // 'grade' => [
                            //     'grade_id' => $grade->id,
                            //     'grade_name' => $grade->grade_name,
                            // ],
                          
                        ];
                        foreach ($categorySkill as $skill) {
                            // Access individual skill properties
                            $id = $skill->id;
                            $name = $skill->skill_name;
                            // ... (add more properties as needed)
                            
                            // Add the skill data to the 'category_skills' array
                            $response['data']['category_skills'][] = [
                                'id' => $id,
                                'name' => $name,
                                // ... (add more properties as needed)
                            ];
                        }
                    }
                } else if ($user->role == 'tutor') {
                    $tutor = User::where('users.id', $user->id)->join('tutor_details', 'users.id', '=', 'tutor_details.user_id')->first();
                    if ($tutor) {
                        $country = $tutor->country;
                        // $batches = Batch::where('user_id', $tutor->user_id)->get();
                        // $subscribers = Payment::select('user_id')->whereIn('tutor_id',$tutor->user_id)->distinct()->get();
                        $subscribers = DB::table('payments')
                            ->groupBy(['user_id'])
                            ->where('tutor_id', $tutor->user_id)
                            ->select('id')
                            ->get();
                        $payments = Payment::where('tutor_id', $tutor->user_id)->get();
                        $amount = $new_amount = 0;

                        $general_settings = Generalsetting::find(1);
                        $percentage = (int) $general_settings->commision;

                        if ($payments) {
                            foreach ($payments as $payment) {
                                $amount += $payment->amount;
                            }
                            $new_amount = ($percentage / 100) * $amount;
                        }

                        // dd($subscribers);

                        $category = Category::where('id', $tutor->category)->first();
                        $response['data'] = [
                            'token' => $tutor->token,
                            // 'refresh_token' => $refresh_token,
                            'type' => $tutor->role,
                            'id' => $tutor->user_id,
                            'name' => $tutor->name,
                            'email' => $tutor->email,
                            'phone_number' => $tutor->phone_number,
                            'location' => [
                                'country_id' => $country->id,
                                'country_name' => $country->country_name,
                            ],
                            'profile_image' => $tutor->profile_image != "" ? url('/' . $tutor->profile_image) : "",
                            'category' => [
                                'category_id' => $category->id,
                                'category' => $category->category_name,
                                'image' => url('/' . $category->image),
                            ],
                            'dob' => $tutor->dob,
                            'about' => $tutor->about,
                            'rating' => $tutor->rating,
                           
                            'total_subscribers' => count($subscribers),
                            'total_earnings' => $amount - $new_amount,
                            // 'sub_category' => $tutor->sub_category,
                            // 'skills' => $tutor->skills,
                            // 'experience' => json_decode($tutor->experience),

                        ];
                    }
                }

                return response()->json($response, 200);

            } catch (\Illuminate\Database\QueryException$e) {
                dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        } else {
            $response['errors']['code'] = 403;
            $response['errors'] = 'Invalid user credentials';
            return response()->json($response, 400);
        }
    }

    public function studentRegister(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users|max:150',
            'location' => 'required|numeric',
            'phone_number' => 'required|numeric',
            'dob' => 'required|date_format:Y-m-d',
            'password' => 'required',
            'password_confirm' => 'required|same:password',
            'grade' => 'required|exists:grades,id',
            'skills' => 'required|exists:skills,id',
            //    'parent_mobile' => 'required',
//            'role' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->first()], 401);
        }

        try {

            $otp_key = rand(1234, 9999); ///api_token' => Str::random(60),use Illuminate\Support\Str;
            $to = $request->email;
            $subject = 'Buddy Guru';
            $name = $request->name;
            $data = array("body" => "Your OTP to verify email id in Buddy Guru account is " . $otp_key);

            $mail = Mail::send([], [], function ($message) use ($to, $otp_key) {
                $message->to($to)
                    ->subject('Buddy Guru Application')
                    ->from('buddyguru@cordiace.com', 'Buddy Guru Support')
                    ->setBody('Your OTP to verify email id in Buddy Guru account is ' . $otp_key, 'text/plain');
            });

            $response['status'] = "true";
            $response['data']['otp'] = $otp_key;
            $response['data']['role'] = 'student';
            $response['data']['name'] = $request->name;
            $response['data']['email'] = $request->email;
            $response['data']['location'] = $request->location;
            $response['data']['phone_number'] = $request->phone_number;
            $response['data']['dob'] = $request->dob;
            $response['data']['password'] = $request->password;
            $response['data']['skills'] = $request->skills;
            $response['data']['grade'] = $request->grade;
            return response()->json($response, 200);
        } catch (\Illuminate\Database\QueryException$e) {
            // dd($e);
            $response['errors'] = 'error saving data';
            return response()->json($response, 400);
        } catch (\Exception$e) {
            // dd($e);
            $response['errors'] = 'error saving data';
            return response()->json($response, 400);
        }
    }

    public function studentEditProfile(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
//            return response()->json(['errors' => $validatedData->errors()->first('token')], 400);
        } else {
            $student = User::where("token", "=", $request->token)->first();
            $student_details = StudentDetail::where("user_id", "=", $student->id)->first();

            $validator = Validator::make($request->all(), [
                'name' => 'required',
//            'email' => 'required|email|',
                'email' => 'unique:users,email,' . $student->id,
                'phone_number' => 'required',
                // 'dob' => 'required|date_format:Y-m-d',
                'grade' => 'required|exists:grades,id',
                'skills' => 'required|exists:skills,id',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->first()], 401);
            }
            try {
                $student->name = $request->name;
                $student->email = $request->email;
                $student->phone_number = $request->phone_number;
                // $student->dob = $request->dob;
                $student_details->grade = $request->grade;
                $student_details->skills = $request->skills;
                $file = $request->file('profile_image');
                if ($file) {
                    $path = $file->store('/uploads', ['disk' => 'public']);
                    $student->profile_image = $path;
                }
                if ($student->save() && $student_details->save()) {

                    $country = Country::find($student->location);
                    $grade = Grade::find($student_details->grade);
                    $skill = Skill::find($student_details->skills);

                    $response['status'] = "true";
                    $response['data'] = [
                        'token' => $request->token,
                        'type' => $student->role,
                        'id' => $student->id,
                        'name' => $student->name,
                        'email' => $student->email,
                        'phone_number' => $student->phone_number,
                        'dob' => date('Y-m-d', strtotime($student->dob)),
                        'profile_image' => $student->profile_image != "" ? url('/' . $student->profile_image) : "",
                        'location' => [
                            'country_id' => $country->id,
                            'country_name' => $country->country_name,
                        ],
                        'profile_image' => $student->profile_image != "" ? url('/' . $student->profile_image) : "",
                        'grade' => [
                            'grade_id' => $grade->id,
                            'grade_name' => $grade->grade_name,
                        ],
                        'skills' => [
                            'skill_id' => $skill->id,
                            'skill_name' => $skill->skill_name,
                        ],
                    ];
                    return response()->json($response, 200);
                } else {
                    $response['errors'] = 'error saving student data';
                    return response()->json($response, 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                // dd($e);
                $response['errors'] = 'error saving student data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                // dd($e);
                $response['errors'] = 'error saving student data';
                return response()->json($response, 400);
            }
        }
    }

    public function tutorRegister(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users|max:150',
            'location' => 'required|numeric',
            'phone_number' => 'required|numeric',
            'category' => 'required|exists:categories,id',
            'dob' => 'required|date_format:Y-m-d',
            'password' => 'required',
//            'role' => 'required',
            'password_confirm' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->first()], 401);
        }

        try {

            $otp_key = rand(1234, 9999); ///api_token' => Str::random(60),use Illuminate\Support\Str;
            $to = $request->email;
            $subject = 'Buddy Guru';
            $name = $request->name;

            $data = array("body" => "Your OTP to verify email id in Buddy Guru account is " . $otp_key);

            $mail = Mail::send([], [], function ($message) use ($to, $otp_key) {
                $message->to($to)
                    ->subject('Buddy Guru Application')
                    ->from('buddyguru@cordiace.com', 'Buddy Guru Support')
                    ->setBody('Your OTP to verify email id in Buddy Guru account is ' . $otp_key, 'text/plain');
            });

            $response['status'] = "true";
            $response['data']['otp'] = $otp_key;
            $response['data']['role'] = 'tutor';
            $response['data']['name'] = $request->name;
            $response['data']['email'] = $request->email;
            $response['data']['location'] = $request->location;
            $response['data']['phone_number'] = $request->phone_number;
            $response['data']['category'] = $request->category;
            $response['data']['dob'] = $request->dob;
            $response['data']['password'] = $request->password;

            return response()->json($response, 200);

        } catch (\Illuminate\Database\QueryException$e) {
            $response['errors'] = 'error saving data';
            $response['errors_messge'] = $e->getMessage();
            return response()->json($response, 400);
        } catch (\Exception$e) {
            $response['errors'] = 'error saving data';
            $response['errors_messge'] = $e->getMessage();
            return response()->json($response, 400);
        }
    }

    public function tutorEditProfile(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
//            return response()->json(['errors' => $validatedData->errors()->first('token')], 400);
        } else {
            $tutor = User::where("token", "=", $request->token)->first();
            $tutor_details = TutorDetail::where("user_id", "=", $tutor->id)->first();

            $validator = Validator::make($request->all(), [
                'name' => 'required',
//                'email' => 'required|email',
                'email' => 'unique:users,email,' . $tutor->id,
                'phone_number' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->first()], 401);
            }
            try {
                $tutor->name = $request->name;
                $tutor->email = $request->email;
                $tutor->phone_number = $request->phone_number;
                $file = $request->file('profile_image');
                if ($file) {
                    $path = $file->store('/uploads', ['disk' => 'public']);
                    $tutor->profile_image = $path;
                }
                if ($tutor->save()) {
                    $tutor_details->category = $request->category;
                    // $tutor_details->sub_category = $request->sub_category;
                    // $tutor_details->skills = $request->skills;
                    $tutor_details->about = $request->about;
                    // $tutor_details->experience = $request->experience;
                    if ($tutor_details->save()) {
                        $country = $tutor->country;
                        $category = Category::where('id', $tutor_details->category)->first();
                        $response['status'] = "true";
                        $response['data'] = [
                            'token' => $tutor->token,
                            'type' => $tutor->role,
                            'id' => $tutor->id,
                            'name' => $tutor->name,
                            'email' => $tutor->email,
                            'phone_number' => $tutor->phone_number,
                            'location' => [
                                'country_id' => $country->id,
                                'country_name' => $country->country_name,
                            ],
                            'profile_image' => $tutor->profile_image != "" ? url('/' . $tutor->profile_image) : "",
                            'category' => [
                                'category_id' => $category->id,
                                'category' => $category->category_name,
                                'image' => url('/' . $category->image),
                            ],
                            'dob' => $tutor->dob,
                            'about' => $tutor_details->about,
                            // 'sub_category' => $tutor_details->sub_category,
                            // 'skills' => $tutor_details->skills,
                            // 'experience' => json_decode($tutor_details->experience),
                        ];
                        return response()->json($response, 200);
                    } else {
                        $response['status'] = "false";
                        $response['errors'] = 'error saving tutor data';
                        return response()->json($response, 400);
                    }
                } else {
                    $response['status'] = "false";
                    $response['errors'] = 'error saving tutor data';
                    return response()->json($response, 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['status'] = "false";
                $response['errors'] = 'error saving tutor data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['status'] = "false";
                $response['errors'] = 'error saving tutor data';
                return response()->json($response, 400);
            }
        }
    }

    public function verifyOtp(Request $request)
    {
        $user = new User();
        $student_details = new StudentDetail();
        $tutor_details = new TutorDetail();
        $validatedData = Validator::make($request->all(), [
            // 'otp' => 'required|exists:users,otp_verification',
            'name' => 'required',
            'email' => 'required|unique:users|max:150',
            'location' => 'required|exists:countries,id',
            'password' => 'required',
            'dob' => 'required|date_format:Y-m-d',
        ]);
        if ($request->role == 'student') {
            $validatedData = Validator::make($request->all(), [
                // 'otp' => 'required|exists:users,otp_verification',
                'grade' => 'required|exists:grades,id',
                'skills' => 'required|exists:skills,id',
            ]);
        } elseif ($request->role == 'tutor') {
            $validatedData = Validator::make($request->all(), [
                'category' => 'required|exists:categories,id',
            ]);
        }
        if ($validatedData->fails()) {
            return response()->json(['errors' => $validatedData->errors()->first()], 400);
        } else {
            try {
                $user->name = $request->name;
                $user->email = $request->email;
                $user->password = Hash::make($request->password);
                $user->role = $request->role;
                $user->dob = $request->dob;
                $user->phone_number = $request->phone_number;
                $user->location = $request->location;
                $user->slug = $request->name;
                $token_key = Str::random(24); ///api_token' => Str::random(60),use Illuminate\Support\Str;
                $user->token = $token_key;
                $user->save();

                $file = $request->file('profile_image');
                if ($file) {
                    $path = $file->store('/uploads', ['disk' => 'public']);
                    $user->profile_image = $path;
                }
                $user = User::where("email", $request->email)->first();
                if ($request->role == 'student') {
                    $student_details->user_id = $user->id;
                    $student_details->grade = $request->grade;
                    $student_details->skills = $request->skills;
                    $student_details->save();
                } elseif ($request->role == 'tutor') {
                    $tutor_details->user_id = $user->id;
                    $tutor_details->category = $request->category;
                    $tutor_details->sub_category = 3;
                    $tutor_details->skills = 1;
                    $tutor_details->save();
                }

                $response['status'] = "true";
                if ($request->role == 'student') {
                    $grade = Grade::find($student_details->grade);
                    $skill = Skill::find($student_details->skills);
                    $country = $user->country;
                    $response['data'] = [
                        'token' => $user->token,
                        'type' => $user->role,
                        'id' => $user->id,
                        'name' => $user->name,
                        'email' => $user->email,
                        'phone_number' => $user->phone_number,
                        'dob' => $user->dob,
                        'location' => [
                            'country_id' => $country->id,
                            'country_name' => $country->country_name,
                        ],
                        'profile_image' => $user->profile_image != "" ? url('/' . $user->profile_image) : "",
                        'grade' => [
                            'grade_id' => $grade->id,
                            'grade_name' => $grade->grade_name,
                        ],
                        'skills' => [
                            'skill_id' => $skill->id,
                            'skill_name' => $skill->skill_name,
                        ],
                    ];
                } elseif ($request->role == 'tutor') {
                    $country = $user->country;
                    $category = Category::where('id', $tutor_details->category)->first();

                    $response['data'] = [
                        'token' => $user->token,
                        'type' => $user->role,
                        'id' => $user->id,
                        'name' => $user->name,
                        'email' => $user->email,
                        'phone_number' => $user->phone_number,
                        'location' => [
                            'country_id' => $country->id,
                            'country_name' => $country->country_name,
                        ],
                        'profile_image' => $user->profile_image != "" ? url('/' . $user->profile_image) : "",
                        'category' => [
                            'category_id' => $category->id,
                            'category' => $category->category_name,
                            'image' => url('/' . $category->image),
                        ],
                        'dob' => $user->dob,
                        'about' => $tutor_details->about,
                        'total_batches' => 0,
                        'total_subscribers' => 0,
                        'total_earnings' => 0,

                    ];
                }
                //   }

                return response()->json($response, 200);

            } catch (\Illuminate\Database\QueryException$e) {
                // dd($e);
                $response['status'] = "false";
                $response['errors'] = 'error saving tutor data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                // dd($e);
                $response['status'] = "false";
                $response['errors'] = 'error saving tutor data';
                return response()->json($response, 400);
            }
        }
    }

    public function forgotPassword(Request $request)
    {
        $validatedData = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,Email',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => $validatedData->errors()->first()], 400);
        } else {
            try {
                $user = User::where('Email', $request->Input('email'))->first();
                $token_key = rand(1234, 9999);
                $user->Password = Hash::make($token_key);
                $user->save();
                if ($user) {
                    $to = $user->email;
                    $subject = 'Buddy Guru';
                    $name = $user->name;
                    $data = array("body" => "Your OTP to reset password in Buddy Guru is " . $token_key);

                    $mail = Mail::send([], [], function ($message) use ($to, $token_key) {
                        $message->to($to)
                            ->subject('Buddy Guru Application')
                            ->from('buddyguru@cordiace.com', 'Buddy Guru Support')
                            ->setBody('Your temporary password for Buddy Guru application is ' . $token_key, 'text/plain');
                    });
                    $response['status'] = "true";
                    $response['data']['otp'] = $token_key;
                    $response['data']['email'] = $user->email;
                    return response()->json($response, 200);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    public function resetPassword(Request $request)
    {

        if (Auth::attempt(['email' => $request->email, 'password' => $request->code])) {
            $validatedData = Validator::make($request->all(), [
                'email' => 'required|email|exists:users,Email',
                'password' => 'required',
//                'c_password' => 'required|same:password',
            ]);
            if ($validatedData->fails()) {
                return response()->json(['errors' => $validatedData->errors()->first()], 400);
            } else {
                try {
                    $password = Hash::make($request->password);
                    $user = User::where('email', $request->email)->first();
                    $user->password = Hash::make($request->password);
                    $user->save();
                    $response['status'] = "true";
                    $response['data']['token'] = $user->token;
                    $response['data']['type'] = $user->role;
                    return response()->json($response, 200);
                } catch (\Illuminate\Database\QueryException$e) {
                    $response['errors'] = 'error getting data';
                    return response()->json($response, 400);
                } catch (\Exception$e) {
                    $response['errors'] = 'error getting data';
                    return response()->json($response, 400);
                }
            }
        } else {
            $response['errors']['code'] = 403;
            $response['status'] = "false";
            $response['errors'] = 'Invalid user credentials';
            return response()->json($response, 400);
        }
    }

    public function changePassword(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        if (Auth::attempt(['token' => $request->token, 'password' => $request->password])) {
            $validatedData = Validator::make($request->all(), [
                'token' => 'required|exists:users,token',
                'password' => 'required',
                'n_password' => 'required',
//                'c_password' => 'required|same:n_password',
            ]);
            if ($validatedData->fails()) {
                return response()->json(['errors' => $validatedData->errors()->first()], 400);
            } else {
                try {
                    $password = Hash::make($request->password);
                    $user = User::where('token', $request->token)->first();
                    $user->password = Hash::make($request->n_password);
                    $user->save();
                    $response['status'] = "true";
                    $response['data']['token'] = $user->token;
//                $response['data']['type'] = $user->role;
                    return response()->json($response, 200);

                } catch (\Illuminate\Database\QueryException$e) {
                    $response['errors'] = 'error getting data';
                    return response()->json($response, 400);
                } catch (\Exception$e) {
                    $response['errors'] = 'error getting data';
                    return response()->json($response, 400);
                }
            }
        } else {
            $response['errors']['code'] = 403;
            $response['status'] = "false";
            $response['errors'] = 'Invalid user credentials';
            return response()->json($response, 400);
        }
    }

    public function createSessionOld(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
//            return response()->json(['errors' => $validatedData->errors()->first('token')], 400);
        } else {
            $session = new Sessions();
            $student_details = new StudentDetail();
            $validator = Validator::make($request->all(), [
                'type' => 'required',
                'session_name' => 'required',
                'session_date' => 'required|date',
                'session_time' => 'required',
                'duration' => 'required',
                'amount' => 'required',
                'description' => 'required',
                'learn' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->first()], 401);
            }
            try {
                $learn = trim($request->learn, '[\"');
                $learn = trim($learn, '\"]');
                $learn = explode('","', $learn);
                $learn = preg_replace('/\\\\/', '\\', $learn);
                $learn = json_encode($learn);
                $session->type = $request->type;
                $session->grade = $request->grade;
                $session->subject = $request->subject;
                $session->skill = $request->skill;
                $session->session_name = $request->session_name;
                $session->session_date = $request->session_date;
                $session->session_time = $request->session_time;
                $session->duration = $request->duration;
                $session->amount = number_format((float) $request->amount, 2, '.', '');
                $session->description = $request->description;
                $session->learn = $request->learn;
                $session->session_link = $request->session_link;
                $user = User::where("token", $request->token)->first();
                $session->user_id = $user->id;
                if ($session->save()) {
                    $response['status'] = "true";
                    $response['data'] = [
                        'id' => $session->id,
                        'tutor' => $session->user->name,
                        'tutor_id' => $session->user_id,
                        'type' => $session->type,
                        'grade' => $session->grade,
                        'subject' => $session->subject,
                        'skill' => $session->skill,
                        'session_name' => $session->session_name,
                        'session_date' => $session->session_date,
                        'session_time' => $session->session_time,
                        'duration' => $session->duration,
                        'amount' => $session->amount,
                        'description' => $session->description,
                        'session_link' => $session->session_link,
                        'learn' => json_decode($session->learn),
                    ];
                    return response()->json($response, 200);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error saving data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error saving data';
                return response()->json($response, 400);
            }
        }
    }

    public function updateSessionOld(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
//            return response()->json(['errors' => $validatedData->errors()->first('token')], 400);
        } else {
            $session = Sessions::where(['id' => $request->id])->first();

            $validator = Validator::make($request->all(), [
                'type' => 'required',
                'session_name' => 'required',
                'session_date' => 'required|date',
                'session_time' => 'required',
                'duration' => 'required',
                'amount' => 'required',
                'description' => 'required',
                'learn' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->first()], 401);
            }
            try {
                $token = $request->Input('token');
                $user = User::where('token', $token)->first();
                if ($session->user_id == $user->id) {
                    $learn = trim($request->learn, '[\"');
                    $learn = trim($learn, '\"]');
                    $learn = explode('","', $learn);
                    $learn = preg_replace('/\\\\/', '\\', $learn);
                    $learn = json_encode($learn);
                    $session->type = $request->type;
                    $session->grade = $request->grade;
                    $session->subject = $request->subject;
                    $session->skill = $request->skill;
                    $session->session_name = $request->session_name;
                    $session->session_date = $request->session_date;
                    $session->session_time = $request->session_time;
                    $session->duration = $request->duration;
                    $session->amount = number_format((float) $request->amount, 2, '.', '');
                    $session->session_link = $request->session_link;
                    $session->description = $request->description;
                    $session->learn = $request->learn;
                    $user = User::where("token", $request->token)->first();
                    $session->user_id = $user->id;
                    if ($session->save()) {
                        $response['status'] = "true";
                        $response['data'] = [
                            'id' => $session->id,
                            'tutor' => $session->user->name,
                            'tutor_id' => $session->user_id,
                            'type' => $session->type,
                            'grade' => $session->grade,
                            'subject' => $session->subject,
                            'skill' => $session->skill,
                            'session_name' => $session->session_name . '456',
                            'session_date' => $session->session_date,
                            'session_time' => $session->session_time,
                            'session_link' => $session->session_link,
                            'duration' => $session->duration,
                            'amount' => number_format((float) $session->amount, 2, '.', ''),
                            'description' => $session->description,
//                            'learn' => explode ('","', $learn),
                            'learn' => json_decode($session->learn),
                        ];
                        return response()->json($response, 200);
                    }
                } else {
                    $response['errors'] = 'Session not found for this user';
                    return response()->json($response, 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    public function deleteSessionOld(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
//            return response()->json(['errors' => $validatedData->errors()->first('token')], 400);
        } else {
            try {
                $token = $request->Input('token');
                $user = User::where('token', $token)->first();
                if ($user) {
                    $session = Sessions::findOrFail($request->id);
                    if ($session->user_id == $user->id) {
                        if ($session->delete()) {
                            $response['status'] = "true";
                            $response['data'] = ["Session deleted successfully"];
                            return response()->json($response, 200);
                        } else {
                            $response['errors'] = 'error deleting session data';
                            return response()->json($response, 400);
                        }
                    } else {
                        $response['errors'] = 'Session not found for this user';
                        return response()->json($response, 400);
                    }
                } else {
                    $response['errors'] = 'error deleting session data';
                    return response()->json($response, 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error deleting session data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting session data';
                return response()->json($response, 400);
            }
        }
    }

    public function getSessionOld(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'id' => 'numeric',
            'token' => 'required_without:id|exists:users,token|bail',
        ]);

        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
//            return response()->json(['errors' =>  $validatedData->errors()->first('token')], 400);
        } else {
            $token = $request->Input('token');
            try {
                if (!empty($request->input('token'))) {
                    $user = User::where('token', $token)->first();
                }
                if ($user) {
                    $session = Sessions::findOrFail($request->id);
                    $subscription = Payment::where(['user_id' => $user->id, 'batch_id' => $session->id])->first();
                    if ($session) {
                        $response['status'] = "true";
                        $response['data'] = [
                            'id' => $session->id,
                            'tutor' => $session->user->name,
                            'tutor_id' => $session->user_id,
                            'type' => $session->type,
                            'grade' => $session->grade,
                            'subject' => $session->subject,
                            'skill' => $session->skill,
                            'session_name' => $session->session_name . ' 123',
                            'session_date' => $session->session_date,
                            'session_time' => $session->session_time,
                            'session_link' => $session->session_link,
                            'duration' => $session->duration,
                            'amount' => number_format((float) $session->amount, 2, '.', ''),
                            'description' => $session->description,
                            'learn' => json_decode($session->learn),
                            'payment_id' => $subscription ? $subscription->id : 0,
                        ];

                        return response()->json($response, 200);
                    } else {
                        return response()->json(['errors' => 'No session found'], 400);
                    }
                } else {
                    return response()->json(['errors' => 'No user found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    public function tutorUpcomingSessions(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
//            return response()->json(['errors' =>  $validatedData->errors()->first('token')], 400);
        } else {
            try {
                $tutor = User::where("token", $request->token)->first();
                if ($tutor->id) {

                    $user_id = $tutor->id;

                    $sessions = TutorSession::from('tutor_sessions as ts')
                        ->select('ts.*', 'bt.user_id', 'us.name')
                        ->join('batches as bt', 'bt.id', 'ts.batch_id')
                        ->join('users as us', 'us.id', 'bt.user_id')
                        ->where('bt.user_id', $user_id)
                        ->where('ts.date', '>=', date('Y-m-d'))
                        ->get();

                    //$sessions = TutorSession::where(['user_id' => $tutor->id, ['date', '>=', date('Y-m-d')]])->get();
                    if (count($sessions) > 0) {
                        $response['status'] = "true";
                        foreach ($sessions as $session) {
                            $response['data'][] = [
                                'id' => $session->id,
                                'tutor' => $session->name,
                                'tutor_id' => $session->user_id,
                                'batch_id' => $session->batch_id,
                                'category_id' => $session->category_id,
                                'grade_id' => $session->grade_id,
                                'session_name' => $session->session_name,
                                'description' => $session->description,
                                'what_learn' => unserialize($session->what_learn),
                                'date' => $session->date,
                                'time' => $session->time,
                                'meeting_link' => $session->meeting_link,

                            ];
                        }
                        return response()->json($response, 200);
                    } else {
                        return response()->json(['errors' => 'No sessions found'], 400);
                    }

                } else {
                    return response()->json(['errors' => 'No tutor found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                // dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                // dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }

    }

    public function tutorAllSessions(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
//            return response()->json(['errors' =>  $validatedData->errors()->first('token')], 400);
        } else {
            try {
                $tutor = User::where("token", $request->token)->first();
                if ($tutor->id) {
                    $upcomingsessions = TutorSession::where(['user_id' => $tutor->id, ['date', '>=', date('Y-m-d')]])->orderBy('date', 'ASC')->get();
                    $completedsessions = TutorSession::where(['user_id' => $tutor->id, ['date', '<', date('Y-m-d')]])->orderBy('date', 'DESC')->get();
                    if (count($upcomingsessions) > 0 || count($completedsessions) > 0) {
                        $response['status'] = "true";
                        foreach ($completedsessions as $csession) {
                            $count = Payment::where([['tutor_id', $tutor->id], ['batch_id', $csession->id]])->count();
                            $response['data']['completed'][] = [
                                'id' => $csession->id,
                                'tutor' => $csession->user->name,
                                'tutor_id' => $csession->user_id,
                                'batch_id' => $csession->batch_id,
                                'category_id' => $csession->category_id,
                                'grade_id' => $csession->grade_id,
                                'session_name' => $csession->session_name,
                                'description' => $csession->description,
                                'what_learn' => unserialize($session->what_learn),
                                'date' => $csession->date,
                                'time' => $csession->time,
                                'meeting_link' => $csession->meeting_link,
                                'enrolled_students' => $count > 0 ? $count : 0,

                            ];
                        }
                        foreach ($upcomingsessions as $usession) {
                            $count = Payment::where([['tutor_id', $tutor->id], ['batch_id', $usession->id]])->count();
                            $response['data']['upcoming'][] = [
                                'id' => $usession->id,
                                'tutor' => $usession->user->name,
                                'tutor_id' => $usession->user_id,
                                'batch_id' => $usession->batch_id,
                                'category_id' => $suession->category_id,
                                'grade_id' => $usession->grade_id,
                                'session_name' => $usession->session_name,
                                'description' => $usession->description,
                                'what_learn' => unserialize($session->what_learn),
                                'date' => $usession->date,
                                'time' => $usession->time,
                                'meeting_link' => $usession->meeting_link,
                                'enrolled_students' => $count > 0 ? $count : 0,
                            ];
                        }
                        return response()->json($response, 200);
                    } else {
                        $response['status'] = "true";
                        $response['data'] = [];
                        $response['message'] = "No sessions found";
                        return response()->json($response, 200);
                    }

                } else {
                    return response()->json(['errors' => 'No tutor found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }

    }

    /*
     * get all upcoming tutor sessions by id
     */
    public function tutorSessionsById(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'id' => 'numeric',
            'token' => 'required_without:id|exists:users,token|bail',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
//            return response()->json(['errors' =>  $validatedData->errors()->first('token')], 400);
        } else {
            try {
                $tutor = User::where("token", $request->token)->first();
                if ($tutor->id) {

                    $user_id = $tutor->id;
                    $upcomingsessions = TutorSession::from('tutor_sessions as ts')
                        ->select('ts.*', 'bt.user_id', 'us.name')
                        ->join('batches as bt', 'bt.id', 'ts.batch_id')
                        ->join('users as us', 'us.id', 'bt.user_id')
                        ->where('bt.user_id', $request->id)
                        ->where('ts.date', '>=', date('Y-m-d'))
                        ->get();

                    // $upcomingsessions = TutorSession::where(['user_id' => $request->id, ['date', '>=', date('Y-m-d')]])->orderBy('date', 'ASC')->get();

                    if (count($upcomingsessions) > 0) {
                        $response['status'] = "true";
                        foreach ($upcomingsessions as $usession) {
                            $subscription = Payment::where(['user_id' => $tutor->id, 'batch_id' => $usession->id])->first();
                            $response['data'][] = [
                                'id' => $usession->id,
                                'tutor' => $usession->name,
                                'tutor_id' => $usession->user_id,
                                'batch_id' => $usession->batch_id,
                                'category_id' => $usession->category_id,
                                'grade_id' => $usession->grade_id,
                                'session_name' => $usession->session_name,
                                'description' => $usession->description,
                                'what_learn' => unserialize($session->what_learn),
                                'date' => $usession->date,
                                'time' => $usession->time,
                                'meeting_link' => $usession->meeting_link,
                                'payment_id' => $subscription ? $subscription->id : 0,
                            ];
                        }
                        return response()->json($response, 200);
                    } else {
                        $response['status'] = "true";
                        $response['data'] = [];
                        $response['message'] = "No sessions found";
                        return response()->json($response, 200);
                        // return response()->json(['errors' => 'No sessions found'], 400);
                    }

                } else {

                    return response()->json(['errors' => 'No tutor found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                // dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                // dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }

    }

    public function tutorCompletedSessions(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
//            return response()->json(['errors' =>  $validatedData->errors()->first('token')], 400);
        } else {
            try {
                $tutor = User::where("token", $request->token)->first();
                if ($tutor->id) {
                    $sessions = TutorSession::where(['user_id' => $tutor->id, ['date', '<', date('Y-m-d')]])->get();
                    if (count($sessions) > 0) {
                        $response['status'] = "true";
                        foreach ($sessions as $session) {
                            $response['data'][] = [
                                'id' => $session->id,
                                'tutor' => $session->user->name,
                                'tutor_id' => $session->user_id,
                                'batch_id' => $usession->batch_id,
                                'category_id' => $suession->category_id,
                                'grade_id' => $usession->grade_id,
                                'session_name' => $usession->session_name,
                                'description' => $usession->description,
                                'what_learn' => unserialize($session->what_learn),
                                'date' => $usession->date,
                                'time' => $usession->time,
                                'meeting_link' => $usession->meeting_link,

                            ];
                        }
                        return response()->json($response, 200);
                    } else {
                        $response['status'] = "true";
                        $response['data'] = [];
                        $response['message'] = "No sessions found";
                        return response()->json($response, 200);
                        // return response()->json(['errors' => 'No sessions found'], 400);
                    }

                } else {

                    return response()->json(['errors' => 'No tutor found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }

    }

    public function tutorSessions(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
            'id' => 'numeric',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
//            return response()->json(['errors' => $validatedData->errors()->first('token')], 400);
        } else {
            try {
                $tutor = User::where("token", $request->token)->first();
                if ($tutor->id) {
                    /*     $sessions = TutorSession::where(['user_id' => $request->id, ['date', '>=', date('Y-m-d')]])->get();*/

                    $user_id = $tutor->id;

                    $sessions = TutorSession::from('tutor_sessions as ts')
                        ->select('ts.*', 'bt.user_id', 'us.name')
                        ->join('batches as bt', 'bt.id', 'ts.batch_id')
                        ->join('users as us', 'us.id', 'bt.user_id')
                        ->where('bt.user_id', $user_id)
                        ->where('ts.date', '>=', date('Y-m-d'))
                        ->get();

                    if (count($sessions) > 0) {
                        $response['status'] = "true";
                        foreach ($sessions as $session) {
                            $response['data'][] = [
                                'id' => $session->id,
                                'tutor' => $session->user->name,
                                'tutor_id' => $session->user_id,
                                'batch_id' => $usession->batch_id,
                                'category_id' => $usession->category_id,
                                'grade_id' => $usession->grade_id,
                                'session_name' => $usession->session_name,
                                'description' => $usession->description,
                                'what_learn' => unserialize($session->what_learn),
                                'date' => $usession->date,
                                'time' => $usession->time,
                                'meeting_link' => $usession->meeting_link,

                            ];
                        }
                        return response()->json($response, 200);
                    } else {
                        $response['status'] = "true";
                        $response['data'] = [];
                        $response['message'] = "No upcoming sessions found";
                        return response()->json($response, 200);
                        // return response()->json(['errors' => 'No upcoming sessions found'], 400);
                    }

                } else {
                    return response()->json(['errors' => 'No tutor found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }

    }

    public function studentSessions(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
//            return response()->json(['errors' =>  $validatedData->errors()->first('token')], 400);
        } else {
            try {
                $student = User::where("token", $request->token)->first();
                if ($student->id) {
                    $sessions = Payment::where(['payments.user_id' => $student->id])->get();
                    if (count($sessions) > 0) {
                        foreach ($sessions as $mysession) {
                            $response['data'][] = [
                                'id' => $mysession->session->id,
                                'tutor' => $mysession->session->user->name,
                                'tutor_image' => $mysession->session->user->profile_image != "" ? url('/' . $mysession->session->user->profile_image) : "",
                                'tutor_id' => $mysession->session->user_id,
                                'type' => $mysession->session->type,
                                'grade' => $mysession->session->grade,
                                'subject' => $mysession->session->subject,
                                'skill' => $mysession->session->skill,
                                'session_name' => $mysession->session->session_name,
                                'session_date' => $mysession->session->session_date,
                                'session_time' => $mysession->session->session_time,
                                'session_link' => $mysession->session->session_link,
                                'duration' => $mysession->session->duration,
                                'amount' => number_format((float) $mysession->session->amount, 2, '.', ''),
                                'description' => $mysession->session->description,
                                'learn' => json_decode($mysession->session->learn),

                            ];
                        }
                        $response['status'] = "true";
                        return response()->json($response, 200);
                    } else {
                        $response['status'] = "true";
                        $response['data'] = [];
                        $response['message'] = "No sessions found";
                        return response()->json($response, 200);
                        // return response()->json(['errors' => 'No sessions found'], 400);
                    }

                } else {
                    return response()->json(['errors' => 'No tutor found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                // dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                // dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }

    }

    public function studentBatches(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
            //            return response()->json(['errors' =>  $validatedData->errors()->first('token')], 400);
        } else {
            try {
                $student = User::where("token", $request->token)->first();
                if ($student->id) {
                    $response['data']['completed'] = [];
                    $response['data']['inprogress'] = [];
                    $response['data']['upcoming'] = [];

                    $batches = Payment::where(['payments.user_id' => $student->id, 'payments.payment_status' => 1])
                        ->join('packages', 'payments.package_id', '=', 'packages.id')
                        ->join('batches', 'payments.batch_id', '=', 'batches.id')
                        ->where('batches.end', '<', date('Y-m-d'))
                        ->get();

                    if (count($batches) > 0) {
                        foreach ($batches as $batch) {
                            $sessions = [];
                            $sessionOb = TutorSession::where('batch_id', $batch->id)->get();
                            if ($sessionOb) {
                                foreach ($sessionOb as $ses) {
                                    $sessions[] = [
                                        'id' => $ses->id,
                                        'batch_id' => $ses->batch_id,
                                        'category_id' => $ses->category_id,
                                        'grade_id' => $ses->grade_id,
                                        'session_name' => $ses->session_name,
                                        'description' => $ses->description,
                                        'what_learn' => ($ses->what_learn) ? unserialize($ses->what_learn) : $ses->what_learn,
                                        'date' => $ses->date,
                                        'time' => $ses->time,
                                        'meeting_link' => $ses->meeting_link,
                                    ];
                                }
                                usort($sessions, 'date_compare');
                            }

                            $response['status'] = "true";
                            $response['data']['completed'][] = [
                                'batch_id' => $batch->id,
                                'batch_name' => $batch->batch_name,
                                'tutor' => $batch->user->name,
                                'tutor_id' => $batch->user_id,
                                'profile_image' => $batch->user->profile_image != "" ? url('/' . $batch->user->profile_image) : "",
                                'package_name' => $batch->package_name,
                                'description' => $batch->description,
                                'start' => $batch->start,
                                'end' => $batch->end,
                                'duration' => $batch->duration,
                                'no_of_participants' => $batch->no_of_participants,
                                'no_of_sessions' => $batch->no_of_sessions,
                                'payment_type' => $batch->payment_type,
                                'subscription_fee' => $batch->subscription_fee,
                                'sessions' => $sessions,
                            ];
                        }

                    }
                    //Inprogress
                    $batches = Payment::where(['payments.user_id' => $student->id, 'payments.payment_status' => 1])
                        ->join('packages', 'payments.package_id', '=', 'packages.id')
                        ->join('batches', 'payments.batch_id', '=', 'batches.id')
                        ->where('batches.start', '<', date('Y-m-d'))
                        ->where('batches.end', '>', date('Y-m-d'))
                        ->get();

                    if (count($batches) > 0) {
                        foreach ($batches as $batch) {
                            $sessions = [];
                            $date_key = 0;
                            $sessionOb = TutorSession::where('batch_id', $batch->id)->get();
                            if ($sessionOb) {
                                foreach ($sessionOb as $ses) {
                                    $sessions[] = [
                                        'id' => $ses->id,
                                        'batch_id' => $ses->batch_id,
                                        'category_id' => $ses->category_id,
                                        'grade_id' => $ses->grade_id,
                                        'session_name' => $ses->session_name,
                                        'description' => $ses->description,
                                        'what_learn' => ($ses->what_learn) ? unserialize($ses->what_learn) : $ses->what_learn,
                                        'date' => $ses->date,
                                        'time' => $ses->time,
                                        'meeting_link' => $ses->meeting_link,
                                    ];
                                }
                                usort($sessions, 'date_compare');

                                $date_key = get_upcoming_date($sessions);

                            }

                            $response['status'] = "true";
                            $response['data']['inprogress'][] = [
                                'batch_id' => $batch->id,
                                'batch_name' => $batch->batch_name,
                                'tutor' => $batch->user->name,
                                'tutor_id' => $batch->user_id,
                                'profile_image' => $batch->user->profile_image != "" ? url('/' . $batch->user->profile_image) : "",
                                'package_name' => $batch->package_name,
                                'description' => $batch->description,
                                'start' => $batch->start,
                                'end' => $batch->end,
                                'duration' => $batch->duration,
                                'no_of_participants' => $batch->no_of_participants,
                                'no_of_sessions' => $batch->no_of_sessions,
                                'payment_type' => $batch->payment_type,
                                'subscription_fee' => $batch->subscription_fee,
                                'upcoming_session_date' => isset($date_key) ? $sessions[$date_key]['date'] . 'T' . $sessions[$date_key]['time'] : '',
                                'sessions' => $sessions,
                            ];
                        }
                    }

                    $batches = Payment::where(['payments.user_id' => $student->id, 'payments.payment_status' => 1])
                        ->join('packages', 'payments.package_id', '=', 'packages.id')
                        ->join('batches', 'payments.batch_id', '=', 'batches.id')
                        ->where('batches.start', '>', date('Y-m-d'))
                        ->get();

                    if (count($batches) > 0) {
                        foreach ($batches as $batch) {
                            $sessions = [];
                            $sessionOb = TutorSession::where('batch_id', $batch->id)->get();
                            if ($sessionOb) {
                                foreach ($sessionOb as $ses) {
                                    $sessions[] = [
                                        'id' => $ses->id,
                                        'batch_id' => $ses->batch_id,
                                        'category_id' => $ses->category_id,
                                        'grade_id' => $ses->grade_id,
                                        'session_name' => $ses->session_name,
                                        'description' => $ses->description,
                                        'what_learn' => ($ses->what_learn) ? unserialize($ses->what_learn) : $ses->what_learn,
                                        'date' => $ses->date,
                                        'time' => $ses->time,
                                        'meeting_link' => $ses->meeting_link,
                                    ];
                                }
                                usort($sessions, 'date_compare');
                            }

                            $response['status'] = "true";
                            $response['data']['upcoming'][] = [
                                'batch_id' => $batch->id,
                                'batch_name' => $batch->batch_name,
                                'tutor' => $batch->user->name,
                                'tutor_id' => $batch->user_id,
                                'profile_image' => $batch->user->profile_image != "" ? url('/' . $batch->user->profile_image) : "",
                                'package_name' => $batch->package_name,
                                'description' => $batch->description,
                                'start' => $batch->start,
                                'end' => $batch->end,
                                'duration' => $batch->duration,
                                'no_of_participants' => $batch->no_of_participants,
                                'no_of_sessions' => $batch->no_of_sessions,
                                'payment_type' => $batch->payment_type,
                                'subscription_fee' => $batch->subscription_fee,
                                'upcoming_session_date' => (array_key_exists(0, $sessions)) ? $sessions[0]['date'] . 'T' . $sessions[0]['time'] : '',
                                'sessions' => $sessions,
                            ];
                        }
                    }
                    $response['status'] = "true";
                    return response()->json($response, 200);

                } else {
                    return response()->json(['errors' => 'No tutor found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }

    }
    public function getSubscription($user)
    {
        $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET_KEY'));
        $userSubscriptions = UserSubscription::where('user_id', $user->id)->get();

        if ($userSubscriptions && count($userSubscriptions) > 0) {

            foreach ($userSubscriptions as $userSubscription) {

                $subscription = $stripe->subscriptions->retrieve(
                    $userSubscription->stripe_subscription_id,
                    []
                );

                if ($subscription->status == 'past_due') {
                    $payment = Payment::where('batch_id', $subscription->batch_id)
                        ->where('package_id', $subscription->package_id)
                        ->where('user_id', $subscription->user_id)
                        ->first();
                    if ($payment) {
                        $payment->payment_status = 0;
                        $payment->save();
                        $subscriber = Subscriber::where(['tutor_id' => $payment->tutor_id, 'user_id' => $payment->user_id])->first();
                        if ($subscriber) {
                            $subscriber->status = 0;
                            $subscriber->save();
                        }
                    }
                } else {
                    $payment = Payment::where('batch_id', $subscription->batch_id)
                        ->where('package_id', $subscription->package_id)
                        ->where('user_id', $subscription->user_id)
                        ->first();

                    if ($payment) {
                        $payment->payment_status = 1;
                        $payment->save();
                        $subscriber = Subscriber::where(['tutor_id' => $payment->tutor_id, 'user_id' => $payment->user_id])->first();
                        if ($subscriber) {
                            $subscriber->status = 1;
                            $subscriber->save();
                        }
                    }
                }
            }
        }
    }
    public function getTutor(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'tutor_id' => 'required|numeric',
            'token' => 'required_without:id|exists:users,token|bail',
        ]);

        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
//            return response()->json(['errors' => $validatedData->errors()->first('token')], 400);
        } else {
            try {
                $token = $request->Input('token');
                if (!empty($request->input('token'))) {
                    $user = User::where('token', $token)->first();
                }
                if ($user) {
                    $tutor = User::where('users.id', $request->tutor_id)->join('tutor_details', 'users.id', '=', 'tutor_details.user_id')->select('users.*', 'tutor_details.category', 'tutor_details.sub_category', 'tutor_details.skills', 'tutor_details.about', 'tutor_details.rating')->first();
                    if ($tutor) {
                        $portfolio_data = [];
                        $portfolios = TutorPortfolio::where('user_id', $tutor->id)->get();
                        if ($portfolios && count($portfolios) > 0) {
                            foreach ($portfolios as $portfolio) {
                                $portfolio_data[] = [
                                    'id' => $portfolio->id,
                                    'tutor_id' => $portfolio->user_id,
                                    'title' => $portfolio->title,
                                    'description' => $portfolio->description,
                                    'video_url' => url('/' . $portfolio->video_url),
                                    'video_thumbnail' => url('/' . $portfolio->video_thumbnail),
                                ];
                            }
                        }

                        $subscribers = Subscriber::where(['tutor_id' => $tutor->id, 'user_id' => $user->id])->first();
                        $category = Category::where('id', $tutor->category)->first();
                        $country = Country::where('id', $tutor->location)->first();
                        $response['status'] = "true";
                        $response['data'] = [
                            'id' => $tutor->id,
                            'name' => $tutor->name,
                            'email' => $tutor->email,
                            'phone_number' => $tutor->phone_number,
                            'location' => [
                                'country_id' => $country->id,
                                'country_name' => $country->country_name,
                            ],
                            'profile_image' => $tutor->profile_image != "" ? url('/' . $tutor->profile_image) : "",
                            'category' => [
                                'category_id' => $category->id,
                                'category' => $category->category_name,
                                'image' => url('/' . $category->image),
                            ],
                            'dob' => $tutor->dob,
                            'about' => $tutor->about,
                            'rating' => $tutor->rating,
                            'subscription_status' => ($subscribers) ? $subscribers->status : 0,
                            'portfolios' => $portfolio_data,
                        ];
                        return response()->json($response, 200);
                    } else {
                        $response['status'] = "true";
                        $response['data'] = [];
                        $response['message'] = "No tutor found";
                        return response()->json($response, 200);
                        // return response()->json(['errors' => 'No tutor found'], 400);
                    }
                } else {
                    return response()->json(['errors' => 'No user found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    public function subscribeTutor(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'tutor_id' => 'required|numeric',
            'token' => 'required_without:id|exists:users,token|bail',
        ]);

        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
//            return response()->json(['errors' => $validatedData->errors()->first('token')], 400);
        } else {
            try {
                $token = $request->Input('token');
                if (!empty($request->input('token'))) {
                    $user = User::where('token', $token)->first();
                }
                $tutor = User::find($request->tutor_id);
                if ($user) {
                    if ($tutor && $tutor->role == 'tutor') {

                        $subscriber = Subscriber::where(['tutor_id' => $tutor->id, 'user_id' => $user->id])->first();
                        if ($subscriber) {
                            $status = $subscriber->status;

                            $subscriber->status = ($status == 0) ? 1 : 0;
                            $subscriber->save();
                        } else {
                            $subscriber = new Subscriber();
                            $subscriber->tutor_id = $tutor->id;
                            $subscriber->user_id = $user->id;
                            $subscriber->status = 1;
                            $subscriber->save();
                        }

                        $response['status'] = "true";
                        $response['data'] = [
                            'id' => $subscriber->id,
                            'tutor_id' => $subscriber->tutor_id,
                            'user_id' => $subscriber->user_id,
                            'status' => $subscriber->status,
                        ];
                        return response()->json($response, 200);
                    } else {
                        $response['status'] = "true";
                        $response['data'] = [];
                        $response['message'] = "No tutor found";
                        return response()->json($response, 200);
                        // return response()->json(['errors' => 'No tutor found'], 400);
                    }
                } else {
                    return response()->json(['errors' => 'No user found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    public function getSubscriber(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'tutor_id' => 'required|numeric',
            'token' => 'required_without:id|exists:users,token|bail',
        ]);

        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
//            return response()->json(['errors' => $validatedData->errors()->first('token')], 400);
        } else {
            try {
                $token = $request->Input('token');
                if (!empty($request->input('token'))) {
                    $user = User::where('token', $token)->first();
                }
                $tutor = User::find($request->tutor_id);
                if ($user) {
                    if ($tutor && $tutor->role == 'tutor') {

                        $subscriber = Subscriber::where(['tutor_id' => $tutor->id, 'user_id' => $user->id])->first();
                        if ($subscriber) {
                            $response['status'] = "true";
                            $response['data'] = [
                                'id' => $subscriber->id,
                                'tutor_id' => $subscriber->tutor_id,
                                'user_id' => $subscriber->user_id,
                                'status' => $subscriber->status,
                            ];
                            return response()->json($response, 200);
                        } else {
                            $response['status'] = "true";
                            $response['data'] = [];
                            $response['message'] = "Not subscribed yet";
                            return response()->json($response, 200);
                        }

                    } else {
                        $response['status'] = "true";
                        $response['data'] = [];
                        $response['message'] = "No tutor found";
                        return response()->json($response, 200);
                        // return response()->json(['errors' => 'No tutor found'], 400);
                    }
                } else {
                    return response()->json(['errors' => 'No user found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    public function getAllSubscriber(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required_without:id|exists:users,token|bail',
        ]);

        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
//            return response()->json(['errors' => $validatedData->errors()->first('token')], 400);
        } else {
            try {
                $token = $request->Input('token');
                if (!empty($request->input('token'))) {
                    $user = User::where('token', $token)->first();
                }
                if ($user) {

                    $subscribers = Subscriber::where('user_id', $user->id)->get();
                    if ($subscribers && count($subscribers) > 0) {
                        foreach ($subscribers as $subscriber) {
                            $response['status'] = "true";
                            $response['data'][] = [
                                'id' => $subscriber->id,
                                'tutor_id' => $subscriber->tutor_id,
                                'user_id' => $subscriber->user_id,
                                'status' => $subscriber->status,
                            ];
                        }

                        return response()->json($response, 200);
                    } else {
                        $response['status'] = "true";
                        $response['data'] = [];
                        $response['message'] = "Not subscribed yet";
                        return response()->json($response, 200);
                    }

                } else {
                    return response()->json(['errors' => 'No user found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    public function getAllTutor(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required_without:id|exists:users,token|bail',
        ]);

        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
        } else {
            try {
                $token = $request->Input('token');
                if (!empty($request->input('token'))) {
                    $user = User::where('token', $token)->first();
                }
                if ($user) {
                    $tutors = User::where('users.role', 'tutor')->join('tutor_details', 'users.id', '=', 'tutor_details.user_id')->select('users.*', 'tutor_details.*')->simplePaginate($request->limit);
                    if (count($tutors) > 0) {
                        foreach ($tutors as $tutor) {
                            $portfolio_data = [];
                            $portfolios = TutorPortfolio::where('user_id', $tutor->user_id)->get();
                            if ($portfolios && count($portfolios) > 0) {
                                foreach ($portfolios as $portfolio) {
                                    $portfolio_data[] = [
                                        'id' => $portfolio->id,
                                        'tutor_id' => $portfolio->user_id,
                                        'title' => $portfolio->title,
                                        'description' => $portfolio->description,
                                        'video_url' => url('/' . $portfolio->video_url),
                                        'video_thumbnail' => url('/' . $portfolio->video_thumbnail),
                                    ];
                                }
                            }
                            $subscribers = Subscriber::where(['tutor_id' => $tutor->user_id, 'user_id' => $user->id])->first();
                            $category = Category::where('id', $tutor->category)->first();
                            $country = Country::where('id', $tutor->location)->first();
                            $response['status'] = "true";
                            $response['data'][] = [
                                'id' => $tutor->user_id,
                                'name' => $tutor->name,
                                'email' => $tutor->email,
                                'phone_number' => $tutor->phone_number,
                                'location' => [
                                    'country_id' => $country->id,
                                    'country_name' => $country->country_name,
                                ],
                                'profile_image' => $tutor->profile_image != "" ? url('/' . $tutor->profile_image) : "",
                                'category' => [
                                    'category_id' => $category->id,
                                    'category' => $category->category_name,
                                    'image' => url('/' . $category->image),
                                ],
                                'dob' => $tutor->dob,
                                'about' => $tutor->about,
                                'rating' => $tutor->rating,
                                'subscription_status' => ($subscribers) ? $subscribers->status : 0,
                                'portfolios' => $portfolio_data,
                            ];
                        }
                        return response()->json($response, 200);
                    } else {
                        $response['status'] = "true";
                        $response['data'] = [];
                        $response['message'] = "No tutor found";
                        return response()->json($response, 200);
                        // return response()->json(['errors' => 'No tutor found'], 400);
                    }
                } else {
                    return response()->json(['errors' => 'No user found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    public function getStudent(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'id' => 'numeric',
            'token' => 'required_without:id|exists:users,token|bail',
        ]);

        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
//            return response()->json(['errors' => $validatedData->errors()->first('token')], 400);
        } else {
            try {
                $token = $request->Input('token');
                if (!empty($request->input('token'))) {
                    $user = User::where('token', $token)->first();
                }
                if ($user) {
                    $student = User::where('users.id', $user->id)->join('student_details', 'users.id', '=', 'student_details.user_id')->first();
                    if ($student) {
                        $response['status'] = "true";
                        $response['data'] = [
                            'id' => $student->id,
                            'name' => $student->name,
                            'email' => $student->email,
                            'phone_number' => $student->phone_number,
                            'dob' => $student->dob,
                            'location' => $student->location,
                            'profile_image' => $student->profile_image != "" ? url('/' . $student->profile_image) : "",
                            // 'parent_name' => $student->parent_name,
                            // 'parent_email' => $student->parent_email,
                            // 'parent_mobile' => $student->parent_mobile,
                            'grade' => $student->grade,
                            'skills' => $student->skills,
                        ];
                        return response()->json($response, 200);
                    } else {
                        $response['status'] = "true";
                        $response['data'] = [];
                        $response['message'] = "No student found";
                        return response()->json($response, 200);
                        // return response()->json(['errors' => 'No student found'], 400);
                    }
                } else {
                    return response()->json(['errors' => 'No user found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    public function recommendedTutors(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
//            return response()->json(['errors' =>  $validatedData->errors()->first('token')], 400);
        } else {
            try {
                $student = User::where("token", $request->token)->join('student_details', 'users.id', '=', 'student_details.user_id')->first();
                $skill = Skill::find($student->skills);
                $category = Category::where('category_name', $skill->skill_name)->first();

                if ($student->id && $category) {
                    $tutors = User::where(['role' => 'tutor'])
                        ->join('tutor_details', 'users.id', '=', 'tutor_details.user_id')
                    // ->join('tutor_portfolio','users.id', '=','tutor_portfolio.user_id')
                        ->where('tutor_details.category', '=', $category->id)
                        ->select('users.*', 'tutor_details.*')
                        ->get();

                    if (count($tutors) <= 0) {
                        $tutors = User::join('tutor_details', 'users.id', '=', 'tutor_details.user_id')->select('users.*', 'tutor_details.*')->get();
                    }
                    if (count($tutors) > 0) {
                        foreach ($tutors as $tutor) {
                            $country = $tutor->country;
                            $subscribers = Subscriber::where(['tutor_id' => $tutor->user_id, 'user_id' => $student->id])->first();
                            $category = Category::where('id', $tutor->category)->first();
                            $portfolios = TutorPortfolio::where('user_id', $tutor->user_id)->get();
                            $portfolio_data = [];
                            if (count($portfolios) > 0) {
                                foreach ($portfolios as $portfolio) {
                                    $portfolio_data[] = [
                                        'id' => $portfolio->id,
                                        'tutor_id' => $portfolio->user_id,
                                        'title' => $portfolio->title,
                                        'description' => $portfolio->description,
                                        'video_url' => url('/' . $portfolio->video_url),
                                        'video_thumbnail' => url('/' . $portfolio->video_thumbnail),
                                    ];
                                }

                            }
                            $response['data'][] = [
                                'id' => $tutor->user_id,
                                'name' => $tutor->name,
                                'email' => $tutor->email,
                                'phone_number' => $tutor->phone_number,
                                'location' => [
                                    'country_id' => $country->id,
                                    'country_name' => $country->country_name,
                                ],
                                'profile_image' => $tutor->profile_image != "" ? url('/' . $tutor->profile_image) : "",
                                'category' => [
                                    'category_id' => $category->id,
                                    'category' => $category->category_name,
                                    'image' => url('/' . $category->image),
                                ],
                                'about' => $tutor->about,
                                'amount' => 100,
                                'rating' => $tutor->rating,
                                'subscription_status' => ($subscribers) ? $subscribers->status : 0,
                                'portfolios' => $portfolio_data,
                                // 'experience' => json_decode($tutor->experience),

                            ];
                        }
                        $response['status'] = "true";
                        return response()->json($response, 200);
                    } else {
                        $response['status'] = "true";
                        $response['data'] = [];
                        $response['message'] = "No tutors found";
                        return response()->json($response, 200);
                        // return response()->json(['errors' => 'No tutors found'], 400);
                    }

                } else {
                    return response()->json(['errors' => 'No user found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }
    public function getVideoByCategory(Request $request)
    {
        try {
            $categories = Category::all();
            if ($categories) {
                $categoryData = [];
                foreach ($categories as $category) {
                    $tutorDetails = TutorDetail::where('category', $category->id)
                        ->join('tutor_portfolio', 'tutor_details.user_id', '=', 'tutor_portfolio.user_id')
                        ->join('users', 'tutor_details.user_id', '=', 'users.id')
                        ->get();

                    if ($tutorDetails) {
                        $tutorData = [];
                        foreach ($tutorDetails as $tutorDetail) {
                            $category = Category::where('id', $tutorDetail->category)->first();
                            $country = Country::where('id', $tutorDetail->location)->first();
                            $tutorData[] = [
                                'id' => $tutorDetail->id,
                                'name' => $tutorDetail->name,
                                'email' => $tutorDetail->email,
                                'phone_number' => $tutorDetail->phone_number,
                                'location' => [
                                    'country_id' => $country->id,
                                    'country_name' => $country->country_name,
                                ],
                                'category' => [
                                    'category_id' => $category->id,
                                    'category' => $category->category_name,
                                    'image' => url('/' . $category->image),
                                ],
                                'title' => $tutorDetail->title,
                                'image' => $tutorDetail->profile_image != "" ? url('/' . $tutorDetail->profile_image) : "",
                                'description' => $tutorDetail->description,
                                'video_thumbnail' => $tutorDetail->video_thumbnail != "" ? url('/' . $tutorDetail->video_thumbnail) : "",
                                'video_url' => $tutorDetail->video_url != "" ? url('/' . $tutorDetail->video_url) : "",
                                'rating' => $tutorDetail->rating,
                            ];
                        }
                    }

                    $response['data'][] = [
                        'id' => $category->id,
                        'category_name' => $category->category_name,
                        'videos' => isset($tutorData) ? $tutorData : '',
                    ];
                }
                $response['status'] = "true";
                return response()->json($response, 200);
            } else {
                $response['status'] = "true";
                $response['data'] = [];
                $response['message'] = "No category found";
                return response()->json($response, 200);
            }

        } catch (\Exception$e) {
            // dd($e);
            $response['errors'] = 'error getting data';
            return response()->json($response, 400);

        }

    }

    public function suggestedVideos(Request $request)
    {
        if ($request->bearerToken()) {
            $token = $request->bearerToken();
            $request->request->add(['token' => $token]);

            $validatedData = Validator::make($request->all(), [
                'token' => 'required|exists:users,token|bail',
            ]);

        }

        if (isset($validatedData) && $validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
//            return response()->json(['errors' =>  $validatedData->errors()->first('token')], 400);
        } else {
            try {
                if ($request->token) {
                    $student = User::where("token", $request->token)->join('student_details', 'users.id', '=', 'student_details.user_id')->first();
                    $skill = Skill::find($student->skills);
                    $category = Category::where('category_name', $skill->skill_name)->first();
                    if ($category) {
                        $tutors = User::where(['role' => 'tutor'])
                            ->join('tutor_details', 'users.id', '=', 'tutor_details.user_id')
                            ->join('tutor_portfolio', 'users.id', '=', 'tutor_portfolio.user_id')
                            ->where('tutor_details.category', '=', $category->id)
                            ->get();
                    }

                    if (isset($tutors) && count($tutors) <= 0) {
                        // $tutors = User::where(['location' => $student->location])->join('tutor_details', 'users.id', '=', 'tutor_details.user_id')->select('users.*', 'tutor_details.category', 'tutor_details.about')->get();
                        $tutors = User::join('tutor_details', 'users.id', '=', 'tutor_details.user_id')
                            ->join('tutor_portfolio', 'users.id', '=', 'tutor_portfolio.user_id')
                            ->get();
                    }
                } else {
                    $tutors = User::join('tutor_details', 'users.id', '=', 'tutor_details.user_id')
                        ->join('tutor_portfolio', 'users.id', '=', 'tutor_portfolio.user_id')
                        ->get();
                }

                if (count($tutors) > 0) {
                    foreach ($tutors as $tutor) {
                        $country = $tutor->country;
                        $category = Category::where('id', $tutor->category)->first();
                        $response['data'][] = [
                            'id' => $tutor->user_id,
                            'name' => $tutor->name,
                            'email' => $tutor->email,
                            'phone_number' => $tutor->phone_number,
                            'location' => [
                                'country_id' => $country->id,
                                'country_name' => $country->country_name,
                            ],
                            // 'profile_image' => $tutor->profile_image != "" ? url('/' . $tutor->profile_image) : "",
                            'category' => [
                                'category_id' => $category->id,
                                'category' => $category->category_name,
                                'image' => url('/' . $category->image),
                            ],
                            'title' => $tutor->title,
                            'description' => $tutor->description,
                            'video_url' => $tutor->video_url != "" ? url('/' . $tutor->video_url) : "",
                            'video_thumbnail' => $tutor->video_thumbnail != "" ? url('/' . $tutor->video_thumbnail) : "",
                            'rating' => $tutor->rating,
                            // 'experience' => json_decode($tutor->experience),

                        ];
                    }
                    $response['status'] = "true";
                    return response()->json($response, 200);
                } else {
                    $response['status'] = "true";
                    $response['data'] = [];
                    $response['message'] = "No tutors found";
                    return response()->json($response, 200);
                    // return response()->json(['errors' => 'No tutors found'], 400);
                }

            } catch (\Illuminate\Database\QueryException$e) {
                // dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                // dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    public function getEarning(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);

        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
//            return response()->json(['errors' =>  $validatedData->errors()->first('token')], 400);
        } else {
            try {
                $token = $request->Input('token');
                if (!empty($request->input('token'))) {
                    $user = User::where('token', $token)->first();
                }
                if ($user) {
                    $tutors = User::where('payments.tutor_id', $user->id)->join('payments', 'users.id', '=', 'payments.user_id')
                        ->select('users.id', 'users.name', 'payments.created_at', 'payments.amount')->get();
                    if ($tutors) {
                        $general_settings = Generalsetting::find(1);
                        $percentage = (int) $general_settings->commision;

                        if (count($tutors) > 0) {
                            $response['status'] = "true";
                            foreach ($tutors as $tutor) {
                                $new_amount = ($percentage / 100) * $tutor->amount;
                                $response['data'][] = [
                                    'student_id' => $tutor->id,
                                    'student_name' => $tutor->name,
                                    'payment_date' => date('M d, H:i A', strtotime($tutor->created_at)),
                                    // 'payment_amount' => '$' . number_format((float) $tutor->amount, 2, '.', ''),
                                    'payment_amount' => $tutor->amount - $new_amount,

                                ];
                            }
                            return response()->json($response, 200);
                        } else {
                            $response['status'] = "true";
                            $response['data'] = [];
                            $response['message'] = "No payment found";
                            return response()->json($response, 200);
                            // return response()->json(['errors' => 'No payment found'], 400);
                        }
                    } else {
                        return response()->json(['errors' => 'No payment found'], 400);
                    }
                } else {
                    return response()->json(['errors' => 'No user found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    public function uploadImage(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
            'profile_image' => 'required|image|bail',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
//            return response()->json(['errors' =>  $validatedData->errors()->first('token')], 400);
        } else {
            try {
                $token = $request->Input('token');
                if (!empty($request->input('token'))) {
                    $user = User::where('token', $token)->first();
                }
                $file = $request->file('profile_image');
                $path = $file->store('/uploads', ['disk' => 'public']);
                $user->profile_image = $path;
                $user->save();
                $url = Storage::url($user->Photo);
                if ($url) {
                    $response['status'] = "true";
                    $response['data']['token'] = $user->token;
                    $response['data']['Photo'] = $path != "" ? url('/' . $path) : "";
                    return response()->json($response, 200);
                } else {
                    $response['status'] = "false";
                    $response['data'] = 'Error Uploading Image';
                    return response()->json($response, 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    public function searchSessions(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
            'page' => 'numeric|required|',
            'limit' => 'numeric|required|',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
//            return response()->json(['errors' => $validatedData->errors()->first('token')], 400);
        } else {
            try {
                $search = Sessions::where('users.role', 'tutor')->join('users', 'users.id', '=', 'sessions.user_id')
                    ->select('users.id', 'users.location', 'users.name', 'users.role', 'sessions.user_id');
                if ($request->search_type == 'location') {
                    $search = $search->where('users.location', 'like', '%' . $request->keyword . '%');
                }
                if ($request->search_type == 'subject') {
                    $search = $search->where('subject', 'like', '%' . $request->keyword . '%');
                    $search = $search->where('session_date', '>=', date('Y-m-d'));
                }
                if ($request->search_type == 'skill') {
                    $search = $search->where('skill', 'like', '%' . $request->keyword . '%');
                    $search = $search->where('session_date', '>=', date('Y-m-d'));
                }
                if ($request->search_type == 'common') {
                    $keyword = $request->keyword;
                    $search = $search->where('users.role', 'tutor')
                        ->where(function ($orfilter) use ($keyword) {
                            $orfilter->Where('users.location', 'like', '%' . $keyword . '%')
                                ->orWhere('users.name', 'like', '%' . $keyword . '%')
                                ->orWhere('sessions.type', 'like', '%' . $keyword . '%')
                                ->orWhere('sessions.grade', 'like', '%' . $keyword . '%')
                                ->orWhere('sessions.subject', 'like', '%' . $keyword . '%')
                                ->orWhere('sessions.skill', 'like', '%' . $keyword . '%')
                                ->orWhere('sessions.session_name', 'like', '%' . $keyword . '%')
                                ->orWhere('sessions.description', 'like', '%' . $keyword . '%')
                                ->orWhere('sessions.learn', 'like', '%' . $keyword . '%');
                        });

                }
                $search = $search->groupBy('sessions.user_id')->paginate($request->Input('limit'));
                if (count($search) > 0) {

                    foreach ($search as $search_data) {
                        $response['data'][] = [
                            'id' => $search_data->id,
                            'name' => $search_data->user->name,
                            'email' => $search_data->user->email,
                            'phone_number' => $search_data->user->phone_number,
                            'location' => $search_data->location,
                            'profile_image' => $search_data->user->profile_image != "" ? url('/' . $search_data->user->profile_image) : "",
                            'category' => $search_data->user->tutor_detail->category,
                            'sub_category' => $search_data->user->tutor_detail->sub_category,
                            'skills' => $search_data->user->tutor_detail->skills,
                            'about' => $search_data->user->tutor_detail->about,
                            'experience' => json_decode($search_data->user->tutor_detail->experience),
                        ];
                    }
                    $response['status'] = "true";
                    $response['current-page'] = $search->currentPage();
                    $response['limit'] = $search->perPage();
                    $response['total'] = count($search);
                    return response()->json($response, 200);
                } else {
                    $response['status'] = "true";
                    $response['data'] = [];
                    $response['message'] = "No result found";
                    return response()->json($response, 200);
                    // return response()->json(['errors' => 'No result found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }
    public function getPayments($user)
    {
        $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET_KEY'));
        $userSubscriptions = UserSubscription::where('user_id', $user->id)->get();

        if ($userSubscriptions && count($userSubscriptions) > 0) {

            foreach ($userSubscriptions as $userSubscription) {

                $subscription = $stripe->subscriptions->retrieve(
                    $userSubscription->stripe_subscription_id,
                    []
                );

                if ($subscription->status == 'past_due') {
                    $payment = Payment::where('package_id', $subscription->package_id)
                        ->where('user_id', $subscription->user_id)
                        ->first();
                    if ($payment) {
                        $payment->payment_status = 0;
                        $payment->save();
                        $subscriber = Subscriber::where(['tutor_id' => $payment->tutor_id, 'user_id' => $payment->user_id])->first();
                        if ($subscriber) {
                            $subscriber->status = 0;
                            $subscriber->save();
                        }
                    }
                } else {
                    $payment = Payment::where('package_id', $subscription->package_id)
                        ->where('user_id', $subscription->user_id)
                        ->first();

                    if ($payment) {
                        $payment->payment_status = 1;
                        $payment->save();
                        $subscriber = Subscriber::where(['tutor_id' => $payment->tutor_id, 'user_id' => $payment->user_id])->first();
                        if ($subscriber) {
                            $subscriber->status = 1;
                            $subscriber->save();
                        }
                    }
                }
            }
        }
    }

    public function createRefund(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
            'batch_id' => 'required|exists:batches,id',
        ]);

        $batch = Batch::where("id", $request->batch_id)->first();
        $user = User::where("token", $request->token)->first();

        if ($batch->payment_type != 'monthly') {

            $existsPayment = Payment::where('batch_id', $request->batch_id)
                ->where('package_id', $request->package_id)
                ->where('user_id', $user->id)
                ->first();

            if ($existsPayment) {

                if ($existsPayment->charge_id) {
                    $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET_KEY'));
                    $refund = $stripe->refunds->create([
                        'charge' => $existsPayment->charge_id,
                    ]);

                    if ($refund['status'] = 'succeeded') {
                        $existsPayment->payment_status = 2;
                        $existsPayment->save();

                        $subscriber = Subscriber::where(['tutor_id' => $batch->user_id, 'user_id' => $user->id])->first();
                        if ($subscriber) {
                            $subscriber->status = 0;
                            $subscriber->save();
                        }

                        $response['status'] = "true";
                        $response['data']['token'] = $request->token;
                        $response['data']['refund-status'] = 'refund success';

                        return response()->json($response, 200);
                    }
                }
            } else {
                $response['status'] = "true";
                $response['data']['token'] = $request->token;
                $response['data']['refund-status'] = 'refund failed';

                return response()->json($response, 400);
            }

        }

    }

    public function createPayment(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
            'card_number' => 'required',
            'card_name' => 'required',
            'expiry_month' => 'required',
            'expiry_year' => 'required',
            'cvv' => 'required',
            'batch_id' => 'required|exists:batches,id',
            'package_id' => 'required|exists:packages,id',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
//            return response()->json(['errors' => $validatedData->errors()->first('token')], 400);
        } else {
            $batch = Batch::where("id", $request->batch_id)->first();
            $user = User::where("token", $request->token)->first();

            $existsPayment = Payment::where('batch_id', $request->batch_id)
                ->where('package_id', $request->package_id)
                ->where('user_id', $user->id)
                ->first();

            if ($existsPayment) {
                $response['status'] = "true";
                $response['data']['token'] = $request->token;
                $response['data']['payment-status'] = 'You are already purchased this batch';

                return response()->json($response, 400);
            }
            $payment = new Payment();
            // dd($batch->start.'-'.$batch->end);
            $payment_iterations = nb_mois($batch->start, $batch->end);

            $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET_KEY'));

            if ($batch->payment_type != 'monthly') {
                try {
                    $token = $stripe->tokens->create([
                        'card' => [
                            'number' => $request->card_number,
                            'exp_month' => $request->expiry_month,
                            'exp_year' => $request->expiry_year,
                            'cvc' => $request->cvv,
                        ],
                    ]);
                } catch (\Exception$e) {
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $response['errors'] = $err['message'];
                    // $response['errors'] = $e;
                    return response()->json($response, 400);
                }

                if (!isset($token['id'])) {
                    return response()->json(['errors' => 'error in token generation'], 401);
                }
                try {
                    $charge = $stripe->charges->create([
                        'card' => $token['id'],
                        'currency' => 'inr',
                        'amount' => (float) $batch->subscription_fee * 100,
                        'description' => 'wallet',
                    ]);
                } catch (\Exception$e) {
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $response['errors'] = $err['message'];
                    return response()->json($response, 400);
                }

                if ($charge['status'] == 'succeeded') {

                    $payment->amount = $batch->subscription_fee;
                    $payment->payment_method = $request->payment_method;
                    $payment->card_number = $request->card_number;
                    $payment->card_name = $request->card_name;
                    $payment->expiry_date = $request->expiry_date;
                    $payment->cvv = $request->cvv;
                    $payment->batch_id = $request->batch_id;
                    $payment->package_id = $request->package_id;
                    $payment->tutor_id = $batch->user_id;
                    $payment->user_id = $user->id;
                    $payment->payment_status = 1;
                    $payment->payment_method = $batch->payment_type;
                    $payment->charge_id = $charge['id'];
                    if ($payment->save()) {
                        $subscriber = Subscriber::where(['tutor_id' => $batch->user_id, 'user_id' => $user->id])->first();
                        if ($subscriber) {
                            $subscriber->status = 1;
                            $subscriber->save();
                        } else {
                            $subscriber = new Subscriber();
                            $subscriber->tutor_id = $batch->user_id;
                            $subscriber->user_id = $user->id;
                            $subscriber->status = 1;
                            $subscriber->save();
                        }
                        $response['status'] = "true";
                        $response['data']['token'] = $request->token;
                        $response['data']['payment-status'] = $payment->payment_status;
                        return response()->json($response, 200);
                    } else {
                        return response()->json(['errors' => 'error saving payment'], 401);
                    }
                } else {
                    $response['errors'] = 'error in payment';
                    return response()->json($response, 400);
                }
            } else {
                \Stripe\Stripe::setApiKey(env('STRIPE_SECRET_KEY'));

                // subscrption code
                $planName = $batch->batch_name;
                $planPrice = (int) $batch->subscription_fee;
                $planInterval = 'month';

                // Convert price to cents
                $planPriceCents = round($planPrice * 100);

                // Add customer to stripe
                try {
                    $customer = \Stripe\Customer::create([
                        'name' => $user->name,
                        'email' => $user->email,
                    ]);
                } catch (Exception $e) {
                    $api_error = $e->getMessage();
                }

                try {
                    $token = $stripe->tokens->create([
                        'card' => [
                            'number' => $request->card_number,
                            'exp_month' => $request->expiry_month,
                            'exp_year' => $request->expiry_year,
                            'cvc' => $request->cvv,
                        ],
                    ]);
                } catch (\Exception$e) {
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $response['errors'] = $err['message'];
                    // $response['errors'] = $e;
                    return response()->json($response, 400);
                }

                if ($token && $customer) {
                    try {
                        $stripe->customers->updateSource(
                            $customer->id,
                            $token->card->id,
                            ['name' => $request->card_name]
                        );
                    } catch (\Exception$e) {

                    }

                }

                if (empty($api_error) && $customer) {
                    try {
                        // Create price with subscription info and interval
                        $price = \Stripe\Price::create([
                            'unit_amount' => $planPriceCents,
                            'currency' => 'USD',
                            'recurring' => ['interval' => $planInterval],
                            'product_data' => ['name' => $planName],
                        ]);
                    } catch (Exception $e) {
                        $api_error = $e->getMessage();
                    }
                    if (empty($api_error) && $price) {
                        // Create a new subscription
                        try {
                            $subscription = \Stripe\SubscriptionSchedule::create([
                                'customer' => $customer->id,
                                'start_date' => 'now',
                                'end_behavior' => 'release',
                                'phases' => [
                                    [
                                        'items' => [
                                            [
                                                'price' => $price->id,
                                                'quantity' => 1,
                                            ],
                                        ],
                                        'iterations' => $payment_iterations,
                                    ],
                                ],
                            ]);
                        } catch (Exception $e) {
                            $api_error = $e->getMessage();
                        }
                        if (empty($api_error) && $subscription) {
                            $userSub = new UserSubscription();
                            $userSub->user_id = $user->id;
                            $userSub->batch_id = $batch->id;
                            $userSub->package_id = $request->package_id;
                            $userSub->stripe_subscription_id = $subscription->subscription;
                            $userSub->stripe_customer_id = $customer->id;
                            $userSub->sub_schedule_id = $subscription->id;
                            $userSub->created = $subscription->created;
                            $userSub->status = $subscription->status;
                            $userSub->plan_period_start = $subscription->current_phase->start_date;
                            $userSub->plan_period_end = $subscription->current_phase->end_date;

                            $payment->amount = $batch->subscription_fee;
                            $payment->payment_method = $request->payment_method;
                            $payment->card_number = $request->card_number;
                            $payment->card_name = $request->card_name;
                            $payment->expiry_date = $request->expiry_date;
                            $payment->cvv = $request->cvv;
                            $payment->batch_id = $request->batch_id;
                            $payment->package_id = $request->package_id;
                            $payment->tutor_id = $batch->user_id;
                            $payment->user_id = $user->id;
                            $payment->payment_status = ($subscription->status == 'active') ? 1 : 0;
                            $payment->payment_method = $batch->payment_type;

                            if ($payment->save() && $userSub->save()) {
                                $subscriber = Subscriber::where(['tutor_id' => $batch->user_id, 'user_id' => $user->id])->first();
                                if ($subscriber) {
                                    $subscriber->status = 1;
                                    $subscriber->save();
                                } else {
                                    $subscriber = new Subscriber();
                                    $subscriber->tutor_id = $batch->user_id;
                                    $subscriber->user_id = $user->id;
                                    $subscriber->status = 1;
                                    $subscriber->save();
                                }

                                $response['status'] = "true";
                                $response['data']['token'] = $request->token;
                                $response['data']['payment-status'] = ($payment->payment_status == 1) ? 'completed' : 'incomplete';
                                return response()->json($response, 200);
                            } else {
                                return response()->json(['errors' => 'error saving payment'], 401);
                            }
                        } else {
                            echo json_encode(['error' => $api_error]);
                        }
                    } else {
                        echo json_encode(['error' => $api_error]);
                    }
                } else {
                    echo json_encode(['error' => $api_error]);
                }

            } // end else

        } //else close
    }
    public function getAllRegions(Request $request)
    {
        try {
            $token = $request->Input('token');
            $regions = Region::all();
            if ($regions) {
                if (count($regions) > 0) {
                    $response['status'] = "true";
                    foreach ($regions as $region) {
                        $response['data'][] = [
                           
                            'region_id' => $region->id,
                            'region_name' => $region->name,
                        ];
                    }
                    return response()->json($response, 200);
                } else {
                    $response['status'] = "true";
                    $response['data'] = [];
                    $response['message'] = "No countries found";
                    return response()->json($response, 200);
                    // return response()->json(['errors' => 'No regions found'], 400);
                }
            } else {
                $response['status'] = "true";
                $response['data'] = [];
                $response['message'] = "No countries found";
                return response()->json($response, 200);
                // return response()->json(['errors' => 'No countries found'], 400);
            }
        } catch (\Illuminate\Database\QueryException$e) {
            $response['errors'] = 'error getting data';
            return response()->json($response, 400);
        } catch (\Exception$e) {
            $response['errors'] = 'error getting data';
            return response()->json($response, 400);
        }
    }
    public function getAllCountries(Request $request)
    {
        try {
            $token = $request->Input('token');
            $countries = Country::all();
            if ($countries) {
                if (count($countries) > 0) {
                    $response['status'] = "true";
                    foreach ($countries as $country) {
                        $response['data'][] = [
                            'country_id' => $country->id,
                            'region_id' => $country->region_id,
                            'country_name' => $country->country_name,
                        ];
                    }
                    return response()->json($response, 200);
                } else {
                    $response['status'] = "true";
                    $response['data'] = [];
                    $response['message'] = "No countries found";
                    return response()->json($response, 200);
                    // return response()->json(['errors' => 'No countries found'], 400);
                }
            } else {
                $response['status'] = "true";
                $response['data'] = [];
                $response['message'] = "No countries found";
                return response()->json($response, 200);
                // return response()->json(['errors' => 'No countries found'], 400);
            }
        } catch (\Illuminate\Database\QueryException$e) {
            $response['errors'] = 'error getting data';
            return response()->json($response, 400);
        } catch (\Exception$e) {
            $response['errors'] = 'error getting data';
            return response()->json($response, 400);
        }
    }
    public function getAllSkills(Request $request)
    {
        try {
            $token = $request->Input('token');
            $skills = CategorySkills::all();
            if ($skills) {
                if (count($skills) > 0) {
                    $response['status'] = "true";
                    foreach ($skills as $skill) {
                        $response['data'][] = [
                            'skill_id' => $skill->id,
                            'category_name' => $skill->category->category_name,
                            'skill_name' => $skill->skill_name,
                        ];
                    }
                    return response()->json($response, 200);
                } else {
                    $response['status'] = "true";
                    $response['data'] = [];
                    $response['message'] = "No skills found";
                    return response()->json($response, 200);
                    return response()->json(['errors' => 'No skills found'], 400);
                }
            } else {
                $response['status'] = "true";
                $response['data'] = [];
                $response['message'] = "No skills found";
                return response()->json($response, 200);
                return response()->json(['errors' => 'No skills found'], 400);
            }
        } catch (\Illuminate\Database\QueryException$e) {
            $response['errors'] = 'error getting data';
            return response()->json($response, 400);
        } catch (\Exception$e) {
            $response['errors'] = 'error getting data';
            return response()->json($response, 400);
        }
    }
    public function getAllCategories(Request $request)
    {
        try {
            $categories = Category::all();
            if ($categories) {
                if (count($categories) > 0) {
                    $response['status'] = "true";
                    foreach ($categories as $category) {
                        $response['data'][] = [
                            'category_id' => $category->id,
                            'category' => $category->category_name,
                            // 'image' => url('/' . $category->image),
                        ];
                    }
                    return response()->json($response, 200);
                } else {
                    $response['status'] = "true";
                    $response['data'] = [];
                    $response['message'] = "No categories found";
                    return response()->json($response, 200);
                    // return response()->json(['errors' => 'No categories found'], 400);
                }
            } else {
                $response['status'] = "true";
                $response['data'] = [];
                $response['message'] = "No categories found";
                return response()->json($response, 200);
                // return response()->json(['errors' => 'No categories found'], 400);
            }

        } catch (\Illuminate\Database\QueryException$e) {
            $response['errors'] = 'error getting data';
            return response()->json($response, 400);
        } catch (\Exception$e) {
            $response['errors'] = 'error getting data';
            return response()->json($response, 400);
        }
    }

    public function getCatsWithSubcats(Request $request)
    {
        $validatedData = Validator::make($request->all(), [
            'category_id' => 'required',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'Category Required'], 400);
//            return response()->json(['errors' => $validatedData->errors()->first('token')], 400);
        } else {
            try {
                $sub_categories = SubCategory::where('category_id', $request->category_id)->get();
                if ($sub_categories) {
                    if (count($sub_categories) > 0) {
                        $response['status'] = "true";
                        foreach ($sub_categories as $sub_category) {
                            $response['data'][] = [
                                'sub_category_id' => $sub_category->id,
                                'sub_category_name' => $sub_category->sub_category_name,
                            ];
                        }
                        return response()->json($response, 200);
                    } else {
                        $response['status'] = "true";
                        $response['data'] = [];
                        $response['message'] = "No sub categories found";
                        return response()->json($response, 200);
                        // return response()->json(['errors' => 'No sub categories found'], 400);
                    }
                } else {
                    $response['status'] = "true";
                    $response['data'] = [];
                    $response['message'] = "No sub categories found";
                    return response()->json($response, 200);
                    // return response()->json(['errors' => 'No sub categories found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }
    public function getSubCategories(Request $request)
    {
        $validatedData = Validator::make($request->all(), [
            'category_id' => 'required',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'Category Required'], 400);
//            return response()->json(['errors' => $validatedData->errors()->first('token')], 400);
        } else {
            try {
                $sub_categories = SubCategory::where('category_id', $request->category_id)->get();
                if ($sub_categories) {
                    if (count($sub_categories) > 0) {
                        $response['status'] = "true";
                        foreach ($sub_categories as $sub_category) {
                            $response['data'][] = [
                                'sub_category_id' => $sub_category->id,
                                'sub_category_name' => $sub_category->sub_category_name,
                            ];
                        }
                        return response()->json($response, 200);
                    } else {
                        $response['status'] = "true";
                        $response['data'] = [];
                        $response['message'] = "No sub categories found";
                        return response()->json($response, 200);
                        // return response()->json(['errors' => 'No sub categories found'], 400);
                    }
                } else {
                    $response['status'] = "true";
                    $response['data'] = [];
                    $response['message'] = "No sub categories found";
                    return response()->json($response, 200);
                    // return response()->json(['errors' => 'No sub categories found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }
    public function getCms(Request $request)
    {
        try {
            $cms_fields = CmsField::all();
            if ($cms_fields) {
                if (count($cms_fields) > 0) {
                    $response['status'] = "true";
                    foreach ($cms_fields as $cms_field) {
                        $response['data'][] = [
                            'id' => $cms_field->id,
                            'display_order' => $cms_field->display_order,
                            'cms_heading' => $cms_field->cms_heading,
                            'cms_description' => $cms_field->cms_description,
                            'cms_image' => $cms_field->cms_image != '' ? url('/' . $cms_field->cms_image) : "",
                            'button_text' => $cms_field->button_text,
                        ];
                    }
                    return response()->json($response, 200);
                } else {
                    $response['status'] = "true";
                    $response['data'] = [];
                    $response['message'] = "No sub categories found";
                    return response()->json($response, 200);
                    // return response()->json(['errors' => 'No sub categories found'], 400);
                }
            } else {
                $response['status'] = "true";
                $response['data'] = [];
                $response['message'] = "No sub categories found";
                return response()->json($response, 200);
                // return response()->json(['errors' => 'No sub categories found'], 400);
            }
        } catch (\Illuminate\Database\QueryException$e) {
            $response['errors'] = 'error getting data';
            return response()->json($response, 400);
        } catch (\Exception$e) {
            $response['errors'] = 'error getting data';
            return response()->json($response, 400);
        }
    }

    public function getAllGrades(Request $request)
    {
        try {
            $grades = Grade::all();
            if ($grades) {
                if (count($grades) > 0) {
                    $response['status'] = "true";
                    foreach ($grades as $grade) {
                        $response['data'][] = [
                            'grade_id' => $grade->id,
                            'grade_name' => $grade->grade_name,
                        ];
                    }
                    return response()->json($response, 200);
                } else {
                    $response['status'] = "true";
                    $response['data'] = [];
                    $response['message'] = "No grades found";
                    return response()->json($response, 200);
                    // return response()->json(['errors' => 'No grades found'], 400);
                }
            } else {
                $response['status'] = "true";
                $response['data'] = [];
                $response['message'] = "No grades found";
                return response()->json($response, 200);
                // return response()->json(['errors' => 'No grades found'], 400);
            }

        } catch (\Illuminate\Database\QueryException$e) {
            $response['errors'] = 'error getting data';
            return response()->json($response, 400);
        } catch (\Exception$e) {
            $response['errors'] = 'error getting data';
            return response()->json($response, 400);
        }
    }

    public function createPortfolio(Request $request)
    {

        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);

        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
        } else {
            $portfolio = new TutorPortfolio();
            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'description' => 'required',
                'video_url' => 'required|mimes:mp4,mov,ogg,qt|max:100000',
                // 'video_thumbnail' => 'required|mimes:jpeg,bmp,png',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->first()], 401);
            }
            try {
                if ($request->video_url) {
                    $video = $request->file('video_url');
                    $path_video = $video->store('/uploads/tutor/portfolio', ['disk' => 'public']);

                    $filename = str_replace("uploads/tutor/portfolio/", "", $path_video);
                    $filenameWithoutExtenstion = pathinfo($filename, PATHINFO_FILENAME);

                    $thumbnail = "uploads/tutor/portfolio/" . $filenameWithoutExtenstion . ".png";
                    $sec = 10;

                    $this->getImageFromVideo($sec, $path_video, $thumbnail);
                }

                $user = User::where("token", $request->token)->first();
                $portfolio->user_id = $user->id;
                $portfolio->title = $request->title;
                $portfolio->description = $request->description;
                $portfolio->video_url = $path_video;
                $portfolio->video_thumbnail = $thumbnail;
                if ($portfolio->save()) {
                    $response['status'] = "true";
                    $response['data'] = [
                        'id' => $portfolio->id,
                        'tutor_id' => $portfolio->user_id,
                        'title' => $portfolio->title,
                        'description' => $portfolio->description,
                        'video_url' => url('/' . $portfolio->video_url),
                        'video_thumbnail' => url('/' . $portfolio->video_thumbnail),
                    ];
                    return response()->json($response, 200);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error saving data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error saving data';
                return response()->json($response, 400);
            }
        }
    }

    public function editPortfolio(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);

        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
        } else {

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'title' => 'required',
                'description' => 'required',
                'video_url' => 'nullable|mimes:mp4,mov,ogg,qt|max:20000',
                // 'video_thumbnail' => 'nullable|mimes:jpeg,bmp,png',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->first()], 401);
            }
            try {
                $user = User::where("token", $request->token)->first();
                $portfolio = TutorPortfolio::findOrFail($request->id);
                if ($request->video_url) {
                    if ($portfolio->id) {
                        if ($portfolio->video_url) {
                            if (Storage::disk('public')->exists($portfolio->video_url)) {
                                Storage::disk('public')->delete($portfolio->video_url);
                            }
                        }
                        if ($portfolio->video_thumbnail) {
                            if (Storage::disk('public')->exists($portfolio->video_thumbnail)) {
                                Storage::disk('public')->delete($portfolio->video_thumbnail);
                            }
                        }
                    }
                    $video = $request->file('video_url');
                    $path_video = $video->store('/uploads/tutor/portfolio', ['disk' => 'public']);

                    $filename = str_replace("uploads/tutor/portfolio/", "", $path_video);
                    $filenameWithoutExtenstion = pathinfo($filename, PATHINFO_FILENAME);

                    $thumbnail = "uploads/tutor/portfolio/" . $filenameWithoutExtenstion . ".png";
                    $sec = 10;

                    $this->getImageFromVideo($sec, $path_video, $thumbnail);

                    $portfolio->video_url = $path_video;
                    $portfolio->video_thumbnail = $thumbnail;

                }

                $portfolio->user_id = $user->id;
                $portfolio->title = $request->title;
                $portfolio->description = $request->description;

                if ($portfolio->save()) {
                    $response['status'] = "true";
                    $response['data'] = [
                        'id' => $portfolio->id,
                        'tutor_id' => $portfolio->user_id,
                        'title' => $portfolio->title,
                        'description' => $portfolio->description,
                        'video_url' => url('/' . $portfolio->video_url),
                        'video_thumbnail' => url('/' . $portfolio->video_thumbnail),
                    ];
                    return response()->json($response, 200);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error saving data';
                // dd($e);
                return response()->json($response, 400);
            } catch (\Exception$e) {
                // dd($e);
                $response['errors'] = 'error saving data';
                return response()->json($response, 400);
            }
        }
    }

    public function getPortfolio(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);

        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
        } else {

            try {
                $token = $request->Input('token');
                if (!empty($request->input('token'))) {
                    $user = User::where('token', $token)->first();
                }
                if ($user) {
                    $portfolios = TutorPortfolio::where('user_id', $user->id)->get();
                    if ($portfolios && count($portfolios) > 0) {
                        $response['status'] = "true";
                        foreach ($portfolios as $portfolio) {
                            $response['data'][] = [
                                'id' => $portfolio->id,
                                'tutor_id' => $portfolio->user_id,
                                'title' => $portfolio->title,
                                'description' => $portfolio->description,
                                'video_url' => url('/' . $portfolio->video_url),
                                'video_thumbnail' => url('/' . $portfolio->video_thumbnail),
                            ];
                        }

                        return response()->json($response, 200);
                    } else {
                        $response['status'] = "true";
                        $response['data'] = [];
                        $response['message'] = "No portfolio found";
                        return response()->json($response, 200);
                        // return response()->json(['errors' => 'No portfolio found'], 400);
                    }
                } else {
                    $response['status'] = "true";
                    $response['data'] = [];
                    $response['message'] = "No user found";
                    return response()->json($response, 200);
                    // return response()->json(['errors' => 'No user found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    public function deletePortfolio(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
        } else {
            $validator = Validator::make($request->all(), [
                'id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->first()], 401);
            }
            try {
                $token = $request->Input('token');
                $user = User::where('token', $token)->first();
                if ($user) {
                    $portfolio = TutorPortfolio::findOrFail($request->id);
                    if ($portfolio->user_id == $user->id) {
                        if ($portfolio->delete()) {

                            $response['status'] = "true";
                            $response['data'] = ["Portfolio deleted successfully"];
                            return response()->json($response, 200);
                        } else {
                            $response['errors'] = 'error deleting portfolio data';
                            return response()->json($response, 400);
                        }
                    } else {
                        $response['errors'] = 'Portfolio not found for this user';
                        return response()->json($response, 400);
                    }
                } else {
                    $response['errors'] = 'error deleting portfolio data';
                    return response()->json($response, 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error deleting portfolio data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting portfolio data';
                return response()->json($response, 400);
            }
        }
    }

    public function searchPortfolio(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
        } else {
            try {
                $conditions['keyword'] = $request->keyword;
                $conditions['location'] = $request->location;
                $conditions['subcategory'] = $request->subcategory;
                $conditions['rating'] = $request->rating;

                $tutors = TutorDetail::join('users', 'users.id', '=', 'tutor_details.user_id')
                    ->where(function ($query) use ($conditions) {
                        if ($conditions['keyword'] != ''):
                            $query->where('users.name', 'like', '%' . $conditions['keyword'] . '%');
                            $query->orWhere('tutor_details.about', 'like', '%' . $conditions['keyword'] . '%');
                        endif;
                        if ($conditions['location'] != ''):
                            $query->where('users.location', $conditions['location']);
                        endif;
                        if ($conditions['subcategory'] != ''):
                            $query->where('users.sub_category', $conditions['subcategory']);
                        endif;
                        if ($conditions['rating'] != ''):
                            $query->where('tutor_details.rating', $conditions['rating']);
                        endif;
                    })->get();
                if (count($tutors) > 0) {
                    $response['status'] = "true";
                    foreach ($tutors as $tutor) {
                        $response['data'][] = [
                            'id' => $tutor->id,
                            'name' => $tutor->name,
                            'email' => $tutor->email,
                            'phone_number' => $tutor->phone_number,
                            'location' => $tutor->location,
                            'profile_image' => $tutor->profile_image != "" ? url('/' . $tutor->profile_image) : "",
                            'category' => $tutor->category,
                            'sub_category' => $tutor->sub_category,
                            'skills' => $tutor->skills,
                            'about' => $tutor->about,
                            'experience' => json_decode($tutor->experience),
                        ];
                    }

                    return response()->json($response, 200);
                } else {
                    $response['status'] = "true";
                    $response['data'] = [];
                    $response['message'] = "No tutor found";
                    return response()->json($response, 200);
                    // return response()->json(['errors' => 'No tutor found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    public function addRating(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);

        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
        } else {

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'rating' => 'required|numeric|max:5',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->first()], 401);
            }
            try {
                $tutor = TutorDetail::where("user_id", $request->id)->first();
                if ($tutor) {
                    $tutor->rating = $request->rating;
                    if ($tutor->save()) {
                        $response['status'] = "true";
                        $response['data'] = [
                            'id' => $tutor->user_id,
                            'rating' => $tutor->rating,
                        ];
                        return response()->json($response, 200);
                    }
                } else {
                    return response()->json(['errors' => 'No tutor found'], 400);
                }

            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error saving data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error saving data' . $e;
                return response()->json($response, 400);
            }
        }
    }

    /*
     * get all upcoming tutor sessions by id
     */
    public function tutorPortfolioById(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'tutor_id' => 'numeric',
            'token' => 'required_without:id|exists:users,token|bail',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
        } else {
            try {
                $portfolios = TutorPortfolio::where(['user_id' => $request->tutor_id])->get();
                if (count($portfolios) > 0) {
                    $response['status'] = "true";
                    foreach ($portfolios as $portfolio) {
                        $response['data'][] = [
                            'id' => $portfolio->id,
                            'tutor_id' => $portfolio->user_id,
                            'title' => $portfolio->title,
                            'description' => $portfolio->description,
                            'video_url' => asset($portfolio->video_url),
                        ];
                    }
                    // return response()->json($response, 200);
                    return json_encode($response, JSON_UNESCAPED_SLASHES);
                } else {
                    $response['status'] = "true";
                    $response['data'] = [];
                    $response['message'] = "No portfolios found";
                    return response()->json($response, 200);
                    // return response()->json(['errors' => 'No portfolios found'], 400);
                }

            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }

    }

    public function createPackage(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        // dd($request->batch_id);
        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
            //            return response()->json(['errors' => $validatedData->errors()->first('token')], 400);
        } else {
            $package = new Package();
            $validator = Validator::make($request->all(), [
                // 'batch_id' => 'required',
                'package_name' => 'required|unique:packages',
                'description' => '',
                // 'subscription_fee' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->first()], 401);
            }
            try {

                // $package->batch_id = $request->batch_id;
                $package->package_name = $request->package_name;
                $package->description = $request->description;
                // $package->subscription_fee = $request->subscription_fee;

                $user = User::where("token", $request->token)->first();
                $package->user_id = $user->id;
                if ($package->save()) {
                    if (isset($request->batch_id)) {
                        foreach ($request->batch_id as $id) {
                            $packageBatch = new PackageBatch();
                            $packageBatch->package_id = $package->id;
                            $packageBatch->batch_id = $id;
                            $packageBatch->save();
                        }
                    }

                    $batches = [];

                    foreach ($package->batches as $batch) {
                        $batches[] = [
                            'batch_id' => $batch->id,
                            'batch_name' => $batch->batch_name,
                            'tutor' => $batch->user->name,
                            'tutor_id' => $batch->user_id,
                            'package_id' => $batch->package_id,
                            'description' => $batch->description,
                            'start' => $batch->start,
                            'end' => $batch->end,
                            'duration' => $batch->duration,
                            'no_of_participants' => $batch->no_of_participants,
                            'no_of_sessions' => $batch->no_of_sessions,
                            'payment_type' => $batch->payment_type,
                            'subscription_fee' => $batch->subscription_fee,
                            'subscribers' => count($batch->payments()),
                        ];
                    }

                    $response['status'] = "true";
                    $response['data'] = [
                        'id' => $package->id,
                        'tutor' => $package->user->name,
                        'tutor_id' => $package->user_id,
                        'batch' => $batches,
                        'package_name' => $package->package_name,
                        'description' => $package->description,
                        // 'subscription_fee' => $package->subscription_fee,
                    ];
                    return response()->json($response, 200);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                // dd($e);
                $response['errors'] = 'error saving data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                // dd($e);
                $response['errors'] = 'error saving data';
                return response()->json($response, 400);
            }
        }
    }

    public function updatePackage(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
            //            return response()->json(['errors' => $validatedData->errors()->first('token')], 400);
        } else {
            $package = Package::where(['id' => $request->id])->first();

            $validator = Validator::make($request->all(), [
                // 'batch_id' => 'required',
                'package_name' => 'required|unique:packages,package_name,' . $package->id,
                'description' => '',
                // 'subscription_fee' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->first()], 401);
            }
            try {
                $token = $request->Input('token');
                $user = User::where('token', $token)->first();
                if ($package->user_id == $user->id) {

                    // $package->batch_id = $request->batch_id;

                    $package->package_name = $request->package_name;
                    $package->description = $request->description;
                    // $package->subscription_fee = $request->subscription_fee;

                    $user = User::where("token", $request->token)->first();
                    $package->user_id = $user->id;
                    if ($package->save()) {
                        $packageBatch = PackageBatch::where('package_id', $request->id)->delete();
                        if (isset($request->batch_id)) {
                            foreach ($request->batch_id as $id) {
                                $packageBatch = new PackageBatch();
                                $packageBatch->package_id = $package->id;
                                $packageBatch->batch_id = $id;
                                $packageBatch->save();
                            }
                        }

                        $batches = [];

                        foreach ($package->batches as $batch) {
                            $batches[] = [
                                'batch_id' => $batch->id,
                                'batch_name' => $batch->batch_name,
                                'tutor' => $batch->user->name,
                                'tutor_id' => $batch->user_id,
                                'package_id' => $batch->package_id,
                                'description' => $batch->description,
                                'start' => $batch->start,
                                'end' => $batch->end,
                                'duration' => $batch->duration,
                                'no_of_participants' => $batch->no_of_participants,
                                'no_of_sessions' => $batch->no_of_sessions,
                                'payment_type' => $batch->payment_type,
                                'subscription_fee' => $batch->subscription_fee,
                                'subscribers' => count($batch->payments()),
                            ];
                        }

                        $response['status'] = "true";
                        $response['data'] = [
                            'id' => $package->id,
                            'tutor' => $package->user->name,
                            'tutor_id' => $package->user_id,
                            'batch' => $batches,
                            'package_name' => $package->package_name,
                            'description' => $package->description,
                            // 'subscription_fee' => $package->subscription_fee,
                        ];
                        return response()->json($response, 200);
                    }
                } else {
                    $response['errors'] = 'Package not found for this user';
                    return response()->json($response, 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                // dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                // dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    public function deletePackage(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
            //            return response()->json(['errors' => $validatedData->errors()->first('token')], 400);
        } else {
            try {
                $token = $request->Input('token');
                $user = User::where('token', $token)->first();
                if ($user) {
                    $package = Package::findOrFail($request->id);
                    if ($package->user_id == $user->id) {
                        if ($package->delete()) {
                            $packageBatch = PackageBatch::where('package_id', $request->id)->delete();
                            $response['status'] = "true";
                            $response['data'] = ["package deleted successfully"];
                            return response()->json($response, 200);
                        } else {
                            $response['errors'] = 'error deleting package data';
                            return response()->json($response, 400);
                        }
                    } else {
                        $response['errors'] = 'package not found for this user';
                        return response()->json($response, 400);
                    }
                } else {
                    $response['errors'] = 'error deleting package data';
                    return response()->json($response, 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error deleting package data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting package data';
                return response()->json($response, 400);
            }
        }
    }

    public function getPackages(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required_without:id|exists:users,token|bail',
        ]);

        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
        } else {
            $token = $request->Input('token');
            try {
                if (!empty($request->input('token'))) {
                    $user = User::where('token', $token)->first();
                }
                if ($user) {
                    $batches = [];
                    $packages = Package::where('user_id', $user->id)->with('batches')->get();

                    if ($packages && count($packages) > 0) {
                        $response['status'] = "true";
                        foreach ($packages as $package) {
                            $batches = [];
                            foreach ($package->batches as $batch) {

                                $batches[] = [
                                    'batch_id' => $batch->id,
                                    'batch_name' => $batch->batch_name,
                                    'tutor' => $batch->user->name,
                                    'tutor_id' => $batch->user_id,
                                    'package_id' => $batch->package_id,
                                    'description' => $batch->description,
                                    'start' => $batch->start,
                                    'end' => $batch->end,
                                    'duration' => $batch->duration,
                                    'no_of_participants' => $batch->no_of_participants,
                                    'no_of_sessions' => $batch->no_of_sessions,
                                    'payment_type' => $batch->payment_type,
                                    'subscription_fee' => $batch->subscription_fee,
                                    'subscribers' => count($batch->payments()),
                                ];
                            }
                            $response['data'][] = [
                                'id' => $package->id,
                                'tutor' => $package->user->name,
                                'tutor_id' => $package->user_id,
                                // 'batch' => $package->batches,
                                'batch' => $batches,
                                'package_name' => $package->package_name,
                                'description' => $package->description,
                                // 'subscription_fee' => $package->subscription_fee,
                            ];
                        }

                        return response()->json($response, 200);
                    } else {
                        $response['status'] = "true";
                        $response['data'] = [];
                        $response['message'] = "No package found";
                        return response()->json($response, 200);
                        // return response()->json(['errors' => 'No package found'], 400);
                    }
                } else {

                    return response()->json(['errors' => 'No user found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                // dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                // dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    public function getPackageById(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'id' => 'numeric',
            'token' => 'required_without:id|exists:users,token|bail',
        ]);

        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
        } else {
            $token = $request->Input('token');
            try {
                if (!empty($request->input('token'))) {
                    $user = User::where('token', $token)->first();
                }
                if ($user) {
                    $batches = [];
                    $package = Package::findOrFail($request->id);
                    if ($package) {
                        foreach ($package->batches as $batch) {
                            $batches[] = [
                                'batch_id' => $batch->id,
                                'batch_name' => $batch->batch_name,
                                'tutor' => $batch->user->name,
                                'tutor_id' => $batch->user_id,
                                'package_id' => $batch->package_id,
                                'description' => $batch->description,
                                'start' => $batch->start,
                                'end' => $batch->end,
                                'duration' => $batch->duration,
                                'no_of_participants' => $batch->no_of_participants,
                                'no_of_sessions' => $batch->no_of_sessions,
                                'payment_type' => $batch->payment_type,
                                'subscription_fee' => $batch->subscription_fee,
                                'subscribers' => count($batch->payments()),
                            ];
                        }
                        if ($user->role == 'student' && $batches) {
                            usort($batches, 'batch_date_compare');
                            $batche_key = get_upcoming_date_batch($batches);
                            if (isset($batche_key) && $batche_key != '') {
                                $batches_data = array_splice($batches, $batche_key, count($batches) - 1);
                                $batches = $batches_data;
                            }

                        }
                        $response['status'] = "true";
                        $response['data'] = [
                            'id' => $package->id,
                            'tutor' => $package->user->name,
                            'tutor_id' => $package->user_id,
                            'batch' => $package->batches,
                            'batch' => $batches,
                            'package_name' => $package->package_name,
                            'description' => $package->description,
                            // 'subscription_fee' => $package->subscription_fee,
                        ];

                        return response()->json($response, 200);
                    } else {
                        $response['status'] = "true";
                        $response['data'] = [];
                        $response['message'] = "No package found";
                        return response()->json($response, 200);
                        // return response()->json(['errors' => 'No package found'], 400);
                    }
                } else {

                    return response()->json(['errors' => 'No user found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    public function getPackagesByTutor(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'tutor_id' => 'required|exists:users,id',
            'token' => 'required_without:id|exists:users,token|bail',
        ]);

        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
        } else {
            $token = $request->Input('token');
            try {
                if (!empty($request->input('token'))) {
                    $user = User::where('token', $token)->first();
                }
                if ($user) {
                    $batches = [];
                    $packages = Package::where('user_id', $request->tutor_id)->with('batches')->get();

                    if ($packages && count($packages) > 0) {
                        $response['status'] = "true";
                        foreach ($packages as $package) {
                            $batches = [];
                            foreach ($package->batches as $batch) {

                                if ($user->role == 'student' && strtotime($batch->start) < strtotime(date("Y-m-d")) && strtotime($batch->end) < strtotime(date("Y-m-d"))) {
                                    continue;
                                }

                                $batches[] = [
                                    'batch_id' => $batch->id,
                                    'batch_name' => $batch->batch_name,
                                    'tutor' => $batch->user->name,
                                    'tutor_id' => $batch->user_id,
                                    'package_id' => $batch->package_id,
                                    'description' => $batch->description,
                                    'start' => $batch->start,
                                    'end' => $batch->end,
                                    'duration' => $batch->duration,
                                    'no_of_participants' => $batch->no_of_participants,
                                    'no_of_sessions' => $batch->no_of_sessions,
                                    'payment_type' => $batch->payment_type,
                                    'subscription_fee' => $batch->subscription_fee,
                                    'subscribers' => count($batch->payments()),
                                ];
                            }
                            $response['data'][] = [
                                'id' => $package->id,
                                'tutor' => $package->user->name,
                                'tutor_id' => $package->user_id,
                                // 'batch' => $package->batches,
                                'batch' => $batches,
                                'package_name' => $package->package_name,
                                'description' => $package->description,
                                // 'subscription_fee' => $package->subscription_fee,
                            ];
                        }

                        return response()->json($response, 200);
                    } else {
                        $response['status'] = "true";
                        $response['data'] = [];
                        $response['message'] = "No package found";
                        return response()->json($response, 200);
                        // return response()->json(['errors' => 'No package found'], 400);
                    }
                } else {

                    return response()->json(['errors' => 'No user found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                // dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                // dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    ///////////////
    public function createBatch(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
            //            return response()->json(['errors' => $validatedData->errors()->first('token')], 400);
        } else {
            $batch = new Batch();
            $validator = Validator::make($request->all(), [
                // 'package_id' => 'required',
                'batch_name' => 'required',
                'description' => '',
                'start' => 'required',
                'end' => 'required',
                'duration' => 'required',
                'no_of_participants' => 'required',
                'no_of_sessions' => 'required',
                'payment_type' => 'required',
                'subscription_fee' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->first()], 401);
            }
            try {

                $batch->package_id = $request->package_id;
                $batch->batch_name = $request->batch_name;
                $batch->description = $request->description;
                $batch->start = $request->start;
                $batch->end = $request->end;
                $batch->duration = $request->duration;
                $batch->no_of_participants = $request->no_of_participants;
                $batch->no_of_sessions = $request->no_of_sessions;
                $batch->payment_type = $request->payment_type;
                $batch->subscription_fee = $request->subscription_fee;

                $user = User::where("token", $request->token)->first();
                $batch->user_id = $user->id;
                if ($batch->save()) {
                    if ($request->package_id) {
                        $packageBatch = new PackageBatch();
                        $packageBatch->package_id = $request->package_id;
                        $packageBatch->batch_id = $batch->id;
                        $packageBatch->save();
                    }

                    $response['status'] = "true";
                    $response['data'] = [
                        'batch_id' => $batch->id,
                        'batch_name' => $batch->batch_name,
                        'tutor' => $batch->user->name,
                        'tutor_id' => $batch->user_id,
                        'package_id' => $batch->package_id,
                        'description' => $batch->description,
                        'start' => $batch->start,
                        'end' => $batch->end,
                        'duration' => $batch->duration,
                        'no_of_participants' => $batch->no_of_participants,
                        'no_of_sessions' => $batch->no_of_sessions,
                        'payment_type' => $batch->payment_type,
                        'subscription_fee' => $batch->subscription_fee,
                        'subscribers' => count($batch->payments()),
                    ];
                    return response()->json($response, 200);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error saving data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error saving data';
                return response()->json($response, 400);
            }
        }
    }

    public function updateBatch(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
            //            return response()->json(['errors' => $validatedData->errors()->first('token')], 400);
        } else {
            $batch = Batch::where(['id' => $request->id])->first();

            $validator = Validator::make($request->all(), [
                // 'package_id' => 'required',
                'batch_name' => 'required',
                'description' => '',
                'start' => 'required',
                'end' => 'required',
                'duration' => 'required',
                'no_of_participants' => 'required',
                'no_of_sessions' => 'required',
                'payment_type' => 'required',
                'subscription_fee' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->first()], 401);
            }
            try {
                $token = $request->Input('token');
                $user = User::where('token', $token)->first();
                if ($batch->user_id == $user->id) {

                    $batch->package_id = $request->package_id;
                    $batch->batch_name = $request->batch_name;
                    $batch->description = $request->description;
                    $batch->start = $request->start;
                    $batch->end = $request->end;
                    $batch->duration = $request->duration;
                    $batch->no_of_participants = $request->no_of_participants;
                    $batch->no_of_sessions = $request->no_of_sessions;
                    $batch->payment_type = $request->payment_type;
                    $batch->subscription_fee = $request->subscription_fee;

                    $user = User::where("token", $request->token)->first();
                    $batch->user_id = $user->id;
                    if ($batch->save()) {
                        $response['status'] = "true";
                        $response['data'] = [
                            'batch_id' => $batch->id,
                            'batch_name' => $batch->batch_name,
                            'tutor' => $batch->user->name,
                            'tutor_id' => $batch->user_id,
                            'package_id' => $batch->package_id,
                            'description' => $batch->description,
                            'start' => $batch->start,
                            'end' => $batch->end,
                            'duration' => $batch->duration,
                            'no_of_participants' => $batch->no_of_participants,
                            'no_of_sessions' => $batch->no_of_sessions,
                            'payment_type' => $batch->payment_type,
                            'subscription_fee' => $batch->subscription_fee,
                            'subscribers' => count($batch->payments()),
                        ];
                        return response()->json($response, 200);
                    }
                } else {
                    $response['errors'] = 'Batch not found for this user';
                    return response()->json($response, 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    public function deleteBatch(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
        } else {
            try {
                $token = $request->Input('token');
                $user = User::where('token', $token)->first();
                if ($user) {
                    $batch = Batch::findOrFail($request->id);
                    if ($batch->user_id == $user->id) {
                        $packageBatch = PackageBatch::where('batch_id', $request->id)->delete();
                        $sessions = TutorSession::where('batch_id', $request->id)->delete();
                        if ($batch->delete()) {
                            $response['status'] = "true";
                            $response['data'] = ["batch deleted successfully"];
                            return response()->json($response, 200);
                        } else {
                            $response['errors'] = 'error deleting batch data';
                            return response()->json($response, 400);
                        }
                    } else {
                        $response['errors'] = 'batch not found for this user';
                        return response()->json($response, 400);
                    }
                } else {
                    $response['errors'] = 'error deleting batch data';
                    return response()->json($response, 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error deleting batch data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting batch data';
                return response()->json($response, 400);
            }
        }
    }

    public function getBatches(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        // $token = $request->bearerToken();
        // $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required_without:id|exists:users,token|bail',
        ]);

        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
        } else {
            $token = $request->Input('token');
            try {
                if (!empty($request->input('token'))) {
                    $user = User::where('token', $token)->first();
                }
                if ($user) {
                    $batches = Batch::where('user_id', $user->id)->where('start', '>', date('Y-m-d'))->orderBy('start', 'DESC')->get();

                    if ($batches && count($batches) > 0) {
                        $response['status'] = "true";
                        foreach ($batches as $batch) {

                            $response['data'][] = [
                                'batch_id' => $batch->id,
                                'batch_name' => $batch->batch_name,
                                'tutor' => $batch->user->name,
                                'tutor_id' => $batch->user_id,
                                'package_id' => $batch->package_id,
                                'description' => $batch->description,
                                'start' => $batch->start,
                                'end' => $batch->end,
                                'duration' => $batch->duration,
                                'no_of_participants' => $batch->no_of_participants,
                                'no_of_sessions' => $batch->no_of_sessions,
                                'payment_type' => $batch->payment_type,
                                'subscription_fee' => $batch->subscription_fee,
                                'subscribers' => count($batch->payments()),
                            ];
                        }

                        return response()->json($response, 200);
                    } else {
                        $response['status'] = "true";
                        $response['data'] = [];
                        $response['message'] = "No batch found";
                        return response()->json($response, 200);
                        // return response()->json(['errors' => 'No batche found'], 400);
                    }
                } else {
                    return response()->json(['errors' => 'No user found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                // dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                // dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    public function getBatchesByTutor(Request $request)
    {

        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        // $token = $request->bearerToken();
        // $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'tutor_id' => 'required|exists:users,id',
            'token' => 'required_without:id|exists:users,token|bail',
        ]);

        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
        } else {
            $token = $request->Input('token');
            try {
                if (!empty($request->input('token'))) {
                    $user = User::where('token', $token)->first();
                }
                if ($user) {
                    $batches = Batch::where('user_id', $request->tutor_id)->orderBy('start', 'DESC')->get();

                    if ($batches && count($batches) > 0) {
                        $response['status'] = "true";
                        foreach ($batches as $batch) {

                            $response['data'][] = [
                                'batch_id' => $batch->id,
                                'batch_name' => $batch->batch_name,
                                'tutor' => $batch->user->name,
                                'tutor_id' => $batch->user_id,
                                'package_id' => $batch->package_id,
                                'description' => $batch->description,
                                'start' => $batch->start,
                                'end' => $batch->end,
                                'duration' => $batch->duration,
                                'no_of_participants' => $batch->no_of_participants,
                                'no_of_sessions' => $batch->no_of_sessions,
                                'payment_type' => $batch->payment_type,
                                'subscription_fee' => $batch->subscription_fee,
                                'subscribers' => count($batch->payments()),
                            ];
                        }

                        return response()->json($response, 200);
                    } else {
                        $response['status'] = "true";
                        $response['data'] = [];
                        $response['message'] = "No batch found";
                        return response()->json($response, 200);
                        // return response()->json(['errors' => 'No batche found'], 400);
                    }
                } else {
                    return response()->json(['errors' => 'No user found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                // dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                // dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    public function getBatchById(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'id' => 'numeric',
            'token' => 'required_without:id|exists:users,token|bail',
        ]);

        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
        } else {
            $token = $request->Input('token');
            try {
                if (!empty($request->input('token'))) {
                    $user = User::where('token', $token)->first();
                }
                if ($user) {
                    $batch = Batch::findOrFail($request->id);

                    $batches = Payment::where(['payments.user_id' => $user->id, 'batch_id' => $batch->id])
                        ->join('packages', 'payments.package_id', '=', 'packages.id')
                        ->join('batches', 'payments.batch_id', '=', 'batches.id')
                        ->first();

                    if ($batch) {
                        $subscribers = [];
                        if ($batch->subscribers) {
                            foreach ($batch->subscribers as $sub) {
                                $subscribers[] = [
                                    'user_id' => $sub->user->id,
                                    'name' => $sub->user->name,
                                    'email' => $sub->user->email,
                                    'phone_number' => $sub->user->phone_number,
                                    'profile_image' => $sub->user->profile_image != "" ? url('/' . $sub->user->profile_image) : "",
                                ];
                            }
                        }

                        $sessions = [];
                        if ($batch->sessions) {
                            foreach ($batch->sessions as $ses) {
                                $sessions[] = [
                                    'id' => $ses->id,
                                    'batch_id' => $ses->batch_id,
                                    'category_id' => $ses->category_id,
                                    'grade_id' => $ses->grade_id,
                                    'session_name' => $ses->session_name,
                                    'description' => $ses->description,
                                    'what_learn' => ($ses->what_learn) ? unserialize($ses->what_learn) : $ses->what_learn,
                                    'date' => $ses->date,
                                    'time' => $ses->time,
                                    'meeting_link' => ($user->role == 'student') ? $ses->meeting_link : $ses->start_url,
                                ];
                            }
                        }

                        $response['status'] = "true";
                        $response['data'] = [
                            'batch_id' => $batch->id,
                            'batch_name' => $batch->batch_name,
                            'tutor' => $batch->user->name,
                            'tutor_id' => $batch->user_id,
                            'package_id' => $batch->package_id,
                            'description' => $batch->description,
                            'start' => $batch->start,
                            'end' => $batch->end,
                            'duration' => $batch->duration,
                            'no_of_participants' => $batch->no_of_participants,
                            'no_of_sessions' => $batch->no_of_sessions,
                            'payment_type' => $batch->payment_type,
                            'subscription_fee' => $batch->subscription_fee,
                            'subscribers' => count($batch->payments()),
                            'subscribers_list' => $subscribers,
                            'sessions' => $sessions,
                        ];

                        if ($user->role == 'student') {
                            $subscribers = Subscriber::where(['tutor_id' => $batch->user_id, 'user_id' => $user->id])->first();
                            unset($response['data']['subscribers']);
                            unset($response['data']['subscribers_list']);
                            unset($response['data']['package_id']);
                            $response['data']['package_name'] = ($batches) ? $batches->package_name : '';
                            $response['data']['subscription_status'] = ($batches) ? $batches->payment_status : 0;

                        }

                        return response()->json($response, 200);
                    } else {
                        $response['status'] = "true";
                        $response['data'] = [];
                        $response['message'] = "No batch found";
                        return response()->json($response, 200);
                        // return response()->json(['errors' => 'No package found'], 400);
                    }
                } else {
                    return response()->json(['errors' => 'No user found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                // dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                // dd($e);

                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    /////////
    public function createSession(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
            //            return response()->json(['errors' => $validatedData->errors()->first('token')], 400);
        } else {
            $session = new TutorSession();
            $validator = Validator::make($request->all(), [
                // 'category_id' => 'numeric',
                // 'grade_id' => 'numeric',
                'batch_id' => 'required|numeric',
                'session_name' => 'required',
                'description' => 'required',
                'what_learn' => 'array',
                'date' => 'required',
                'time' => 'required',
                'meeting_link' => '',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->first()], 401);
            }
            $batch = Batch::findOrFail($request->batch_id);
            try {
                $session->user_id = $batch->user_id;
                $session->batch_id = $request->batch_id;
                $session->category_id = $request->category_id;
                $session->grade_id = $request->grade_id;
                $session->session_name = $request->session_name;
                $session->description = $request->description;
                $session->what_learn = serialize($request->what_learn);
                $session->date = $request->date;
                $session->time = $request->time;
                // $session->meeting_link = $request->meeting_link;

                $data = [
                    'topic' => $session->session_name,
                    'start_time' => $session->date . 'T' . $session->time,
                    'agenda' => $session->description,
                ];

                $path = 'users/me/meetings';
                $zoom_response = $this->zoomPost($path, [
                    'topic' => $data['topic'],
                    'type' => self::MEETING_TYPE_SCHEDULE,
                    'start_time' => $this->toZoomTimeFormat($data['start_time']),
                    'duration' => 30,
                    'agenda' => $data['agenda'],
                    'settings' => [
                        'host_video' => false,
                        'participant_video' => false,
                        'waiting_room' => true,
                    ],
                ]);

                $result = json_decode($zoom_response->body(), true);

                if ($zoom_response->status() === 201) {
                    $session->meeting_link = $result['join_url'];
                    $session->start_url = $result['start_url'];
                    $session->meeting_id = $result['id'];
                }

                if ($session->save()) {

                    $bookings = Payment::where('batch_id', $session->batch_id)->get();
                    if ($bookings && count($bookings) > 0) {
                        $batch = Batch::find($session->batch_id);
                        foreach ($bookings as $value) {
                            $user = User::find($value->user_id);

                            $to = $user->email;
                            $meeting_link = $session->meeting_link;
                            $batch_name = $batch->batch_name;
                            $date = $session->date;
                            $time = $session->time;
                            $mail = Mail::send([], [], function ($message) use ($to, $meeting_link, $batch_name, $date, $time) {
                                $message->to($to)
                                    ->subject('Buddy Guru Session Notification')
                                    ->from('buddyguru@cordiace.com', 'Buddy Guru')
                                    ->setBody('A new session created under batch ' . $batch_name . ' on ' . $date . ' at ' . $time . '. Click on the attached meeting link to join ' . $meeting_link, 'text/plain');
                            });
                        }
                    }

                    $response['status'] = "true";
                    $response['data'] = [
                        'id' => $session->id,
                        'batch_id' => $session->batch_id,
                        'category_id' => $session->category_id,
                        'grade_id' => $session->grade_id,
                        'session_name' => $session->session_name,
                        'description' => $session->description,
                        'what_learn' => unserialize($session->what_learn),
                        'date' => $session->date,
                        'time' => $session->time,
                        'meeting_link' => $session->meeting_link,
                    ];
                    return response()->json($response, 200);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error saving data';
                // dd($e);
                return response()->json($response, 400);
            } catch (\Exception$e) {
                // dd($e);
                $response['errors'] = 'error saving data';
                return response()->json($response, 400);
            }
        }
    }

    public function updateSession(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
            //            return response()->json(['errors' => $validatedData->errors()->first('token')], 400);
        } else {
            $session = TutorSession::where(['id' => $request->id])->first();

            $validator = Validator::make($request->all(), [
                // 'category_id' => 'numeric',
                // 'grade_id' => 'numeric',
                'batch_id' => 'required|numeric',
                'session_name' => 'required',
                'description' => 'required',
                'what_learn' => 'array',
                'date' => 'required',
                'time' => 'required',
                'meeting_link' => '',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->first()], 401);
            }
            $batch = Batch::findOrFail($request->batch_id);
            try {
                $session->user_id = $batch->user_id;
                $session->batch_id = $request->batch_id;
                $session->category_id = $request->category_id;
                $session->grade_id = $request->grade_id;
                $session->session_name = $request->session_name;
                $session->description = $request->description;
                $session->what_learn = serialize($request->what_learn);
                $session->date = $request->date;
                $session->time = $request->time;

                $data = [
                    'topic' => $request->session_name,
                    'start_time' => $request->date . 'T' . $request->time,
                    'agenda' => $request->description,
                ];

                $path = 'meetings/' . $session->meeting_id;
                $zoom_response = $this->zoomPatch($path, [
                    'topic' => $data['topic'],
                    'type' => self::MEETING_TYPE_SCHEDULE,
                    'start_time' => (new \DateTime($data['start_time']))->format('Y-m-d\TH:i:s'),
                    'duration' => 30,
                    'agenda' => $data['agenda'],
                    'settings' => [
                        'host_video' => false,
                        'participant_video' => false,
                        'waiting_room' => true,
                    ],
                ]);

                $result = json_decode($zoom_response->body(), true);

                if ($zoom_response->status() === 201) {
                    $session->meeting_link = $result['join_url'];
                    $session->start_url = $result['start_url'];
                }

                if ($session->save()) {
                    $bookings = Payment::where('batch_id', $session->batch_id)->get();
                    if ($bookings && count($bookings) > 0) {
                        $batch = Batch::find($session->batch_id);
                        foreach ($bookings as $value) {
                            $user = User::find($value->user_id);

                            $to = $user->email;
                            $meeting_link = $session->meeting_link;
                            $batch_name = $batch->batch_name;
                            $date = $session->date;
                            $time = $session->time;
                            $mail = Mail::send([], [], function ($message) use ($to, $meeting_link, $batch_name, $date, $time) {
                                $message->to($to)
                                    ->subject('Buddy Guru Session Notification')
                                    ->from('buddyguru@cordiace.com', 'Buddy Guru')
                                    ->setBody('A session details updated under batch ' . $batch_name . ' on ' . $date . ' at ' . $time . '. Click on the attached meeting link to join ' . $meeting_link, 'text/plain');
                            });
                        }
                    }

                    $response['status'] = "true";
                    $response['data'] = [
                        'id' => $session->id,
                        'batch_id' => $session->batch_id,
                        'category_id' => $session->category_id,
                        'grade_id' => $session->grade_id,
                        'session_name' => $session->session_name,
                        'description' => $session->description,
                        'what_learn' => unserialize($session->what_learn),
                        'date' => $session->date,
                        'time' => $session->time,
                        'meeting_link' => $session->meeting_link,
                    ];
                    return response()->json($response, 200);
                }

            } catch (\Illuminate\Database\QueryException$e) {
                // dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                // dd($e);
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    public function deleteSession(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
        } else {
            try {
                $session = TutorSession::findOrFail($request->id);
                $path = 'meetings/' . $session->meeting_id;
                $zoom_response = $this->zoomDelete($path);
                if ($session->delete()) {
                    $response['status'] = "true";
                    $response['data'] = ["session deleted successfully"];
                    return response()->json($response, 200);
                } else {
                    $response['errors'] = 'error deleting session data';
                    return response()->json($response, 400);
                }

            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error deleting session data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting session data';
                return response()->json($response, 400);
            }
        }
    }

    public function getSessions(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required_without:id|exists:users,token|bail',
        ]);

        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
        } else {
            $token = $request->Input('token');
            try {
                if (!empty($request->input('token'))) {
                    $user = User::where('token', $token)->first();
                }
                if ($user) {
                    $sessions = TutorSession::where('user_id', $user->id)->get();
                    if ($sessions && count($sessions) > 0) {
                        $response['status'] = "true";
                        foreach ($sessions as $session) {
                            $response['data'][] = [
                                'id' => $session->id,
                                'batch_id' => $session->batch_id,
                                'category_id' => $session->category_id,
                                'grade_id' => $session->grade_id,
                                'session_name' => $session->session_name,
                                'description' => $session->description,
                                'what_learn' => unserialize($session->what_learn),
                                'date' => $session->date,
                                'time' => $session->time,
                                'meeting_link' => $session->meeting_link,
                            ];
                        }

                        return response()->json($response, 200);
                    } else {
                        $response['status'] = "true";
                        $response['data'] = [];
                        $response['message'] = "No session found";
                        return response()->json($response, 200);
                        // return response()->json(['errors' => 'No batche found'], 400);
                    }
                } else {
                    return response()->json(['errors' => 'No user found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    public function getAllSessions(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required_without:id|exists:users,token|bail',
        ]);

        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
        } else {
            $token = $request->Input('token');
            try {
                if (!empty($request->input('token'))) {
                    $user = User::where('token', $token)->first();
                }
                if ($user) {
                    $payments = Payment::where(['payments.user_id' => $user->id])->get();
                    $sessions = [];
                    if ($payments) {
                        foreach ($payments as $payment) {
                            $batch = $payment->batch;

                            if ($batch->sessions) {
                                foreach ($batch->sessions as $ses) {
                                    $category = Category::where('id', $batch->user->tutor_detail->category)->first();
                                    $sessions[] = [
                                        'id' => $ses->id,
                                        'batch_id' => $ses->batch_id,
                                        // 'category_id' => $ses->category_id,
                                        // 'grade_id' => $ses->grade_id,
                                        'session_name' => $ses->session_name,
                                        'description' => $ses->description,
                                        'what_learn' => ($ses->what_learn) ? unserialize($ses->what_learn) : $ses->what_learn,
                                        'date_time' => $ses->date . 'T' . $ses->time . 'Z',
                                        'meeting_link' => $ses->meeting_link,
                                        'tutor_name' => $batch->user->name,
                                        'category' => [
                                            'category_id' => $category->id,
                                            'category' => $category->category_name,
                                            'image' => url('/' . $category->image),
                                        ],
                                        'profile_image' => $batch->user->profile_image != "" ? url('/' . $batch->user->profile_image) : "",
                                        'rating' => $batch->user->tutor_detail->rating,
                                    ];
                                }
                            }
                        }

                        $response['status'] = "true";
                        $response['data'] = $sessions;

                        return response()->json($response, 200);
                    } else {
                        $response['status'] = "true";
                        $response['data'] = [];
                        $response['message'] = "No sessions found";
                        return response()->json($response, 200);
                    }

                } else {
                    return response()->json(['errors' => 'No user found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    public function getAllUpcomingSessions(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required_without:id|exists:users,token|bail',
        ]);

        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
        } else {
            $token = $request->Input('token');
            try {
                if (!empty($request->input('token'))) {
                    $user = User::where('token', $token)->first();
                }
                if ($user) {
                    $payments = Payment::where(['payments.user_id' => $user->id])->get();
                    $sessions = [];
                    if ($payments) {
                        foreach ($payments as $payment) {
                            $batch = $payment->batch;

                            $upcomingSessions = $batch->upcomingSession();
                            if ($upcomingSessions) {
                                foreach ($upcomingSessions as $ses) {
                                    $category = Category::where('id', $batch->user->tutor_detail->category)->first();
                                    $sessions[] = [
                                        'id' => $ses->id,
                                        'batch_id' => $ses->batch_id,
                                        // 'category_id' => $ses->category_id,
                                        // 'grade_id' => $ses->grade_id,
                                        'session_name' => $ses->session_name,
                                        'description' => $ses->description,
                                        'what_learn' => ($ses->what_learn) ? unserialize($ses->what_learn) : $ses->what_learn,
                                        'date_time' => $ses->date . 'T' . $ses->time . 'Z',
                                        'meeting_link' => $ses->meeting_link,
                                        'tutor_name' => $batch->user->name,
                                        'category' => [
                                            'category_id' => $category->id,
                                            'category' => $category->category_name,
                                            'image' => url('/' . $category->image),
                                        ],
                                        'profile_image' => $batch->user->profile_image != "" ? url('/' . $batch->user->profile_image) : "",
                                        'rating' => $batch->user->tutor_detail->rating,
                                    ];
                                }
                            }
                        }

                        $response['status'] = "true";
                        $response['data'] = $sessions;

                        return response()->json($response, 200);
                    } else {
                        $response['status'] = "true";
                        $response['data'] = [];
                        $response['message'] = "No sessions found";
                        return response()->json($response, 200);
                    }

                } else {
                    return response()->json(['errors' => 'No user found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    public function getSessionById(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'id' => 'numeric',
            'token' => 'required_without:id|exists:users,token|bail',
        ]);

        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
        } else {
            $token = $request->Input('token');
            try {
                if (!empty($request->input('token'))) {
                    $user = User::where('token', $token)->first();
                }
                if ($user) {
                    $session = TutorSession::findOrFail($request->id);
                    if ($session) {
                        $response['status'] = "true";
                        $response['data'] = [
                            'id' => $session->id,
                            'batch_id' => $session->batch_id,
                            'category_id' => $session->category_id,
                            'grade_id' => $session->grade_id,
                            'session_name' => $session->session_name,
                            'description' => $session->description,
                            'what_learn' => unserialize($session->what_learn),
                            'date' => $session->date,
                            'time' => $session->time,
                            'meeting_link' => $session->meeting_link,
                        ];

                        return response()->json($response, 200);
                    } else {
                        $response['status'] = "true";
                        $response['data'] = [];
                        $response['message'] = "No session found";
                        return response()->json($response, 200);
                        // return response()->json(['errors' => 'No package found'], 400);
                    }
                } else {
                    return response()->json(['errors' => 'No user found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    public function getSessionByBatch(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'batch_id' => 'numeric',
            'token' => 'required_without:id|exists:users,token|bail',
        ]);

        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
        } else {
            $token = $request->Input('token');
            try {
                if (!empty($request->input('token'))) {
                    $user = User::where('token', $token)->first();
                }
                if ($user) {
                    $sessions = TutorSession::where('batch_id', $request->batch_id)->get();
                    if ($sessions && count($sessions) > 0) {
                        $response['status'] = "true";
                        foreach ($sessions as $session) {

                            $response['data'][] = [
                                'id' => $session->id,
                                'batch_id' => $session->batch_id,
                                'category_id' => $session->category_id,
                                'grade_id' => $session->grade_id,
                                'session_name' => $session->session_name,
                                'description' => $session->description,
                                'what_learn' => unserialize($session->what_learn),
                                'date' => $session->date,
                                'time' => $session->time,
                                'meeting_link' => $session->meeting_link,
                            ];
                        }

                        return response()->json($response, 200);
                    } else {
                        $response['status'] = "true";
                        $response['data'] = [];
                        $response['message'] = "No session found";
                        return response()->json($response, 200);
                        // return response()->json(['errors' => 'No sessions found'], 400);
                    }
                } else {
                    return response()->json(['errors' => 'No user found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {

                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    public function createReview(Request $request)
    {

        $validatedData = Validator::make($request->all(), [
            'guru_id' => 'required',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidguruId'], 400);
            //            return response()->json(['errors' => $validatedData->errors()->first('token')], 400);
        } else {
            $rsession = new Review();

            try {
                $rsession->guru_id = $request->guru_id;
                $rsession->title = $request->title;
                $rsession->message = $request->message;
                if ($rsession->save()) {
                    $response['status'] = "true";
                    $response['data'] = [
                        'title' => $rsession->title,
                        'message' => $rsession->message,
                    ];
                    return response()->json($response, 200);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error saving data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error saving data';
                return response()->json($response, 400);
            }
        }
    }
    public function getBanners(Request $request)
    {
        $token = $request->bearerToken();
        $request->request->add(['token' => $token]);

        $validatedData = Validator::make($request->all(), [
            'token' => 'required_without:id|exists:users,token|bail',
        ]);

        if ($validatedData->fails()) {
            return response()->json(['errors' => 'InvalidToken'], 403);
        } else {
            $token = $request->Input('token');
            try {
                if (!empty($request->input('token'))) {
                    $user = User::where('token', $token)->first();
                }
                if ($user) {
                    $banners = Banner::all();
                    if ($banners && count($banners) > 0) {
                        $response['status'] = "true";
                        foreach ($banners as $banner) {
                            $response['data'][] = [
                                'id' => $banner->id,
                                'location' => $banner->location,
                                'image' => $banner->image,
                                'heading' => $banner->heading,
                                'description' => $banner->description,
                            ];
                        }

                        return response()->json($response, 200);
                    } else {
                        $response['status'] = "true";
                        $response['data'] = [];
                        $response['message'] = "No banner found";
                        return response()->json($response, 200);
                        // return response()->json(['errors' => 'No banner found'], 400);
                    }
                } else {
                    return response()->json(['errors' => 'No user found'], 400);
                }
            } catch (\Illuminate\Database\QueryException$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            } catch (\Exception$e) {
                $response['errors'] = 'error getting data';
                return response()->json($response, 400);
            }
        }
    }

    public function createMeeting(Request $request)
    {

        $this->generateToken();

    }

    // Get access token
    public function get_access_token()
    {
        $token = Token::first();
        return json_decode($token->access_token);
    }

    // Update access token
    public function update_access_token($token)
    {
        $token = Token::where('access_token', $token)->first();

        if ($token) {
            $token->access_token = $token;
            $token->save();
        } else {
            $token = new Token();
            $token->access_token = $token;
            $token->save();
        }
    }

    public function generateToken()
    {
        try {
            $client = new Client();
            $response = $client->post('https://zoom.us//oauth/token', [
                "headers" => [
                    "Authorization" => "Basic " . base64_encode(env('ZOOM_CLIENT_ID') . ':' . env('ZOOM_CLIENT_SECRET')),
                ],
                'form_params' => [
                    "grant_type" => "authorization_code",
                    // "code" => $_GET['code'],
                    "redirect_uri" => env('ZOOM_REDIRECT_URI'),
                ],
            ]);

            $result = $response->getBody()->getContents();

            dd($result);

            // $response = $client->request('POST', '/oauth/token', [
            //     "headers" => [
            //         "Authorization" => "Basic ". base64_encode(env('ZOOM_CLIENT_ID').':'.env('ZOOM_CLIENT_SECRET'))
            //     ],
            //     'form_params' => [
            //         "grant_type" => "authorization_code",
            //         // "code" => $_GET['code'],
            //         "redirect_uri" => env('ZOOM_REDIRECT_URI')
            //     ],
            // ]);

            dd($response);

            $token = json_decode($response->getBody()->getContents(), true);

            $db = new DB();

            if ($db->is_table_empty()) {
                $db->update_access_token(json_encode($token));
                echo "Access token inserted successfully.";
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

}
