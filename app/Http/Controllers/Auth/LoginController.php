<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

     public function redirectToGoogle()
    {
        return Socialite::driver('google')->stateless()->redirect();
    }

    public function handleGoogleCallback(Request $request)
    {
        $googleUser = Socialite::driver('google')->stateless()->user();

        // Check if the user already exists in the database
        $user = User::where('email', $googleUser->email)->first();

        if (!$user) {
            // Create a new user
            $user = new User();
            $user->name = $googleUser->name;
            $user->email = $googleUser->email;
            // Set any additional fields you want to populate
            $user->save();
        }

        // Return a JSON response with the user details or any other relevant information
        return response()->json([
            'message' => 'Google sign-up successful',
            'user' => $user
        ]);
    }
    public function showLoginForm()
    {
      $pageConfigs = ['myLayout' => 'blank'];
      return view('admin.authentications.auth-login', ['pageConfigs' => $pageConfigs]);
    // return view('auth.login');
    }
     public function login(Request $request)
  {
   
    $validatedData = $request->validate([
        'email' => 'required|email',
        'password' => 'required',
    ]);

    // attempt to authenticate the user
    if (Auth::attempt($validatedData)) {
        // authentication successful
        return redirect()->intended('/dashboard');
    } else {
        // authentication failed
        return redirect()->back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ]);
    }



    //   $credentials = $request->only('email', 'password');

    //   if (Auth::guard('admin')->attempt($credentials)) {
    //      // Authentication passed...
    //     if($request->email =='admin@gmail.com'){
    //       return redirect()->route('welcome');
    //     }
    //     else{
    //       return back()->withErrors(['email' => 'Invalid credentials']);
    //     }
         
         
    //   }

    //   return back()->withErrors(['email' => 'Invalid credentials']);
  }
  public function logout(Request $request)
  {
      Auth::logout();
  
      $request->session()->invalidate();
  
      $request->session()->regenerateToken();
  
      return redirect('/');
  }
}
