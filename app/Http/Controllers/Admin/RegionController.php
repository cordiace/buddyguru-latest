<?php

namespace App\Http\Controllers\Admin;

use App\Models\Region;
use App\Models\Country;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function getRegions(Request $request)
    {
        //$countries=Country::all();
        $regions=Region::orderBy('name')->paginate($request->input('per_page', 10));
        return response([
            'data'=> $regions,
           
           
        ], 201);
    }
    public function index()
    {
        //$countries=Country::all();
        $regions=Region::orderBy('name')->get();
        return view('admin.region.index',\compact('regions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.region.create_edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
        ]);
        $Region =  Region::create([
            'name' => $request->name,

        ]);
        return redirect()->route('cms-region.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $region = Region::findOrFail($id);
        return view('admin.region.create_edit', \compact('region'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
        ]);
        $country =  Region::updateOrCreate(
            ['id' =>$id],
            [ 'name' => $request->name,]
        );
        return redirect()->route('cms-region.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $Region = Region::find($id);
        $Country = Country::where('region_id',$id)->delete();
        $Region->delete();

        return redirect()->route('cms-region.index');
    }
}
