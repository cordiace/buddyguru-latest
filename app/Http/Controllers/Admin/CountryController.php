<?php

namespace App\Http\Controllers\Admin;

use App\Models\Country;
use App\Models\Region;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCountry(Request $request)
    {
        //$countries=Country::all();
        $countries=Country::where('region_id',$request->region_id)->paginate($request->input('per_page', 10));
        return response([
            'data' => $countries,
    
        ], 201);
    }
    public function index()
    {
        //$countries=Country::all();
        $countries=Country::orderBy('country_name')->orderBy('country_name', 'asc')->get();
        return view('admin.country.index',\compact('countries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Region = Region::all();
       
        return view('admin.country.create_edit', \compact('Region'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'region_id' => ['required'],
            'country_name' => ['required', 'string', 'max:255'],
        ]);
        $country =  Country::create([
            'region_id' => $request->region_id,
            'country_name' => $request->country_name,

        ]);
        return redirect()->route('cms-country.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country = Country::findOrFail($id);
        $Region = Region::all();
        return view('admin.country.create_edit', \compact('country','Region'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'region_id' => ['required'],
            'country_name' => ['required', 'string', 'max:255'],
        ]);
        $country =  Country::find($id);
        $country->update([
          
           'region_id' => $request->region_id,
            'country_name' => $request->country_name
        ]);
        return redirect()->route('cms-country.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $countries = Country::find($id);
        $countries->delete();

        return redirect()->route('cms-country.index');
    }
}
