<?php

namespace App\Http\Controllers\Admin;

use App\Models\TrendingVideos;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getVideos(Request $request)
    {
       
        $TrendingVideos=TrendingVideos::paginate($request->input('per_page', 10));
        if($request->video_id){
        $TrendingVideo=TrendingVideos::find($request->video_id);
            if($request->watch){
                $TrendingVideo->update([
'watch_status'=> ($TrendingVideo->watch_status) + ($request->watch)
                ]);
            }
            if($request->like){
                $TrendingVideo->update([
'like_status'=> ($TrendingVideo->watch_status) + ($request->like)
                ]);
            }
            return response([
                'data'=> $TrendingVideo,
                'watch'=>$TrendingVideo->watch_status,
                'like'=>$TrendingVideo->like_status
               
               
               
            ], 201);
        }
        // $watchCount =  $TrendingVideos->watch_status;
        // $likesCount =  $TrendingVideos->like_status;
        return response([
            'data'=> $TrendingVideos,
   
        ], 201);
    }
    public function index()
    {
       
        $TrendingVideos=TrendingVideos::all();
        return view('admin.trendingVideos.index',\compact('TrendingVideos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Users = User::where('role','tutor') ->orderBy('name', 'asc')->get();
       
        return view('admin.trendingVideos.create_edit', \compact('Users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id' => ['required'],
            'video_url' => ['required'],
        ]);

        if ($request->hasFile('video_url')) {
           
            $profile_video = $request->file('video_url');
            $profile_filename =  $profile_video->getClientOriginalName();
            $profile_path_video = Storage::disk('s3')->url('tutor/TrendingVideos/'.$profile_filename);
            $filenameWithoutExtenstion = pathinfo($profile_filename, PATHINFO_FILENAME);
            $profile_thumbnail = $filenameWithoutExtenstion . ".png";

            $profile_path_thumbnail = Storage::disk('s3')->url('tutor/TrendingVideos/'.$profile_thumbnail);
            // $sec = 10;
            $videofile = $request->file('video_url');
            $video_file_name= $videofile->getClientOriginalName();
        //    $result = $this->getImageFromVideo($sec, $path_video, $thumbnail);

        }
        $TrendingVideos =  TrendingVideos::create([
            'user_id' => $request->user_id,
            'video_url' =>  $profile_path_video,
            'thumbnail' =>$profile_path_thumbnail,
            'file_name' =>$video_file_name,

        ]);
        return redirect()->route('cms-videos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Video = TrendingVideos::findOrFail($id);
        $Users = User::where('role','tutor')->get();
        return view('admin.trendingVideos.create_edit', \compact('Video','Users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'user_id' => ['required'],
          
        ]);
        if ($request->video_url) {
            $request->validate([
                'video_url' => 'required|mimes:mp4,jpeg',
            ]);
        }
        $TrendingVideos =  TrendingVideos::find($id);

        if ($request->hasFile('video_url')) {
            if(TrendingVideos::where('id', $id)->exists()){
                $TrendingVideos = TrendingVideos::where('user_id',$TrendingVideos->id)->first();
                if($TrendingVideos->video_url){
                    if(Storage::disk('s3')->exists($TrendingVideos->video_url)){
                        Storage::disk('s3')->delete($TrendingVideos->video_url);
                    }
                }
            }
         
           
    
            $video = $request->file('video_url');
                
            $filename =  $video->getClientOriginalName();
          
            $path_video = Storage::disk('s3')->url('tutor/TrendingVideos/'.$filename);
    
            $filenameWithoutExtenstion = pathinfo($filename, PATHINFO_FILENAME);
            $profile_thumbnail = $filenameWithoutExtenstion . ".png";

            $profile_path_thumbnail = Storage::disk('s3')->url('tutor/TrendingVideos/'.$profile_thumbnail);
            // $sec = 10;
    
           // $result = $this->getImageFromVideo($sec, $path_video, $thumbnail);
    
        }
        if ($request->hasFile('video_url')) {
            $TrendingVideos->update([
                'video_url' => $path_video,
                'video_thumbnail' => $profile_path_thumbnail,
                'file_name' =>$filename,
            ]);
        }
        $TrendingVideos->update([
          
           'user_id' => $request->user_id,
        ]);
        return redirect()->route('cms-videos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $TrendingVideos = TrendingVideos::find($id);
        $TrendingVideos->delete();

        return redirect()->route('cms-videos.index');
    }

    public function storeTrendingVideo(Request $request)
        {
           
            $video = request()->videoUrl;
              
            $filename =  $video->getClientOriginalName();
            $storage_path = $video->storeAs('tutor','TrendingVideos/'.$filename,'s3'); 
    
            $path_video = Storage::cloud()->url($storage_path);
            return response()->json(['path_video'=>$path_video]);    
            // return response()->json(['success'=>'You have successfully upload file.']);
       
        }
}
