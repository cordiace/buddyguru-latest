<?php

namespace App\Http\Controllers\Admin;

use App\Models\AppliedGurus;
use App\Models\User;
use App\Models\Category;
use App\Models\CategorySkills;
use App\Models\AppliedGuruSkills;
use App\Models\TutorDetail;
use App\Models\TutorSkills;
use App\Models\Country;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Publitio\PublitioAPI;
use App\Services\PublitoService;
class AppliedGuruController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $PublitoService;

    public function __construct(PublitoService $publitoService)
    {
        $this->publitoService = $publitoService;
    }

    public function upload(Request $request)
    {
        $file = $request->file('videoUrl');

        $url = $this->publitoService->uploadFile($file);

        // You can store the URL or perform other operations with the data

        return response()->json(['url' => $url]);
    }
     public function appliedGuru(Request $request){
  
        $validator = Validator::make($request->all(), [
            'phone_number' => ['required'],
            'cover_letter' => ['required'],
            'category' => ['required'],
            'skill' => ['required'],
            'bio' => ['required'],
        ],[
            'phone_number.required' => 'The phone number is required.',
            'cover_letter.required' => 'The cover letter is required.',
            'category.required' => 'The category is required.',
            'bio.required' => 'The bio is required.',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'false',
                'errors' => $validator->errors(),
            ], 400);
        }

        if($request->hasFile('cover_letter')) {
            $cover_letter = time().'.'.$request->cover_letter->extension();  
            $letter_name = $request->cover_letter.$request->cover_letter->extension();
            // return $letter_name;
   
        //    $path_letter= $request->cover_letter->store('/uploads/tutor/', ['disk' => 'public']);

           $coverLetter = $request->file('cover_letter');
           $filename =  $coverLetter->getClientOriginalName();
           $storage_path = $coverLetter->storeAs('tutor','appliedguru/'.$filename,'s3');

           $path_letter = Storage::cloud()->url($storage_path);

           $coverletterfile = $request->file('cover_letter');
           $cover_letter_fileName= $coverletterfile->getClientOriginalName();

        }
        if ($request->hasFile('video_url')) {
            // $path_video ="hh";
            // $thumbnail ="uu";
            // $video_file_name ='abc';
            $profile_video = $request->file('video_url');
            $profile_filename =  $profile_video->getClientOriginalName();
            $profile_path_video = Storage::disk('s3')->url('tutor/appliedguru/'.$profile_filename);
            $filenameWithoutExtenstion = pathinfo($profile_filename, PATHINFO_FILENAME);
            $profile_thumbnail = $filenameWithoutExtenstion . ".png";

            $profile_path_thumbnail = Storage::disk('s3')->url('tutor/appliedguru/'.$profile_thumbnail);
            // $sec = 10;
            $videofile = $request->file('video_url');
            $video_file_name= $videofile->getClientOriginalName();
        //    $result = $this->getImageFromVideo($sec, $path_video, $thumbnail);

        }

        $AppliedGuru =  AppliedGurus::create([
            'phone_number' => $request->phone_number,
            'bio' =>  $request->bio,
            'cover_letter' => $path_letter, 
            'video_url'=>$profile_path_video,
            'thumbnail'=> $profile_path_thumbnail    

        ]);
        if($request->skills){
            foreach ($request->skills as $key=>$value){
                $tutor_skills=AppliedGuruSkills::create([
    'tutor_id'=> $AppliedGuru->id,
    'skill_id'=>$value
                ]);
            }
        }
        if($request->country){
            $AppliedGuru->update([
'location'=>$request->country
            ]);
        }

        return response([
            'status'=> "true",
          
            'message' => 'Created successfully'
        ], 201);
     }
    public function index()
    {
       
        $AppliedGurus=AppliedGurus::where('status',0)->get();
        return view('admin.AppliedGuru.index',\compact('AppliedGurus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $category_subset=Category::all();
        $skill_subset=CategorySkills::all();
        return view('admin.AppliedGuru.create_edit', \compact('category_subset','skill_subset'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate([
        //     'phone_number' => ['required'],
        //     'cover_letter' => ['required'],
        //     'category' => ['required'],
        //     'skill' => ['required'],
        //     'bio' => ['required'],
        // ]);

     if($request->hasFile('cover_letter')) {
            $cover_letter = time().'.'.$request->cover_letter->extension();  
            $letter_name = $request->cover_letter.$request->cover_letter->extension();
            // return $letter_name;
   
        //    $path_letter= $request->cover_letter->store('/uploads/tutor/', ['disk' => 'public']);

           $coverLetter = $request->file('cover_letter');
           $filename =  $coverLetter->getClientOriginalName();
           $storage_path = $coverLetter->storeAs('tutor','appliedguru/'.$filename,'s3');

           $path_letter = Storage::cloud()->url($storage_path);

           $coverletterfile = $request->file('cover_letter');
           $cover_letter_fileName= $coverletterfile->getClientOriginalName();

        }
        if ($request->hasFile('video_url')) {
            // $path_video ="hh";
            // $thumbnail ="uu";
            // $video_file_name ='abc';
            $profile_video = $request->file('video_url');
            $profile_filename =  $profile_video->getClientOriginalName();
            $profile_path_video = Storage::disk('s3')->url('tutor/appliedguru/'.$profile_filename);
            $filenameWithoutExtenstion = pathinfo($profile_filename, PATHINFO_FILENAME);
            $profile_thumbnail = $filenameWithoutExtenstion . ".png";

            $profile_path_thumbnail = Storage::disk('s3')->url('tutor/appliedguru/'.$profile_thumbnail);
            // $sec = 10;
            $videofile = $request->file('video_url');
            $video_file_name= $videofile->getClientOriginalName();
        //    $result = $this->getImageFromVideo($sec, $path_video, $thumbnail);

        }

        $AppliedGuru =  AppliedGurus::create([
            'phone_number' => $request->phone_number,
            'bio' =>  $request->bio,
            'cover_letter' => $path_letter, 
            'video_url'=>$profile_path_video,
            'thumbnail'=> $profile_path_thumbnail    

        ]);
        if($request->skills){
            foreach ($request->skills as $key=>$value){
                $tutor_skills=AppliedGuruSkills::create([
    'tutor_id'=> $AppliedGuru->id,
    'skill_id'=>$value
                ]);
            }
        }
        return redirect()->route('cms-applied_gurus.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $AppliedGuru = AppliedGurus::findOrFail($id);
        $category_subset=Category::all();
        $tutorskill =AppliedGuruSkills::where('tutor_id',$id)->pluck('skill_id');
        $tutorcategorys =CategorySkills::whereIn('id',$tutorskill)->pluck('category_id');
        $skill_subset=CategorySkills::all();
        return view('admin.AppliedGuru.edit', \compact('tutorskill','tutorcategorys','AppliedGuru','category_subset','skill_subset'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve(Request $request,$id,$status)
    {
        $AppliedGurus=AppliedGurus::where('id',$id)->first();
        $AppliedGuruSkills=AppliedGuruSkills::where('tutor_id',$id)->get();
        // dd( $AppliedGurus); 
        if($status ==1 ){
            $AppliedGurus->update([
                'status'=>1
                        ]);
                        do {
                            $token_key = Str::random(24); ///api_token' => Str::random(60),use Illuminate\Support\Str;
                        } while (User::where("token", "=", $token_key)->first() instanceof User);
   $user =  User::create([
 'name' => '',
  'commision' => '',
   'description'=>$AppliedGurus->bio,
'slug' => Str::slug($request->slug),
  'dob' =>null,
   'email' =>'' ,
    'phone_number' => $AppliedGurus->phone_number,
 'location' => null,
   'token' => $token_key,
'role' => 'tutor',
'password' => '',
                          
   ]);
   $tutor_detail=TutorDetail::create([
    'user_id'=>$user->id,
    'category'=>2,
    'sub_category'=>3,
    'skills'=>1,
    // 'cover_letter'=>$path_letter,
    // 'cover_letter_fileName'=>$cover_letter_fileName,
    'video_url' => $AppliedGurus->video_url,
    'video_thumbnail' => $AppliedGurus->thumnail,
    'video_file_name' => '',
    // 'about'=>$request->about,
    // 'experience'=>json_encode($experience),
]);
if($AppliedGuruSkills){
    foreach ($AppliedGuruSkills as $AppliedGuruSkill){
        $tutor_skills=TutorSkills::create([
'tutor_id'=> $user->id,
'skill_id'=>$AppliedGuruSkill->skill_id
        ]);
    }
}
   
//    $tutorskill =AppliedGuruSkills::where('tutor_id',$userId)->pluck('skill_id');
  
   return redirect()->route('admin.tutor.edit', $user->id);

        }
        if($status ==2 ){
            $AppliedGurus->update([
                'status'=>2
                        ]);
   return redirect()->route('cms-applied_gurus.index');
        }

       
        dd($id,$status);
    }
    public function update(Request $request, $id)
    {
        
        $AppliedGuru =  AppliedGurus::find($id);
        if($request->hasFile('cover_letter')){
            if(AppliedGurus::where('id', $id)->exists()){
                $AppliedGuru = AppliedGurus::find($id);
                if($AppliedGuru->cover_letter){
                    if(Storage::disk('s3')->exists($AppliedGuru->cover_letter)){
                        Storage::disk('s3')->delete($AppliedGuru->cover_letter);
                    }
                }
            }
            $file = $request->file('cover_letter');
            
            $filename =  $file->getClientOriginalName();
            $storage_path = $file->storeAs('tutor/appliedguru/'.$filename,'s3');
 
            $path_cover_letter = Storage::cloud()->url($storage_path);


        }

        if ($request->hasFile('video_url')) {
            if(AppliedGurus::where('id', $id)->exists()){
                $TutorDetail = AppliedGurus::where('id',$id)->first();
                if($TutorDetail->video_url){
                    if(Storage::disk('s3')->exists($TutorDetail->video_url)){
                        Storage::disk('s3')->delete($TutorDetail->video_url);
                    }
                }
            }
         
            // $path_video = $video->store('/uploads/tutor/', ['disk' => 'public']);
    
            $video = $request->file('video_url');
                
            $filename =  $video->getClientOriginalName();
            // $storage_path = $video->storeAs('tutor/'.$request->user_id,$filename,'s3');
    
            // $path_video = Storage::cloud()->url($storage_path);
            $path_video = Storage::disk('s3')->url('tutor/appliedguru'.$filename);
    
            $filename = str_replace("uploads/tutor/", "", $path_video);
            $filenameWithoutExtenstion = pathinfo($filename, PATHINFO_FILENAME);
            $thumbnail = "uploads/tutor/" . $filenameWithoutExtenstion . ".png";
            $sec = 10;
    
           // $result = $this->getImageFromVideo($sec, $path_video, $thumbnail);
    
        }
       
        if ($request->hasFile('cover_letter')) {
            $AppliedGuru->update([
                'cover_letter' => $path_cover_letter,
                
            ]);
        }

        if ($request->hasFile('video_url')) {
            $AppliedGuru->update([
                'video_url' => $path_video,
                'thumbnail' => $thumbnail
            ]);
        }
        $AppliedGuru->update([
          
            'phone_number' => $request->phone_number,
            'bio' =>  $request->bio,
        ]);

        $tutor_skills=AppliedGuruSkills::where('tutor_id',$AppliedGuru->id)->get();
        if (!empty($tutor_skills)) {
            $tutor_skills=AppliedGuruSkills::where('tutor_id',$AppliedGuru->id)->delete();
      }
     
      if($request->skills){
        foreach ($request->skills as $key=>$value){
            $tutor_skills=AppliedGuruSkills::create([
'tutor_id'=> $AppliedGuru->id,
'skill_id'=>$value
            ]);
        }
    }
        return redirect()->route('cms-applied_gurus.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $AppliedGuru = AppliedGurus::find($id);
        $AppliedGuru->delete();

        return redirect()->route('cms-applied_gurus.index');
    }

    public function storeGuruVideo(Request $request)
        {
           
            $video = request()->videoUrl;
              
            $filename =  $video->getClientOriginalName();
            $storage_path = $video->storeAs('tutor','appliedguru/'.$filename,'s3'); 
    
            $path_video = Storage::cloud()->url($storage_path);
            return response()->json(['path_video'=>$path_video]);    
            // return response()->json(['success'=>'You have successfully upload file.']);
       
        }
}
