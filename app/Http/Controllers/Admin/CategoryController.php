<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;

use App\Models\CategorySkills;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function getCategory(Request $request)
     {
         $Category = Category::paginate($request->input('per_page', 10));
         return response([
             'data'=> $Category,
            
            
         ], 201);
     }
    public function index()
    {
        $categories=Category::all();
        return view('admin.category.index',\compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create_edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'category_name' => ['required', 'string', 'max:255'],
            // 'image' => 'required|mimes:jpeg,bmp,png',
        ]);
        // if ($request->image) {
        //     $category = $request->file('image');
        //     $image = $category->store('/uploads/category', ['disk' => 'public']);
        // }
        $category =  Category::create([
            'category_name' => $request->category_name,
            // 'image' => $image,

        ]);
        return redirect()->route('cms-category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('admin.category.create_edit', \compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'category_name' => ['required', 'string', 'max:255'],
            // 'image' => 'sometimes|mimes:jpeg,bmp,png',
        ]);
        // if ($request->image) {
            // $category = $request->file('image');
            // $image = $category->store('/uploads/category', ['disk' => 'public']);
        // }
        $category =  Category::updateOrCreate(
            ['id' =>$id],
            [ 'category_name' => $request->category_name,
            //   'image' => ($request->image) ? $image : $request->temp_image,
            ]
        );
        return redirect()->route('cms-category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
    public function delete($id)
    {
        $categories = Category::find($id);
        $skillCategory = CategorySkills::where('category_id',$id)->delete();
        try {
          $categories->delete();
        } catch (\Exception $e) {
          return redirect()->route('category.index');
        }


        return redirect()->route('cms-category.index');
    }
}
