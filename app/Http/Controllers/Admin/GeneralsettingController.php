<?php

namespace App\Http\Controllers\Admin;
use DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use App\Models\GeneralSettings\Generalsetting;
use App\Models\GeneralSettings\CurrencySettings;
use App\Models\GeneralSettings\CurrencyRangeTier;
use App\Models\GeneralSettings\TierManagement;
use App\Models\GeneralSettings\Tier;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Pion\Laravel\ChunkUpload\Exceptions\UploadMissingFileException;
use Pion\Laravel\ChunkUpload\Handler\AbstractHandler;
use Pion\Laravel\ChunkUpload\Handler\HandlerFactory;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;
use Illuminate\Support\Facades\Storage;

class GeneralsettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function getCurrencyRange(Request $request){
        $CurrencyRangeTiers=CurrencyRangeTier::paginate($request->input('per_page', 10));
        return response([
            'status'=>"true",
            'data'=> $CurrencyRangeTiers,
  
           
        ], 201);
     }
    
    public function getCurrency(Request $request){
        $convertedAmounts =array();
        $CurrencySettings = Currencysettings::paginate($request->input('per_page', 10));
        if(!empty($CurrencySettings)){
            foreach($CurrencySettings as $key=>$value) {
                // $amount = ($value->value)?($value->value):(1);
    
                $apikey = 'eqmDldTFJYCWKxVTp92fvlgehAPGenxy';
          
                $from_Currency = 'USD';
                $to_Currency = strtoupper($value->symbol);
               
               $amount = $value->value;
               $url = 'https://api.exchangerate-api.com/v4/latest/USD';
        $json = file_get_contents($url);
        $exp = json_decode($json);
    
        $convert = $exp->rates->$to_Currency;
    
        $convertedAmounts[$key]= ($convert * $amount);
        }

        return response([
            'status'=>"true",
            'data'=> $CurrencySettings,
            'rate'=> $convertedAmounts
           
           
        ], 201);
    }
    }
     public function getTier(Request $request)
     {
         $TierManagements=TierManagement::paginate($request->input('per_page', 10));
         return response([
            'data'=> $TierManagements,
           
           
        ], 201);
     }
    public function index()
    {
        // $banners=Generalsetting::all();
        // return view('admin.generalsetting.index',\compact('banners'));
        $id=1;  
        $banner = Generalsetting::find($id);
        if($banner){
            return view('admin.generalsetting.create_edit', \compact('banner'));
        }
        else{
            return view('admin.generalsetting.create_edit');
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.generalsetting.create_edit');
    }
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'commision' => ['required', 'numeric', 'max:99'],
        ]);
       
        $banner =  Generalsetting::create([
            'commision' => $request->commision,
          

        ]);
        return redirect()->route('general-generalsetting.index');
    }
    public function edit($id)
    {   
        $id=1;
        
        $banner = Generalsetting::findOrFail($id);
        return view('admin.generalsetting.create_edit', \compact('banner'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'commision' => ['required', 'numeric', 'max:99'],
           
        ]);
       
        $banner =  Generalsetting::updateOrCreate(
            ['id' =>$id],
            [ 
             'commision' => $request->commision,

            ]
        );
        return redirect()->route('general-generalsetting.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categories = Generalsetting::find($id);
        $categories->delete();

        return redirect()->route('general-generalsetting.index');
    }

    public function viewCurrency()
    {
        // $id=1;  
        // $currencys['symbol'] = [];
        // $currencys['value'] = [];
        
        $convertedAmounts =array();
        $CurrencySettings = Currencysettings::all();
        if(!empty($CurrencySettings)){
            foreach($CurrencySettings as $key=>$value) {
                // $amount = ($value->value)?($value->value):(1);
    
                $apikey = 'eqmDldTFJYCWKxVTp92fvlgehAPGenxy';
          
                $from_Currency = 'USD';
                $to_Currency = strtoupper($value->symbol);
               
               $amount = $value->value;
               $url = 'https://api.exchangerate-api.com/v4/latest/USD';
        $json = file_get_contents($url);
        $exp = json_decode($json);
    
        $convert = $exp->rates->$to_Currency;
    
        $convertedAmounts[$key]= ($convert * $amount);
        }
      
          
            // change to the free URL if you're using the free version
            // $json = file_get_contents("http://free.currencyconverterapi.com/api/v5/convert?q={$query}&amp;compact=y&amp;apiKey={$apikey}");
    //   $url = 'https://data.fixer.io/api/' + 'convert' + '?access_key=' +  $apikey +'&amp;from=' + $from_Currency + '&amp;to=' +  $to_Currency + '&amp;amount=' + $amount

    //     return $url; 
      
    //         $formatValue = number_format($total, 2, '.', '');
    //   $convertedAmounts->push($formatValue);
            // $data = "$amount $from_Currency = $to_Currency $formatValue";
        }
        // dd($convertedAmounts);
        // $url = 'https://api.exchangerate-api.com/v4/latest/USD';
        // $json = file_get_contents($url);
        // $exp = json_decode($json);
    
        // $convert = $exp->rates->USD;
    
        // $convertedAmount =  $convert * 2;
        
            return view('admin.generalsetting.currencysetting.index', \compact('CurrencySettings','convertedAmounts'));
      
    }
    public function currencyStore()
    {
      
    }
    public function currencyUpdate(Request $request)
    {
      
       if($request->commision){
        foreach($request->commision as $key=>$value) {
if($value ==''){
    
    $value = 0;
}
            $CurrencySetting = Currencysettings::find($key);
            $CurrencySetting->update([
'value'=>$value,
'update_date' => date('Y-m-d H:i:s')
            ]);
        }
       }
      
       return redirect()->route('general-admin.settings.viewCurrency');
    }
    public function createCurrency()
    {
        // $TierManagements=TierManagement::find($id);
       
        return view('admin.generalsetting.currencysetting.create');
    }
    public function storeCurrency(Request $request)
    {
        $messages = [
            'currency.in' => 'The  currency code is not valid.',
            'currency.required' => 'The  currency  is required.',
            'currency.unique' => 'The  currency  is already enterd.',
            'price.required' => 'The  price  is required.',
           
        ];
        $currencyCode = strtoupper($request->input('currency'));

        $validatedData = $request->validate([
            'currency' => [
                'required',
                Rule::in(['USD', 'EUR', 'GBP', 'AUD', 'CAD', 'JPY', $currencyCode]),
                Rule::unique('currency_settings', 'symbol')->where(function ($query) use ($currencyCode) {
                    return $query->where('symbol', $currencyCode);
                }),
                function ($attribute, $value, $fail) use ($currencyCode) {
                    if ($value !== $currencyCode) {
                        $fail("The $attribute field must be in uppercase.");
                    }
                }
               
            ],
            'price' => 'required|numeric',
        ], $messages);
        
        // $valid_currencies = array(
        //     'USD', 'EUR', 'GBP', 'AUD', 'CAD', 'JPY'
        // );
        
        $currency_code = $request->currency;
        // if (in_array($currency_code, $valid_currencies)) {
        $getCurrency =  Currencysettings::create([
            'symbol' => $request->currency,
            'value' => $request->price,
            // 'inr' => $request->currency_inr,
            // 'aed' => $request->currency_aed,
            // 'pound' => $request->currency_pound,
            // 'aud' => $request->currency_aud,
            'update_date' => date('Y-m-d H:i:s'),
        ]);
        return redirect()->route('general-admin.settings.viewCurrency');
    // } else {
    //     // return an error message to the user
    //     return response()->json(['error' => 'Invalid currency code']);
    // }
    }
    public function editCurrency($id)
    {
        
        $Currencysetting = Currencysettings::findOrFail($id);
       
        return view('general-admin.generalsetting.currencysetting.edit', \compact('Currencysetting'));
    }
    public function updateCurrency(Request $request, $id)
    {
        $request->validate([
            'currency' => 'required|max:1000|unique:currency_settings,symbol',
            'price' => ['required', 'numeric', 'max:1000'],
            // 'currency_inr' => ['required', 'numeric', 'max:1000'],
            // 'currency_aed' => ['required', 'numeric', 'max:1000'],
            // 'currency_pound' => ['required', 'numeric', 'max:1000'],
            // 'currency_aud' => ['required', 'numeric', 'max:1000'],
        ]);
        $getCurrency =  Currencysettings::find($id);
       
        $getCurrency->update(
           [ 'symbol' => $request->currency,
            'value' => $request->price,
            'update_date' => date('Y-m-d H:i:s'),
            ]
        );
        return redirect()->route('general-admin.settings.viewCurrency');
    }

    public function deleteCurrency($id){
        $Currencysettings=Currencysettings::find($id);
        $Currencysettings->delete();
        return redirect()->back();
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    


    public function get_tier()
    {
        $TierManagements=TierManagement::all();
        return view('admin.tier.index',\compact('TierManagements'));
    }
    public function createTier()
    {
        // $TierManagements=TierManagement::find($id);
        $Tiers = Tier::all();
        $userClasses = DB::table('classes')->get();
        $currency = CurrencySettings::all();
        return view('admin.tier.create_edit',\compact('Tiers','currency'));
    }
    public function storeTier(Request $request)
    {
        // dd($request->all());
        // $request->validate([
        //     'tier_id' => ['required', 'string', 'max:255'],
        //     'country_name' => ['required', 'string', 'max:255'],
        //     'class_type_id' => ['required', 'string', 'max:255'],
            
        // ]);
       
        $Tier =  TierManagement::create([
            'tier_id' => $request->tier,
            'country_name' =>$request->country,
            'currency_type_id' => $request->currency_type,
        

        ]);
        return redirect()->route('general-admin.tier.index');
    }


    public function editTier($id)
    {
        $Tier = TierManagement::findOrFail($id);
        $Tiers = Tier::all();
        $currency = CurrencySettings::all();
        return view('admin.tier.edit', \compact('Tier','Tiers','currency'));
    }


    public function updateTier(Request $request, $id)
    {
        // $request->validate([
        //     'location' => ['required', 'string', 'max:255'],
        //     'image' => ['sometimes','mimes:jpeg,jpg,png,gif'],
        // ]);
        $TierManagement =  TierManagement::find($id);
        $TierManagement->update(
            [ 'tier_id' => $request->tier,
            'country_name' =>$request->country,
            'currency_type_id' => $request->currency_type,

            ]
        );
        return redirect()->route('general-admin.tier.index');
    }

    public function deleteTier($id)
    {
        $TierManagements=TierManagement::find($id);
        $TierManagements->delete();
        return redirect()->back();
    }

    public function view_currency_range()
    {
        $CurrencyRangeTiers=CurrencyRangeTier::all();
        return view('admin.tier.currencyRange.index',\compact('CurrencyRangeTiers'));
    }

    public function createTierCurrency()
    {
        // $TierManagements=TierManagement::find($id);
        $Tiers = Tier::all();
        $userClasses = DB::table('classes')->get();
        return view('admin.tier.currencyRange.create',\compact('Tiers','userClasses'));
    }

    public function storeTierCurrency(Request $request)
    {
        // dd($request->all());
        // $request->validate([
        //     'tier_id' => ['required', 'string', 'max:255'],
        //     'country_name' => ['required', 'string', 'max:255'],
        //     'class_type_id' => ['required', 'string', 'max:255'],
            
        // ]);
       
        $Tier =  CurrencyRangeTier::create([
            'tier_id' => $request->tier,
            'class_id' =>$request->class_type,
            'price_range' => $request->price_range,
            'update_date' => date('Y-m-d H:i:s'),

        ]);
        return redirect()->route('general-admin.tier.currency_range.index');
    }


    public function editTierCurrency($id)
    {
        $CurrencyRangeTier = CurrencyRangeTier::findOrFail($id);
        $Tiers = Tier::all();
        $userClasses = DB::table('classes')->get();
        return view('admin.tier.currencyRange.edit', \compact('CurrencyRangeTier','Tiers','userClasses'));
    }


    public function updateTierCurrency(Request $request, $id)
    {
        // $request->validate([
        //     'location' => ['required', 'string', 'max:255'],
        //     'image' => ['sometimes','mimes:jpeg,jpg,png,gif'],
        // ]);
        $CurrencyRangeTier =  CurrencyRangeTier::find($id);
        $CurrencyRangeTier->update(
            [ 'tier_id' => $request->tier,
            'class_id' =>$request->class_type,
            'price_range' => $request->price_range,
            'update_date' => date('Y-m-d H:i:s'),
            ]
        );
        return redirect()->route('general-admin.tier.currency_range.index');
    }

    public function deleteTierCurrency($id)
    {
        $CurrencyRangeTier=CurrencyRangeTier::find($id);
        $CurrencyRangeTier->delete();
        return redirect()->back();
    }


    public function UpdateCurrencySetting(Request $request, $id)
    {
        $request->validate([
            // 'currency' => ['required', 'max:1000'],
            // 'price' => ['required', 'numeric', 'max:1000'],
            // 'currency_inr' => ['required', 'numeric', 'max:1000'],
            // 'currency_aed' => ['required', 'numeric', 'max:1000'],
            // 'currency_pound' => ['required', 'numeric', 'max:1000'],
            // 'currency_aud' => ['required', 'numeric', 'max:1000'],
        ]);
        $getCurrency =  Currencysettings::find($id);
       
        $getCurrency->update(
           [ 'symbol' => $request->currency,
            'value' => $request->price,
            'update_date' => date('Y-m-d H:i:s'),
            ]
        );
        return response([
            'status'=> "true",
            'data' => $getCurrency, 'message' => 'Updated successfully'
        ], 201);
    }



    public function ShowCurrencyRange()
    {
        $CurrencyRangeTiers=CurrencyRangeTier::all();
        return response([
            'status'=> "true",
            'data' => $CurrencyRangeTiers
        ], 200);
    }

    public function ShowTierManagement()
    {
        $TierManagements=TierManagement::all();
        return response([
            'status'=> "true",
            'data' => $TierManagements
        ], 200);
    }
}
