<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\CategorySkills;
use App\Models\StudentSkills;
use App\Models\Country;
use App\Models\Grade;
use App\Models\Skill;
use App\Http\Controllers\Controller;
use App\Models\StudentDetail;
use App\TutorDetail;
use Illuminate\Http\Request;
use App\Models\User;
//use App\Church;
//use App\Banner;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
     
      $validatedData = $request->validate([
          'email' => 'required|email',
          'password' => 'required',
      ]);
  
      // attempt to authenticate the user
      if (Auth::attempt($validatedData)) {
          // authentication successful
          $user = User::find(Auth::id());
          return response([
            'status'=> "true",
            "data"=>$user ,
            'message' => 'Logged successfully'
        ], 200);
      } else {
        return response()->json([
            'error' => 'Unauthorized',
            'message' => 'Invalid email or password'
        ], 401);
        
      }
    }
    public function buddyRegister(Request $request){
        $user = User::find(Auth::id());
        if(! $user){
            $validator =  $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                // 'dob' => ['required', 'date'],
                // 'location' => ['required', 'string', 'max:255'],
                // 'phone_number' => ['required', 'numeric', 'digits:10','unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
                // 'profile_image' => ['nullable','mimes:jpg,jpeg,png','max:1024'],
            ]);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->first()], 401);
            }
            do {
                $token_key = Str::random(24); ///api_token' => Str::random(60),use Illuminate\Support\Str;
            } while (User::where("token", "=", $token_key)->first() instanceof User);
    
            $path_image = '';
            // if($request->profile_image){
            //     $pro_image = $request->file('profile_image');
            //     $path_image = $pro_image->store('/uploads/student',['disk' => 'public']);
            // }
    
            $user =  User::create([
                'name' => $request->name,
                // 'description'=>$request->description,
                'slug' => Str::slug($request->slug),
                // 'location' => $request->location,
                'email' => $request->email,
                // 'dob' => $request->dob,
                'commision' => 0,
                // 'phone_number' => $request->phone_number,
                'token' => $token_key,
                'role' => 'student',
                'password' => Hash::make($request->password),
                // 'profile_image' => $path_image,
            ]);
            if($request->hasFile('profile_image')){
                $pro_image = $request->file('profile_image');
                // $path_image = $pro_image->store('/uploads/tutor',['disk' => 'public']);
    
                $filename =  $pro_image->getClientOriginalName();
                $storage_path = $pro_image->storeAs('student','profile/'.$filename,'s3');
              
                $path_image = Storage::cloud()->url($storage_path);
     
                $imagefile = $request->file('profile_image');
                $imageFileName= $imagefile->getClientOriginalName();
              
                $user->update([
                    'profile_image' => $path_image,
                  
               ]);
            }
    
            return response([
                'status'=> "true",
                "role"=>"student" ,
                'token' => $token_key, 'message' => 'Created successfully'
            ], 201);
           
            // $user_detail=StudentDetail::create([
            //     'user_id'=>$user->id,
            //     // 'parent_name'=>$request->parent_name,
            //     // 'parent_email'=>$request->parent_email,
            //     // 'parent_mobile'=>$request->parent_mobile,
            //     'grade'=>$request->grade,
            //     'skills'=>1,
            // ]);
    
          
        }
        else{
            if($request->location){
                $user->update([
'location'=>$request->location
                ]);
                return response([
                    'status'=> "true",
                    "role"=>"student" ,
                    'message' => 'Location Added successfully'
                ], 201);
            }
            if($request->skills){
                foreach ($request->skills as $key=>$value){
                    $student_skills=StudentSkills::create([
        'student_id'=> $user->id,
        'skill_id'=>$value
                    ]);
                }
                return response([
                    'status'=> "true",
                    "role"=>"student" ,
                    'message' => 'Skills Added successfully'
                ], 201);
            }
        }
      
    }
    public function index()
    {
        //
        $users = User::where('role','student')->get();
        return view('admin.user.index',\compact('users'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subset=Category::all();
        $category = $subset->map(function ($subset) {
            return collect($subset->toArray())
                ->only(['id', 'category_name'])
                ->all();
        });
        $grade_subset=Grade::all();
        $grade = $grade_subset->map(function ($grade_subset) {
            return collect($grade_subset->toArray())
                ->only(['id', 'grade_name'])
                ->all();
        });
        $country_subset=Country::all();
        $countries = $country_subset->map(function ($country_subset) {
            return collect($country_subset->toArray())
                ->only(['id', 'country_name'])
                ->all();
        });
        $categories=Category::all();
        $skill_subset=CategorySkills::all();
        $skills = $skill_subset->map(function ($skill_subset) {
            return collect($skill_subset->toArray())
                ->only(['id', 'skill_name'])
                ->all();
        });
        return view('admin.user.create_edit',\compact('skill_subset','category','skills','categories','countries','grade'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'dob' => ['required', 'date'],
            'location' => ['required', 'string', 'max:255'],
            'phone_number' => ['required', 'numeric', 'digits:10','unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'profile_image' => ['nullable','mimes:jpg,jpeg,png','max:1024'],
        ]);
        do {
            $token_key = Str::random(24); ///api_token' => Str::random(60),use Illuminate\Support\Str;
        } while (User::where("token", "=", $token_key)->first() instanceof User);

        $path_image = '';
        // if($request->profile_image){
        //     $pro_image = $request->file('profile_image');
        //     $path_image = $pro_image->store('/uploads/student',['disk' => 'public']);
        // }

        $user =  User::create([
            'name' => $request->name,
            'description'=>$request->description,
            'slug' => Str::slug($request->slug),
            'location' => $request->location,
            'email' => $request->email,
            'dob' => $request->dob,
            'commision' => 0,
            'phone_number' => $request->phone_number,
            'token' => $token_key,
            'role' => 'student',
            'password' => Hash::make($request->password),
            // 'profile_image' => $path_image,
        ]);
        if($request->hasFile('profile_image')){
            $pro_image = $request->file('profile_image');
            // $path_image = $pro_image->store('/uploads/tutor',['disk' => 'public']);

            $filename =  $pro_image->getClientOriginalName();
            $storage_path = $pro_image->storeAs('student','profile/'.$filename,'s3');
          
            $path_image = Storage::cloud()->url($storage_path);
 
            $imagefile = $request->file('profile_image');
            $imageFileName= $imagefile->getClientOriginalName();
        }

        $user->update([
             'profile_image' => $path_image,
           
        ]);
        if($request->skills){
            foreach ($request->skills as $key=>$value){
                $student_skills=StudentSkills::create([
    'student_id'=> $user->id,
    'skill_id'=>$value
                ]);
            }
        }
        $user_detail=StudentDetail::create([
            'user_id'=>$user->id,
            // 'parent_name'=>$request->parent_name,
            // 'parent_email'=>$request->parent_email,
            // 'parent_mobile'=>$request->parent_mobile,
            'grade'=>$request->grade,
            'skills'=>1,
        ]);

        // Send Credentials to Church
//        Mail::to($request->email)->send(new SendCredentialsToChurch(
//            $request->input('email'),
//            $request->input('password'),
//            $request->input('name')
//        ));

        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('admin.user.show', \compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $userId)
    {
        $user = User::findOrFail($userId);
        $studentskill =StudentSkills::where('student_id',$userId)->pluck('skill_id');
        $studentcategorys =CategorySkills::whereIn('id',$studentskill)->pluck('category_id');
        $StudentDetail=StudentDetail::where('user_id',$userId)->first();
        $subset=Category::all();
        $category = $subset->map(function ($subset) {
            return collect($subset->toArray())
                ->only(['id', 'category_name'])
                ->all();
        });
        $grade_subset=Grade::all();
        $grades = $grade_subset->map(function ($grade_subset) {
            return collect($grade_subset->toArray())
                ->only(['id', 'grade_name'])
                ->all();
        });
        $country_subset=Country::all();
        $countries = $country_subset->map(function ($country_subset) {
            return collect($country_subset->toArray())
                ->only(['id', 'country_name'])
                ->all();
        });
        $category_subset=Category::all();
        $skill_subset=CategorySkills::all();
        $skills = $skill_subset->map(function ($skill_subset) {
            return collect($skill_subset->toArray())
                ->only(['id', 'skill_name'])
                ->all();
        });
        return view('admin.user.edit', \compact('studentcategorys','studentskill','StudentDetail','category_subset','skill_subset','user','category','skills','countries','grades'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'email' => 'required|email|unique:users,email,'.$id,
//            'slug' => 'required|unique:users,slug,'.$id,
            'dob' => 'required',
            'phone_number' => 'required|unique:users,phone_number,'.$id,
            'name' => 'required',
            'password' => 'sometimes|confirmed',
            'profile_image' => ['nullable','mimes:jpg,jpeg,png','max:1024'],
        ]);

        // $path_image = '';
        // if($request->profile_image){
        //     if(User::where('id', $id)->exists()){
        //         $user = User::find($id);
        //         if($user->profile_image){
        //             if(Storage::disk('public')->exists($user->profile_image)){
        //                 Storage::disk('public')->delete($user->profile_image);
        //             }
        //         }
        //     }
        //     $pro_image = $request->file('profile_image');
        //     $path_image = $pro_image->store('/uploads/student',['disk' => 'public']);
        // }

        $path_image = '';
        if($request->hasFile('profile_image')){
            if(User::where('id', $id)->exists()){
                $user = User::find($id);
                if($user->profile_image){
                    if(Storage::disk('s3')->exists($user->profile_image)){
                        Storage::disk('s3')->delete($user->profile_image);
                    }
                }
            }
            $pro_image = $request->file('profile_image');
            
            $filename =  $pro_image->getClientOriginalName();
            $storage_path = $pro_image->storeAs('tutor/'.$id,$filename,'s3');
 
            $path_image = Storage::cloud()->url($storage_path);


        }

        if($request->password)
        {
            $data = [
                'name' => $request->name,
                'description'=>$request->description,
                'commision'=>0,
                'email' => $request->email,
                'dob' => $request->dob,
                'slug' => Str::slug($request->slug),
                'phone_number' => $request->phone_number,
                'location' => $request->location,
                'password' => Hash::make($request->password),
                // 'profile_image' => $path_image,
            ];
        }
        else{
            $data = [
                'name' => $request->name,
                'description'=>$request->description,
                'commision'=>0,
                'email' => $request->email,
                'dob' => $request->dob,
                'slug' => Str::slug($request->slug),
                'phone_number' => $request->phone_number,
                'location' => $request->location,
                // 'profile_image' => $path_image,
            ];
        }

        $detail_data = [
            // 'parent_name'=>$request->parent_name,
            // 'parent_email'=>$request->parent_email,
            // 'parent_mobile'=>$request->parent_mobile,
            'grade'=>$request->grade,
            'skills'=>1,
        ];

        $user =  User::updateOrCreate(
            ['id' =>$id],
            $data
        );
        if ($request->hasFile('profile_image')) {
            $user->update([
                'profile_image' => $path_image
            ]);
        }
      
        $StudentSkills=StudentSkills::where('student_id',$user->id)->get();
        if (!empty($StudentSkills)) {
            $tutor_skills=StudentSkills::where('student_id',$user->id)->delete();
      }
     
      if($request->skills){
        foreach ($request->skills as $key=>$value){
            $tutor_skills=StudentSkills::create([
'student_id'=> $user->id,
'skill_id'=>$value
            ]);
        }
    }
      

        $user_detail =  StudentDetail::updateOrCreate(
            ['user_id' =>$id],
            $detail_data
        );

        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $user = User::find($id);
        $user_details = StudentDetail::where('user_id',$id)->get();
        foreach ($user_details as $user_detail) {
            $user_detail->delete();
        }
        $StudentSkills = StudentSkills::where('user_id',$id)->get();
        foreach ($StudentSkills as $user_detail) {
            $user_detail->delete();
        }
        $user->delete();
        return redirect()->route('user.index');
    }

    public function logoutApi(Request $request)
    {
        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => $validatedData->errors()->first('token')], 400);
        } else {
            $user = User::where("token", "=", $request->token)->first();
            do {
                $token_key = Str::random(24); ///api_token' => Str::random(60),use Illuminate\Support\Str;
            } while (User::where("token", "=", $token_key)->first() instanceof User);
            $user->token = $token_key;
            $user->save();
        }
    }

}
