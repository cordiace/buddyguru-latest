<?php

namespace App\Http\Controllers;
use App\Models\Package;
use App\Models\User;
use App\Models\Message;
use Illuminate\Http\Request;

class ChatController extends Controller
{

    public function sendMessage(Request $request)
    {
        $userId = $request->user_id;
        $guruId = $request->guru_id;
        $packageId = $request->input('package_id');
        $messageContent = $request->input('message');

        // Validate package ID and user
        $package = Package::find($packageId);
        if (!$package) {
            return response()->json(['error' => 'Package not found'], 404);
        }

        if (!$userId) {
            return response()->json(['error' => 'User not found'], 404);
        }
        if (!$guruId) {
            return response()->json(['error' => 'User not found'], 404);
        }

        // Create a new message
        $message = new Message();
        $message->content = $messageContent;
        $message->package_id = $packageId;
        $message->user_id = $userId;
        $message->guru_id = $guruId;
        $message->save();

        // Find the users (gurus) associated with the package and notify them
        $users = User::where('id', $userId)->get();
        foreach ($users as $user) {
            // Here, you can send a notification to the user (guru) or perform any necessary actions
        }

        return response()->json([
            'status'=>'true',
            'data'=>$message,
            'message' => 'Message sent'
        ]);
    }

    public function replyToMessage(Request $request)
    {
        $guruId = $request->guru_id;
        $messageId = $request->input('message_id');
        $replyContent = $request->input('message');

        // Validate message ID and user
        $message = Message::find($messageId);
        if (!$message) {
            return response()->json(['error' => 'Message not found'], 404);
        }

        if (!$guruId) {
            return response()->json(['error' => 'User not found'], 404);
        }
        if (!$messageId) {
            return response()->json(['error' => 'parent not found'], 404);
        }

        // Create a new reply message
        $replyMessage = new Message();
        $replyMessage->content = $replyContent;
        $replyMessage->package_id = $message->package_id;
        $replyMessage->user_id = $message->user_id;
        $replyMessage->guru_id =  $guruId;
        $replyMessage->parent_id = $messageId;
        $replyMessage->save();

        // Find the user (original sender) and notify them
        $originalSender = User::find($message->user_id);
        if ($originalSender) {
            // Here, you can send a notification to the original sender or perform any necessary actions
        }

        return response()->json([
            'status'=>'true',
            'data'=>$replyMessage,
            'message' => 'Reply sent'
        ]);
    }

}
