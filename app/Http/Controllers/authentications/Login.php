<?php
namespace App\Http\Controllers\authentications;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Login extends Controller
{
  public function index()
  {
    $pageConfigs = ['myLayout' => 'blank'];
    return view('admin.authentications.auth-login', ['pageConfigs' => $pageConfigs]);
  }

  public function login(Request $request)
  {
   
      $credentials = $request->only('email', 'password');

      if (Auth::guard('admin')->attempt($credentials)) {
         // Authentication passed...
        if($request->email =='admin@gmail.com'){
          return redirect()->route('welcome');
        }
        else{
          return back()->withErrors(['email' => 'Invalid credentials']);
        }
         
         
      }

      return back()->withErrors(['email' => 'Invalid credentials']);
  }

  public function logout()
{
    Auth::logout();

    return redirect('/');
}
}
