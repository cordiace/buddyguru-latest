<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TutorDetail extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id', 'category','sub_category','skills', 'about', 'experience','cover_letter','cover_letter_fileName','video_file_name','video_url','video_thumbnail'
    ];

    //TutorDetail.php
    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }

    // public function tutorskill()
    // {
    //     return $this->hasOne('App\Skill', 'skills','id');
    // }

    public function category()
    {
        return $this->belongsTo('App\Model\Category','category', 'id');
    }
    /**
     * Get the category that owns the TutorDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getCategory()
    {
        return $this->belongsTo('\App\Model\Category', 'category', 'id');
    }

    /**
     * Get the sub-category that owns the TutorDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getSubCategory()
    {
        return $this->belongsTo('\App\Model\SubCategory', 'sub_category', 'id');
    }

    /**
     * Get the skill that owns the TutorDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getSkill()
    {
        return $this->belongsTo('\App\Model\Skill', 'skills', 'id');
    }
}
