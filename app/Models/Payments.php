<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    use HasFactory;
    public $table = "payments";
    protected $fillable = [
        'user_id','tutor_id','batch_id', 'package_id','payment_method','amount','charge_id','card_number','card_name',
        'expiry_date','cvv','payment_status','email_status'
    ];

  
   

   
}
