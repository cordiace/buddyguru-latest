<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Grade extends Model
{
    use Notifiable;
    protected $fillable = [
        'grade_name'
    ];
}
