<?php

namespace App\Models\GeneralSettings;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CurrencyRangeTier extends Model
{
    use HasFactory;
    public $timestamps = false;
    public $table = "currency_range_tier";
    protected $fillable = [
        'tier_id','class_id','price_range','update_date'
    ];
    public function classes()
    {
        // return $this->belongsTo('App\Batch');
        return $this->hasOne('App\Models\Classes','class_id', 'class_id');
    }
    public function tier()
    {
        // return $this->belongsTo('App\Batch');
        return $this->hasOne('App\Models\GeneralSettings\Tier','id', 'tier_id');
    }
}
