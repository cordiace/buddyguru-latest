<?php

namespace App\Models\GeneralSettings;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CurrencySettings extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'symbol','value','update_date',
    ];
}
