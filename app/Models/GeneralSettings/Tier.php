<?php

namespace App\Models\GeneralSettings;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tier extends Model
{
    use HasFactory;
    public $timestamps = false;
    public $table = "tier";
    protected $fillable = [
        'name'
    ];
}
