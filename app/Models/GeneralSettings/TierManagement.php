<?php

namespace App\Models\GeneralSettings;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TierManagement extends Model
{
    use HasFactory;
    public $table = "tier_management";
    public $timestamps = false;

    protected $fillable = [
        'tier_id','country_name','currency_type_id'
    ];
    public function currencySetting()
    {
        // return $this->belongsTo('App\Batch');
        return $this->hasOne('App\Models\GeneralSettings\CurrencySettings','id', 'currency_type_id');
    }
    public function tier()
    {
        // return $this->belongsTo('App\Batch');
        return $this->hasOne('App\Models\GeneralSettings\Tier','id', 'tier_id');
    }
}
