<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppliedGurus extends Model
{
    use HasFactory;
    protected $fillable = [
        'phone_number','bio','cover_letter','video_url','thumbnail','status'
    ];
}
