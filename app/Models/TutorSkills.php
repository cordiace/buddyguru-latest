<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TutorSkills extends Model
{
    public $table = "tutor_skills";
    public $timestamps = false;
    use HasFactory;
    protected $fillable = [
        'tutor_id','skill_id'
    ];

    public function CategorySkills()
    {
        return $this->hasOne('App\Models\CategorySkills', 'id','skill_id');
    }
    public function user() {
        return $this->belongsTo(User::class, 'tutor_id');
    }
   
  
}
