<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use HasFactory;
    public $table = "packages";
    protected $fillable = [
        'user_id','batch_id', 'intro_title','intro_description','video_url','thumbanail','total_price',
    ];

    //SessionDetail.php
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    //SessionDetail.php
    public function tutor()
    {
        return $this->belongsTo('App\Models\User');
    }
    public function PackageDetail()
    {
        return $this->hasMany('App\Models\PackageDetail','package_id','id');
    }

   
}
