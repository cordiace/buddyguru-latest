<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    use HasFactory;
    protected $fillable = [
        'class_id','class_name','description',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    //SessionDetail.php
    public function tutor()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function packages()
    {
        return $this->belongsToMany('App\Models\Package');
    }
}
