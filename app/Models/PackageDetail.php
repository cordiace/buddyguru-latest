<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PackageDetail extends Model
{
    use HasFactory;
    public $table = "package_detail";
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'user_id','package_id','batch_id', 'title','description','video_url','thumbnail'
        // 'class_type','start_date','end_date',
        // 'start_time','end_time','duration','meeting_link','no_of_class', 'total_price'
    ];

    
    public function classes()
    {
        // return $this->belongsTo('App\Batch');
        return $this->hasOne('App\Models\Classes','class_id', 'batch_id');
    }
    public function package()
    {
        // return $this->belongsTo('App\Batch');
        return $this->hasOne('App\Models\Package','id', 'package_id');
    }
    public function packages()
    {
        return $this->belongsTo(Package::class);
    }

     public function liveClass()
    {
        return $this->hasOne('App\Models\LiveClass','user_id','user_id');
    }
}
