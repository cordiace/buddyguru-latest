<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class TrendingVideos extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'video_url','thumbnail','file_name','watch_status','like_status','created_at','updated_at'
    ];

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id','user_id');
    }
   
}