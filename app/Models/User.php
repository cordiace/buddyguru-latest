<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    public $table = "users";
     protected $fillable = [
        'name','commision','description','email','dob', 'slug','password','role','phone_number','location','token','otp_verification','profile_image','image_file_name'
    ];
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function country()
    {
        return $this->belongsTo('\App\Models\Country', 'location', 'id');
    }
    public function TutorSkill()
    {
        return $this->hasOne('App\Models\TutorSkills', 'id','tutor_id');
    }
   
    public function tutorSkills() {
        return $this->hasMany(TutorSkills::class, 'tutor_id');
    }


}
