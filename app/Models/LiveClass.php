<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LiveClass extends Model
{
    use HasFactory;
    public $table = "live_class";
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     * 
     * @var array
     */
    protected $fillable = [
        'user_id','price','batch_id','intro_title','intro_description', 'title','description','video_url','video_file_name','thumbnail','class_type','start_date','end_date',
        'start_time','end_time','duration','meeting_link','no_of_class','no_of_participents'
    ];

    //SessionDetail.php
    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }
    //SessionDetail.php
    public function tutor()
    {
        return $this->belongsTo('App\Model\User');
    }

    public function classes()
    {
        // return $this->belongsTo('App\Batch');
        return $this->hasOne('App\Model\Classes','class_id', 'batch_id');
    }
}
