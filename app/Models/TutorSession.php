<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TutorSession extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id','category_id','grade_id','batch_id','session_name', 'description','date','time', 'meeting_link','start_url','meeting_id','what_learn',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    //SessionDetail.php
    public function tutor()
    {
        return $this->belongsTo('App\Models\User');
    }
}
