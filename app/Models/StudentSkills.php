<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class StudentSkills extends Model
{
    use Notifiable;
    public $timestamps = false;
    public $table ='student_skills';
    protected $fillable = [
        'student_id','skill_id'
    ];
}
