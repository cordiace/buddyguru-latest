<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppliedGuruSkills extends Model
{
    use HasFactory;
    public $timestamps =false;
    protected $fillable = [
        'tutor_id','skill_id'
    ];
}
