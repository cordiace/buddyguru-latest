<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class StudentDetail extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'parent_name','parent_email','parent_mobile','grade','skills'
    ];

//StudentDetail.php
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function getSkill()
    {
        return $this->belongsTo('\App\Models\Skill', 'skills', 'id');
    }

    public function getGrade()
    {
        return $this->belongsTo('\App\Models\Grade', 'grade', 'id');
    }
}