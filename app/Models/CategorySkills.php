<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategorySkills extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'category_id','skill_name'
    ];

    public function category()
    {
        // return $this->belongsTo('App\Batch');
        return $this->hasOne('App\Models\Category','id', 'category_id');
    }
  
}
