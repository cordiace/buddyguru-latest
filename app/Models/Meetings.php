<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Meetings extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id','package_id' 
    ];

    //TutorDetail.php
    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }

    // public function tutorskill()
    // {
    //     return $this->hasOne('App\Skill', 'skills','id');
    // }

    public function category()
    {
        return $this->belongsTo('App\Model\Category','category', 'id');
    }
    /**
     * Get the category that owns the TutorDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getCategory()
    {
        return $this->belongsTo('\App\Model\Category', 'category', 'id');
    }

    /**
     * Get the sub-category that owns the TutorDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getSubCategory()
    {
        return $this->belongsTo('\App\Model\SubCategory', 'sub_category', 'id');
    }

    /**
     * Get the skill that owns the TutorDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getSkill()
    {
        return $this->belongsTo('\App\Model\Skill', 'skills', 'id');
    }
}
