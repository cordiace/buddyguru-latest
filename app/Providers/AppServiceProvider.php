<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Storage;
use Publitio\PublitioAPI;
use Publitio\Storage\PublitioStorageAdapter;
class AppServiceProvider extends ServiceProvider
{
  
  /**
   * Register any application services.
   *
   * @return void
   */
  
  public function register()
  {
    $this->app->singleton('publitio', function ($app) {
      $config = $app['config']->get('filesystems.disks.publitio');
      $adapter = new PublitioStorageAdapter(new PublitioAPI($config['api_key'], $config['api_secret']), $config['default_workspace']);
      return new Filesystem($adapter);
  });
  }

  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot()
  {
    //
  }
}
