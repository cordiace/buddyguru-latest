'use strict';

(function () {
  // Init custom option check
  window.Helpers.initCustomOptionCheck();
 
  // Bootstrap validation example
  //------------------------------------------------------------------------------------------
  // const flatPickrEL = $('.flatpickr-validation');
  const flatPickrList = [].slice.call(document.querySelectorAll('.flatpickr-validation'));
  // Flat pickr
  if (flatPickrList) {
    flatPickrList.forEach(flatPickr => {
      flatPickr.flatpickr({
        allowInput: true,
        monthSelectorType: 'static'
      });
    });
  }

  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  const bsValidationForms = document.querySelectorAll('.form');
  const myForm = document.getElementById('myForm');

  // Loop over them and prevent submission
  Array.prototype.slice.call(bsValidationForms).forEach(function (form) {
    form.addEventListener(
      'submit',
      function (event) {
        if (!form.checkValidity()) {
          event.preventDefault();
          event.stopPropagation();
        } else {
          // Submit your form
          // alert('Submitted!!!');
          // myForm.submit();
        }

        form.classList.add('was-validated');
      },
      false
    );
  });
})();
/**
 * Form Validation (https://formvalidation.io/guide/examples)
 * ? Primary form validation plugin for this template
 * ? In this example we've try to covered as many form inputs as we can.
 * ? Though If we've miss any 3rd party libraries, then refer: https://formvalidation.io/guide/examples/integrating-with-3rd-party-libraries
 */
//------------------------------------------------------------------------------------------
document.addEventListener('DOMContentLoaded', function (e) {
  (function () {
   
    const formValidationExamples = document.getElementById('formAccountSettings'),
      formValidationSelect2Location = jQuery(formValidationExamples.querySelector('[name="location"]')),
      formValidationskills = jQuery(formValidationExamples.querySelector('[name="skills[]"]')),
      formValidationcategory = formValidationExamples.querySelector('[name="category[]"]'),
      
      tech = [
        'ReactJS',
        'Angular',
        'VueJS',
        'Html',
        'Css',
        'Sass',
        'Pug',
        'Gulp',
        'Php',
        'Laravel',
        'Python',
        'Bootstrap',
        'Material Design',
        'NodeJS'
      ];

    const fv = FormValidation.formValidation(formValidationExamples, {
      fields: {
        name: {
          validators: {
            notEmpty: {
              message: 'Please enter a name'
            },
          
            regexp: {
              regexp: /^[a-zA-Z0-9 ]+$/,
              message: 'The name can only consist of alphabetical, number and space'
            }
          }
        },
        email: {
          validators: {
            notEmpty: {
              message: 'Please enter your email'
            },
            emailAddress: {
              message: 'The value is not a valid email address'
            }
          }
        },
        // password: {
        //   validators: {
        //     notEmpty: {
        //       message: 'Please enter your password'
        //     }
        //   }
        // },
        formValidationConfirmPass: {
          validators: {
            // notEmpty: {
            //   message: 'Please confirm password'
            // },
            identical: {
              compare: function () {
                return formValidationExamples.querySelector('[name="password"]').value;
              },
              message: 'The password and its confirm are not the same'
            },
            passwordNotEmpty: {
              message: 'Please enter your password',
              enabled: false,
              validateIfEmpty: true,
              validator: function() {
                var password = formValidationExamples.querySelector('[name="password"]').value;
                return (password !== '');
              }
            }
          }
        },
        phone_number: {
          validators: {
            notEmpty: {
              message: 'Please enter your phone number'
            }
          }
        },
        commision: {
          validators: {
            notEmpty: {
              message: 'Please enter your  commision'
            }
          }
        },
        description: {
          validators: {
            notEmpty: {
              message: 'Please enter your  description'
            }
          }
        },
        location: {
          validators: {
            notEmpty: {
              message: 'Please select your country'
            },
          
          }
        },
        dob: {
          validators: {
            notEmpty: {
              message: 'Please select your DOB'
            },
            // date: {
            //   format: 'YYYY/MM/DD',
            //   message: 'The value is not a valid date'
            // }
          }
        },
        'category[]': {
          validators: {
            notEmpty: {
              message: 'Please select a category'
            }
          }
        },
        'skills[]': {
          validators: {
            notEmpty: {
              message: 'Please add your skills'
            }
          }
        }
       
      },
      plugins: {
        trigger: new FormValidation.plugins.Trigger(),
        bootstrap5: new FormValidation.plugins.Bootstrap5({
          // Use this for enabling/changing valid/invalid class
          // eleInvalidClass: '',
          eleValidClass: '',
          rowSelector: function (field, ele) {
            // field is the field name & ele is the field element
            switch (field) {
              case 'intro_title':
              case 'formValidationEmail':
              case 'formValidationPass':
              case 'formValidationConfirmPass':
              case 'formValidationFile':
              case 'formValidationDob':
              case 'formValidationSelect2':
              case 'formValidationLang':
              case 'formValidationTech':
              case 'formValidationHobbies':
              case 'formValidationBio':
              case 'formValidationGender':
                return '.col-md-6';
              case 'formValidationPlan':
                return '.col-xl-3';
              case 'formValidationSwitch':
              case 'formValidationCheckbox':
                return '.col-12';
              default:
                return '.row';
            }
          }
        }),
        submitButton: new FormValidation.plugins.SubmitButton(),
        // Submit the form when all fields are valid
        defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
        autoFocus: new FormValidation.plugins.AutoFocus()
      },
      init: instance => {
        instance.on('plugins.message.placed', function (e) {
        
          //* Move the error message out of the `input-group` element
          if (e.element.parentElement.classList.contains('input-group')) {
            // `e.field`: The field name
            // `e.messageElement`: The message element
            // `e.element`: The field element
            e.element.parentElement.insertAdjacentElement('afterend', e.messageElement);
          }
          //* Move the error message out of the `row` element for custom-options
          if (e.element.parentElement.parentElement.classList.contains('custom-option')) {
            e.element.closest('.row').insertAdjacentElement('afterend', e.messageElement);
          }
        });
      }
      
    });

    //? Revalidation third-party libs inputs on change trigger

    // Flatpickr
    // flatpickr(formValidationExamples.querySelector('[name="formValidationDob"]'), {
    //   enableTime: false,
    //   // See https://flatpickr.js.org/formatting/
    //   dateFormat: 'Y/m/d',
    //   // After selecting a date, we need to revalidate the field
    //   onChange: function () {
    //     fv.revalidateField('formValidationDob');
    //   }
    // });
    // if (select2.length) {
    //   select2.each(function () {
    //     var $this = $(this);
    //     $this.select2().on('change', function () {
    //       FormValidation1.revalidateField('skills[]');
    //     });
    //   });
    // }

    // if (select2.length) {
    //   select2.each(function () {
    //     var $this = $(this);
    //     $this.select2().on('change', function () {
    //       FormValidation1.revalidateField('category[]');
    //     });
    //   });
    // }
    // Select2 (Country)
    if (formValidationSelect2Location.length) {
      formValidationSelect2Location.wrap('<div class="position-relative"></div>');
      formValidationSelect2Location
        .select2({
          placeholder: 'Select country',
          dropdownParent: formValidationSelect2Location.parent()
        })
        .on('change.select2', function () {
          // Revalidate the color field when an option is chosen
          fv.revalidateField('location');
        });
    }

    // Typeahead

    // String Matcher function for typeahead
    const substringMatcher = function (strs) {
      return function findMatches(q, cb) {
        var matches, substrRegex;
        matches = [];
        substrRegex = new RegExp(q, 'i');
        $.each(strs, function (i, str) {
          if (substrRegex.test(str)) {
            matches.push(str);
          }
        });

        cb(matches);
      };
    };

    // Check if rtl
    if (isRtl) {
      const typeaheadList = [].slice.call(document.querySelectorAll('.typeahead'));

      // Flat pickr
      if (typeaheadList) {
        typeaheadList.forEach(typeahead => {
          typeahead.setAttribute('dir', 'rtl');
        });
      }
    }
    
    // Tagify
    // let formValidationLangTagify = new Tagify(formValidationLangEle);
    // formValidationLangEle.addEventListener('change', onChange);
    // function onChange() {
    //   fv.revalidateField('formValidationLang');
    // }

    //Bootstrap select
    // formValidationHobbiesEle.on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
    //   fv.revalidateField('formValidationHobbies');
    // });
  })();
});
