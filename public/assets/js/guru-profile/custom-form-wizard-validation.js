/**
 *  Form Wizard
 */

'use strict';

(function () {
  const select2 = $('.select2'),
    selectPicker = $('.selectpicker');

  // Wizard Validation
  // --------------------------------------------------------------------
  const wizardValidation = document.querySelector('#wizard-validation');
  const guruClass = document.querySelector('#guruIn');
  const moduleClass = document.querySelector('#moduleIn');
  const liveClass = document.querySelector('#liveIn');
  const isValid = document.querySelector('#live_check_validation');
 
  if (typeof wizardValidation !== undefined && wizardValidation !== null) {
    
    // Wizard form
    const wizardValidationForm = wizardValidation.querySelector('#wizard-validation-form');
    // Wizard steps
    const wizardValidationFormStep1 = wizardValidationForm.querySelector('#account-details-validation');
   const wizardValidationFormStep2 = wizardValidationForm.querySelector('#guru-class-validation');
   const wizardValidationFormStep3 = wizardValidationForm.querySelector('#module-class-validation');
   const wizardValidationFormStep4 = wizardValidationForm.querySelector('#live-class-validation');
    // Wizard next prev button
    const wizardValidationNext = [].slice.call(wizardValidationForm.querySelectorAll('.btn-next'));
    const wizardValidationPrev = [].slice.call(wizardValidationForm.querySelectorAll('.btn-prev'));
    const wizardValidationSkip = [].slice.call(wizardValidationForm.querySelectorAll('.btn-skip'));

    const validationStepper = new Stepper(wizardValidation, {
      linear: true
    });

    // Account details
    const FormValidation1 = FormValidation.formValidation(wizardValidationFormStep1, {
      fields: {
        name: {
          validators: {
            notEmpty: {
              message: 'The name is required'
            },
          
            regexp: {
              regexp: /^[a-zA-Z0-9 ]+$/,
              message: 'The name can only consist of alphabetical, number and space'
            }
          }
        },
        email: {
          validators: {
            notEmpty: {
              message: 'The Email is required'
            },
            emailAddress: {
              message: 'The value is not a valid email address'
            }
          }
        },
        password: {
          validators: {
            notEmpty: {
              message: 'The password is required'
            }
          }
        },
        formValidationConfirmPass: {
          validators: {
            notEmpty: {
              message: 'The Confirm Password is required'
            },
            identical: {
              compare: function () {
                return wizardValidationFormStep1.querySelector('[name="password"]').value;
              },
              message: 'The password and its confirm are not the same'
            }
          }
        },
        phone_number: {
          validators: {
            notEmpty: {
              message: 'The phone number is required'
            }
          }
        },
        commision: {
          validators: {
            notEmpty: {
              message: 'The commision is required'
            }
          }
        },
        location: {
          validators: {
            notEmpty: {
              message: 'The location is required'
            }
          }
        },
       'skills[]': {
          validators: {
            notEmpty: {
              message: 'The skill is required'
            }
          }
        },
        'category[]': {
          validators: {
            notEmpty: {
              message: 'The category is required'
            }
          }
        },
        dob: {
          validators: {
            notEmpty: {
              message: 'The DOB is required'
            }
          }
        },
        description: {
          validators: {
            notEmpty: {
              message: 'The description is required'
            }
          }
        },
        profile_image: {
          validators: {
            notEmpty: {
              message: 'The profile image is required'
            }
          }
        },
        video_url: {
          validators: {
            notEmpty: {
              message: 'The intro video is required'
            }
          }
        },
      },
      plugins: {
        trigger: new FormValidation.plugins.Trigger(),
        bootstrap5: new FormValidation.plugins.Bootstrap5({
          // Use this for enabling/changing valid/invalid class
          // eleInvalidClass: '',
          eleValidClass: '',
          rowSelector: '.col-sm-6'
        }),
        autoFocus: new FormValidation.plugins.AutoFocus(),
        submitButton: new FormValidation.plugins.SubmitButton()
      },
      init: instance => {
        instance.on('plugins.message.placed', function (e) {
          //* Move the error message out of the `input-group` element
          if (e.element.parentElement.classList.contains('input-group')) {
            e.element.parentElement.insertAdjacentElement('afterend', e.messageElement);
          }
        });
      }
    }).on('core.form.valid', function () {
      // Jump to the next step when all fields in the current step are valid
      validationStepper.next();
    });

    // Guru class
   // Define validation rules for each dynamic field
const dynamicFieldRules = {
  'guru_title[]': {
    validators: {
      notEmpty: {
        message: 'The title is required'
      }
    }
  },
  'guru_description[]': {
    validators: {
      notEmpty: {
        message: 'The description is required'
      }
    }
  },
  'guru_video[]': {
    validators: {
      notEmpty: {
        message: 'The video is required'
      }
    }
  }
};
// Define a function to add the field to the validator
function addDynamicFieldValidationGuru(fieldName) {
  FormValidation2.addField(fieldName, dynamicFieldRules);
}    
    const FormValidation2 = FormValidation.formValidation(wizardValidationFormStep2, {
     
      fields: {
        guru_intro_title: {
          validators: {
            notEmpty: {
              message: 'The intro title is required'
            }
          }
        },
        guru_intro_description: {
          validators: {
            notEmpty: {
              message: 'The intro description is required'
            }
          }
        },
        guru_trailer: {
          validators: {
            notEmpty: {
              message: 'The trailer is required'
            }
          }
        },
        'guru_title[]': {
          validators: {
            notEmpty: {
              message: 'The title is required'
            }
          }
        },
       'guru_description[]': {
          validators: {
            notEmpty: {
              message: 'The description is required'
            }
          }
        },
        'guru_video[]': {
          validators: {
            notEmpty: {
              message: 'The video is required'
            }
          }
        },
        guru_total_price: {
          validators: {
            notEmpty: {
              message: 'The total price is required'
            }
          }
        }
      },
      plugins: {
        trigger: new FormValidation.plugins.Trigger(),
        bootstrap5: new FormValidation.plugins.Bootstrap5({
          // Use this for enabling/changing valid/invalid class
          // eleInvalidClass: '',
          eleValidClass: '',
          //rowSelector: '.col-sm-6'
        }),
        autoFocus: new FormValidation.plugins.AutoFocus(),
        submitButton: new FormValidation.plugins.SubmitButton()
      },
      init: instance => {
        instance.on('plugins.message.placed', function (e) {
          //* Move the error message out of the `input-group` element
          if (e.element.parentElement.classList.contains('input-group')) {
            e.element.parentElement.insertAdjacentElement('afterend', e.messageElement);
          }
        });
      }
    }).on('core.form.valid', function () {
        guruClass.value ="true";
      // Jump to the next step when all fields in the current step are valid
      validationStepper.next();
    });

    // Bootstrap Select (i.e Language select)
    if (select2.length) {
      select2.each(function () {
        var $this = $(this);
        $this.select2().on('change', function () {
          FormValidation1.revalidateField('skills[]');
        });
      });
    }

    if (select2.length) {
      select2.each(function () {
        var $this = $(this);
        $this.select2().on('change', function () {
          FormValidation1.revalidateField('category[]');
        });
      });
    }
    // select2
    if (select2.length) {
      select2.each(function () {
        var $this = $(this);
        $this.wrap('<div class="position-relative"></div>');
        $this
          .select2({
            placeholder: 'Select an country',
            dropdownParent: $this.parent()
          })
          .on('change.select2', function () {
            // Revalidate the color field when an option is chosen
            FormValidation1.revalidateField('skills[]');
          });
      });
    }

// module class
function addDynamicFieldValidationModule(fieldName) {
  FormValidation3.addField(fieldName, dynamicFieldRules);
}  
const FormValidation3 = FormValidation.formValidation(wizardValidationFormStep3, {
  fields: {
    module_intro_title: {
      validators: {
        notEmpty: {
          message: 'The intro title is required'
        }
      }
    },
    module_intro_description: {
      validators: {
        notEmpty: {
          message: 'The intro description is required'
        }
      }
    },
    module_trailer: {
      validators: {
        notEmpty: {
          message: 'The trailer is required'
        }
      }
    },
    'module_title[]': {
      validators: {
        notEmpty: {
          message: 'The title is required'
        }
      }
    },
    'module_description[]': {
      validators: {
        notEmpty: {
          message: 'The description is required'
        }
      }
    },
    'module_video[]': {
      validators: {
        notEmpty: {
          message: 'The video is required'
        }
      }
    },
    module_total_price: {
      validators: {
        notEmpty: {
          message: 'The total price is required'
        }
      }
    }
  },
  plugins: {
    trigger: new FormValidation.plugins.Trigger(),
    bootstrap5: new FormValidation.plugins.Bootstrap5({
      // Use this for enabling/changing valid/invalid class
      // eleInvalidClass: '',
      eleValidClass: '',
     // rowSelector: '.col-sm-6'
    }),
    autoFocus: new FormValidation.plugins.AutoFocus(),
    submitButton: new FormValidation.plugins.SubmitButton()
  }
}).on('core.form.valid', function () {
   moduleClass.value ="true";

  // Jump to the next step when all fields in the current step are valid
  validationStepper.next();
});

    // Live class
    const FormValidation4 = FormValidation.formValidation(wizardValidationFormStep4, {
      fields: {
        live_intro_title: {
          validators: {
            notEmpty: {
              message: 'The intro title is required'
            }
          }
        },
        live_intro_description: {
          validators: {
            notEmpty: {
              message: 'The intro description is required'
            }
          }
        },
        live_title: {
          validators: {
            notEmpty: {
              message: 'The title is required'
            }
          }
        },
        live_description: {
          validators: {
            notEmpty: {
              message: 'The description is required'
            }
          }
        },
        live_video: {
          validators: {
            notEmpty: {
              message: 'The video is required'
            }
          }
        },
        class_type: {
          validators: {
            notEmpty: {
              message: 'The class_type is required'
            }
          }
        },
        no_of_class: {
          validators: {
            notEmpty: {
              message: 'The no_of_class is required'
            }
          }
        },
        start_time: {
          validators: {
            notEmpty: {
              message: 'The start_time is required'
            }
          }
        },
        end_time: {
          validators: {
            notEmpty: {
              message: 'The end_time is required'
            }
          }
        },
        start_date: {
          validators: {
            notEmpty: {
              message: 'The start_date is required'
            }
          }
        },
        end_date: {
          validators: {
            notEmpty: {
              message: 'The end_date is required'
            }
          }
        },
        duration: {
          validators: {
            notEmpty: {
              message: 'The duration is required'
            }
          }
        },
        
        price: {
          validators: {
            notEmpty: {
              message: 'The  price is required'
            }
          }
        }
      },
      plugins: {
        trigger: new FormValidation.plugins.Trigger(),
        bootstrap5: new FormValidation.plugins.Bootstrap5({
          // Use this for enabling/changing valid/invalid class
          // eleInvalidClass: '',
          eleValidClass: '',
         // rowSelector: '.col-sm-6'
        }),
        autoFocus: new FormValidation.plugins.AutoFocus(),
        submitButton: new FormValidation.plugins.SubmitButton()
      }
    }).on('core.form.valid', function () {
      isValid.value =true;
      // You can submit the form
      // wizardValidationForm.submit()
      console.log($(wizardValidationForm).serialize());
     liveClass.value ="true";

      var formData = {
        name: $("#name").val(),
        email: $("#email").val(),
      
      };

      // wizardValidationForm.submit(function(e) {
      
        // $.ajax({
        //   url: wizardValidationForm.action,
        //   type: wizardValidationForm.method,
        //   data:$(wizardValidationForm).serialize(),
        //   success: function(data) {
        //     console.log(data); // the object returned from your Action will be displayed here.
        //     // window.location.href = "http://127.0.0.1:8000/admin/tutor";
        //   }
        // });
      // });
      // or send the form data to server via an Ajax request
      // To make the demo simple, I just placed an alert
      // alert('Submitted..!!');

      // window.location.href = "http://127.0.0.1:8000/admin/tutor";
    });

      function submitForm() {
        console.log($(wizardValidationForm).serialize());
        // You can submit the form
        // wizardValidationForm.submit()
       
      //   $.ajaxSetup({
      //     headers: {
      //         'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
      //     }
      // });
     
      //      // AJAX request
      //      var url = "{{ route('tutor.guru.show',6) }}";
         
      //      $.ajax({      
      //          url: "/admin/tutor/package/view/7",
      //          type: "GET",
          
      //          success: (result) => {
      //    // Add employee details
                  
      // },
              
      //      });
        
//  wizardValidationForm.submit(function(e) {
//       console.log("welcome");
//   $.ajax({
//     url: this.action,
//     type: this.method,
//     data:$(this).serialize(),
//     success: function(data) {
//       console.log(data); // the object returned from your Action will be displayed here.
//       // window.location.href = "http://127.0.0.1:8000/admin/tutor";
//     }
//   });
// });
       
        // or send the form data to server via an Ajax request
        // To make the demo simple, I just placed an alert
        // alert('Submitted..!!');
  
        // window.location.href = "http://127.0.0.1:8000/admin/tutor";
      }
    wizardValidationNext.forEach(item => {
      item.addEventListener('click', event => {
        // When click the Next button, we will validate the current step
        switch (validationStepper._currentIndex) {
          case 0:
             FormValidation1.validate();
            // validationStepper.next();
            break;

          case 1:
            addDynamicFieldValidationGuru('guru_title[]');
            addDynamicFieldValidationGuru('guru_description[]');
            addDynamicFieldValidationGuru('guru_video[]');
            FormValidation2.validate();
            break;

          case 2:
            addDynamicFieldValidationModule('module_title[]');
            addDynamicFieldValidationModule('module_description[]');
            addDynamicFieldValidationModule('module_video[]');
            FormValidation3.validate();
            break;
            case 3:
              FormValidation4.validate();
              break;

          default:
            break;
        }
      });
    });
    wizardValidationSkip.forEach(item => {
      item.addEventListener('click', event => {
        // When click the Next button, we will validate the current step
        switch (validationStepper._currentIndex) {
          case 0:
            
            validationStepper.next();
            break;

          case 1:
            guruClass.value ="false";
            validationStepper.next();
            break;

          case 2:
            moduleClass.value ="false";
            validationStepper.next();
            break;
            case 3:
              liveClass.value ="false";
                submitForm();
              break;
             
          default:
            break;
        }
      });
    });

    wizardValidationPrev.forEach(item => {
      item.addEventListener('click', event => {
        switch (validationStepper._currentIndex) {
          case 3:
            validationStepper.previous();
            break;
          case 2:
            validationStepper.previous();
            break;

          case 1:
            validationStepper.previous();
            break;

          case 0:

          default:
            break;
        }
      });
    });
  }
})();
