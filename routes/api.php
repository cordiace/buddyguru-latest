<?php
use App\Http\Controllers\Admin\RegionController;
use App\Http\Controllers\Admin\CountryController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\SkillController;
use App\Http\Controllers\Admin\VideoController;
use App\Http\Controllers\Admin\TutorController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\ChatController;
use App\Http\Controllers\SplashScreenController;
use App\Http\Controllers\Admin\AppliedGuruController;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\GeneralsettingController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('tutor-video',[TutorController::class, 'storeTestVideo']);

Route::get('module-list/{empid}',[TutorController::class, 'showModule'])->name('module.view');
// Route::middleware('auth')->get('get-regions', function (Request $request) {
//     return response()->json([        'message' => 'This API is protected by authentication'    ]);
// });
Route::post('video-upload', [AppliedGuruController::class, 'upload']);
Route::post('splash-screen', [SplashScreenController::class, 'create']);

Route::post('send-chat',[ChatController::class, 'sendMessage']);
Route::post('reply-chat',[ChatController::class, 'replyToMessage']);

//api getRegions
Route::get('get-regions',[RegionController::class, 'getRegions']);
Route::get('get-countries',[CountryController::class, 'getCountry']);
Route::get('get-categories',[CategoryController::class, 'getCategory']);
Route::get('get-skills',[SkillController::class, 'getSkills']);
Route::get('get-videos',[VideoController::class, 'getVideos']);
Route::get('guru-list',[TutorController::class, 'tutorList']);
Route::get('guru-profile',[TutorController::class, 'guruProfile']);
Route::get('get-popular-guru-list',[TutorController::class, 'getPopularGuruList']);

Route::get('get-classes',[TutorController::class, 'getClasses']);
// Route::get('get-module-class',[TutorController::class, 'getModuleClass']);
// Route::get('get-live-class',[TutorController::class, 'getLiveClass']);
Route::get('get-access-token', 'CommonController@getAccessToken');
//payments
Route::post('get-payment',[ApiController::class,'getPayments']);

Route::get('get-terms-and-policy',[ApiController::class,'getTermsAndPolicy']);

Route::get('get-my-meetings',[TutorController::class, 'getMyMeetings']);
Route::get('get-my-classes',[TutorController::class, 'getMyClasses']);
Route::get('get-my-subscriptions',[TutorController::class, 'getMySubscriptions']);

Route::post('guru-register',[TutorController::class, 'tutorRegister']);
Route::post('buddy-register',[UserController::class, 'buddyRegister']);
Route::post('user-login',[ApiController::class,'userLogin']);
Route::post('forgot-password', [ApiController::class,'forgotPassword']);
//signup with goggle
Route::post('/google/signup', [LoginController::class, 'googleSignup']);
Route::post('/auth/google', [LoginController::class, 'redirectToGoogle']);
Route::post('/auth/google/callback', [LoginController::class, 'handleGoogleCallback']);
// Route::post('buddy-login',[UserController::class, 'login']);
Route::post('applied-guru',[AppliedGuruController::class, 'appliedGuru']);

Route::get('get-tier-management',[GeneralsettingController::class, 'getTier']);

Route::get('get-currency-setting',[GeneralsettingController::class, 'getCurrency']);
Route::get('get-currency-range',[GeneralsettingController::class, 'getCurrencyRange']);


Route::get('tutor-upcoming', 'ApiController@tutorUpcomingSessions');
Route::get('tutor-completed', 'ApiController@tutorCompletedSessions');
Route::get('tutor-all-session', 'ApiController@tutorAllSessions');
Route::get('tutor-session-by-id', 'ApiController@tutorSessionsById');
Route::get('tutor-upcoming-sessions', 'ApiController@tutorSessions');
Route::get('student-sessions', 'ApiController@studentSessions');
Route::get('student-booking', 'ApiController@studentBatches');


Route::get('get-cms', 'ApiController@getCms');
Route::get('recommended-tutors', 'ApiController@recommendedTutors');
// Route::get('suggested-videos', 'ApiController@suggestedVideos');
// Route::post('upload-image', 'ApiController@uploadImage');
Route::get('search-sessions', 'ApiController@searchSessions');
Route::post('payment-create', 'ApiController@createPayment');
Route::post('payment-refund', 'ApiController@createRefund');
Route::post('otp-verification', 'ApiController@verifyOtp');
Route::post('logout', [UserController::class,'logoutApi']);

Route::post('search-tutor',[ApiController::class,'searchPortfolio']);
Route::post('add-rating', 'ApiController@addRating');



// Route::post('create-batch', 'ApiController@createBatch');
// Route::post('update-batch', 'ApiController@updateBatch');
// Route::get('delete-batch', 'ApiController@deleteBatch');
// Route::get('get-batches', 'ApiController@getBatches');
// Route::get('get-batches-by-tutor', 'ApiController@getBatchesByTutor');
// Route::get('get-batch-by-id', 'ApiController@getBatchById');
// Route::get('get-video-by-category', 'ApiController@getVideoByCategory');

// Route::post('create-session', 'ApiController@createSession');
// Route::post('update-session', 'ApiController@updateSession');
// Route::get('delete-session', 'ApiController@deleteSession');
// Route::get('get-session', 'ApiController@getSessions');
// Route::get('get-all-sessions', 'ApiController@getAllSessions');
// Route::get('get-upcoming-sessions', 'ApiController@getAllUpcomingSessions');
// Route::get('get-banners', 'ApiController@getBanners');
// Route::get('get-session-by-id', 'ApiController@getSessionById');
// Route::get('get-session-by-batch', 'ApiController@getSessionByBatch');
Route::post('create-review', 'ApiController@createReview');

Route::post('subscribe-tutor',[ApiController::class,'subscribeTutor']);
Route::get('get-subscribed-tutor',[ApiController::class,'getSubscriber']);
Route::get('get-all-subscribed-tutor', [ApiController::class,'getAllSubscriber']);

Route::post('create-meeting', 'ApiController@createMeeting');
Route::get('zoom-callback', 'ApiController@zoomCallback');

// Get list of meetings.
Route::get('/meetings', 'Zoom\MeetingController@list');

// Create meeting room using topic, agenda, start_time.
Route::post('/meetings', 'Zoom\MeetingController@create');

// Get information of the meeting room by ID.
Route::get('/meetings/{id}', 'Zoom\MeetingController@get')->where('id', '[0-9]+');
Route::patch('/meetings/{id}', 'Zoom\MeetingController@update')->where('id', '[0-9]+');
Route::delete('/meetings/{id}', 'Zoom\MeetingController@delete')->where('id', '[0-9]+');

