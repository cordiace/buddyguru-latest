<?php

use App\Http\Controllers\Admin\TutorController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$controller_path = 'App\Http\Controllers';
Route::get('/test',function(){
dd(phpinfo());
});

Route::get('/page-2', $controller_path . '\pages\Page2@index')->name('pages-page-2');

// pages
Route::get('/pages/misc-error', $controller_path . '\pages\MiscError@index')->name('pages-misc-error');

// authentication
Route::get('/',$controller_path . '\Auth\LoginController@showLoginForm')->name('auth-login-check');
// Route::get('/', $controller_path . '\Auth\LoginController@index')->name('auth-login-check');
Route::post('/login', $controller_path . '\Auth\LoginController@login')->name('login');

Route::get('/logout', $controller_path . '\Auth\LoginController@logout')->name('logout');

Route::get('/auth/register-basic', $controller_path . '\authentications\RegisterBasic@index')->name('auth-register-basic');

// Route::middleware(['auth'])->group(function () {
    Route::group(['middleware' => 'auth'], function () {
    Route::group(['middleware' => 'isadmin'], function () {
    $controller_path = 'App\Http\Controllers';
// Main Page Route
Route::get('/dashboard', $controller_path . '\pages\HomePage@index')->name('welcome');
//video store section
Route::post('/file-upload/profile',  $controller_path . '\Admin\TutorController@storeProfileVideo')->name('profile.StoreVideo');
// Route::post('/file-upload/{id}',  $controller_path . '\Admin\TutorController@storePortFolioVideo')->name('portfolio.StoreVideo');
Route::post('/file-upload/package',  $controller_path . '\Admin\TutorController@storePackageVideo')->name('package.StoreVideo');
Route::post('/file-upload/module',  $controller_path . '\Admin\TutorController@storeModuleVideo')->name('module.StoreVideo');
Route::post('/file-upload/live', $controller_path . '\Admin\TutorController@storeLiveVideo')->name('live.StoreVideo');
//skills and category
// Route::resource('/admin/category',  $controller_path . '\Admin\CategoryController')->except(['show']); 
// Route::get('/admin/category/delete/{id}',   $controller_path . '\Admin\CategoryController@delete')->name('category.delete');
Route::get('/admin/category',   $controller_path . '\Admin\CategoryController@index')->name('cms-category.index');
Route::get('/admin/category/create',   $controller_path . '\Admin\CategoryController@create')->name('cms-category.create');
Route::post('/admin/category/store',   $controller_path . '\Admin\CategoryController@store')->name('cms-category.store');
Route::get('/admin/category/edit/{id}',   $controller_path . '\Admin\CategoryController@edit')->name('cms-category.edit');
Route::post('/admin/category/update/{id}',   $controller_path . '\Admin\CategoryController@update')->name('cms-category.update');
Route::get('/admin/category/delete/{id}',   $controller_path . '\Admin\CategoryController@delete')->name('cms-category.delete');

//applied gurus
Route::get('/admin/applied/gurus/approve/{id}/{val}',  $controller_path . '\Admin\AppliedGuruController@approve')->name('cms-applied_gurus.approve');
Route::post('/admin/applied/gurus/video/save',  $controller_path . '\Admin\AppliedGuruController@storeGuruVideo')->name('applied_gurus.storeVideo');
Route::get('/admin/applied/gurus',   $controller_path . '\Admin\AppliedGuruController@index')->name('cms-applied_gurus.index');
Route::get('/admin/applied/gurus/create',   $controller_path . '\Admin\AppliedGuruController@create')->name('cms-applied_gurus.create');
Route::post('/admin/applied/gurus/store',   $controller_path . '\Admin\AppliedGuruController@store')->name('cms-applied_gurus.store');
Route::get('/admin/applied/gurus/edit/{id}',   $controller_path . '\Admin\AppliedGuruController@edit')->name('cms-applied_gurus.edit');
Route::post('/admin/applied/gurus/update/{id}',   $controller_path . '\Admin\AppliedGuruController@update')->name('cms-applied_gurus.update');
Route::get('/admin/applied/gurus/delete/{id}',   $controller_path . '\Admin\AppliedGuruController@delete')->name('cms-applied_gurus.delete');

//trending videos

Route::post('/trendingVideo/save',  $controller_path . '\Admin\VideoController@storeTrendingVideo')->name('trendingVideo.save');
Route::get('/admin/videos',   $controller_path . '\Admin\VideoController@index')->name('cms-videos.index');
Route::get('/admin/videos/create',   $controller_path . '\Admin\VideoController@create')->name('cms-videos.create');
Route::post('/admin/videos/store',   $controller_path . '\Admin\VideoController@store')->name('cms-videos.store');
Route::get('/admin/videos/edit/{id}',   $controller_path . '\Admin\VideoController@edit')->name('cms-videos.edit');
Route::post('/admin/videos/update/{id}',   $controller_path . '\Admin\VideoController@update')->name('cms-videos.update');
Route::get('/admin/videos/delete/{id}',   $controller_path . '\Admin\VideoController@delete')->name('cms-videos.delete');

Route::resource('/admin/skill',   $controller_path . '\Admin\SkillController')->except(['show']);
Route::get('/admin/skill/',   $controller_path . '\Admin\SkillController@index')->name('cms-skill.index');
Route::get('/admin/skill/create',   $controller_path . '\Admin\SkillController@create')->name('cms-skill.create');
Route::post('/admin/skill/store',   $controller_path . '\Admin\SkillController@store')->name('cms-skill.store');
Route::get('/admin/skill/edit/{id}',   $controller_path . '\Admin\SkillController@edit')->name('cms-skill.edit');
Route::post('/admin/skill/update/{id}',   $controller_path . '\Admin\SkillController@update')->name('cms-skill.update');
Route::get('/admin/skill/delete/{id}',   $controller_path . '\Admin\SkillController@delete')->name('cms-skill.delete');
// Route::get('/admin/skill/delete/{id}',   $controller_path . '\Admin\SkillController@delete')->name('skill.delete');

//region and country
// Route::resource('/admin/region',   $controller_path .'\Admin\RegionController')->except(['show']);
Route::get('/admin/region',  $controller_path .'\Admin\RegionController@index')->name('cms-region.index');
Route::get('/admin/region/create',   $controller_path .'\Admin\RegionController@create')->name('cms-region.create');
Route::post('/admin/region/store',   $controller_path .'\Admin\RegionController@store')->name('cms-region.store');
Route::get('/admin/region/edit/{id}',   $controller_path .'\Admin\RegionController@edit')->name('cms-region.edit');
Route::post('/admin/region/update/{id}',   $controller_path .'\Admin\RegionController@update')->name('cms-region.update');
Route::get('/admin/region/delete/{id}',   $controller_path .'\Admin\RegionController@delete')->name('cms-region.delete');
// Route::get('/admin/region/delete/{id}',   $controller_path .'\Admin\RegionController@delete')->name('region.delete');
//country
// Route::resource('/admin/country',   $controller_path .'\Admin\CountryController')->except(['show']);
Route::get('/admin/country',  $controller_path .'\Admin\CountryController@index')->name('cms-country.index');
Route::get('/admin/country/create',   $controller_path .'\Admin\CountryController@create')->name('cms-country.create');
Route::post('/admin/country/store',   $controller_path .'\Admin\CountryController@store')->name('cms-country.store');
Route::get('/admin/country/edit/{id}',   $controller_path .'\Admin\CountryController@edit')->name('cms-country.edit');
Route::post('/admin/country/update/{id}',   $controller_path .'\Admin\CountryController@update')->name('cms-country.update');
Route::get('/admin/country/delete/{id}',   $controller_path .'\Admin\CountryController@delete')->name('cms-country.delete');
//skill
Route::get('/admin/tutor/skills',  $controller_path .'\Admin\TutorController@getSkills')->name('skills');
//User
Route::resource('/admin/user', $controller_path .'\Admin\UserController');
Route::post('/admin/update/{id}', $controller_path .'\Admin\UserController@update')->name('user.update');
Route::get('/admin/delete', $controller_path .'\Admin\UserController@delete')->name('user.delete');
//admin tutor profile
Route::resource('/admin/tutor',  $controller_path .'\Admin\TutorController');
Route::post('/admin/tutor/store',  $controller_path .'\Admin\TutorController@store')->name('tutor.store');
Route::get('/admin/tutor/edit/{id}',  $controller_path .'\Admin\TutorController@edit')->name('admin.tutor.edit');
Route::post('/admin/tutor/update/{id}',  $controller_path .'\Admin\TutorController@update')->name('admin.tutor.update');
Route::get('/admin/tutor/delete/{id}',  $controller_path .'\Admin\TutorController@destroy')->name('admin.tutor.delete');


Route::get('/admin/tutor/portfolio/create/{id}/{showNext}', $controller_path .'\Admin\TutorController@createPortfolio')->name('tutor.portfolio.create');
Route::post('/admin/tutor/portfolio/{showNext}', $controller_path .'\Admin\TutorController@storePortfolio')->name('tutor.portfolio.store');
Route::get('/admin/tutor/portfolio/{id}/edit',  $controller_path .'\Admin\TutorController@editPortfolio')->name('tutor.portfolio.edit');
Route::post('/admin/tutor/portfolio/update/{id}',  $controller_path .'\Admin\TutorController@updatePortfolio')->name('tutor.portfolio.update');
Route::get('/admin/tutor/portfolio/delete/{id}', $controller_path .'\Admin\TutorController@deletePortfolio')->name('tutor.portfolio.delete');

// Route::get('/admin/tutor/package/{id}', $controller_path .'\Admin\TutorController@viewPackage')->name('tutor.package.view');
Route::get('/admin/tutor/package/view/{id}', $controller_path .'\Admin\TutorController@showPackage')->name('tutor.guru.show');
Route::get('/admin/tutor/package/create/{id}', $controller_path .'\Admin\TutorController@createPackage')->name('tutor.package.create');
Route::get('/admin/tutor/package/{id}/edit', $controller_path .'\Admin\TutorController@editPackage')->name('tutor.package.edit');
Route::post('/admin/tutor/package/store', $controller_path .'\Admin\TutorController@storePackage')->name('tutor.package.store');
Route::post('/admin/tutor/package/update/{id}', $controller_path .'\Admin\TutorController@updatePackage')->name('tutor.package.update');
Route::get('/admin/tutor/package/delete/{id}', $controller_path .'\Admin\TutorController@deletePackage')->name('tutor.package.delete');

Route::get('/admin/tutor/module/view/{empid}', [TutorController::class, 'showModule'])->name('tutor.module.show');
// Route::get('/admin/tutor/module/{id}',$controller_path .'\Admin\TutorController@viewModule')->name('tutor.module.view');
Route::get('/admin/tutor/module/create/{id}', $controller_path .'\Admin\TutorController@createModule')->name('tutor.module.create');
Route::get('/admin/tutor/module/{id}/edit', $controller_path .'\Admin\TutorController@editModule')->name('tutor.module.edit');
Route::post('/admin/tutor/module/store', $controller_path .'\Admin\TutorController@storeModule')->name('tutor.module.store');
Route::post('/admin/tutor/module/update/{id}', $controller_path .'\Admin\TutorController@updateModule')->name('tutor.module.update');
Route::get('/admin/tutor/module/delete/{id}',$controller_path .'\Admin\TutorController@deleteModule')->name('tutor.module.delete');

Route::get('/admin/tutor/liveclass/view/{id}', $controller_path .'\Admin\TutorController@showLiveClass')->name('tutor.live_class.show');
// Route::get('/admin/tutor/liveclass/{id}', 'Admin\TutorController@viewLiveClass')->name('tutor.live_class.view');
Route::get('/admin/tutor/liveclass/create/{id}', $controller_path .'\Admin\TutorController@createLiveClass')->name('tutor.live_class.create');
Route::post('/admin/tutor/liveclass/store', $controller_path .'\Admin\TutorController@storeLiveClass')->name('tutor.live_class.store');
Route::get('/admin/tutor/liveclass/edit/{id}', $controller_path .'\Admin\TutorController@editLiveClass')->name('tutor.live_class.edit');
Route::post('/admin/tutor/liveclass/update/{id}', $controller_path .'\Admin\TutorController@updateLiveClass')->name('tutor.live_class.update');
Route::get('/admin/tutor/liveclass/delete/{id}', $controller_path .'\Admin\TutorController@deleteBatch')->name('tutor.live_class.delete');
//commision
// Route::resource('/admin/generalsetting',  $controller_path .'\Admin\GeneralsettingController')->except(['show']);
Route::get('/admin/generalsetting/',  $controller_path .'\Admin\GeneralsettingController@index')->name('general-generalsetting.index');
Route::get('/admin/generalsetting/create',  $controller_path .'\Admin\GeneralsettingController@create')->name('general-generalsetting.create');
Route::post('/admin/generalsetting/store',   $controller_path .'\Admin\GeneralsettingController@store')->name('general-generalsetting.store');
Route::get('/admin/generalsetting/edit/{id}',  $controller_path .'\Admin\GeneralsettingController@edit')->name('general-generalsetting.edit');
Route::post('/admin/generalsetting/update/{id}',   $controller_path .'\Admin\GeneralsettingController@update')->name('general-generalsetting.update');
Route::get('/admin/generalsetting/delete/{id}',  $controller_path .'\Admin\GeneralsettingController@destroy')->name('general-generalsetting.delete');
//general settings
Route::get('/admin/generalsetting/view-currency', $controller_path .'\Admin\GeneralsettingController@viewCurrency')->name('general-admin.settings.viewCurrency');
Route::post('/admin/generalsetting/view-currency/store',  $controller_path .'\Admin\GeneralsettingController@currencyStore')->name('general-settings.currencyStore');
Route::post('/admin/generalsetting/view-currency/update', $controller_path .'\Admin\GeneralsettingController@currencyUpdate')->name('general-settings.currencyUpdate');

Route::get('/admin/generalsetting/view-currency/create',  $controller_path .'\Admin\GeneralsettingController@createCurrency')->name('general-admin.currency_settings.createCurrency');
Route::post('/admin/generalsetting/storeCurrency',   $controller_path .'\Admin\GeneralsettingController@storeCurrency')->name('general-admin.currency_settings.storeCurrency');
Route::get('/admin/generalsetting/view-currency/{id}/edit',  $controller_path .'\Admin\GeneralsettingController@editCurrency')->name('general-admin.currency_settings.editCurrency');
Route::post('/admin/generalsetting/updateCurrency/{id}',   $controller_path .'\Admin\GeneralsettingController@updateCurrency')->name('general-admin.currency_settings.updateCurrency');
Route::get('/admin/generalsetting/deleteCurrency/{id}',  $controller_path .'\Admin\GeneralsettingController@deleteCurrency')->name('general-admin.currency_settings.deleteCurrency');
//Tier
Route::get('/admin/generalsetting/tier',   $controller_path .'\Admin\GeneralsettingController@get_tier')->name('general-admin.tier.index');
Route::get('/admin/generalsetting/tier/create',   $controller_path .'\Admin\GeneralsettingController@createTier')->name('general-admin.tier.create');
Route::post('/admin/generalsetting/tier',   $controller_path .'\Admin\GeneralsettingController@storeTier')->name('general-admin.tier.storeTier');
Route::get('/admin/generalsetting/tier/{id}/edit',  $controller_path .'\Admin\GeneralsettingController@editTier')->name('general-admin.tier.edit');
Route::post('/admin/generalsetting/tier/update/{id}',  $controller_path .'\Admin\GeneralsettingController@updateTier')->name('general-admin.tier.update');
Route::get('/admin/generalsetting/tier/delete/{id}',   $controller_path .'\Admin\GeneralsettingController@deleteTier')->name('general-admin.tier.delete');
//currency Range Mapping
Route::get('/admin/generalsetting/tier/currencyRange',   $controller_path .'\Admin\GeneralsettingController@view_currency_range')->name('general-admin.tier.currency_range.index');
Route::get('/admin/generalsetting/tier/currencyRange/create',   $controller_path .'\Admin\GeneralsettingController@createTierCurrency')->name('general-admin.tier.currency_range.create');
Route::post('/admin/generalsetting/tier/currencyRange/store',   $controller_path .'\Admin\GeneralsettingController@StoreTierCurrency')->name('general-admin.tier.currency_range.store');
Route::get('/admin/generalsetting/tier/currencyRange/{id}/edit',   $controller_path .'\Admin\GeneralsettingController@editTierCurrency')->name('general-admin.tier.currency_range.edit');
Route::post('/admin/generalsetting/tier/currencyRange/update/{id}',   $controller_path .'\Admin\GeneralsettingController@updateTierCurrency')->name('general-admin.tier.currency_range.update');
Route::get('/admin/generalsetting/tier/currencyRange/delete/{id}',   $controller_path .'\Admin\GeneralsettingController@deleteTierCurrency')->name('general-admin.tier.currency_range.delete');
});
});
Auth::routes();


