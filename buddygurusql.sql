-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 14, 2023 at 11:21 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `buddyguru`
--

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `location` varchar(191) DEFAULT NULL,
  `image` varchar(191) DEFAULT NULL,
  `heading` text NOT NULL,
  `description` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `location`, `image`, `heading`, `description`) VALUES
(2, 'buddy-home', 'uploads/0gQW0BKksdE99Q5BqoJY5nrLudrLmeGSOla4Gbp8.png', '', ''),
(4, 'guru-home', 'uploads/P4gUteL0rOkOwriO2VJHhVB5HDHfuKZ9wJCDITcU.jpeg', 'Heading 2', 'Description 2');

-- --------------------------------------------------------

--
-- Table structure for table `batches`
--

CREATE TABLE `batches` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `batch_name` varchar(191) NOT NULL,
  `description` varchar(191) DEFAULT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `duration` int(11) NOT NULL,
  `no_of_participants` int(11) NOT NULL,
  `no_of_sessions` int(11) NOT NULL,
  `payment_type` varchar(191) DEFAULT NULL,
  `subscription_fee` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `batches`
--

INSERT INTO `batches` (`id`, `user_id`, `package_id`, `batch_name`, `description`, `start`, `end`, `duration`, `no_of_participants`, `no_of_sessions`, `payment_type`, `subscription_fee`, `created_at`, `updated_at`) VALUES
(39, 195, 0, 'Master', 'basic dance batch', '2022-03-12', '2022-03-31', 10, 10, 10, 'monthly', 1500, '2022-03-09 05:37:58', '2022-03-09 05:37:58'),
(40, 195, 0, 'Module', NULL, '2022-03-01', '2022-03-31', 30, 10, 10, 'monthly', 1000, '2022-03-11 11:41:44', '2022-03-11 11:41:44');

-- --------------------------------------------------------

--
-- Table structure for table `batch_package`
--

CREATE TABLE `batch_package` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `batch_package`
--

INSERT INTO `batch_package` (`id`, `package_id`, `batch_id`) VALUES
(70, 76, 39),
(71, 77, 40),
(95, 97, 63);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_name` varchar(256) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Dance', 1, '2021-03-12 00:24:31', '2022-12-16 12:37:42'),
(4, 'Painting', 1, '2021-03-12 00:24:49', '2022-12-16 12:37:42'),
(5, 'Sculpture', 1, '2021-03-12 00:24:58', '2022-12-16 12:37:42'),
(6, 'Drawing', 1, '2021-12-17 10:31:11', '2022-12-16 12:37:42'),
(9, 'musics', 1, '2023-04-08 02:13:44', '2023-04-08 02:13:52');

-- --------------------------------------------------------

--
-- Table structure for table `category_skills`
--

CREATE TABLE `category_skills` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `skill_name` varchar(180) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_skills`
--

INSERT INTO `category_skills` (`id`, `category_id`, `skill_name`) VALUES
(4, 4, 'carmel'),
(6, 2, 'Kuchipudi\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `class_id` bigint(20) NOT NULL,
  `class_name` varchar(500) NOT NULL,
  `description` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`class_id`, `class_name`, `description`) VALUES
(1, ' Guru Class', ''),
(2, 'Module Class', ''),
(3, 'Live Class', '');

-- --------------------------------------------------------

--
-- Table structure for table `cms_fields`
--

CREATE TABLE `cms_fields` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cms_heading` varchar(256) NOT NULL,
  `cms_description` text NOT NULL,
  `cms_image` varchar(256) NOT NULL,
  `display_order` tinyint(4) NOT NULL,
  `button_text` varchar(256) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_fields`
--

INSERT INTO `cms_fields` (`id`, `cms_heading`, `cms_description`, `cms_image`, `display_order`, `button_text`, `status`, `created_at`, `updated_at`) VALUES
(1, 'heading2', 'desc<br>1322', 'cms/53O3niauhOiZPvey01wrlhCZB8XMAlm6qqP9cWyx.png', 2, 'button2', 1, '2021-03-12 05:22:52', '2022-12-16 12:37:42');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `region_id` int(11) NOT NULL,
  `country_name` varchar(256) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `region_id`, `country_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'India', 1, '2021-03-12 05:00:22', '2023-03-17 11:48:25'),
(3, 1, 'USA', 1, '2021-03-12 05:00:44', '2023-03-17 11:48:32'),
(4, 1, 'United Kingdom', 1, '2021-03-12 05:00:50', '2023-03-17 11:48:36'),
(5, 1, 'Singapore', 1, '2021-03-12 05:00:56', '2023-03-17 11:48:39'),
(6, 1, 'Afghanistan', 1, '2022-10-06 05:41:24', '2023-04-08 05:03:41'),
(7, 1, 'Aland Islands', 1, '2022-10-06 05:44:22', '2023-03-17 11:48:48'),
(8, 1, 'Albania', 1, '2022-10-06 05:44:22', '2023-03-17 11:48:52'),
(9, 1, 'Algeria', 1, '2022-10-06 05:44:22', '2023-03-17 11:48:56'),
(10, 1, 'American Samoa', 1, '2022-10-06 05:44:22', '2023-03-17 11:49:06'),
(11, 1, 'Andorra', 1, '2022-10-06 05:44:22', '2023-03-17 11:49:21'),
(12, 1, 'Angola', 1, '2022-10-06 05:44:22', '2023-03-17 11:49:16'),
(13, 1, 'Anguilla', 1, '2022-10-06 05:44:22', '2023-03-17 11:49:12'),
(14, 1, 'Antarctica', 1, '2022-10-06 05:44:22', '2023-03-17 11:49:25'),
(15, 1, 'Antigua and Barbuda', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(16, 1, 'Argentina', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(17, 1, 'Armenia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(18, 1, 'Aruba', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(19, 1, 'Australia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(20, 1, 'Austria', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(21, 1, 'Azerbaijan', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(22, 1, 'Bahamas', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(23, 1, 'Bahrain', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(24, 1, 'Bangladesh', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(25, 1, 'Barbados', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(26, 1, 'Belarus', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(27, 1, 'Belgium', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(28, 1, 'Belize', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(29, 1, 'Benin', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(30, 1, 'Bermuda', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(31, 1, 'Bhutan', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(32, 1, 'Bolivia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(33, 1, 'Bonaire, Sint Eustatius and Saba', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(34, 1, 'Bosnia and Herzegovina', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(35, 1, 'Botswana', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(36, 1, 'Bouvet Island', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(37, 1, 'Brazil', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(38, 1, 'British Indian Ocean Territory', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(39, 1, 'Brunei', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(40, 1, 'Bulgaria', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(41, 1, 'Burkina Faso', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(42, 1, 'Burundi', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(43, 1, 'Cambodia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(44, 1, 'Cameroon', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(45, 1, 'Canada', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(46, 1, 'Cape Verde', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(47, 1, 'Cayman Islands', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(48, 1, 'Central African Republic', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(49, 1, 'Chad', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(50, 1, 'Chile', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(51, 1, 'China', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(52, 1, 'Christmas Island', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(53, 1, 'Cocos (Keeling) Islands', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(54, 1, 'Colombia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(55, 1, 'Comoros', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(56, 1, 'Congo', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(57, 1, 'Cook Islands', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(58, 1, 'Costa Rica', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(59, 1, 'Ivory Coast', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(60, 1, 'Croatia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(61, 1, 'Cuba', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(62, 1, 'Curacao', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(63, 1, 'Cyprus', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(64, 1, 'Czech Republic', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(65, 1, 'Democratic Republic of the Congo', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(66, 1, 'Denmark', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(67, 1, 'Djibouti', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(68, 1, 'Dominica', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(69, 1, 'Dominican Republic', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(70, 1, 'Ecuador', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(71, 1, 'Egypt', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(72, 1, 'El Salvador', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(73, 1, 'Equatorial Guinea', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(74, 1, 'Eritrea', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(75, 1, 'Estonia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(76, 1, 'Ethiopia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(77, 1, 'Falkland Islands (Malvinas)', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(78, 1, 'Faroe Islands', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(79, 1, 'Fiji', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(80, 1, 'Finland', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(81, 3, 'France', 1, '2022-10-06 05:44:22', '2023-03-22 05:53:02'),
(82, 1, 'French Guiana', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(83, 1, 'French Polynesia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(84, 1, 'French Southern Territories', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(85, 1, 'Gabon', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(86, 1, 'Gambia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(87, 1, 'Georgia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(88, 1, 'Germany', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(89, 1, 'Ghana', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(90, 1, 'Gibraltar', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(91, 1, 'Greece', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(92, 1, 'Greenland', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(93, 1, 'Grenada', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(94, 1, 'Guadaloupe', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(95, 1, 'Guam', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(96, 1, 'Guatemala', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(97, 1, 'Guernsey', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(98, 1, 'Guinea', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(99, 1, 'Guinea-Bissau', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(100, 1, 'Guyana', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(101, 1, 'Haiti', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(102, 1, 'Heard Island and McDonald Islands', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(103, 1, 'Honduras', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(104, 1, 'Hong Kong', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(105, 1, 'Hungary', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(106, 1, 'Iceland', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(107, 1, 'Indonesia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(108, 1, 'Iran', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(109, 1, 'Iraq', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(110, 1, 'Ireland', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(111, 1, 'Isle of Man', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(112, 1, 'Israel', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(113, 1, 'Italy', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(114, 1, 'Jamaica', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(115, 1, 'Japan', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(116, 1, 'Jersey', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(117, 1, 'Jordan', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(118, 1, 'Kazakhstan', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(119, 1, 'Kenya', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(120, 1, 'Kiribati', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(121, 1, 'Kosovo', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(122, 1, 'Kuwait', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(123, 1, 'Kyrgyzstan', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(124, 1, 'Laos', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(125, 1, 'Latvia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(126, 1, 'Lebanon', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(127, 1, 'Lesotho', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(128, 1, 'Liberia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(129, 1, 'Libya', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(130, 1, 'Liechtenstein', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(131, 1, 'Lithuania', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(132, 1, 'Luxembourg', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(133, 1, 'Macao', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(134, 1, 'Macedonia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(135, 1, 'Madagascar', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(136, 1, 'Malawi', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(137, 1, 'Malaysia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(138, 1, 'Maldives', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(139, 1, 'Mali', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(140, 1, 'Malta', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(141, 1, 'Marshall Islands', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(142, 1, 'Martinique', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(143, 1, 'Mauritania', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(144, 1, 'Mauritius', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(145, 1, 'Mayotte', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(146, 1, 'Mexico', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(147, 1, 'Micronesia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(148, 1, 'Moldava', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(149, 1, 'Monaco', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(150, 1, 'Mongolia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(151, 1, 'Montenegro', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(152, 1, 'Montserrat', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(153, 1, 'Morocco', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(154, 1, 'Mozambique', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(155, 1, 'Myanmar (Burma)', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(156, 1, 'Namibia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(157, 1, 'Nauru', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(158, 1, 'Nepal', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(159, 1, 'Netherlands', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(160, 1, 'New Caledonia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(161, 1, 'New Zealand', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(162, 1, 'Nicaragua', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(163, 1, 'Niger', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(164, 1, 'Nigeria', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(165, 1, 'Niue', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(166, 1, 'Norfolk Island', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(167, 1, 'North Korea', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(168, 1, 'Northern Mariana Islands', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(169, 1, 'Norway', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(170, 1, 'Oman', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(171, 1, 'Pakistan', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(172, 1, 'Palau', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(173, 1, 'Palestine', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(174, 1, 'Panama', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(175, 1, 'Papua New Guinea', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(176, 1, 'Paraguay', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(177, 1, 'Peru', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(178, 1, 'Phillipines', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(179, 1, 'Pitcairn', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(180, 1, 'Poland', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(181, 1, 'Portugal', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(182, 1, 'Puerto Rico', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(183, 1, 'Qatar', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(184, 1, 'Reunion', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(185, 1, 'Romania', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(186, 1, 'Russia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(187, 1, 'Rwanda', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(188, 1, 'Saint Barthelemy', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(189, 1, 'Saint Helena', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(190, 1, 'Saint Kitts and Nevis', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(191, 1, 'Saint Lucia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(192, 1, 'Saint Martin', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(193, 1, 'Saint Pierre and Miquelon', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(194, 1, 'Saint Vincent and the Grenadines', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(195, 1, 'Samoa', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(196, 1, 'San Marino', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(197, 1, 'Sao Tome and Principe', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(198, 1, 'Saudi Arabia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(199, 1, 'Senegal', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(200, 1, 'Serbia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(201, 1, 'Seychelles', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(202, 1, 'Sierra Leone', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(203, 1, 'Sint Maarten', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(204, 1, 'Slovakia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(205, 1, 'Slovenia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(206, 1, 'Solomon Islands', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(207, 1, 'Somalia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(208, 1, 'South Africa', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(209, 1, 'South Georgia and the South Sandwich Islands', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(210, 1, 'South Korea', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(211, 1, 'South Sudan', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(212, 1, 'Spain', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(213, 1, 'Sri Lanka', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(214, 1, 'Sudan', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(215, 1, 'Suriname', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(216, 1, 'Svalbard and Jan Mayen', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(217, 1, 'Swaziland', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(218, 1, 'Sweden', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(219, 1, 'Switzerland', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(220, 1, 'Syria', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(221, 1, 'Taiwan', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(222, 1, 'Tajikistan', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(223, 1, 'Tanzania', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(224, 1, 'Thailand', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(225, 1, 'Timor-Leste (East Timor)', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(226, 1, 'Togo', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(227, 1, 'Tokelau', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(228, 1, 'Tonga', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(229, 1, 'Trinidad and Tobago', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(230, 1, 'Tunisia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(231, 1, 'Turkey', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(232, 1, 'Turkmenistan', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(233, 1, 'Turks and Caicos Islands', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(234, 1, 'Tuvalu', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(235, 1, 'Uganda', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(236, 1, 'Ukraine', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(237, 1, 'United Arab Emirates', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(238, 1, 'United States Minor Outlying Islands', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(239, 1, 'Uruguay', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(240, 1, 'Uzbekistan', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(241, 1, 'Vanuatu', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(242, 1, 'Vatican City', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(243, 1, 'Venezuela', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(244, 1, 'Vietnam', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(245, 1, 'Virgin Islands, British', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(246, 1, 'Virgin Islands, US', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(247, 1, 'Wallis and Futuna', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(248, 1, 'Western Sahara', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(249, 1, 'Yemen', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(250, 1, 'Zambia', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18'),
(251, 1, 'Zimbabwe', 1, '2022-10-06 05:44:22', '2023-03-17 11:50:18');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(11) NOT NULL,
  `label` varchar(120) NOT NULL,
  `name` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `currency_range_tier`
--

CREATE TABLE `currency_range_tier` (
  `id` int(11) NOT NULL,
  `tier_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `price_range` varchar(120) NOT NULL,
  `update_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currency_range_tier`
--

INSERT INTO `currency_range_tier` (`id`, `tier_id`, `class_id`, `price_range`, `update_date`) VALUES
(2, 2, 2, '40 -90', '2023-04-10'),
(3, 2, 2, '40 -80', '2023-02-24'),
(4, 2, 2, '40 -80', '2023-02-24'),
(5, 2, 2, 'usd', '2023-03-10'),
(6, 1, 1, '10-20', '2023-03-16'),
(9, 3, 2, '20', '2023-03-16'),
(10, 3, 1, '200', '2023-03-20');

-- --------------------------------------------------------

--
-- Table structure for table `currency_settings`
--

CREATE TABLE `currency_settings` (
  `id` bigint(20) NOT NULL,
  `symbol` varchar(50) NOT NULL,
  `value` varchar(50) NOT NULL,
  `update_date` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currency_settings`
--

INSERT INTO `currency_settings` (`id`, `symbol`, `value`, `update_date`) VALUES
(9, 'inr', '2', '2023-04-10 05:53:36'),
(10, 'aed', '2', '2023-04-10 05:53:36'),
(15, 'AUD', '2', '2023-04-10 05:53:36'),
(16, 'GBP', '2', '2023-04-10 05:53:36'),
(17, 'USD', '2', '2023-04-10 05:53:36'),
(21, 'CAD', '1', '2023-04-10 05:53:36');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `generalsettings`
--

CREATE TABLE `generalsettings` (
  `id` int(11) NOT NULL,
  `commision` varchar(191) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `generalsettings`
--

INSERT INTO `generalsettings` (`id`, `commision`) VALUES
(1, '22');

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `grade_name` varchar(256) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`id`, `grade_name`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Basic', 1, '2021-03-12 00:24:31', '2022-12-16 12:37:42'),
(3, 'Moderate', 1, '2021-03-12 00:24:41', '2022-12-16 12:37:42'),
(4, 'Advanced', 1, '2021-03-12 00:24:49', '2022-12-16 12:37:42'),
(5, 'Expert', 1, '2021-03-12 00:24:58', '2022-12-16 12:37:42');

-- --------------------------------------------------------

--
-- Table structure for table `help_desk`
--

CREATE TABLE `help_desk` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `description` varchar(191) DEFAULT NULL,
  `status` varchar(191) DEFAULT 'Open',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `help_desk`
--

INSERT INTO `help_desk` (`id`, `user_id`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 99, 'Lorem Ipsum is simply dummy text', 'Open', '2021-11-19 10:55:51', '2021-11-19 10:55:51'),
(4, 107, 'Lorem Ipsum is simply dummy text', 'Open', '2021-11-19 10:56:13', '2021-11-19 10:56:13');

-- --------------------------------------------------------

--
-- Table structure for table `home_banners`
--

CREATE TABLE `home_banners` (
  `id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `title` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `image` varchar(191) DEFAULT NULL,
  `page` varchar(191) DEFAULT NULL,
  `learn_more_link` varchar(191) DEFAULT NULL,
  `watch_now_link` varchar(191) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `home_banners`
--

INSERT INTO `home_banners` (`id`, `date`, `title`, `description`, `image`, `page`, `learn_more_link`, `watch_now_link`) VALUES
(4, '2020-10-16', 'Login Banner', '', 'login-banner.jpg', 'login', '#', '#');

-- --------------------------------------------------------

--
-- Table structure for table `home_headers`
--

CREATE TABLE `home_headers` (
  `id` int(11) NOT NULL,
  `logo` varchar(191) DEFAULT NULL,
  `social_links` text DEFAULT NULL,
  `menu` text DEFAULT NULL,
  `copy_right` varchar(191) DEFAULT NULL,
  `short_links` text DEFAULT NULL,
  `min_amount` varchar(191) DEFAULT NULL,
  `title_terms` text DEFAULT NULL,
  `description_terms` text DEFAULT NULL,
  `title_stream` text DEFAULT NULL,
  `description_stream` text DEFAULT NULL,
  `play_store_link` text DEFAULT NULL,
  `app_store_link` text DEFAULT NULL,
  `footer_logo` varchar(191) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `home_headers`
--

INSERT INTO `home_headers` (`id`, `logo`, `social_links`, `menu`, `copy_right`, `short_links`, `min_amount`, `title_terms`, `description_terms`, `title_stream`, `description_stream`, `play_store_link`, `app_store_link`, `footer_logo`) VALUES
(1, 'buddy-guru.png', 'a:4:{i:0;a:2:{s:4:\"name\";s:13:\"icon-facebook\";s:4:\"link\";s:1:\"#\";}i:1;a:2:{s:4:\"name\";s:12:\"icon-twitter\";s:4:\"link\";s:1:\"#\";}i:2;a:2:{s:4:\"name\";s:13:\"icon-whatsapp\";s:4:\"link\";s:1:\"#\";}i:3;a:2:{s:4:\"name\";s:12:\"icon-youtube\";s:4:\"link\";s:1:\"#\";}}', 'a:4:{i:0;a:2:{s:4:\"name\";s:4:\"Home\";s:4:\"link\";s:25:\"http://localhost/worship/\";}i:1;a:2:{s:4:\"name\";s:12:\"Watch Online\";s:4:\"link\";s:37:\"http://localhost/worship/watch-online\";}i:2;a:2:{s:4:\"name\";s:11:\"Church List\";s:4:\"link\";s:33:\"http://localhost/worship/churches\";}i:3;a:2:{s:4:\"name\";s:4:\"Give\";s:4:\"link\";s:35:\"http://localhost/worship/donate-now\";}}', '2020 Onlineworship. All rights reserved.', 'a:3:{i:0;a:2:{s:4:\"name\";s:5:\"About\";s:4:\"link\";s:30:\"http://localhost/worship/about\";}i:1;a:2:{s:4:\"name\";s:15:\"Privacy & legal\";s:4:\"link\";s:45:\"http://localhost/worship/terms-and-conditions\";}i:2;a:2:{s:4:\"name\";s:7:\"Contact\";s:4:\"link\";s:32:\"http://localhost/worship/contact\";}}', '5.99', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `live_class`
--

CREATE TABLE `live_class` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `intro_title` varchar(180) NOT NULL,
  `intro_description` varchar(200) DEFAULT NULL,
  `price` int(100) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `video_url` varchar(100) DEFAULT NULL,
  `video_file_name` varchar(180) NOT NULL,
  `thumbnail` varchar(100) DEFAULT NULL,
  `class_type` int(55) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `duration` int(55) DEFAULT NULL,
  `meeting_link` varchar(100) DEFAULT NULL,
  `no_of_class` int(55) DEFAULT NULL,
  `no_of_participents` int(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `live_class`
--

INSERT INTO `live_class` (`id`, `user_id`, `intro_title`, `intro_description`, `price`, `batch_id`, `title`, `description`, `video_url`, `video_file_name`, `thumbnail`, `class_type`, `start_date`, `end_date`, `start_time`, `end_time`, `duration`, `meeting_link`, `no_of_class`, `no_of_participents`) VALUES
(2, 258, '', NULL, 1000, 3, 'live', 'abcdfe', 'uploads/tutor//H6PZJg5dxYRsJ2LdXeVapeUCisTiJuyB8mcWny5B.mp4', '', 'uploads/tutor/H6PZJg5dxYRsJ2LdXeVapeUCisTiJuyB8mcWny5B.png', 1, '2023-02-18', '2023-02-19', '15:48:00', '16:47:00', 55, 'link', 2, NULL),
(3, 258, '', NULL, 890, 3, 'hai hello', 'mkxskjsjs', 'uploads/tutor//UrOmzremLWGSduE3MMFIrXYLPkKFKw0Wj1tC3sEr.mp4', '', 'uploads/tutor/UrOmzremLWGSduE3MMFIrXYLPkKFKw0Wj1tC3sEr.png', 1, '2023-02-17', '2023-02-18', '16:13:00', '17:13:00', 7, 'vhvhv', 7, NULL),
(4, 259, '', NULL, 900, 3, 'hloo live two', 'hhvyh', 'uploads/tutor//7rK8tV2Z61HRpQBSDaGaTsWPBejCVFQnEoFOrVuK.flv', '', 'uploads/tutor/7rK8tV2Z61HRpQBSDaGaTsWPBejCVFQnEoFOrVuK.png', 1, '2023-02-22', '2023-02-23', '10:24:00', '11:24:00', 60, 'vhvhvhv', 9, NULL),
(5, 259, '', NULL, 1000, 3, 'test4', 'hdhdehde', 'uploads/tutor//CSXXcHPG6l3GyveLrOtIEaWhvQ6cHhIEjQNNLGJs.flv', '', 'uploads/tutor/CSXXcHPG6l3GyveLrOtIEaWhvQ6cHhIEjQNNLGJs.png', 1, '2023-02-20', '2023-02-22', '17:33:00', '18:33:00', 80, 'test.com', 7, NULL),
(6, 259, '', NULL, 7600, 3, 'hloo234', 'jdjdhd', 'uploads/tutor/MQ1LqIeXD8SVOFAvxVrxJNzj5AvqGmkRdLFPYrRO.flv', '', 'uploads/tutorMQ1LqIeXD8SVOFAvxVrxJNzj5AvqGmkRdLFPYrRO.png', 1, '2023-02-20', '2023-02-23', '17:43:00', '21:39:00', 78, 'n n', 7, NULL),
(7, 259, '', NULL, 2000, 3, 'hlo9000', 'v xxvcxvcv', 'uploads/tutor/PpYw13occGPOYQs8L71hz5VeKOuOkwYAG4rEhPBY.flv', '', 'uploads/tutorPpYw13occGPOYQs8L71hz5VeKOuOkwYAG4rEhPBY.png', 1, '2023-02-22', '2023-02-21', '17:43:00', '19:43:00', 67, 'vvg', 4, NULL),
(10, 261, '', NULL, 4500, 3, 'new live65', 'abcd test', 'uploads/tutor/KP9WtUqk7dBIKavktbsfOAw7knX1AgoTHi8ZA9IN.flv', 'SampleVideo_1280x720_1mb.flv', 'uploads/tutorKP9WtUqk7dBIKavktbsfOAw7knX1AgoTHi8ZA9IN.png', 1, '2023-02-23', '2023-02-22', '10:35:00', '11:35:00', 7, 'vhvhvhv', 2, NULL),
(11, 261, '', NULL, 100, 3, 'morning', 'group class', 'uploads/tutor/X93aAS4bbtzgLRxTrsuBrYLCTSgvH7Dn03ztJUWZ.mp4', 'TEST VIDEO.mp4', 'uploads/tutorX93aAS4bbtzgLRxTrsuBrYLCTSgvH7Dn03ztJUWZ.png', 2, '2023-02-23', '2023-03-23', '10:30:00', '11:30:00', 1, NULL, 15, NULL),
(12, 263, '', NULL, 120, 3, 'eveninig', 'advanced bharthanattyam', 'uploads/tutor/S3PqOXhFj0d8St1Wzjo3LOfASSp2nSuIrqQepBhi.mp4', 'TEST VIDEO.mp4', 'uploads/tutorS3PqOXhFj0d8St1Wzjo3LOfASSp2nSuIrqQepBhi.png', 1, '2023-02-24', '2023-03-24', '16:00:00', '17:00:00', 1, NULL, 15, NULL),
(13, 262, '', NULL, 455, 3, 'hloo', NULL, 'uploads/tutor/FhFQeUZ3ye5Gwn0TrRpb2k4ZeMzy9ML4vKvUbsIb.flv', 'SampleVideo_1280x720_1mb.flv', 'uploads/tutorFhFQeUZ3ye5Gwn0TrRpb2k4ZeMzy9ML4vKvUbsIb.png', 1, '2023-02-22', '2023-02-23', NULL, NULL, NULL, NULL, NULL, NULL),
(14, 272, '', NULL, 1000, 3, 'hloo', 'nhadshhdshjds', 'uploads/tutor/xQ1vgOUyu6OSBlyev4shaxy6xVZBgsYfTx8WLzVL.mp4', 'file_example_MP4_480_1_5MG.mp4', 'uploads/tutorxQ1vgOUyu6OSBlyev4shaxy6xVZBgsYfTx8WLzVL.png', 1, '2023-02-23', '2023-02-25', '20:06:00', '13:02:00', 7, 'gasgdsg', 5, NULL),
(15, 277, '', NULL, 1000, 3, 'test', 'ffufufvvv', 'uploads/tutor/GzRQnMdjwukryeFASKiBP4T4Mj7azaNUQJdWOzo6.mp4', 'file_example_MP4_480_1_5MG.mp4', 'uploads/tutorGzRQnMdjwukryeFASKiBP4T4Mj7azaNUQJdWOzo6.png', 1, '2023-02-26', '2023-02-28', NULL, NULL, NULL, NULL, NULL, NULL),
(16, 278, '', NULL, 120, 3, 'trailer', 'orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the i', 'uploads/tutor/MfTWgswR1zcEX6xOc2CXG9dUVGj3AJtqZcn6kZch.mp4', 'TEST VIDEO.mp4', 'uploads/tutorMfTWgswR1zcEX6xOc2CXG9dUVGj3AJtqZcn6kZch.png', 1, '2023-02-28', '2023-03-11', '18:00:00', '19:00:00', 1, NULL, 5, NULL),
(17, 278, '', NULL, 75, 3, 'trailer', 'orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the i', 'uploads/tutor/CGXBKPSCzaPt9zNvTfxN1XCDfDSw6H6sHtaoYHKS.mp4', 'TEST VIDEO.mp4', 'uploads/tutorCGXBKPSCzaPt9zNvTfxN1XCDfDSw6H6sHtaoYHKS.png', 2, '2023-03-03', '2023-03-10', '19:55:00', '20:56:00', 1, NULL, 5, 10),
(18, 102, 'Fugiat commodo dolor', 'Lorem inventore magn', 425, 3, 'Quo incididunt vel u', 'Voluptatum omnis ips', 'uploads/tutor/fTYCFbuGbjxdNxeP7F2pHpbtZQ9q0Tk2hiowAjzE.mp4', 'TEST VIDEO.mp4', 'uploads/tutorfTYCFbuGbjxdNxeP7F2pHpbtZQ9q0Tk2hiowAjzE.png', 2, '1971-06-22', '2020-06-28', '00:24:00', '16:15:00', 84, NULL, 65, NULL),
(19, 102, 'Fugiat commodo dolor', 'Lorem inventore magn', 425, 3, 'Quo incididunt vel u', 'Voluptatum omnis ips', 'uploads/tutor/binWdXWyLawtBpgWzJQqT102bVmyufLC8QEG3uoL.mp4', 'TEST VIDEO.mp4', 'uploads/tutorbinWdXWyLawtBpgWzJQqT102bVmyufLC8QEG3uoL.png', 2, '1971-06-22', '2020-06-28', '00:24:00', '16:15:00', 84, NULL, 65, NULL),
(20, 102, 'Fugiat commodo dolor', 'Lorem inventore magn', 425, 3, 'Quo incididunt vel u', 'Voluptatum omnis ips', 'uploads/tutor/Ff3okeKOLMjp68dzvymTHF8Bnnpe8HabttgRLWDa.mp4', 'TEST VIDEO.mp4', 'uploads/tutorFf3okeKOLMjp68dzvymTHF8Bnnpe8HabttgRLWDa.png', 2, '1971-06-22', '2020-06-28', '00:24:00', '16:15:00', 84, NULL, 65, NULL),
(21, 103, 'Consequatur modi dol', 'Et adipisci quis tem', 911, 3, 'Tempor obcaecati vol', 'Perferendis ut fuga', 'uploads/tutor/PrJEeW4lQGp3xoCKEBey1lktaGwK6oqv5NSQzDvN.mp4', 'file_example_MP4_480_1_5MG.mp4', 'uploads/tutorPrJEeW4lQGp3xoCKEBey1lktaGwK6oqv5NSQzDvN.png', 2, '1992-03-12', '1970-11-23', '11:50:00', '07:43:00', 22, NULL, 13, NULL),
(22, 103, 'Aliquip nemo qui qui', 'Deserunt eaque sit', 441, 3, 'Deserunt vel delenit', 'Et omnis velit velit', 'uploads/tutor/EVv8Xq0pnlD87UvVTCrgbkwOlgfOsnentxyUsvDa.mp4', 'file_example_MP4_480_1_5MG.mp4', 'uploads/tutorEVv8Xq0pnlD87UvVTCrgbkwOlgfOsnentxyUsvDa.png', 2, '2002-04-17', '2014-03-31', '00:44:00', '03:41:00', 82, NULL, 38, NULL),
(23, 103, 'Dicta adipisci imped', 'Ipsa ut quo asperna', 846, 3, 'Reprehenderit ullam', 'Distinctio Repellen', 'uploads/tutor/n4IfG37sDEnSxjVuxio11oq9I6iFBHBE0CHP88E1.mp4', 'file_example_MP4_480_1_5MG.mp4', 'uploads/tutorn4IfG37sDEnSxjVuxio11oq9I6iFBHBE0CHP88E1.png', 2, '1976-11-22', '2015-06-02', '00:10:00', '01:31:00', 41, NULL, 21, NULL),
(24, 292, 'Velit tempora eos', 'Harum deserunt ipsum', 801, 3, 'Et reiciendis ex ali', 'Voluptatem Dolores', 'uploads/tutor/bEjxIFDELRWrSJ8zDuFMQZ54mqtyYfpDu2MrXXj6.mp4', 'file_example_MP4_480_1_5MG.mp4', 'uploads/tutorbEjxIFDELRWrSJ8zDuFMQZ54mqtyYfpDu2MrXXj6.png', 2, '1972-06-29', '1999-01-08', '22:23:00', '18:26:00', 2, NULL, 60, NULL),
(25, 292, 'Autem et consequat', 'Incididunt eius non', 580, 3, 'Sit tenetur volupta', 'Nisi et perspiciatis', 'uploads/tutor/c0tsWeGMhMo9ZDIizivxjuJgYD6OZI5IMZz2lNmz.mp4', 'file_example_MP4_480_1_5MG.mp4', 'uploads/tutorc0tsWeGMhMo9ZDIizivxjuJgYD6OZI5IMZz2lNmz.png', 2, '2013-03-14', '1988-12-05', '02:24:00', '23:13:00', 70, NULL, 46, NULL),
(26, 292, 'Sed est ut consectet', 'Id explicabo Et sit', 233, 3, 'Consequatur et dolor', 'Voluptate ipsa null', 'uploads/tutor/rigBuQY9z2VahI0BS7zWw4kBHhrMM0nHuu3Q9A9j.mp4', 'file_example_MP4_480_1_5MG.mp4', 'uploads/tutorrigBuQY9z2VahI0BS7zWw4kBHhrMM0nHuu3Q9A9j.png', 2, '2012-12-03', '1995-09-29', '06:21:00', '15:57:00', 65, NULL, 42, NULL),
(27, 292, 'Veritatis quo repreh', 'Necessitatibus conse', 563, 3, 'Saepe nostrud quas e', 'Molestias consequatu', 'uploads/tutor/hXIC4Kvfp2FsWkt9nhpfRXBzlAPxJzmlAXCRJiAR.mp4', 'file_example_MP4_480_1_5MG.mp4', 'uploads/tutorhXIC4Kvfp2FsWkt9nhpfRXBzlAPxJzmlAXCRJiAR.png', 2, '1977-09-06', '1980-09-06', '10:43:00', '04:26:00', 95, NULL, 74, NULL),
(28, 292, 'Eu est doloremque n', 'Voluptas corporis en', 763, 3, 'Aliquam illum ipsa', 'Possimus dolore dol', 'uploads/tutor/j9O9cDW8tuFK1d2WS2GrQ5qnXYWQVQK0nLcAu4yB.mp4', 'file_example_MP4_480_1_5MG.mp4', 'uploads/tutorj9O9cDW8tuFK1d2WS2GrQ5qnXYWQVQK0nLcAu4yB.png', 2, '1972-10-16', '1992-10-04', '07:24:00', '01:10:00', 41, NULL, 97, NULL),
(29, 292, 'Deserunt obcaecati d', 'Provident cum qui e', 693, 3, 'Voluptas maiores rep', 'Ex obcaecati nostrud', 'uploads/tutor/lXlF5ld3nZK2e4ce0mWUrS9HwHdNnkVL8047vj4h.mp4', 'file_example_MP4_480_1_5MG.mp4', 'uploads/tutorlXlF5ld3nZK2e4ce0mWUrS9HwHdNnkVL8047vj4h.png', 2, '2009-01-02', '2002-04-18', '00:57:00', '09:15:00', 19, NULL, 25, NULL),
(30, 292, 'Voluptas consectetur', 'Ullamco cumque quis', 409, 3, 'Elit adipisicing vo', 'Pariatur Exercitati', 'uploads/tutor/ybK3bPUhqlP1sqlWvNRaRbsZ6HTRh8sBsozmDQae.mp4', 'file_example_MP4_480_1_5MG.mp4', 'uploads/tutorybK3bPUhqlP1sqlWvNRaRbsZ6HTRh8sBsozmDQae.png', 1, '2001-03-20', '1985-11-13', '13:50:00', '10:11:00', 71, NULL, 63, NULL),
(31, 293, 'Culpa eos veniam e', 'Eu illo officia temp', 787, 3, 'Ut vel occaecat veri', 'Esse voluptas ea mol', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/liveclass/293/file_example_MP4_480_1_5MG.mp', 'file_example_MP4_480_1_5MG.mp4', 'uploads/tutorfile_example_MP4_480_1_5MG.png', 1, '2010-01-06', '1985-02-24', '03:21:00', '21:02:00', 8, NULL, 100, NULL),
(32, 295, 'demo', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type a', 100, 3, 'advanced', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the ', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/liveclass/295/TEST%20VIDEO.mp4', 'TEST VIDEO.mp4', 'uploads/tutorTEST VIDEO.png', 2, '2023-03-08', '2023-03-24', '11:39:00', '12:39:00', 2, NULL, 5, 10),
(33, 298, 'Quisquam ullam ab ob', 'Ut est beatae quia a', 916, 3, 'Id cupidatat consequ', 'Dolore assumenda dig', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/liveclass/298/TEST%20VIDEO.mp4', 'TEST VIDEO.mp4', 'uploads/tutorTEST VIDEO.png', 2, '1988-03-18', '1989-10-24', '02:22:00', '00:25:00', 1, NULL, 59, 10),
(34, 306, 'Voluptates maxime ut', 'Necessitatibus simil', 293, 3, 'Non natus eu exercit', 'Natus saepe aut quod', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/liveclass/306/TEST%20VIDEO.mp4', 'TEST VIDEO.mp4', 'uploads/tutorTEST VIDEO.png', 1, '1972-06-24', '1993-09-09', '08:50:00', '22:05:00', 65, NULL, 13, 40),
(36, 317, 'Expedita qui obcaeca', 'Hic qui et impedit', 209, 3, 'Quod do omnis pariat', 'Nostrud sed nobis mo', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/317/liveclass/TEST%20VIDEO.mp4', 'TEST VIDEO.mp4', 'uploads/tutorTEST VIDEO.png', 2, '2005-10-14', '1980-12-02', '22:43:00', '20:25:00', 80, NULL, 84, 12),
(37, 317, 'Dolore quasi alias m', 'Sit sed id et labor', 635, 3, 'Accusamus pariatur', 'Aut enim in minim ex', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/317/liveclass/TEST%20VIDEO.mp4', 'TEST VIDEO.mp4', 'uploads/tutorTEST VIDEO.png', 2, '2004-03-13', '1978-07-15', '12:24:00', '11:20:00', 96, NULL, 44, NULL),
(38, 317, 'Asperiores non exerc', 'Amet quas minima au', 750, 3, 'Nisi voluptas volupt', 'In cillum id minima', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/317/liveclass/TEST%20VIDEO.mp4', 'TEST VIDEO.mp4', 'uploads/tutorTEST VIDEO.png', 2, '1970-07-22', '1997-05-18', '11:41:00', '04:34:00', 24, NULL, 57, 10),
(39, 317, 'Enim neque consequat', 'Et quasi consequatur', 542, 3, 'Et consectetur in e', 'Voluptas in sunt arc', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/317/liveclass/file_example_MP4_1920_18MG.mp', 'file_example_MP4_1920_18MG.mp4', 'uploads/tutorfile_example_MP4_1920_18MG.png', 2, '1990-11-25', '1997-05-02', '21:41:00', '03:53:00', 40, NULL, 3, NULL),
(40, 317, 'In dolorem ex ad rer', 'Fugiat ratione enim', 52, 3, 'Fugiat ratione enim', 'Suscipit non et sequ', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/317/liveclass/file_example_MP4_1920_18MG.mp', 'file_example_MP4_1920_18MG.mp4', 'uploads/tutorfile_example_MP4_1920_18MG.png', 1, '2014-08-10', '2019-10-09', '13:47:00', '10:13:00', 27, NULL, 56, NULL),
(41, 318, 'Quidem inventore id', 'Quo ea ad quia maior', 952, 3, 'Velit cumque dolor e', 'Non sit obcaecati q', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/318/liveclass/TEST%20VIDEO.mp4', 'TEST VIDEO.mp4', 'uploads/tutorTEST VIDEO.png', 2, '1984-04-26', '2012-05-02', '12:35:00', '16:36:00', 9, NULL, 86, 12),
(42, 320, 'demo', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type a', 150, 3, 'trail', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the ', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/320/liveclass/TEST%20VIDEO.mp4', 'TEST VIDEO.mp4', 'uploads/tutorTEST VIDEO.png', 2, '2023-03-15', '2023-03-25', '10:29:00', '11:29:00', 1, NULL, 5, 10),
(43, 322, 'demo', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type a', 20, 3, 'live', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the ', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/322/liveclass/TEST%20VIDEO.mp4', 'TEST VIDEO.mp4', 'uploads/tutorTEST VIDEO.png', 2, '2023-03-14', '2023-03-18', '12:05:00', '13:05:00', 1, NULL, 5, 10),
(44, 323, 'Rem sit expedita qui', 'Accusantium do sunt', 438, 3, 'Fuga Dolorum eum no', 'Fugiat alias maiores', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/323/liveclass/file_example_MP4_1920_18MG.mp', 'file_example_MP4_1920_18MG.mp4', 'uploads/tutorfile_example_MP4_1920_18MG.png', 1, '1996-10-12', '1975-02-16', '01:33:00', '11:50:00', 10, NULL, 28, NULL),
(45, 323, 'Odio voluptatem lab', 'Nostrum sit volupta', 80, 3, 'Voluptate enim excep', 'Impedit amet susci', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/323/liveclass/file_example_MP4_1920_18MG.mp', 'file_example_MP4_1920_18MG.mp4', 'uploads/tutorfile_example_MP4_1920_18MG.png', 1, '1986-11-10', '1976-12-11', '01:07:00', '00:33:00', 79, NULL, 62, NULL),
(46, 325, 'Et sunt nesciunt au', 'Culpa unde est inc', 96, 3, 'Laudantium totam of', 'Suscipit odio pariat', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/325/liveclass/TEST%20VIDEO.mp4', 'TEST VIDEO.mp4', 'uploads/tutorTEST VIDEO.png', 2, '1993-11-06', '1984-08-19', '20:26:00', '04:02:00', 39, NULL, 77, 10),
(47, 326, 'live demo', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type a', 123, 3, 'live class by test12', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the ', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/326/liveclass/TEST%20VIDEO.mp4', 'TEST VIDEO.mp4', 'uploads/tutorTEST VIDEO.png', 2, '2023-03-16', '2023-03-23', '11:57:00', '12:57:00', 1, NULL, 12, 12),
(48, 327, 'Demo live class', 'Users who have been on the market for a long time and have used many different kinds of facial masks know how difficult it can be to find the right one. The facial mask market is flooded with countles', 4000, 3, 'Test live class', 'Users who have been on the market for a long time and have used many different kinds of facial masks', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/327/liveclass/file_example_MP4_480_1_5MG.mp', 'file_example_MP4_480_1_5MG.mp4', 'uploads/tutorfile_example_MP4_480_1_5MG.png', 2, '2023-03-16', '2023-03-17', '12:18:00', '14:18:00', 60, NULL, 12, 20),
(49, 331, 'Aut blanditiis id s', 'Sed qui obcaecati om', 271, 3, 'Fugiat consequuntur', 'Animi proident nob', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/331/liveclass/TEST%20VIDEO.mp4', 'TEST VIDEO.mp4', 'uploads/tutorTEST VIDEO.png', 2, '1991-03-01', '1989-02-11', '03:55:00', '05:11:00', 6, NULL, 45, 20),
(50, 332, 'Ratione quia aut con', 'Enim commodo sunt e', 734, 3, 'Facere reiciendis ul', 'Voluptate enim earum', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/332/liveclass/TEST%20VIDEO.mp4', 'TEST VIDEO.mp4', 'uploads/tutorTEST VIDEO.png', 2, '1996-07-04', '1992-10-31', '11:29:00', '17:36:00', 89, NULL, 24, 10),
(53, 335, 'demo', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type a', 50, 3, 'live', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the ', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/335/liveclass/TEST%20VIDEO.mp4', 'TEST VIDEO.mp4', 'uploads/tutorTEST VIDEO.png', 1, '2023-03-17', '2023-03-24', '16:13:00', '18:15:00', 2, NULL, 10, NULL),
(54, 336, 'demo', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type a', 100, 3, 'live', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the ', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/336/liveclass/TEST%20VIDEO.mp4', 'TEST VIDEO.mp4', 'uploads/tutorTEST VIDEO.png', 1, '2023-03-22', '2023-04-06', '11:08:00', '12:09:00', 1, NULL, 10, NULL),
(56, 336, 'Sed aliquam temporib', 'Voluptatem doloremq', 627, 3, 'Voluptatem eveniet', 'Sint non ullamco exc', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/336/liveclass/TEST%20VIDEO.mp4', 'TEST VIDEO.mp4', 'uploads/tutorTEST VIDEO.png', 2, '1997-06-04', '2008-12-03', '03:46:00', '13:00:00', 71, NULL, 24, 102),
(58, 339, 'Dolorum qui aut dolo', 'Totam eos assumenda', 648, 3, 'Ad facere est nihil', 'Corrupti sunt quo', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/339/liveclass/file_example_MP4_480_1_5MG.mp', 'file_example_MP4_480_1_5MG.mp4', 'uploads/tutorfile_example_MP4_480_1_5MG.png', 1, '1972-02-01', '2010-04-29', '21:51:00', '09:31:00', 28, NULL, 82, NULL),
(59, 341, 'demo intro1', 'sfsfweghwegwe  wegegeg', 1200, 3, 'Test342', 'bdghdwhjwnmn hashas', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/341/liveclass/file_example_MP4_1920_18MG.mp', 'file_example_MP4_1920_18MG.mp4', 'uploads/tutor/file_example_MP4_1920_18MG.png', 2, '2023-03-29', '2023-03-31', '10:58:00', '11:58:00', 10, NULL, 5, 4),
(61, 343, 'Rerum culpa dolorum', 'Minus culpa ex ea c', 959, 3, 'Omnis est omnis corr', 'Nulla qui voluptate', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/343/liveclass/file_example_MP4_1920_18MG.mp', 'file_example_MP4_1920_18MG.mp4', 'uploads/tutorfile_example_MP4_1920_18MG.png', 1, '2013-11-22', '1981-07-09', '10:52:00', '15:42:00', 35, NULL, 95, NULL),
(63, 343, 'Qui doloribus nisi u', 'A et aperiam est vol', 349, 3, 'Qui ut ipsum ea con', 'Sed aliquid culpa o', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/343/liveclass/sample_960x540.mpeg', 'sample_960x540.mkv', 'uploads/tutor/sample_960x540.png', 2, '1976-04-20', '1979-04-25', '06:29:00', '21:48:00', 85, NULL, 92, 8),
(64, 344, 'Kuchipudi -basic', 'beggerewff', 9080, 3, 'demo1', 'feffef', 'rr', 'iii', 'uui', 2, '2023-04-11', '2023-04-12', '17:01:00', '18:01:00', 45, NULL, 5, 4);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_09_24_083016_create_tutor_portfolio', 2),
(5, '2021_09_25_081205_add_rating_to_tutor_details', 3);

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `intro_title` varchar(180) NOT NULL,
  `intro_description` varchar(200) DEFAULT NULL,
  `video_thumbnail` varchar(180) DEFAULT NULL,
  `video_url` varchar(180) DEFAULT NULL,
  `total_price` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `user_id`, `batch_id`, `intro_title`, `intro_description`, `video_thumbnail`, `video_url`, `total_price`, `created_at`, `updated_at`) VALUES
(202, 0, 0, 'demo intro1', 'bhadsbhdsnadsnas', '', '', '1000', '2023-03-14 11:19:17', '2023-03-14 11:19:17'),
(203, 0, 0, 'demo intro', 'bhadsbhdsnadsnas', '', '', '2000', '2023-03-14 11:26:48', '2023-03-14 11:26:48'),
(205, 0, 0, 'Kuchipudi -basic', 'bdfddf', '', '', '29900', '2023-03-14 11:39:17', '2023-03-14 11:39:17'),
(206, 0, 0, 'Kuchipudi -basics', 'bdfddf', '', '', '2990', '2023-03-14 11:39:36', '2023-03-14 11:39:36'),
(210, 325, 1, 'demo', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type a', '', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/325/guruclass/TEST%20VIDEO.mp4', '120', '2023-03-15 12:54:56', '2023-03-15 12:54:56'),
(211, 325, 2, 'demo', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type a', '', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/325/moduleclass/TEST%20VIDEO.mp4', '0', '2023-03-15 13:01:46', '2023-03-15 13:01:46'),
(212, 326, 1, 'demo', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type a', '', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/326/guruclass/TEST%20VIDEO.mp4', '123', '2023-03-16 06:26:11', '2023-03-16 06:26:11'),
(213, 326, 2, 'demo', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type a', '', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/326/moduleclass/TEST%20VIDEO.mp4', '0', '2023-03-16 06:27:22', '2023-03-16 06:27:22'),
(214, 327, 1, 'Guru test', 'Users who have been on the market for a long time and have used many different kinds of facial masks know how difficult it can be to find the right one. The facial mask market is flooded with countles', '', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/327/guruclass/file_example_MP4_1920_18MG.mp4', '1900', '2023-03-16 06:46:04', '2023-03-16 06:46:04'),
(215, 327, 2, 'Module demo', 'Users who have been on the market for a long time and have used many different kinds of facial masks know how difficult it can be to find the right one. The facial mask market is flooded with countles', '', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/327/moduleclass/file_example_MP4_480_1_5MG.mp4', '0', '2023-03-16 06:47:30', '2023-03-16 06:47:30'),
(216, 328, 1, 'Ullam magni voluptas', 'Sit sed ex ipsum iu', '', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/328/guruclass/SampleVideo_1280x720_1mb%20%281%29.mp4', '242', '2023-03-16 09:48:23', '2023-03-16 09:48:23'),
(217, 329, 1, 'Corrupti quo commod', 'Quia eligendi eum do', '', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/329/guruclass/SampleVideo_1280x720_1mb%20%281%29.mp4', '95', '2023-03-16 10:00:07', '2023-03-16 10:00:07'),
(218, 329, 2, 'Exercitationem et cu', 'Tempora minus nobis', '', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/329/moduleclass/SampleVideo_1280x720_1mb%20%281%29.mp4', '0', '2023-03-16 10:39:03', '2023-03-16 10:39:03'),
(229, 335, 1, 'demo', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type a', '', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/335/guruclass/TEST%20VIDEO.mp4', '120', '2023-03-17 10:34:00', '2023-03-17 10:34:00'),
(230, 335, 2, 'demo', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type a', '', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/335/moduleclass/TEST%20VIDEO.mp4', '0', '2023-03-17 10:42:36', '2023-03-17 10:42:36'),
(237, 336, 2, 'Necessitatibus dolor', 'Autem temporibus rep', '', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/336/moduleclass/SampleVideo_1280x720_1mb.flv', '0', '2023-03-20 08:18:27', '2023-03-20 08:18:27'),
(238, 336, 2, 'Cumque reprehenderit', 'Aliqua Est et sint', '', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/336/moduleclass/SampleVideo_1280x720_1mb.flv', '0', '2023-03-20 08:25:38', '2023-03-20 08:25:38'),
(240, 336, 2, 'Culpa nulla non non', 'Lorem quam totam vol', NULL, NULL, '0', '2023-03-20 09:43:17', '2023-03-20 09:43:17'),
(242, 336, 2, 'Corporis corporis mo', 'Veniam quibusdam se', NULL, NULL, '103', '2023-03-21 07:18:04', '2023-03-21 07:18:04'),
(244, 339, 2, 'demo', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type a', NULL, NULL, '120', '2023-03-21 10:22:00', '2023-03-21 10:22:00'),
(246, 339, 2, 'demo12', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type a', NULL, NULL, '110', '2023-03-21 11:26:19', '2023-03-21 11:26:19'),
(247, 339, 2, 'Corporis voluptatibu', 'Pariatur Non dolor', NULL, 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/339/moduleclass/file_example_MP4_480_1_5MG.mp4', '25', '2023-03-21 12:08:58', '2023-03-21 12:08:58'),
(249, 336, 1, 'Ipsum qui sed fugiat', 'Non ullamco alias eu', NULL, 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/336/guruclass/file_example_MP4_480_1_5MG.mp4', '938', '2023-03-25 05:20:31', '2023-03-25 05:20:31'),
(250, 339, 2, 'Nihil ex dolor volup', 'Quos quam atque ipsu', NULL, 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/339/moduleclass/file_example_MP4_480_1_5MG.mp4', '774', '2023-03-27 12:01:49', '2023-03-27 12:01:49'),
(251, 341, 1, 'Kuchipudi -basic', 'ttwhwhwbwjk  lkjnsldkdnasldasc   jbasdc;asbjsabc;jas jsbcsabasjbd', NULL, 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/341/guruclass/file_example_MP4_480_1_5MG.mp4', '8900', '2023-03-29 05:27:04', '2023-03-29 05:27:04'),
(252, 341, 2, 'Numquam praesentium', 'Enim enim ut dolore', NULL, 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/341/moduleclass/file_example_MP4_1920_18MG.mp4', '387', '2023-03-29 05:41:48', '2023-03-29 05:41:48'),
(255, 344, 2, 'Incidunt error poss', 'Ut aut lorem molesti', NULL, 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/343/guruclass/file_example_WEBM_640_1_4MB.webm', '664', '2023-03-31 06:05:06', '2023-03-31 06:05:06');

-- --------------------------------------------------------

--
-- Table structure for table `package_detail`
--

CREATE TABLE `package_detail` (
  `id` int(11) NOT NULL,
  `user_id` int(100) NOT NULL,
  `batch_id` int(100) NOT NULL,
  `package_id` int(70) NOT NULL,
  `title` varchar(140) NOT NULL,
  `description` varchar(150) NOT NULL,
  `video_url` varchar(150) NOT NULL,
  `thumbnail` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `package_detail`
--

INSERT INTO `package_detail` (`id`, `user_id`, `batch_id`, `package_id`, `title`, `description`, `video_url`, `thumbnail`) VALUES
(229, 336, 2, 240, 'Iusto eveniet et id', 'Culpa aliquip qui el', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/336/moduleclass/file_example_MP4_1920_18MG.mp4', 'uploads/tutor/packagehttps://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/336/moduleclass/file_example_MP4_1920_18MG.mp4.png'),
(232, 336, 2, 242, 'In enim et quia magn', 'Officiis voluptatibu', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/336/moduleclass/file_example_MP4_1920_18MG.mp4', 'uploads/tutor/packagehttps://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/336/moduleclass/file_example_MP4_1920_18MG.mp4.png'),
(234, 339, 2, 244, 'advanced', 'asjdgjbfdj', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/339/moduleclass/TEST%20VIDEO.mp4', 'uploads/tutor/packagehttps://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/339/moduleclass/TEST%20VIDEO.mp4.png'),
(236, 339, 2, 246, 'advanced', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/339/moduleclass/TEST%20VIDEO.mp4', 'uploads/tutor/packagehttps://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/339/moduleclass/TEST%20VIDEO.mp4.png'),
(237, 339, 2, 246, 'dance class by diya', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/339/moduleclass/TEST%20VIDEO.mp4', 'uploads/tutor/packagehttps://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/339/moduleclass/TEST%20VIDEO.mp4.png'),
(238, 339, 2, 247, 'Exercitationem minus', 'Cupidatat enim ea et', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/339/moduleclass/file_example_MP4_1920_18MG.mp4', 'uploads/tutor/modulehttps://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/339/moduleclass/file_example_MP4_1920_18MG.mp4.png'),
(239, 339, 2, 247, 'Explicabo Omnis und', 'Dolores ut aliquip q', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/339/moduleclass/file_example_MP4_1920_18MG.mp4', 'p'),
(241, 336, 1, 249, 'Est aut laudantium', 'Quidem ullamco obcae', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/336/guruclass/file_example_MP4_480_1_5MG.mp4', 'h'),
(242, 336, 1, 249, 'Kuchipudi', 'zxcxzczczxc', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/336/guruclass/file_example_MP4_1920_18MG.mp4', 't'),
(243, 339, 2, 250, 'Deserunt ipsum repre', 'Odio veniam et est', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/339/moduleclass/file_example_MP4_1920_18MG.mp4', 'u'),
(244, 339, 2, 250, 'Vero laudantium exe', 'Ipsum eius nobis vol', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/339/moduleclass/SampleVideo_1280x720_1mb%20%281%29.mp4', 'p'),
(245, 339, 2, 250, 'Aliquip aut laboris', 'Autem debitis error', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/339/moduleclass/file_example_MP4_480_1_5MG.mp4', 'l'),
(246, 339, 2, 250, 'Saepe sed et adipisi', 'Culpa exercitation', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/339/moduleclass/file_example_MP4_1920_18MG.mp4', 'o'),
(247, 341, 1, 251, 'Kuchipudi', 'bdshdshdwhn  hbawdjbjkd  vyiqvwdhqwhd', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/341/guruclass/file_example_MP4_1920_18MG.mp4', 'h'),
(248, 341, 1, 251, 'mohiniyattam', 'hdehehehb   hvsahsvhasvd  kabkdkjasdbaskbd', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/341/guruclass/file_example_MP4_1920_18MG.mp4', 't'),
(249, 341, 1, 251, 'karnatic', 'hdshhjdj  jadshdhds', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/341/guruclass/file_example_MP4_1920_18MG.mp4', 't'),
(250, 341, 2, 252, 'Est sapiente ut non', 'Porro proident iust basghashjasjhnmasn', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/341/moduleclass/file_example_MP4_1920_18MG.mp4', 'u'),
(251, 341, 2, 252, 'Ea quibusdam sint se', 'Deleniti doloremque', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/341/moduleclass/file_example_MP4_1920_18MG.mp4', 'p'),
(262, 344, 2, 255, 'Nihil omnis ut sed p', 'Sit sapiente amet i', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/343/guruclass/file_example_WEBM_640_1_4MB.webm', 'h'),
(263, 344, 2, 255, 'guru1', 'Minus dolorum quis s', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/343/guruclass/file_example_WEBM_640_1_4MB.webm', 't'),
(264, 344, 2, 255, 'guru2', 'Do fugiat velit moll', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/343/guruclass/file_example_WEBM_640_1_4MB.webm', 't');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('admin@gmail.com', '$2y$10$Xp1fLBp07Ep.qFvEm7lLHu2rouL..hxoXIxiOVQiBxaiws1yhCZQq', '2020-10-31 00:12:48');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tutor_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `package_id` int(11) DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `payment_method` varchar(191) NOT NULL,
  `charge_id` varchar(191) DEFAULT NULL,
  `card_number` int(11) DEFAULT NULL,
  `card_name` varchar(191) DEFAULT NULL,
  `expiry_date` varchar(191) DEFAULT NULL,
  `cvv` int(11) DEFAULT NULL,
  `payment_status` varchar(191) NOT NULL,
  `email_status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `user_id`, `tutor_id`, `batch_id`, `package_id`, `amount`, `payment_method`, `charge_id`, `card_number`, `card_name`, `expiry_date`, `cvv`, `payment_status`, `email_status`, `created_at`, `updated_at`) VALUES
(4, 99, 195, 40, 77, 1000, 'monthly', NULL, NULL, NULL, NULL, NULL, '1', NULL, '2022-03-11 11:49:14', '2022-03-26 05:44:16'),
(5, 99, 195, 41, 78, 1000, 'monthly', NULL, NULL, NULL, NULL, NULL, '1', NULL, '2022-03-11 11:49:14', '2022-03-26 05:44:20'),
(6, 99, 195, 42, 79, 1000, 'monthly', NULL, NULL, NULL, NULL, NULL, '1', NULL, '2022-03-11 11:49:57', '2022-03-26 05:44:23'),
(17, 196, 172, 54, 83, 100, 'monthly', NULL, 2147483647, 'Navee', NULL, 123, '1', NULL, '2022-03-25 09:23:29', '2022-06-20 10:30:37'),
(23, 99, 103, 55, 84, 1500, 'monthly', NULL, 2147483647, 'test', NULL, 123, '1', NULL, '2022-03-26 04:44:20', '2022-03-26 04:44:20'),
(25, 214, 192, 37, 74, 500, 'monthly', NULL, 2147483647, 'Test', NULL, 123, '1', NULL, '2022-03-31 09:25:48', '2022-03-31 09:25:48'),
(26, 215, 192, 37, 74, 500, 'monthly', NULL, 2147483647, 'Test', NULL, 123, '1', NULL, '2022-03-31 09:31:00', '2022-03-31 09:31:00'),
(27, 214, 192, 57, 74, 500, 'monthly', NULL, 2147483647, 'Test', NULL, 123, '1', NULL, '2022-03-31 10:28:43', '2022-06-20 10:30:41'),
(28, 196, 172, 58, 83, 100, 'One time', NULL, 2147483647, 'Test', NULL, 123, '1', NULL, '2022-04-06 11:12:24', '2022-04-06 11:12:24'),
(29, 230, 224, 59, 86, 200, 'One time', 'ch_1LDLjOFYYVrPIFCxGzstUb4g', 2147483647, 'Zhang San', NULL, 567, '1', NULL, '2022-06-22 05:03:59', '2022-06-22 05:03:59'),
(30, 230, 172, 61, 34, 50, 'One time', 'ch_1LUPIoFYYVrPIFCxZHg1voe9', 2147483647, 'Anc', NULL, 123, '1', NULL, '2022-08-08 06:19:03', '2022-08-08 06:19:03');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expired_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `expired_at`, `created_at`, `updated_at`) VALUES
(1, 'App\\User', 214, 'auth_token', '9e2f18188e7e5e723cb7b6f6766791f729b6a2b749ee9fd5f6e9780d670fec68', '[\"auth\"]', NULL, '2022-07-27 05:49:04', '2022-07-27 05:29:04', '2022-07-27 05:29:04'),
(2, 'App\\User', 214, 'auth_token', 'efa3b657bf8e5d978394e69e5dca2f74e046383ebaddfc5401373d4b156403a5', '[\"auth\"]', NULL, '2022-07-27 10:48:15', '2022-07-27 10:28:15', '2022-07-27 10:28:15'),
(3, 'App\\User', 214, 'refresh_token', 'aec2520097c5bd1ada12289455cd45516a868d38def4d40b9d6faefaa7f5df8e', '[\"refresh\"]', NULL, '2022-07-27 10:48:15', '2022-07-27 10:28:15', '2022-07-27 10:28:15'),
(4, 'App\\User', 172, 'auth_token', '33ef9d1e2aa168af5f7ef3304fc362d458f34fbdca1656ada96c353b8f78b4a8', '[\"auth\"]', NULL, '2022-07-28 17:13:20', '2022-07-28 16:53:20', '2022-07-28 16:53:20'),
(5, 'App\\User', 172, 'refresh_token', '5dd52dcf776fdc1b3d4d6e236de8445103e86a7044aa17ce9694f5b5e5584817', '[\"refresh\"]', NULL, '2022-07-28 17:13:21', '2022-07-28 16:53:21', '2022-07-28 16:53:21'),
(6, 'App\\User', 172, 'auth_token', '657997052e0361e4b9a3d86c539d9d1660b125484696f62f2a52c4af58386b8a', '[\"auth\"]', NULL, '2022-07-28 18:21:10', '2022-07-28 18:01:10', '2022-07-28 18:01:10'),
(7, 'App\\User', 172, 'refresh_token', '7198a48631966cd02830261f70e9d9090e13efeb9219eda9054cfef73ff5ed5c', '[\"refresh\"]', NULL, '2022-07-28 18:21:10', '2022-07-28 18:01:10', '2022-07-28 18:01:10'),
(8, 'App\\User', 172, 'auth_token', 'b20c9012a9c39cd1ec4902cc22ced1cba3d52c702d558ed5d47c4ccbf2d3fd8b', '[\"auth\"]', NULL, '2022-07-28 19:52:25', '2022-07-28 19:32:25', '2022-07-28 19:32:25'),
(9, 'App\\User', 172, 'refresh_token', 'cd1465b53cb05aeccad7191810014e6eecfea6e66828d0bd49e5aacbe5483e2d', '[\"refresh\"]', NULL, '2022-07-28 19:52:25', '2022-07-28 19:32:25', '2022-07-28 19:32:25'),
(10, 'App\\User', 102, 'auth_token', '50f2d015704dccc48d3eaece532d5fda72d7ab5473a2a620a0374c82247a186e', '[\"auth\"]', NULL, '2022-07-29 04:44:50', '2022-07-29 04:24:50', '2022-07-29 04:24:50'),
(11, 'App\\User', 102, 'refresh_token', 'e43b096a2820f91852134566c577a2ed171439b950581837a94591ec8e9d0867', '[\"refresh\"]', NULL, '2022-07-29 04:44:50', '2022-07-29 04:24:50', '2022-07-29 04:24:50'),
(12, 'App\\User', 172, 'auth_token', '71313faa0b933436877ae856df970dfda89d7cc22295ee493cd7c0c0ceec180d', '[\"auth\"]', NULL, '2022-07-29 04:55:10', '2022-07-29 04:35:10', '2022-07-29 04:35:10'),
(13, 'App\\User', 172, 'refresh_token', 'd2c45924b0b4bca256f9d56e45b09f9ba9d40b4e3d5351682007908c54cf3892', '[\"refresh\"]', NULL, '2022-07-29 04:55:10', '2022-07-29 04:35:10', '2022-07-29 04:35:10'),
(14, 'App\\User', 214, 'auth_token', '3d5bab381a61aede434ec6eb3d35202a08c3ea61705f3baf0cd7f4d57d15044c', '[\"auth\"]', NULL, '2022-07-29 15:44:03', '2022-07-29 15:24:03', '2022-07-29 15:24:03'),
(15, 'App\\User', 214, 'refresh_token', '7ab15d146abade58691a06e5aa8e7d45131fe8508dea13b6ef6d0223d5c8ef0f', '[\"refresh\"]', NULL, '2022-07-29 15:44:03', '2022-07-29 15:24:03', '2022-07-29 15:24:03'),
(16, 'App\\User', 172, 'auth_token', '34c655c89a7351d901ad9db9ce8e684f02f9f4956466dd0e1ab094298323e033', '[\"auth\"]', NULL, '2022-07-29 16:52:14', '2022-07-29 16:32:14', '2022-07-29 16:32:14'),
(17, 'App\\User', 172, 'refresh_token', 'de4a2de58a2c78294a7350cb422154c41fe75bd4e7393042c6d15a8a5b87cf6a', '[\"refresh\"]', NULL, '2022-07-29 16:52:14', '2022-07-29 16:32:14', '2022-07-29 16:32:14'),
(18, 'App\\User', 214, 'auth_token', '624d6e8735dccd4c8e0f74c14be3849dba94ae292b3583118e0838e355064d8d', '[\"auth\"]', NULL, '2022-07-30 03:57:46', '2022-07-30 03:37:46', '2022-07-30 03:37:46'),
(19, 'App\\User', 214, 'refresh_token', 'f4cbd643b926e8c67b4e56f81c279e546c43a95dabee1aa1c87ff62246884632', '[\"refresh\"]', NULL, '2022-07-30 03:57:46', '2022-07-30 03:37:46', '2022-07-30 03:37:46'),
(20, 'App\\User', 214, 'auth_token', 'cc52c7dc7e21a5669fc17b5ad15e68c4fb9b8963b1a859eb009a85f15a75f325', '[\"auth\"]', NULL, '2022-07-30 03:57:49', '2022-07-30 03:37:49', '2022-07-30 03:37:49'),
(21, 'App\\User', 214, 'refresh_token', 'b7c2b89c031faba6e3456bff216afb3b7aea68c9637089f1f0065160cafd302b', '[\"refresh\"]', NULL, '2022-07-30 03:57:49', '2022-07-30 03:37:49', '2022-07-30 03:37:49'),
(22, 'App\\User', 214, 'auth_token', '88f2401544d48c3ab0e60ec8316d6aa69a3a4e0a7d57f45ba324b59a1200b0d7', '[\"auth\"]', NULL, '2022-07-30 03:58:43', '2022-07-30 03:38:43', '2022-07-30 03:38:43'),
(23, 'App\\User', 102, 'auth_token', '29b331c861d624f3b2b1aba210a3c74b30bbed3d457208785b77d52146d22eb6', '[\"auth\"]', NULL, '2022-07-30 03:59:04', '2022-07-30 03:39:04', '2022-07-30 03:39:04'),
(24, 'App\\User', 102, 'refresh_token', '1300db5c4210f1503663e0d57d61288631fe92ee6b7fad2e505b2093e7903667', '[\"refresh\"]', NULL, '2022-07-30 03:59:04', '2022-07-30 03:39:04', '2022-07-30 03:39:04'),
(25, 'App\\User', 102, 'auth_token', 'b85624d48206bc589482392734bb84185b4591bf653f60455be1c9a47db1ca15', '[\"auth\"]', NULL, '2022-07-30 03:59:15', '2022-07-30 03:39:15', '2022-07-30 03:39:15'),
(26, 'App\\User', 172, 'auth_token', '6745e3589e5a8253a3bb0357f20e9bdb62096d8271459d4e5fffc7fdb17f2d12', '[\"auth\"]', NULL, '2022-07-30 04:05:42', '2022-07-30 03:45:42', '2022-07-30 03:45:42'),
(27, 'App\\User', 172, 'refresh_token', '812cf6967c2c69accafc4c117368335fd9a866c281da85c79ee460e4aa5a9049', '[\"refresh\"]', NULL, '2022-07-30 04:05:42', '2022-07-30 03:45:42', '2022-07-30 03:45:42'),
(28, 'App\\User', 172, 'auth_token', 'f483ddb116be4eef5db8ea78fa11462033b287bfc82041c7cb81d752ca658b14', '[\"auth\"]', NULL, '2022-07-30 04:13:10', '2022-07-30 03:53:10', '2022-07-30 03:53:10'),
(29, 'App\\User', 172, 'refresh_token', '9a8478f9aa861ce275d4f3a0668be871c5fae72e32e438a1844f602803ed6747', '[\"refresh\"]', NULL, '2022-07-30 04:13:10', '2022-07-30 03:53:10', '2022-07-30 03:53:10'),
(30, 'App\\User', 172, 'auth_token', '263a49a6a60e13bd7c002b602c672df301c81e1f4d688f66daac1136cf388557', '[\"auth\"]', NULL, '2022-07-30 04:13:31', '2022-07-30 03:53:31', '2022-07-30 03:53:31'),
(31, 'App\\User', 172, 'auth_token', '5a9573d334020e0e19813b248647756924429a3cfc01bb2583845c5aa6337a9a', '[\"auth\"]', NULL, '2022-07-30 04:13:59', '2022-07-30 03:53:59', '2022-07-30 03:53:59'),
(32, 'App\\User', 172, 'refresh_token', '7d02d7246da0b03f62c645d2c2d589d3ba99cc6edb6c364977f7824e3bccb6bd', '[\"refresh\"]', NULL, '2022-07-30 04:13:59', '2022-07-30 03:53:59', '2022-07-30 03:53:59'),
(33, 'App\\User', 172, 'auth_token', '242039a4c3ad0251444646144579401fd72dd106f8778b3e9556794abc23adb6', '[\"auth\"]', NULL, '2022-07-30 04:14:21', '2022-07-30 03:54:21', '2022-07-30 03:54:21'),
(34, 'App\\User', 172, 'auth_token', 'd592faccf424da9f3d120a873bf530f759b3b4c37db472fbbbf2933fe2005da4', '[\"auth\"]', NULL, '2022-07-30 04:15:29', '2022-07-30 03:55:29', '2022-07-30 03:55:29'),
(35, 'App\\User', 172, 'refresh_token', 'c116d51f2847109d68544320890e2678f6faa99a5def31637cc8807ceb9f777a', '[\"refresh\"]', NULL, '2022-07-30 04:15:29', '2022-07-30 03:55:29', '2022-07-30 03:55:29'),
(36, 'App\\User', 172, 'auth_token', '473efe0a3e77bbe8bac6860d9f973aef6810fd65d8ea526aa786bf068fd86ebc', '[\"auth\"]', NULL, '2022-07-30 04:16:00', '2022-07-30 03:56:00', '2022-07-30 03:56:00'),
(37, 'App\\User', 172, 'auth_token', 'e0c5525d03a00ee0b2e38f6bd90079b3d9989b2a758f3243e328dcc84bcf3b10', '[\"auth\"]', NULL, '2022-07-30 04:22:37', '2022-07-30 04:02:37', '2022-07-30 04:02:37'),
(38, 'App\\User', 172, 'auth_token', 'aa69432c86f599b25cd74e6265399a235679fec80a98400964143f678ac709cd', '[\"auth\"]', NULL, '2022-07-30 04:23:12', '2022-07-30 04:03:12', '2022-07-30 04:03:12'),
(39, 'App\\User', 172, 'refresh_token', 'a10a850617059a78c8352d112bc4d1f4c3bbf973415653ef51e6a06bcdd058fc', '[\"refresh\"]', NULL, '2022-07-30 04:23:12', '2022-07-30 04:03:12', '2022-07-30 04:03:12'),
(40, 'App\\User', 172, 'auth_token', '07d463c1754bbe56124970938b2a385912a70d86ab5f4f749855573a038a4fe6', '[\"auth\"]', NULL, '2022-07-30 04:23:23', '2022-07-30 04:03:23', '2022-07-30 04:03:23'),
(41, 'App\\User', 214, 'auth_token', '8e5e56d81e9146c393c823fec8d69df9a7436b0eefb5253da8911bed21ddc4c1', '[\"auth\"]', NULL, '2022-07-30 04:43:36', '2022-07-30 04:23:36', '2022-07-30 04:23:36'),
(42, 'App\\User', 214, 'refresh_token', 'ad91b156bdfb9330223549c2c667f3e4ed6e53752ce7da62277f0444c6031f36', '[\"refresh\"]', NULL, '2022-07-30 04:43:36', '2022-07-30 04:23:36', '2022-07-30 04:23:36'),
(43, 'App\\User', 214, 'auth_token', '1f4312a53f036a1575035ced58e6f40562c47b235f3f70a13d87febbff62f5ba', '[\"auth\"]', NULL, '2022-07-30 04:54:31', '2022-07-30 04:34:31', '2022-07-30 04:34:31'),
(44, 'App\\User', 214, 'refresh_token', '6413f048db42d5dd0669e8b8367839ac7045c2697232df4132fe820d58467bd0', '[\"refresh\"]', NULL, '2022-07-30 04:54:31', '2022-07-30 04:34:31', '2022-07-30 04:34:31'),
(45, 'App\\User', 214, 'auth_token', '98c91c27362fe489e9f0eba0283c693ee8817a32de71d8e429b8ae33dbfe8ce3', '[\"auth\"]', NULL, '2022-07-30 05:01:34', '2022-07-30 04:41:34', '2022-07-30 04:41:34'),
(46, 'App\\User', 214, 'refresh_token', '3deeda1dcb47da8990ed0556aca3d453fae804a5d7aefd7bee851bb591d3eccd', '[\"refresh\"]', NULL, '2022-07-30 05:01:34', '2022-07-30 04:41:34', '2022-07-30 04:41:34'),
(47, 'App\\User', 214, 'auth_token', '694c3846319ea7f96e33214c7ef5d07c9010eec609c5988d707165efa44e22b2', '[\"auth\"]', NULL, '2022-07-30 05:02:52', '2022-07-30 04:42:52', '2022-07-30 04:42:52'),
(48, 'App\\User', 214, 'refresh_token', '82bff060698ce7d8a74a01c3e16a8545d771286b53b11ebd10f3e27df392e957', '[\"refresh\"]', NULL, '2022-07-30 05:02:52', '2022-07-30 04:42:52', '2022-07-30 04:42:52'),
(49, 'App\\User', 214, 'auth_token', 'db829711427c5728c08362be343630f7f33859263b97786804641e131011d3a6', '[\"auth\"]', NULL, '2022-07-30 05:10:45', '2022-07-30 04:50:45', '2022-07-30 04:50:45'),
(50, 'App\\User', 214, 'refresh_token', '1dc5f3f00e994d43114d351b31450b7682933d5a26bde462d65196d6f8e9d97c', '[\"refresh\"]', NULL, '2022-07-30 05:10:45', '2022-07-30 04:50:45', '2022-07-30 04:50:45'),
(51, 'App\\User', 214, 'auth_token', '7b9f3cb473836780b18e2214aafb0085e4f14cbf344a8c74a627bf13cb54b829', '[\"auth\"]', NULL, '2022-07-30 05:11:55', '2022-07-30 04:51:55', '2022-07-30 04:51:55'),
(52, 'App\\User', 214, 'refresh_token', '42e39ab52867d2153c76819c379898c393a683ec28cdf9d9a4ac872c0d9dec00', '[\"refresh\"]', NULL, '2022-07-30 05:11:55', '2022-07-30 04:51:55', '2022-07-30 04:51:55'),
(53, 'App\\User', 214, 'auth_token', 'f631f99dc72840f047549cf89b1bd92878a2977ea77a1c04a58d0fc72f2f6b3c', '[\"auth\"]', NULL, '2022-07-30 05:18:04', '2022-07-30 04:58:04', '2022-07-30 04:58:04'),
(54, 'App\\User', 214, 'refresh_token', '58eab8154e4e343b5fb08710ed13792828d84d99196ea9e590b0e47a7bb70bb7', '[\"refresh\"]', NULL, '2022-07-30 05:18:04', '2022-07-30 04:58:04', '2022-07-30 04:58:04'),
(55, 'App\\User', 214, 'auth_token', 'b47a6d62cf202f434001f6e1f358e06fa212b808cdff67fd2c35c225a7c39b25', '[\"auth\"]', NULL, '2022-07-30 05:19:22', '2022-07-30 04:59:22', '2022-07-30 04:59:22'),
(56, 'App\\User', 214, 'refresh_token', '5ce2ee791c0167bd417a6f44739fbb57e9b81f9cbf5f50250a5bb5ab354babbf', '[\"refresh\"]', NULL, '2022-07-30 05:19:22', '2022-07-30 04:59:22', '2022-07-30 04:59:22'),
(57, 'App\\User', 214, 'auth_token', '92c638c5da1b371bbcfb3673c19e0a80912d6e7c4d925d2ba76a74f7c1fe8795', '[\"auth\"]', NULL, '2022-07-30 05:19:55', '2022-07-30 04:59:55', '2022-07-30 04:59:55'),
(58, 'App\\User', 214, 'refresh_token', '68192193ab548f2e83eb502f5b45439fd2764bd9184a3cd31ce331200a35f5c9', '[\"refresh\"]', NULL, '2022-07-30 05:19:55', '2022-07-30 04:59:55', '2022-07-30 04:59:55'),
(59, 'App\\User', 214, 'auth_token', '205a1195e68e7682ab23c145dfd130dbefcc117ad0562dc5c54106db6421ac68', '[\"auth\"]', NULL, '2022-07-30 05:29:18', '2022-07-30 05:09:18', '2022-07-30 05:09:18'),
(60, 'App\\User', 214, 'refresh_token', '79ddb0c5fb7780d44971c4a43214ce16d7a21378a92319cca14bc759a42e480f', '[\"refresh\"]', NULL, '2022-07-30 05:29:18', '2022-07-30 05:09:18', '2022-07-30 05:09:18'),
(61, 'App\\User', 214, 'auth_token', '322a4b0eeac610f5f0d85f7465d955c0ca3dd62dc763333034b6720337038e1f', '[\"auth\"]', NULL, '2022-07-30 06:42:11', '2022-07-30 06:22:11', '2022-07-30 06:22:11'),
(62, 'App\\User', 214, 'auth_token', 'd2e669549a0149f22c1401a1eb3b6e8e0202239243376f26c467a9661cddc47c', '[\"auth\"]', NULL, '2022-07-30 06:42:34', '2022-07-30 06:22:34', '2022-07-30 06:22:34'),
(63, 'App\\User', 172, 'auth_token', '3eaf34fbdd0c9430d41e655856c3014507fdd092efaf5850b27ddbc9caa10bd8', '[\"auth\"]', NULL, '2022-07-30 09:00:11', '2022-07-30 08:40:11', '2022-07-30 08:40:11'),
(64, 'App\\User', 230, 'auth_token', '72b0c13814d304f7dc715c9f0f8f7f152e26069ac9cc2927ef3c0c7e7922d358', '[\"auth\"]', NULL, '2022-08-05 06:41:08', '2022-08-05 06:21:08', '2022-08-05 06:21:08'),
(65, 'App\\User', 230, 'auth_token', 'd094b319f2541c11556e9a2656734bfad3d3a3aaaf84d1a9263cfde912455117', '[\"auth\"]', NULL, '2022-08-08 06:26:44', '2022-08-08 06:06:44', '2022-08-08 06:06:44'),
(66, 'App\\User', 172, 'auth_token', '3ed9a8576de3f05fc32be19d2d7c3b514aca62fc5e6b90358ce31d6905c17574', '[\"auth\"]', NULL, '2022-08-08 06:34:53', '2022-08-08 06:14:53', '2022-08-08 06:14:53'),
(67, 'App\\User', 230, 'auth_token', 'c985312ad48edd54aeb7a1356686f5696142da7eed27433af6f0bd8c3869f712', '[\"auth\"]', NULL, '2022-08-08 06:37:26', '2022-08-08 06:17:26', '2022-08-08 06:17:26'),
(68, 'App\\User', 227, 'auth_token', '0b15361c70bf4028df772aa424aa4d1742a4d9f5ecd2c436c23b45d9d8e8b9e9', '[\"auth\"]', NULL, '2022-08-15 05:42:22', '2022-08-15 05:22:22', '2022-08-15 05:22:22'),
(69, 'App\\User', 227, 'auth_token', '3cfc8729e2a3f235a9c163e7737102b53bd1ec0d736cfa2afcaf8bec2e910fbe', '[\"auth\"]', NULL, '2022-08-16 18:21:54', '2022-08-16 18:01:54', '2022-08-16 18:01:54'),
(70, 'App\\User', 227, 'auth_token', '93b08964af582fc788408892f1c3d9de872560077781e799d91e12c6f0843714', '[\"auth\"]', NULL, '2022-08-16 19:18:29', '2022-08-16 18:58:29', '2022-08-16 18:58:29'),
(71, 'App\\User', 227, 'auth_token', '691f3d40e3bca77ee19607ccac3ba936b54fc7356bfadb8b116e915fcee840fa', '[\"auth\"]', NULL, '2022-08-16 19:18:33', '2022-08-16 18:58:33', '2022-08-16 18:58:33'),
(72, 'App\\User', 200, 'auth_token', '035ed5245ca855c7a79fe059771b8860f6b60d09b7f64d9b5de94ebf997e038e', '[\"auth\"]', NULL, '2022-08-16 19:18:53', '2022-08-16 18:58:53', '2022-08-16 18:58:53'),
(73, 'App\\User', 200, 'auth_token', 'd6a1f2bde15354d535cb860edfeb8cefe0653d3ceab84ac5c2e95404803ac994', '[\"auth\"]', NULL, '2022-08-16 19:18:57', '2022-08-16 18:58:57', '2022-08-16 18:58:57'),
(74, 'App\\User', 200, 'auth_token', '827271a9dbb21da450f170f2e89372ae6c5a550bc6b168d17bba844564abf4fd', '[\"auth\"]', NULL, '2022-08-16 19:19:18', '2022-08-16 18:59:18', '2022-08-16 18:59:18'),
(75, 'App\\User', 227, 'auth_token', '3112be0f34609a55b0b6da63d85c465d06c8165128d39c5632a8f6b906b51deb', '[\"auth\"]', NULL, '2022-08-16 19:20:24', '2022-08-16 19:00:24', '2022-08-16 19:00:24'),
(76, 'App\\User', 227, 'auth_token', 'a8215264b939d2feaddf6fe113587e3cde5bee2b3432dda4b3571e726b22ec56', '[\"auth\"]', NULL, '2022-08-16 19:20:30', '2022-08-16 19:00:30', '2022-08-16 19:00:30'),
(77, 'App\\User', 227, 'auth_token', 'f117a0115ab3f80b0082f68233619a65cc66f44a2c9a3b32a28d9d627e59f212', '[\"auth\"]', NULL, '2022-08-16 19:20:41', '2022-08-16 19:00:41', '2022-08-16 19:00:41'),
(78, 'App\\User', 172, 'auth_token', '0863e00172c7623aba0a1bbcf9226fb196b3d6bf89920594e42c257a1ebe2eac', '[\"auth\"]', NULL, '2022-08-16 19:20:59', '2022-08-16 19:00:59', '2022-08-16 19:00:59'),
(79, 'App\\User', 172, 'auth_token', '4f36efbc9abfec52eded1a96a009ed5817b2ee922449301aad01f473c39103cc', '[\"auth\"]', NULL, '2022-08-16 19:21:03', '2022-08-16 19:01:03', '2022-08-16 19:01:03'),
(80, 'App\\User', 172, 'auth_token', 'ee496853ce6c4a0b37e2ad5c59468ad1cbbaf6d3a19bd453f14e4ed020f3cdc2', '[\"auth\"]', NULL, '2022-08-16 19:21:08', '2022-08-16 19:01:08', '2022-08-16 19:01:08'),
(81, 'App\\User', 172, 'auth_token', 'fcc77db751979738c1fcbdc16642e2eab1bae846626efc50b6052cda2d19b284', '[\"auth\"]', NULL, '2022-08-16 19:21:37', '2022-08-16 19:01:37', '2022-08-16 19:01:37'),
(82, 'App\\User', 172, 'auth_token', '763478414dfbb0e457691b0f5615929945fecf46c8286fd82d533845e9990cd3', '[\"auth\"]', NULL, '2022-08-16 19:21:45', '2022-08-16 19:01:45', '2022-08-16 19:01:45'),
(83, 'App\\User', 172, 'auth_token', 'd7eaa5037565de156476e1710382ec5ea56dc762f02dfcc99d47752f652d0ae9', '[\"auth\"]', NULL, '2022-08-16 19:22:13', '2022-08-16 19:02:13', '2022-08-16 19:02:13'),
(84, 'App\\User', 172, 'auth_token', 'ca8b3ac0df737230f048759aa6558cdabd9cdb279d48fd8b389363777b55fc7e', '[\"auth\"]', NULL, '2022-08-16 19:22:53', '2022-08-16 19:02:53', '2022-08-16 19:02:53'),
(85, 'App\\User', 172, 'auth_token', '81ead90e8c1a8cf95ed08e7fc63f41e232a72beeda7be244b33c824d3c6d42d8', '[\"auth\"]', NULL, '2022-08-16 19:23:34', '2022-08-16 19:03:34', '2022-08-16 19:03:34'),
(86, 'App\\User', 227, 'auth_token', 'f1bd0208c38aee848df1280e1e046ebe7a1686b4b97b8cf2a9bd1ecf567d7bf5', '[\"auth\"]', NULL, '2022-08-16 19:24:52', '2022-08-16 19:04:52', '2022-08-16 19:04:52'),
(87, 'App\\User', 227, 'auth_token', '4f54083d51c33f91f7d4634586f48c6e150858c2efa5b266c561f4a352875630', '[\"auth\"]', NULL, '2022-08-16 19:25:17', '2022-08-16 19:05:17', '2022-08-16 19:05:17'),
(88, 'App\\User', 227, 'auth_token', '93f81a9148c1f79b33dd711afd8556973b8b98aac7c0ec4e4ae3320d3193a307', '[\"auth\"]', NULL, '2022-08-16 19:25:21', '2022-08-16 19:05:21', '2022-08-16 19:05:21'),
(89, 'App\\User', 227, 'auth_token', 'f7c0d6284c5e8dc58b0d3c7018bdf0cc03b6599707b55e44a365fe76685cc838', '[\"auth\"]', NULL, '2022-08-16 19:25:37', '2022-08-16 19:05:37', '2022-08-16 19:05:37'),
(90, 'App\\User', 227, 'auth_token', '8362ba147d1fd7449ec5c9c57c5e6d04b7a9475abdb763371161600303b80695', '[\"auth\"]', NULL, '2022-08-16 19:25:42', '2022-08-16 19:05:42', '2022-08-16 19:05:42'),
(91, 'App\\User', 227, 'auth_token', 'e0df66c183b8423f1a1bc136b94556669b1a45590cb3fedf773e591ca1ce1d35', '[\"auth\"]', NULL, '2022-08-16 19:25:46', '2022-08-16 19:05:46', '2022-08-16 19:05:46'),
(92, 'App\\User', 172, 'auth_token', '40bee200ca14445eefcdb6631ea29b3947508303b067dc976fc9adf10f311f08', '[\"auth\"]', NULL, '2022-08-16 19:28:26', '2022-08-16 19:08:26', '2022-08-16 19:08:26'),
(93, 'App\\User', 172, 'auth_token', '955a92921ffb6115020909be0ba3bd7cc070d9fdc6305bf6a05ba8763591ff40', '[\"auth\"]', NULL, '2022-08-16 19:28:58', '2022-08-16 19:08:58', '2022-08-16 19:08:58'),
(94, 'App\\User', 172, 'auth_token', '407bc5e6640be6b557a018c49b51f87e72b5ca0b9dc8af6dc60c8f59ee1d2e9b', '[\"auth\"]', NULL, '2022-08-16 19:29:35', '2022-08-16 19:09:35', '2022-08-16 19:09:35'),
(95, 'App\\User', 227, 'auth_token', '93c2328803d5bcaaa0ef6e9f0cce245c6faa52d70b8cc149e818bf34992fb5c5', '[\"auth\"]', NULL, '2022-08-17 02:25:06', '2022-08-17 02:05:06', '2022-08-17 02:05:06'),
(96, 'App\\User', 231, 'auth_token', 'ae2624351f32a93ef77b807d48e97eb4b3832fea1cc2b7a3a58c85ccd57c04ff', '[\"auth\"]', NULL, '2022-08-17 15:24:43', '2022-08-17 15:04:43', '2022-08-17 15:04:43'),
(97, 'App\\User', 230, 'auth_token', '0e0cf3016645b610652683a1a7675a0c49e45f4436256a9f4a43e98075481817', '[\"auth\"]', NULL, '2022-08-17 15:30:18', '2022-08-17 15:10:18', '2022-08-17 15:10:18'),
(98, 'App\\User', 231, 'auth_token', '920ea9d1ef5b205f02b076c718db5228f7071be6dda87b0a01a5d318cee78d9f', '[\"auth\"]', NULL, '2022-08-17 15:36:53', '2022-08-17 15:16:53', '2022-08-17 15:16:53'),
(99, 'App\\User', 172, 'auth_token', '5d8a11baed85b45de66d116322ed61a993986abd716d77c969962dffcdac62c7', '[\"auth\"]', NULL, '2022-08-17 15:51:31', '2022-08-17 15:31:31', '2022-08-17 15:31:31'),
(100, 'App\\User', 227, 'auth_token', 'f7e509bd45728aefc01f320b7831fc89564035cccd9c9ddd72b6730109edc033', '[\"auth\"]', NULL, '2022-08-18 02:46:02', '2022-08-18 02:26:02', '2022-08-18 02:26:02'),
(101, 'App\\User', 231, 'auth_token', 'b0908cc8e6448d1c5c3cf23c3bd4b11fd27be098dde6d47eeae06ef6e59c3564', '[\"auth\"]', NULL, '2022-08-19 02:09:58', '2022-08-19 01:49:58', '2022-08-19 01:49:58'),
(102, 'App\\User', 231, 'auth_token', '8153a4796af088b551f343d9efbf4e92c5dd4a60d961a399bd4e6635019253fa', '[\"auth\"]', NULL, '2022-08-19 02:11:25', '2022-08-19 01:51:25', '2022-08-19 01:51:25'),
(103, 'App\\User', 230, 'auth_token', '3413b358b11defa5bae4ca8e3d84e4503f4f9e234f0a8879accf90a3e52c9ac3', '[\"auth\"]', NULL, '2022-08-19 02:12:31', '2022-08-19 01:52:31', '2022-08-19 01:52:31'),
(104, 'App\\User', 230, 'auth_token', 'c387297b921b23e5bddd11f1e8eefb8719abbac316e169baebf81ba3587b5b57', '[\"auth\"]', NULL, '2022-08-19 08:55:23', '2022-08-19 08:35:23', '2022-08-19 08:35:23'),
(105, 'App\\User', 231, 'auth_token', '937ad7833acbec1f2a2c7ed16b3ee82a64cc013a08cca37b83ebe31ebda482c1', '[\"auth\"]', NULL, '2022-08-19 08:57:52', '2022-08-19 08:37:52', '2022-08-19 08:37:52'),
(106, 'App\\User', 230, 'auth_token', 'd49e6ea9f03b28d8a146a7a9f20c4aa5663a9ebe8408167f10dc9160186a1321', '[\"auth\"]', NULL, '2022-09-01 07:15:10', '2022-09-01 06:55:10', '2022-09-01 06:55:10'),
(107, 'App\\User', 231, 'auth_token', '46e008a13386836fa88671b3bb74b79d7f67d46b432b639677a05cc6525882f0', '[\"auth\"]', NULL, '2022-09-12 08:06:15', '2022-09-12 07:46:15', '2022-09-12 07:46:15'),
(108, 'App\\User', 230, 'auth_token', 'de02174d1fc337310ab6fe3a9e855d8c7d4bf9e53beca22b54c459ab05a86734', '[\"auth\"]', NULL, '2022-09-12 09:20:59', '2022-09-12 09:00:59', '2022-09-12 09:00:59'),
(109, 'App\\User', 227, 'auth_token', '8cfbd3d3fd42ece2f1d5cc343f963356437f86bce8a0cf9627d74cdedc84daba', '[\"auth\"]', NULL, '2022-09-12 14:23:53', '2022-09-12 14:03:53', '2022-09-12 14:03:53'),
(110, 'App\\User', 227, 'auth_token', 'afb117877de8db808f63f85a587818ad125ad541ce2c56f7f2cf968dbebb0b98', '[\"auth\"]', NULL, '2022-10-25 03:47:53', '2022-10-25 03:27:53', '2022-10-25 03:27:53'),
(111, 'App\\User', 227, 'auth_token', '3c6bf5c2983a53171279d708bd01adbfcb4216bd1afe9d7b12e8e6c95de8be70', '[\"auth\"]', NULL, '2022-10-25 03:58:21', '2022-10-25 03:38:21', '2022-10-25 03:38:21'),
(112, 'App\\User', 241, 'auth_token', 'e8732ab2a589a383b8f55d847a1ef0246dc501eb084fe345ea2dea4ccd642e3f', '[\"auth\"]', NULL, '2022-10-25 03:59:10', '2022-10-25 03:39:10', '2022-10-25 03:39:10'),
(113, 'App\\User', 227, 'auth_token', 'd6f9b5dbf926d1aac215a2753186e7d3ede9c459b253e567f2287f212e9e1fac', '[\"auth\"]', NULL, '2022-10-30 05:56:37', '2022-10-30 05:36:37', '2022-10-30 05:36:37'),
(114, 'App\\User', 227, 'auth_token', '0631b0428604ecaf3c0e49f276e4246b55ca3d7a59f1aa90f4294e2fb53afb20', '[\"auth\"]', NULL, '2022-10-30 17:48:09', '2022-10-30 17:28:09', '2022-10-30 17:28:09'),
(115, 'App\\User', 231, 'auth_token', '95643ccf5af71ade9a39543acf284c1e3de7636a0a323d776c65e9b8a93b72d7', '[\"auth\"]', NULL, '2022-10-31 05:28:52', '2022-10-31 05:08:52', '2022-10-31 05:08:52'),
(116, 'App\\User', 230, 'auth_token', '156bcaab3d02a0256a8ad569b1d4aff1eb6c8f901dc434ec50cd78a72237f5eb', '[\"auth\"]', NULL, '2022-10-31 05:38:10', '2022-10-31 05:18:10', '2022-10-31 05:18:10'),
(117, 'App\\User', 231, 'auth_token', '4d675e4b07439ff402345c412746ec5b8005fbfb87ccb052b38f384fc945ff7c', '[\"auth\"]', NULL, '2022-11-07 06:56:47', '2022-11-07 06:36:47', '2022-11-07 06:36:47'),
(118, 'App\\User', 231, 'auth_token', 'f922d4d24237b6af1be565845ccdc360b995404a851e34b7e4d6f77fc9e7e8cc', '[\"auth\"]', NULL, '2022-11-07 07:28:30', '2022-11-07 07:08:30', '2022-11-07 07:08:30'),
(119, 'App\\User', 230, 'auth_token', '17773aa345cb405ef62398f3b071b5f4148c378995d715fd52d93a777700b3ff', '[\"auth\"]', NULL, '2022-11-07 07:32:53', '2022-11-07 07:12:53', '2022-11-07 07:12:53'),
(120, 'App\\User', 230, 'auth_token', '87bdb47b73970c20d58facb8bdd30c451615f7895c29b5c7546e518fbb64b969', '[\"auth\"]', NULL, '2022-11-08 06:36:52', '2022-11-08 06:16:52', '2022-11-08 06:16:52'),
(121, 'App\\User', 231, 'auth_token', 'f391add21bcbe619a92d1023988c219d90bc099274ca47e2e6fdd18704b1d3e9', '[\"auth\"]', NULL, '2022-11-08 06:43:55', '2022-11-08 06:23:55', '2022-11-08 06:23:55'),
(122, 'App\\User', 230, 'auth_token', 'ad57ab552d2a1167a1c64cc1c93980d6d52b584e6f7785f830c53eeef3cff94b', '[\"auth\"]', NULL, '2022-11-11 06:17:23', '2022-11-11 05:57:23', '2022-11-11 05:57:23'),
(123, 'App\\User', 231, 'auth_token', '5162832c12852c3a569b9fd5eb2a915b9b10f10deb13d1135a8fc56b107143d6', '[\"auth\"]', NULL, '2022-11-11 06:19:36', '2022-11-11 05:59:36', '2022-11-11 05:59:36'),
(124, 'App\\User', 230, 'auth_token', 'a01836d68a01d5a643b58eb54d336c60d2c05d6e46560deba27ca4f670809655', '[\"auth\"]', NULL, '2022-11-14 10:44:50', '2022-11-14 10:24:50', '2022-11-14 10:24:50'),
(125, 'App\\User', 231, 'auth_token', 'b42e82d69e92ac716dd80e0ed5f106e5ea068494436a37778a71b9f9155dc7ed', '[\"auth\"]', NULL, '2022-11-14 10:46:55', '2022-11-14 10:26:55', '2022-11-14 10:26:55'),
(126, 'App\\User', 241, 'auth_token', 'ad39bae54921511d8171e38d85fdd316ed0895aebca656def73d867c4a3ce5a5', '[\"auth\"]', NULL, '2022-12-21 14:14:02', '2022-12-21 13:54:02', '2022-12-21 13:54:02'),
(127, 'App\\User', 241, 'auth_token', '297c1379402a74818d127bcd2433a2f05f3cec21e25f860121b08ea1628e4471', '[\"auth\"]', NULL, '2022-12-21 14:17:26', '2022-12-21 13:57:26', '2022-12-21 13:57:26'),
(128, 'App\\User', 1, 'auth_token', '32865fdb363596a94d326b86c7855604043297be8e60c1f1a7e0969a7fd362c7', '[\"auth\"]', NULL, '2023-12-27 14:18:49', '2022-12-21 13:58:49', '2022-12-21 13:58:49');

-- --------------------------------------------------------

--
-- Table structure for table `policies`
--

CREATE TABLE `policies` (
  `id` int(11) NOT NULL,
  `points` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `policies`
--

INSERT INTO `policies` (`id`, `points`) VALUES
(1, 'privacy plicy');

-- --------------------------------------------------------

--
-- Table structure for table `regions`
--

CREATE TABLE `regions` (
  `id` int(11) NOT NULL,
  `name` varchar(180) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `regions`
--

INSERT INTO `regions` (`id`, `name`) VALUES
(1, 'Asia'),
(3, 'Europe');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(11) NOT NULL,
  `guru_id` int(11) NOT NULL,
  `title` varchar(191) NOT NULL,
  `message` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `guru_id`, `title`, `message`) VALUES
(1, 25, 'tt', 'msg'),
(2, 1002, 'tt1', 'msg2');

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE `skills` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `skill_name` varchar(256) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`id`, `skill_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Painting', 1, '2021-03-12 04:52:36', '2022-12-16 12:37:42'),
(3, 'Drawing', 1, '2021-03-12 04:53:38', '2022-12-16 12:37:42'),
(4, 'Sculpture', 1, '2021-03-12 04:53:45', '2022-12-16 12:37:42'),
(6, 'Dance', 1, '2021-12-17 10:32:08', '2022-12-16 12:37:42'),
(7, 'Music', 1, '2022-03-07 10:17:12', '2022-12-16 12:37:42');

-- --------------------------------------------------------

--
-- Table structure for table `student_details`
--

CREATE TABLE `student_details` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `parent_name` varchar(150) DEFAULT NULL,
  `parent_email` varchar(150) DEFAULT NULL,
  `parent_mobile` varchar(20) DEFAULT NULL,
  `grade` bigint(20) UNSIGNED NOT NULL,
  `skills` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(11) NOT NULL,
  `tutor_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id`, `tutor_id`, `user_id`, `status`) VALUES
(1, 102, 99, 1),
(2, 103, 99, 1),
(3, 172, 196, 1),
(4, 192, 214, 1),
(5, 192, 215, 1),
(6, 224, 230, 1),
(7, 172, 230, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `sub_category_name` varchar(256) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `category_id`, `sub_category_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 'LKG', 1, '2021-03-12 00:45:43', '2022-12-16 12:37:42'),
(2, 4, 'maths', 1, '2021-03-12 01:40:16', '2022-12-16 12:37:42'),
(3, 3, 'UKG', 1, '2021-03-12 03:15:26', '2022-12-16 12:37:42'),
(4, 6, 'Others', 1, '2021-12-17 10:31:41', '2022-12-16 12:37:42');

-- --------------------------------------------------------

--
-- Table structure for table `terms`
--

CREATE TABLE `terms` (
  `id` int(11) NOT NULL,
  `points` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `terms`
--

INSERT INTO `terms` (`id`, `points`) VALUES
(1, 'terms and condition');

-- --------------------------------------------------------

--
-- Table structure for table `tier`
--

CREATE TABLE `tier` (
  `id` int(11) NOT NULL,
  `name` varchar(180) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tier`
--

INSERT INTO `tier` (`id`, `name`) VALUES
(1, 'Tier 1'),
(2, 'Tier 2'),
(3, 'Tier 3');

-- --------------------------------------------------------

--
-- Table structure for table `tier_management`
--

CREATE TABLE `tier_management` (
  `id` int(11) NOT NULL,
  `tier_id` int(11) NOT NULL,
  `country_name` varchar(180) NOT NULL,
  `currency_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tier_management`
--

INSERT INTO `tier_management` (`id`, `tier_id`, `country_name`, `currency_type_id`) VALUES
(8, 2, 'UK', 10),
(12, 1, 'Canada', 21);

-- --------------------------------------------------------

--
-- Table structure for table `tutor_details`
--

CREATE TABLE `tutor_details` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `category` bigint(20) UNSIGNED NOT NULL,
  `sub_category` bigint(20) UNSIGNED NOT NULL,
  `skills` bigint(20) UNSIGNED NOT NULL,
  `about` text DEFAULT NULL,
  `experience` text DEFAULT NULL,
  `cover_letter` varchar(200) DEFAULT NULL,
  `cover_letter_fileName` varchar(100) DEFAULT NULL,
  `video_url` varchar(200) NOT NULL,
  `video_file_name` varchar(100) NOT NULL,
  `video_thumbnail` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `rating` enum('0','1','2','3','4','5') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tutor_details`
--

INSERT INTO `tutor_details` (`id`, `user_id`, `category`, `sub_category`, `skills`, `about`, `experience`, `cover_letter`, `cover_letter_fileName`, `video_url`, `video_file_name`, `video_thumbnail`, `created_at`, `updated_at`, `rating`) VALUES
(144, 335, 2, 3, 1, NULL, NULL, 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/335/TEST.docx', 'TEST.docx', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/profile/TEST%20VIDEO.mp4', 'TEST VIDEO.mp4', 'uploads/tutor/TEST VIDEO.png', '2023-03-17 10:32:41', '2023-03-17 10:32:41', '0'),
(145, 336, 2, 1, 1, NULL, NULL, 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/336/TEST.pdf', 'TEST.pdf', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/profile/TEST%20VIDEO.mp4', 'TEST VIDEO.mp4', 'uploads/tutor/TEST VIDEO.png', '2023-03-20 05:35:15', '2023-03-20 05:40:16', '0'),
(146, 337, 2, 3, 1, NULL, NULL, 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/337/TEST.pdf', 'TEST.pdf', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/profile/TEST%20VIDEO.mp4', 'TEST VIDEO.mp4', 'uploads/tutor/TEST VIDEO.png', '2023-03-21 05:18:57', '2023-03-21 05:18:57', '0'),
(147, 338, 2, 3, 1, NULL, NULL, 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/338/TEST.docx', 'TEST.docx', 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/profile/TEST%20VIDEO.mp4', 'TEST VIDEO.mp4', 'uploads/tutor/TEST VIDEO.png', '2023-03-21 09:26:08', '2023-03-21 09:26:08', '0'),
(149, 341, 2, 1, 1, NULL, NULL, NULL, NULL, 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/profile/file_example_MP4_480_1_5MG.mp4', 'file_example_MP4_480_1_5MG.mp4', 'uploads/tutor/file_example_MP4_480_1_5MG.png', '2023-03-29 05:24:54', '2023-03-29 05:32:40', '0'),
(151, 343, 2, 1, 1, NULL, NULL, NULL, NULL, 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/profile/file_example_MP4_1920_18MG.mp4', 'file_example_MP4_1920_18MG.mp4', 'uploads/tutor/file_example_MP4_1920_18MG.png', '2023-03-30 05:24:51', '2023-03-30 06:51:46', '0'),
(152, 344, 2, 3, 1, NULL, NULL, NULL, NULL, 'https://buddyguruadmin.s3.ap-south-1.amazonaws.com/tutor/profile/big_buck_bunny_720p_1mb.mp4', 'big_buck_bunny_720p_1mb.mp4', 'uploads/tutor/big_buck_bunny_720p_1mb.png', '2023-03-30 06:36:50', '2023-03-30 06:36:50', '0'),
(153, 345, 2, 1, 1, NULL, NULL, NULL, NULL, 'hh', 'abc', 'uu', '2023-04-06 07:20:34', '2023-04-11 02:50:04', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tutor_portfolio`
--

CREATE TABLE `tutor_portfolio` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `description` longtext NOT NULL,
  `title` varchar(191) NOT NULL,
  `video_url` varchar(191) NOT NULL,
  `video_thumbnail` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tutor_sessions`
--

CREATE TABLE `tutor_sessions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `grade_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `batch_id` int(11) NOT NULL,
  `session_name` varchar(191) NOT NULL,
  `description` varchar(191) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `meeting_link` varchar(191) DEFAULT NULL,
  `start_url` text DEFAULT NULL,
  `meeting_id` varchar(191) DEFAULT NULL,
  `what_learn` varchar(191) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tutor_sessions`
--

INSERT INTO `tutor_sessions` (`id`, `user_id`, `grade_id`, `category_id`, `batch_id`, `session_name`, `description`, `date`, `time`, `meeting_link`, `start_url`, `meeting_id`, `what_learn`, `created_at`, `updated_at`) VALUES
(47, 195, NULL, NULL, 39, 'First session basic dance steps', 'learn basic dance steps', '2022-03-13', '12:30:00', 'https://us05web.zoom.us/j/83302865415?pwd=N3J1Qy9lQWMrOHRtcHczTXZhbGdYUT09', 'https://us05web.zoom.us/s/83302865415?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6IjgzMzAyODY1NDE1IiwiZXhwIjoxNjQ2ODExNTQ1LCJpYXQiOjE2NDY4MDQzNDUsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.68V7-sG0G2iV-V5yF3lqy0j8ySje_QfBB6uOUL07NyQ', '83302865415', 'N;', '2022-03-09 05:39:05', '2022-03-09 05:39:05'),
(48, 195, NULL, NULL, 39, 'Second session basic steps', 'Learn second session', '2022-03-14', '00:30:02', 'https://us05web.zoom.us/j/83569023738?pwd=WVdhbFlDdGF3VFpMbVlSZ2lkaEUrQT09', 'https://us05web.zoom.us/s/83569023738?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6IjgzNTY5MDIzNzM4IiwiZXhwIjoxNjQ2ODExNzMxLCJpYXQiOjE2NDY4MDQ1MzEsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.cXmzMySB3EL5wS0CG9UFiB_MzP4fL6O-2HX4F879cRU', '83569023738', 'N;', '2022-03-09 05:42:11', '2022-03-09 05:42:11'),
(49, 195, NULL, NULL, 41, 'completed session 1', 'completed session 1', '2022-02-20', '17:14:00', 'https://us05web.zoom.us/j/88105817456?pwd=TFN3Vk5ML2Zya3BtUHB6UnFjWnZ2Zz09', 'https://us05web.zoom.us/s/88105817456?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6Ijg4MTA1ODE3NDU2IiwiZXhwIjoxNjQ3MDA2MjU5LCJpYXQiOjE2NDY5OTkwNTksImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.kLowgM2fvD3p0Ql9H3UXt-f-5Y7iBk606WaemSZyUPo', '88105817456', 'N;', '2022-03-11 11:44:19', '2022-03-11 11:44:19'),
(50, 195, NULL, NULL, 41, 'completed session 2', 'completed session 2', '2022-03-01', '17:14:00', 'https://us05web.zoom.us/j/81263236311?pwd=dDY2aEdYV01vbU44U3JBa0ZaVGhBUT09', 'https://us05web.zoom.us/s/81263236311?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6IjgxMjYzMjM2MzExIiwiZXhwIjoxNjQ3MDA2Mjg2LCJpYXQiOjE2NDY5OTkwODYsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.njp4Ob7pl56bJcaaolLASdL-jIkdFO8O4nzYDfoW5d4', '81263236311', 'N;', '2022-03-11 11:44:46', '2022-03-11 11:44:46'),
(51, 195, NULL, NULL, 40, 'inprogress session 1', 'inprogress session 1', '2022-03-05', '17:17:00', NULL, 'https://us05web.zoom.us/s/87659364138?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6Ijg3NjU5MzY0MTM4IiwiZXhwIjoxNjQ3MDA2MzE5LCJpYXQiOjE2NDY5OTkxMTksImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.93_hw5OeTRw6XSTAzZnXlMpLLrw7gNYoMwGafH4IPKM', '87659364138', 'N;', '2022-03-11 11:45:19', '2022-03-11 11:45:28'),
(52, 195, NULL, NULL, 40, 'inprogress session 2', 'inprogress session 2', '2022-03-20', '17:20:00', 'https://us05web.zoom.us/j/88984419857?pwd=M1AvWHNra2VnWnJDMWFKOThIN3JhUT09', 'https://us05web.zoom.us/s/88984419857?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6Ijg4OTg0NDE5ODU3IiwiZXhwIjoxNjQ3MDA2MzUwLCJpYXQiOjE2NDY5OTkxNTAsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.Gx9egNCLdMk3t3amAFsCOtRR9qEPwgatYwuMfj05TTU', '88984419857', 'N;', '2022-03-11 11:45:50', '2022-03-11 11:45:50'),
(53, 195, NULL, NULL, 42, 'upcoming session 1', 'upcoming session 1', '2022-04-02', '17:20:00', 'https://us05web.zoom.us/j/83708322159?pwd=OHFsSURYaDNkVjR3dXFyamYwdFpTdz09', 'https://us05web.zoom.us/s/83708322159?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6IjgzNzA4MzIyMTU5IiwiZXhwIjoxNjQ3MDA2MzgxLCJpYXQiOjE2NDY5OTkxODEsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.oYBvQzABVYpXq2KyKzyc0kt0y9hZ8SJMzZ6qSCJTQHY', '83708322159', 'N;', '2022-03-11 11:46:21', '2022-03-11 11:46:21'),
(54, 195, NULL, NULL, 42, 'upcoming session 2', 'upcoming session 2', '2022-04-10', '17:18:00', 'https://us05web.zoom.us/j/83193280364?pwd=Yjd2VkNPb1RIS2ZQYlFrbVMvaS9MQT09', 'https://us05web.zoom.us/s/83193280364?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6IjgzMTkzMjgwMzY0IiwiZXhwIjoxNjQ3MDA2NDA0LCJpYXQiOjE2NDY5OTkyMDQsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.IcdOke_rERiPv3NI0J97GgjJsj8510rKIxikfzjXdLc', '83193280364', 'N;', '2022-03-11 11:46:44', '2022-03-11 11:46:44'),
(55, 195, NULL, NULL, 42, 'upcoming session 3', 'upcoming session 3', '2022-03-31', '10:14:00', 'https://us05web.zoom.us/j/84923885774?pwd=Y21qOFdlb2hWVzFLWDhBS2ljZVhYQT09', 'https://us05web.zoom.us/s/84923885774?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6Ijg0OTIzODg1Nzc0IiwiZXhwIjoxNjQ3MzI2NTgzLCJpYXQiOjE2NDczMTkzODMsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.z4uR-_lJbIoU2XwWwdIp1q75Zs_emKza3aqjO-6ULZg', '84923885774', 'N;', '2022-03-15 04:43:03', '2022-03-15 04:43:03'),
(56, 195, NULL, NULL, 42, 'upcoming session 4', 'upcoming session 4', '2022-03-28', '10:16:00', 'https://us05web.zoom.us/j/87628555964?pwd=Q1FvaUUxQ1N2WjYydkgzL0dobGx0QT09', 'https://us05web.zoom.us/s/87628555964?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6Ijg3NjI4NTU1OTY0IiwiZXhwIjoxNjQ3MzI2NjA0LCJpYXQiOjE2NDczMTk0MDQsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.1maZvC26SqP4QNDuDsqvp7KjybmmH7Iz6k3GM8QTGcc', '87628555964', 'N;', '2022-03-15 04:43:24', '2022-03-15 04:43:24'),
(57, 195, NULL, NULL, 40, 'inprogress session 3', 'inprogress session 3', '2022-03-10', '10:20:00', 'https://us05web.zoom.us/j/81773867008?pwd=Ykphb3cxODhLTmZhbDdRNUo1Nlhudz09', 'https://us05web.zoom.us/s/81773867008?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6IjgxNzczODY3MDA4IiwiZXhwIjoxNjQ3MzI2ODM3LCJpYXQiOjE2NDczMTk2MzcsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.tugxXsHjGzS9lujO2LJ09vDkUB4JYevrXkTUudY_6YM', '81773867008', 'N;', '2022-03-15 04:47:17', '2022-03-15 04:47:17'),
(58, 195, NULL, NULL, 40, 'inprogress session 4', 'inprogress session 4', '2022-03-22', '10:20:00', 'https://us05web.zoom.us/j/86964516613?pwd=VUdWckIvaEhtVFBUb2xJL21UNGR6Zz09', 'https://us05web.zoom.us/s/86964516613?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6Ijg2OTY0NTE2NjEzIiwiZXhwIjoxNjQ3MzI2ODcyLCJpYXQiOjE2NDczMTk2NzIsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.YcBe1VST7C9U7q-lS-mIzZmV70BfygRWQ9qtuEcVfTs', '86964516613', 'N;', '2022-03-15 04:47:52', '2022-03-15 04:47:52'),
(70, 195, NULL, NULL, 42, 'new session', 'sdsdfff', '2022-04-10', '12:04:00', 'https://us05web.zoom.us/j/84234160712?pwd=eDgrYThSZFpvQklUSUdteHVITitYZz09', 'https://us05web.zoom.us/s/84234160712?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6Ijg0MjM0MTYwNzEyIiwiZXhwIjoxNjQ4NDU2MzIxLCJpYXQiOjE2NDg0NDkxMjEsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.Rpi781vigGB0lReMYijck_2o1Nm9Viv7bDT7YyypMQs', '84234160712', 'N;', '2022-03-28 06:32:01', '2022-03-28 06:32:01'),
(71, 195, NULL, NULL, 42, 'new session', 'sdsdfff', '2022-04-11', '12:04:00', NULL, 'https://us05web.zoom.us/s/87304961070?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6Ijg3MzA0OTYxMDcwIiwiZXhwIjoxNjQ4NDU2MzQ4LCJpYXQiOjE2NDg0NDkxNDgsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.JKcD1a3yCVzSaLZuRL_Ja-wSMA-hbI0RycLrcQZPtZo', '87304961070', 'N;', '2022-03-28 06:32:28', '2022-03-28 06:35:47'),
(72, 195, NULL, NULL, 42, 'Test session', 'Test', '2022-04-16', '17:30:52', 'https://us05web.zoom.us/j/86596087369?pwd=Uld4TGZEYWlOOGQxV0k2WkQzM25TUT09', 'https://us05web.zoom.us/s/86596087369?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6Ijg2NTk2MDg3MzY5IiwiZXhwIjoxNjQ4NDU2ODU4LCJpYXQiOjE2NDg0NDk2NTgsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.cPd7BV-8IlD2l_X07OgpXdqdglPnTZh2nKSaAZYAtoQ', '86596087369', 'N;', '2022-03-28 06:40:58', '2022-03-28 06:43:15'),
(77, 243, NULL, NULL, 63, 'basic bharathanattyam', 'basic steps of bharathyanattyam', '2023-01-30', '12:02:00', 'https://us05web.zoom.us/j/85818221361?pwd=OUlJcWZ4RjRZajV2b0lEclJ1NDJuQT09', 'https://us05web.zoom.us/s/85818221361?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6Ijg1ODE4MjIxMzYxIiwiZXhwIjoxNjc1MDY3NjAwLCJpYXQiOjE2NzUwNjA0MDAsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.FFlnnnQ4DAZ6A_qLaI9NY1wskxfyztMZtXbouDzzVhI', '85818221361', 'N;', '2023-01-30 06:33:20', '2023-01-30 06:33:20');

-- --------------------------------------------------------

--
-- Table structure for table `tutor_skills`
--

CREATE TABLE `tutor_skills` (
  `id` int(11) NOT NULL,
  `tutor_id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tutor_skills`
--

INSERT INTO `tutor_skills` (`id`, `tutor_id`, `skill_id`) VALUES
(1, 0, 2),
(2, 0, 3),
(3, 0, 6),
(4, 0, 3),
(5, 0, 5),
(6, 0, 3),
(7, 0, 4),
(8, 0, 2),
(9, 0, 2),
(10, 0, 3),
(11, 0, 2),
(12, 0, 6),
(13, 0, 2),
(14, 0, 1),
(15, 0, 1),
(16, 0, 2),
(19, 0, 1),
(20, 0, 3),
(21, 0, 4),
(26, 277, 1),
(28, 278, 4),
(31, 279, 4),
(32, 279, 7),
(33, 280, 1),
(34, 281, 7),
(39, 282, 3),
(40, 282, 6),
(41, 283, 3),
(42, 283, 4),
(43, 283, 7),
(45, 284, 3),
(46, 285, 3),
(47, 289, 3),
(48, 290, 3),
(49, 290, 4),
(50, 290, 7),
(51, 291, 4),
(52, 291, 7),
(53, 292, 3),
(54, 292, 6),
(55, 293, 4),
(56, 293, 7),
(57, 294, 4),
(58, 294, 7),
(59, 295, 3),
(60, 296, 1),
(61, 296, 3),
(62, 296, 4),
(63, 297, 3),
(64, 297, 6),
(65, 297, 7),
(66, 298, 3),
(67, 298, 6),
(68, 298, 7),
(69, 299, 3),
(70, 299, 4),
(71, 300, 3),
(72, 300, 6),
(74, 302, 7),
(75, 303, 4),
(76, 303, 6),
(77, 303, 7),
(82, 304, 3),
(83, 304, 6),
(84, 305, 3),
(85, 305, 6),
(86, 306, 1),
(87, 306, 4),
(88, 307, 6),
(89, 308, 6),
(90, 308, 7),
(91, 309, 4),
(92, 309, 7),
(93, 310, 3),
(94, 310, 6),
(95, 311, 3),
(96, 311, 4),
(97, 311, 7),
(98, 312, 4),
(99, 312, 6),
(100, 312, 7),
(106, 313, 3),
(107, 313, 4),
(108, 313, 7),
(109, 314, 4),
(110, 314, 6),
(111, 314, 7),
(112, 315, 1),
(113, 316, 3),
(114, 316, 7),
(117, 317, 3),
(118, 317, 6),
(119, 318, 3),
(120, 318, 4),
(121, 318, 6),
(122, 319, 1),
(123, 319, 3),
(124, 319, 4),
(125, 319, 6),
(126, 320, 4),
(127, 320, 6),
(128, 321, 4),
(129, 321, 6),
(142, 323, 4),
(143, 322, 1),
(144, 322, 3),
(145, 322, 4),
(146, 324, 2),
(147, 324, 6),
(148, 325, 2),
(149, 325, 4),
(152, 326, 6),
(155, 327, 4),
(156, 328, 2),
(157, 329, 2),
(158, 330, 4),
(160, 331, 3),
(161, 331, 6),
(168, 332, 2),
(169, 332, 4),
(170, 333, 4),
(175, 334, 2),
(176, 334, 6),
(177, 335, 2),
(178, 335, 6),
(183, 336, 2),
(184, 336, 6),
(185, 337, 8),
(186, 338, 8),
(195, 339, 2),
(196, 339, 6),
(223, 341, 2),
(224, 341, 4),
(228, 342, 2),
(229, 342, 6),
(230, 344, 6),
(233, 343, 6),
(234, 343, 4),
(236, 345, 4),
(237, 345, 6);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) NOT NULL,
  `commision` varchar(180) NOT NULL,
  `description` varchar(200) NOT NULL,
  `role` varchar(191) DEFAULT NULL,
  `email` varchar(191) NOT NULL,
  `dob` date DEFAULT NULL,
  `slug` varchar(191) NOT NULL,
  `phone_number` varchar(191) DEFAULT NULL,
  `location` bigint(20) UNSIGNED NOT NULL,
  `subscription` int(11) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) NOT NULL,
  `token` varchar(150) NOT NULL,
  `otp_verification` varchar(150) DEFAULT NULL,
  `profile_image` varchar(256) DEFAULT NULL,
  `image_file_name` varchar(200) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `commision`, `description`, `role`, `email`, `dob`, `slug`, `phone_number`, `location`, `subscription`, `email_verified_at`, `password`, `token`, `otp_verification`, `profile_image`, `image_file_name`, `remember_token`, `created_at`, `updated_at`) VALUES
(87, 'admin', '56', 'hjdfhjdfhjdf', 'admin', 'admin@gmail.com', NULL, 'admin', '1111111111', 1, 1, NULL, '$2y$10$jmUVJjBFRYO1yUbfLmIk7.VZr/SAoT.ahsvYUnDOeLM8rNESpgYh6', 'wv1AqPwhbAcYOVbnECW2DyCt', NULL, NULL, 'jnsf', 'y6MFZ3mm9MvqeScpCDRJbB2Fg1f5mUBARGiKSgHoHrsNi82J6ekQk2GRLs6g', '2020-08-26 03:44:43', '2020-10-31 00:09:49'),
(335, 'job', '22', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type a', 'tutor', 'job1@mail.com', '2018-05-25', '', '123456', 12, NULL, NULL, '$2y$10$Df7gZoAMLiXP457oKhZS5egtbx1M7KRXNIp76DHRWDQStgZ/vMc8u', '3ZuD4Q5xa2KVsO4XNu06Edql', NULL, NULL, 'TEST IMAGE.jpg', NULL, '2023-03-17 10:32:40', '2023-03-17 10:32:41'),
(336, 'Britanney Shelton', '37', 'A numquam aut aut se', 'tutor', 'kuvacu@mailinator.com', '1972-07-07', '', '355', 220, NULL, NULL, '$2y$10$YkQxCs3GBdHwoIiS2Eq4z.A/VIEoxYUULYeDATz8C6zCn6/XuInfS', 'acKNGY3MMZmanTrNRC0v9Up3', NULL, NULL, 'SUN-test.jpg', NULL, '2023-03-20 05:35:14', '2023-03-20 05:35:15'),
(337, 'Gillian Rose', '38', 'Rerum omnis ea ad no', 'tutor', 'baqazixa@mailinator.com', '1987-12-25', '', '163', 74, NULL, NULL, '$2y$10$A6KN62aTsWlTzIV4KPcb2eAtQUw0dpyA4FOEOq6rtE9cY.7Rw9eca', 'QCzQ2BzAk9SW15k6pC0CxHpP', NULL, NULL, 'SUN-test.jpg', NULL, '2023-03-21 05:18:56', '2023-03-21 05:18:56'),
(338, 'Hyatt Scott', '46', 'Excepturi qui sunt m  fghffsdrttdrtn\r\ngdghfghj', 'tutor', 'cujyc@mailinator.com', '2021-10-25', '', '526', 64, NULL, NULL, '$2y$10$mQJ6lrDxMtaCNTW3gOnMG.eruvCIeeRha8C0O4jJBwIJUBmEIU382', '1ZKhJAkmsC1Vvvn11zqeHEhy', NULL, NULL, 'TEST IMAGE.jpg', NULL, '2023-03-21 09:26:07', '2023-03-21 09:26:08'),
(339, 'Rahul', '10', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type a', 'tutor', 'rahul@mail.com', '2017-06-21', '', '123456', 4, NULL, NULL, '$2y$10$oSiH0/P.lpeJVNtCg0GZ2eNub7OA5sAN8JbCqAc1Wv0SJweaSK15m', 'HxCVabiP0whmeJ218hUfn7vD', NULL, NULL, 'TEST IMAGE.jpg', NULL, '2023-03-21 10:09:15', '2023-03-21 10:09:16'),
(341, 'Tony', '77', 'gdgdhenjnw wbwkwkwnk   hvwdhkwbvjwdxbwdbw bhbjbjdbd jwd', 'tutor', 'tony@gmail.com', '2023-03-29', '', '8764254256', 1, NULL, NULL, '$2y$10$CTvthwKrkpBpgnxCD1e5o.bKbUitZaZ.7uf2d.fIEAs.IzUMoP7CS', 'aMQc3uHnueMGElYLNpjMSEdA', NULL, NULL, 'IMG_8607.JPG', NULL, '2023-03-29 05:24:53', '2023-03-29 11:39:01'),
(343, 'Vanna Jimenez', '41', 'Laborum Voluptatibu', 'tutor', 'qovykyli@mailinator.com', '2002-10-16', '', '379', 54, NULL, NULL, '$2y$10$ovX9Uzjxrlikfwfu2/4eze4gfz2XlFq/F7IcCpZeUG0see2asP.mm', 'MxzdOyFQCXEscfaIcN4UAzqy', NULL, NULL, 'jayesh photo.JPG', NULL, '2023-03-30 05:24:50', '2023-03-30 05:24:51'),
(344, 'Retheesh', '10', 'test', 'tutor', 'retheesh.kumar5499@gmail.com', '1985-01-20', '', '1243567890', 1, NULL, NULL, '$2y$10$O91Do.YoGYerfk1pQ64xTe/DFiV6.nhfDBmkb.ASV85.tYzgVoLfK', 'CCspkt1dqOULaU2xvPRq1dvS', NULL, NULL, 'user-xxl.png', NULL, '2023-03-30 06:36:50', '2023-03-30 06:36:50'),
(345, 'varun mh', '54', 'hjsdhjdshjdsnjds', 'tutor', 'vv@gmail.com', '2023-04-20', '', '123453637', 1, NULL, NULL, '$2y$10$QcStBpoZBJWAF6EiE4S88uxZqA/R4cmnGAyXA9rhBd6pUDlKjXJcS', 'czzNOzupPHPPHw4R6CVbRxLl', NULL, '', 'IMG_8607.JPG', NULL, '2023-04-06 07:20:34', '2023-04-06 07:20:34');

-- --------------------------------------------------------

--
-- Table structure for table `user_subscriptions`
--

CREATE TABLE `user_subscriptions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `stripe_subscription_id` varchar(191) NOT NULL,
  `stripe_customer_id` varchar(191) NOT NULL,
  `sub_schedule_id` varchar(191) DEFAULT NULL,
  `created` varchar(191) NOT NULL,
  `plan_period_start` varchar(191) DEFAULT NULL,
  `plan_period_end` varchar(191) DEFAULT NULL,
  `status` varchar(191) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_subscriptions`
--

INSERT INTO `user_subscriptions` (`id`, `user_id`, `batch_id`, `package_id`, `stripe_subscription_id`, `stripe_customer_id`, `sub_schedule_id`, `created`, `plan_period_start`, `plan_period_end`, `status`) VALUES
(12, 99, 55, 84, 'sub_1KhRU8FYYVrPIFCxlK3JmdK1', 'cus_LODvJT9PF8ixS2', 'sub_sched_1KhRU8FYYVrPIFCxGve4ngoy', '1648269860', '1648269860', '1658810660', 'active'),
(14, 214, 37, 74, 'sub_1KjKGGFYYVrPIFCxUulu2WrK', 'cus_LQAb16uzeZKDan', 'sub_sched_1KjKGGFYYVrPIFCxfpsGRYsP', '1648718748', '1648718748', '1656581148', 'active'),
(15, 215, 37, 74, 'sub_1KjKLIFYYVrPIFCxsXtgqso7', 'cus_LQAgL7TUz2fTBr', 'sub_sched_1KjKLIFYYVrPIFCxjxTrZmTO', '1648719060', '1648719060', '1656581460', 'active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `batches`
--
ALTER TABLE `batches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `batch_package`
--
ALTER TABLE `batch_package`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_skills`
--
ALTER TABLE `category_skills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`class_id`);

--
-- Indexes for table `cms_fields`
--
ALTER TABLE `cms_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency_range_tier`
--
ALTER TABLE `currency_range_tier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency_settings`
--
ALTER TABLE `currency_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `generalsettings`
--
ALTER TABLE `generalsettings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `help_desk`
--
ALTER TABLE `help_desk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_banners`
--
ALTER TABLE `home_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_headers`
--
ALTER TABLE `home_headers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `live_class`
--
ALTER TABLE `live_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package_detail`
--
ALTER TABLE `package_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `policies`
--
ALTER TABLE `policies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_details`
--
ALTER TABLE `student_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_student` (`user_id`),
  ADD KEY `grade` (`grade`),
  ADD KEY `skills` (`skills`),
  ADD KEY `grade_2` (`grade`),
  ADD KEY `skills_2` (`skills`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terms`
--
ALTER TABLE `terms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tier`
--
ALTER TABLE `tier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tier_management`
--
ALTER TABLE `tier_management`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tutor_details`
--
ALTER TABLE `tutor_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_tutor` (`user_id`),
  ADD KEY `category` (`category`),
  ADD KEY `sub_category` (`sub_category`),
  ADD KEY `skills` (`skills`);

--
-- Indexes for table `tutor_portfolio`
--
ALTER TABLE `tutor_portfolio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tutor_portfolio_user_id_foreign` (`user_id`);

--
-- Indexes for table `tutor_sessions`
--
ALTER TABLE `tutor_sessions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tutor_skills`
--
ALTER TABLE `tutor_skills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `token` (`token`),
  ADD KEY `location` (`location`);

--
-- Indexes for table `user_subscriptions`
--
ALTER TABLE `user_subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `batches`
--
ALTER TABLE `batches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `batch_package`
--
ALTER TABLE `batch_package`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `category_skills`
--
ALTER TABLE `category_skills`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `class_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cms_fields`
--
ALTER TABLE `cms_fields`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=252;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `currency_range_tier`
--
ALTER TABLE `currency_range_tier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `currency_settings`
--
ALTER TABLE `currency_settings`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `generalsettings`
--
ALTER TABLE `generalsettings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `help_desk`
--
ALTER TABLE `help_desk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `home_banners`
--
ALTER TABLE `home_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `home_headers`
--
ALTER TABLE `home_headers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `live_class`
--
ALTER TABLE `live_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=256;

--
-- AUTO_INCREMENT for table `package_detail`
--
ALTER TABLE `package_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=265;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT for table `policies`
--
ALTER TABLE `policies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `regions`
--
ALTER TABLE `regions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `skills`
--
ALTER TABLE `skills`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `student_details`
--
ALTER TABLE `student_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `terms`
--
ALTER TABLE `terms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tier`
--
ALTER TABLE `tier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tier_management`
--
ALTER TABLE `tier_management`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tutor_details`
--
ALTER TABLE `tutor_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=154;

--
-- AUTO_INCREMENT for table `tutor_portfolio`
--
ALTER TABLE `tutor_portfolio`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;

--
-- AUTO_INCREMENT for table `tutor_sessions`
--
ALTER TABLE `tutor_sessions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `tutor_skills`
--
ALTER TABLE `tutor_skills`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=238;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=346;

--
-- AUTO_INCREMENT for table `user_subscriptions`
--
ALTER TABLE `user_subscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `student_details`
--
ALTER TABLE `student_details`
  ADD CONSTRAINT `student_details_ibfk_1` FOREIGN KEY (`grade`) REFERENCES `grades` (`id`),
  ADD CONSTRAINT `student_details_ibfk_2` FOREIGN KEY (`skills`) REFERENCES `skills` (`id`),
  ADD CONSTRAINT `user_student` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tutor_details`
--
ALTER TABLE `tutor_details`
  ADD CONSTRAINT `tutor_details_ibfk_1` FOREIGN KEY (`category`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `tutor_details_ibfk_2` FOREIGN KEY (`sub_category`) REFERENCES `sub_categories` (`id`),
  ADD CONSTRAINT `tutor_details_ibfk_3` FOREIGN KEY (`skills`) REFERENCES `skills` (`id`),
  ADD CONSTRAINT `user_tutor` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tutor_portfolio`
--
ALTER TABLE `tutor_portfolio`
  ADD CONSTRAINT `tutor_portfolio_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
