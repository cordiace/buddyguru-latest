@php
    $issetHeader = isset($contact) ? 1 : 0;
@endphp
@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              Contact Us
            </h4>
          </div>
          <div class="card-body">
            <form action="{{ route('admin.contact.update') }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @csrf
              <input type="hidden" name="id" value="{{ isset($contact->id) ? $contact->id : ''  }}" >
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">(1600 x 600) Banner: {{isset($contact) ? $contact->banner :''}}</label>
                    <input type="file" name="file" class="form-control">
                    @error ('file')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Call us</label>
                    <input required type="text" name="phone" class="form-control" value="{{ isset($contact->phone) ? $contact->phone : ''  }}">
                    @error ('phone')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Primary email</label>
                    <input required type="text" name="primary_email" class="form-control" value="{{ isset($contact->primary_email) ? $contact->primary_email : ''  }}">
                    @error ('description')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Secondary Email</label>
                    <input required type="text" name="secondary_email" class="form-control" value="{{ isset($contact->secondary_email) ? $contact->secondary_email : ''  }}">
                    @error ('secondary_email')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Address</label>
                    <input required type="text" name="address1" class="form-control" value="{{ isset($contact->address1) ? $contact->address1 : ''  }}">
                    @error ('address1')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Address</label>
                    <input required type="text" name="address2" class="form-control" value="{{ isset($contact->address2) ? $contact->address2 : ''  }}">
                    @error ('address2')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Address</label>
                    <input required type="text" name="address3" class="form-control" value="{{ isset($contact->address3) ? $contact->address3 : ''  }}">
                    @error ('address3')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Location</label>
                    <input required type="text" name="location" class="form-control" value="{{ isset($contact->location) ? $contact->location : ''  }}">
                    @error ('location')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>


              <!-- Short Links -->
              <button type="submit" class="btn btn-primary btn-brown pull-right">Submit</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
      <!-- <div class="col-md-4">
        <div class="card card-profile">
          <div class="card-avatar">
            <a href="javascript:;">
              <img class="img" src="../assets/img/faces/marc.jpg" />
            </a>
          </div>
          <div class="card-body">
            <h6 class="card-category text-gray">CEO / Co-Founder</h6>
            <h4 class="card-title">Alec Thompson</h4>
            <p class="card-description">
              Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
            </p>
            <a href="javascript:;" class="btn btn-primary btn-round">Follow</a>
          </div>
        </div>
      </div> -->
    </div>

  </div>
</div>
@endsection
@section('custom-scripts')
<script type="text/javascript">
$( document ).ready(function() {
  $( ".add" ).on( "click", function(e) {
    e.preventDefault();
    $('.add_another').append('<div class="row"><div class="col-md-4"><div class="form-group"><input type="text" required name="name[]" placeholder="Name"></div></div><div class="col-md-4"><div class="form-group"><input type="text" required name="link[]" placeholder="Link"></div></div><div class="col-md-4"><div class="form-group"><button type="button" rel="tooltip" title="" class="remove btn btn-primary btn-link btn-sm"><i class="material-icons">remove</i><div class="ripple-container"></div></button></div></div></div>');
  });
  $( ".add_social" ).on( "click", function(e) {
    e.preventDefault();
    $('.add_another_social_link').append('<div class="row"><div class="col-md-4"><div class="form-group"><input type="text" required name="social_name[]" placeholder="Name"></div></div><div class="col-md-4"><div class="form-group"><input type="text" required name="social_link[]" placeholder="Link"></div></div><div class="col-md-4"><div class="form-group"><button type="button" rel="tooltip" title="" class="remove btn btn-primary btn-link btn-sm"><i class="material-icons">remove</i><div class="ripple-container"></div></button></div></div></div>');
  });
  $( ".add_short" ).on( "click", function(e) {
    e.preventDefault();
    $('.add_another_short_link').append('<div class="row"><div class="col-md-4"><div class="form-group"><input type="text" required name="short_name[]" placeholder="Name"></div></div><div class="col-md-4"><div class="form-group"><input type="text" required name="short_link[]" placeholder="Link"></div></div><div class="col-md-4"><div class="form-group"><button type="button" rel="tooltip" title="" class="remove btn btn-primary btn-link btn-sm"><i class="material-icons">remove</i><div class="ripple-container"></div></button></div></div></div>');
  });
  $( "body" ).on( "click",".remove", function(e) {
    e.preventDefault();
    $(this).parent().parent().parent().remove();
  });
});
</script>
@endsection
