@php
    $formAction = isset($testimonial) ? route('testimonial.update', $testimonial->id) : route('testimonial.store');
    $issetBanner = isset($testimonial) ? 1 : 0;
@endphp
@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              @isset($testimonial)
                  Edit Details
              @else
                  Add New
              @endisset
            </h4>
          </div>
          <div class="card-body">
            <form action="{{ $formAction }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @isset($testimonial) @method('PUT') @endisset
              @csrf
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Title</label>
                    <input type="text" name="title" class="form-control"
                        value="{{ old('title') ?? ($issetBanner ? $testimonial->title : '')  }}">
                    @error ('title')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Description</label>
                    <input type="text" name="description" class="form-control" value="{{ old('description') ?? ($issetBanner ? $testimonial->description : '')  }}">
                    @error ('description')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">(1920 x 788) Image : {{isset($testimonial->image) ? $testimonial->image : ''}}</label>
                    <input type="file" name="image">
                    <input type="hidden" name="image_url" value="{{isset($testimonial->image) ? $testimonial->image : ''}}">
                  </div>
                </div>
              </div>

              <button type="submit" class="btn btn-primary pull-right">Submit</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>

    </div>
    @isset($testimonial)
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              Delete this banner
            </h4>
          </div>
          <div class="card-body">
            <form action="{{ route('testimonial.destroy', $testimonial->id) }}" method="POST">
                @csrf
                @method('DELETE')
              <div class="row">
                <div class="col-md-12">
                  Do you really want to do this ? Make sure you are selected the right item. This action is irreversible.
                  All data associated with this item will be erased permanantly.
                </div>
              </div>
              <button type="submit" class="btn btn-primary pull-right">Delete</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
    </div>
    @endisset
  </div>
</div>
@endsection
