@extends('layouts/layoutMaster')

@section('content')
<div class="card">
<div class="card-header card-header-info">
<div class="dt-buttons btn-group flex-wrap">
     <div class="btn-group">
     </div>
     <a  href="{{route('cms-region.create')}}">
       <button class="btn btn-secondary add-new btn-primary" tabindex="0"
        aria-controls="DataTables_Table_0" type="button">
        <span><i class="ti ti-plus me-0 me-sm-1 ti-xs"></i>
        <span class="d-none d-sm-inline-block">Add Region</span>
      </span></button></a>
     </div>
</div>
  <div class="card-datatable table-responsive">
 
    <table id="example" class="datatables-users table border-top">
                <thead >
                  <th>ID</th>
                  <th>Region</th>
                  <!-- <th>Image</th> -->
                  <th >Action</th>
                </thead>
                <tbody>
                  @isset($regions)
                  @foreach($regions as $region)
                  <tr>
                    <td>{{$loop->index + 1}}</td>
                    <td>{{$region->name}}</td>
                    <td  class="text-center">
                <div class="d-flex align-items-center">
            <div class="dropdown">
              <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown"><i class="ti ti-dots-vertical"></i></button>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="{{ route('cms-region.edit', $region->id)}}"><i class="ti ti-pencil me-1"></i> Edit</a>
                <a class="dropdown-item" href="{{route('cms-region.delete', $region->id)}}"><i class="ti ti-trash me-1"></i> Delete</a>
              </div>
            </div>

</div>
           
          </td>
                   
                  </tr>
                  @endforeach
                  @endisset
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
