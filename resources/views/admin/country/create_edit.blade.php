@php
    $formAction = isset($country) ? route('cms-country.update', $country->id) : route('cms-country.store');
    $issetCountry = isset($country) ? 1 : 0;
@endphp
@extends('layouts/layoutMaster')

@section('content')

<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header card-header-info">
            <h4 class="card-title">
              @isset($country)
                  Edit Country Details
              @else
                  Add New Country<Tutor></Tutor>
              @endisset
            </h4>
            {{--<p class="card-country">Complete your profile</p>--}}
          </div>
          <div class="card-body">
            <form action="{{ $formAction }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @isset($country) @method('POST') @endisset
              @csrf
              <div class="mb-3">
              <label class="form-label" for="region_id">Select Region<span class="text-danger">*</span></label>
                <select   data-placeholder="Select a region"  name="region_id" id="region_id" class="select2 form-select form-select" data-allow-clear="true">
                    
                @isset($Region)
                      @foreach($Region as $value)
                      <!-- <option value="{{$value->id}}">{{strtoupper($value->category_name)}}</option> -->
                      <option value="{{ $value['id'] }}"  {{  ($issetCountry && $country->region_id == $value['id']) || (old('region_id')==$value['id'])  ? 'selected' : ''}}>{{ $value['name'] }}</option>
                      @endforeach
                      @endisset
                    </select>
                    @error ('region_id')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
              
                  <div class="mb-3">
                    <label class="bmd-label-floating">Country Name</label>
                    <input id="country_name" type="text" name="country_name" class="form-control"
                        value="{{ old('country_name') ?? ($issetCountry ? $country->country_name : '')  }}">
                    @error ('country_name')
                        <p class="text-danger">Country is required</p>
                    @enderror
                  
              </div>
              <div class="row">
            <div class="col-12">
              <a href="{{ route('cms-country.index') }}"  class= "pull-left">
                        <button type="button"  class="btn btn-primary float-left  btn-sm">
                          Back
                        </button>
                      </a>
              <button type="submit" class="btn btn-primary float-right btn-sm">Submit</button>
              <div class="clearfix"></div>
</div>
</div>
            </form>
          </div>
        </div>
      </div>
      <!-- <div class="col-md-4">
        <div class="card card-profile">
          <div class="card-avatar">
            <a href="javascript:;">
              <img class="img" src="../assets/img/faces/marc.jpg" />
            </a>
          </div>
          <div class="card-body">
            <h6 class="card-country text-gray">CEO / Co-Founder</h6>
            <h4 class="card-title">Alec Thompson</h4>
            <p class="card-description">
              Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
            </p>
            <a href="javascript:;" class="btn btn-primary btn-round">Follow</a>
          </div>
        </div>
      </div> -->
    </div>
    <!-- @isset($country)
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              Delete this Country
            </h4>
          </div>
          <div class="card-body">
            <form action="" method="POST">
                @csrf
                @method('DELETE')
              <div class="row">
                <div class="col-md-12">
                  Do you really want to do this ? Make sure you are selected the right item. This action is irreversible.
                  All data associated with this item will be erased permanantly.
                </div>
              </div>
              <button type="submit" class="btn btn-primary pull-right">Delete</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
    </div>
    @endisset -->
  </div>
</div>
@endsection
