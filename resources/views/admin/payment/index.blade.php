@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Payments</h4>
            {{--<p class="card-category"> Here is a subtitle for this table</p> -->--}}
            {{--<ul class="nav nav-tabs" data-tabs="tabs">--}}
                        {{--<li class="nav-item">--}}
                          {{--<a class="nav-link active" href="{{route('tutor.create')}}">--}}
                            {{--<i class="material-icons">supervisor_account</i> Add Tutor--}}
                            {{--<div class="ripple-container"></div>--}}
                          {{--<div class="ripple-container"></div></a>--}}
                        {{--</li>--}}
                      {{--</ul>--}}

          </div>
          <div class="card-body">
            <div class="table-responsive">
            <!-- <table class="table">
            <thead>
              <form action="{{route('get_admin_payment')}}" method="GET" >
              <th>
              From date <input id="fdate" value="" type="date" name="fdate" class="">
             
              </th>
              <th>
              To date<input id="tdate" value=""  type="date" name="tdate" class="">
              </th>
              <th>
              <button type="submit" class="btn btn-primary pull-right">show</button>
              </th>
              </form>

              </thead>
              </table> -->


              <table class="table">
             
                <thead class=" text-primary">
                  <th>ID</th>
                  <th>User</th>
                  <th>Tutor</th>
                  <th>Amount</th>
                  <!-- <td>Batch</td>
                  <td>Package</td> -->
                  <th>Payment Type</th>
                
                  <th>Expiry Date</th>
                  <th>CVV</th>
                  <th>Status</th>
                  {{--<th class="text-right">Action</th>--}}
                </thead>
                <tbody>
              

                  @isset($payments)
                  @foreach($payments as $payment)
                  <tr>
                    <td>{{$loop->index + 1}}</td>
                    <td>{{$payment->user->name}}</td>
                    <td>{{$payment->tutor->name}}</td>
                    <td>{{$payment->amount}}</td>
                    <!-- <td>{{$payment->batch->batch_name}}</td>
                    <td>{{$payment->package->package_name}}</td> -->
                    <td>{{$payment->payment_method}}</td>
                   
                    <td>{{$payment->expiry_date}}</td>
                    <td>{{$payment->cvv}}</td>

                    <?php 
                    $status="Fail";
                    if($payment->payment_status){
                      $status="Compleated";
                    }
                    ?>

                    <!-- <td>{{ $payment->payment_status}}</td> -->
                    <td>{{ $status}}</td>
                    {{--<td class="td-actions text-right">--}}
                      {{--<a href="{{ route('payment.edit', $payment->id) }}">--}}
                        {{--<button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-link btn-sm">--}}
                          {{--<i class="material-icons">edit</i>--}}
                        {{--</button>--}}
                      {{--</a>--}}
                    {{--</td>--}}
                  </tr>
                  @endforeach
                  @endisset
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
