@php
    $formAction = isset($banner) ? route('banner.update', $banner->id) : route('banner.store');
    $issetBanner = isset($banner) ? 1 : 0;
@endphp
@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              @isset($banner)
                  Edit Details
              @else
                  Add New
              @endisset
            </h4>
          </div>
          <div class="card-body">
            <form action="{{ $formAction }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @isset($banner) @method('PUT') @endisset
              @csrf

<div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Heading</label>
                    <input name="heading" id="heading" class="form-control" value="{{ old('banner') ?? ($issetBanner ? $banner->heading : '')  }}">
                    @error ('heading')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Description</label>
                    <input name="description" id="description" class="form-control"  value="{{ old('banner') ?? ($issetBanner ? $banner->description: '')  }}">
                    @error ('description')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Location</label>
                    <select name="location" class="form-control">
                      <option value="guru-home">guru-home</option>
                      <option value="buddy-home">buddy-home</option>
                    </select>
                    @error ('location')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Image (409x152) : {{isset($banner->image) ? $banner->image : ''}}</label>
                    <input type="hidden" name="temp_image" value="{{isset($banner->image) ? $banner->image : ''}}">
                    <input type="file" name="image" {{isset($banner->image) ? '' : 'required'}}>
                  </div>
                </div>
              </div>

              <button type="submit" class="btn btn-primary pull-right">Submit</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
    </div>
    @isset($banner)
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              Delete this banner
            </h4>
          </div>
          <div class="card-body">
            <form action="{{ route('banner.destroy', $banner->id) }}" method="POST">
                @csrf
                @method('DELETE')
              <div class="row">
                <div class="col-md-12">
                  Do you really want to do this ? Make sure you are selected the right item. This action is irreversible.
                  All data associated with this item will be erased permanantly.
                </div>
              </div>
              <button type="submit" class="btn btn-primary pull-right">Delete</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
    </div>
    @endisset
  </div>
</div>
@endsection
