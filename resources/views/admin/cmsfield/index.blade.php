@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Cms Fields</h4>
            {{--<ul class="nav nav-tabs" data-tabs="tabs">--}}
              {{--<li class="nav-item">--}}
                {{--<a class="nav-link active" href="{{route('cmsfield.create')}}" >--}}
                  {{--<i class="material-icons">supervisor_account</i> Add Cms Field--}}
                  {{--<div class="ripple-container"></div>--}}
                  {{--<div class="ripple-container"></div></a>--}}
              {{--</li>--}}
            {{--</ul>--}}
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>ID</th>
                  <th>Cms Field Heading</th>
                  <th>Display Order</th>
                  <th class="text-right" width="">Action</th>
                </thead>
                <tbody>
                  @isset($cmsfields)
                  @foreach($cmsfields as $cmsfield)
                  <tr>
                    <td>{{$loop->index + 1}}</td>
                    <td>{{$cmsfield->cms_heading}}</td>
                    <td>{{$cmsfield->display_order}}</td>
                    <td class="td-actions" width="5%">
                      <a href="{{ route('cmsfield.edit', $cmsfield->id) }}">
                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">edit</i>
                        </button>
                      </a>
                    </td>
                  </tr>
                  @endforeach
                  @endisset
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
