@php
    $formAction = isset($category) ? route('cms-category.update', $category->id) : route('cms-category.store');
    $issetCategory = isset($category) ? 1 : 0;
@endphp
@extends('layouts/layoutMaster')



@section('content')
<h4 class="fw-bold py-3 mb-4">
  <span class="text-muted fw-light">Category Set/</span> Validation
</h4>

<div class="col-md">
    <div class="card">
      <h5 class="card-header">
      @isset($category)
                  Edit Category Details
              @else
                  Add New Category<Tutor></Tutor>
              @endisset
      </h5>
      <div class="card-body">
        <form  action="{{ $formAction }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data" >
        @isset($category) @method('POST') @endisset
              @csrf 
              <div class="mb-3">
                    <label class="bmd-label-floating">Category Name</label>
                    <input id="category_name" type="text" name="category_name" class="form-control"
                        value="{{ old('category_name') ?? ($issetCategory ? $category->category_name : '')  }}">
                    @error ('category_name')
                        <p class="text-danger">Category is required</p>
                    @enderror
                  </div>
         
          <div class="row">
            <div class="col-12">
          
            <a href="{{ route('cms-category.index') }}"  class= "pull-left">
                        <button type="button"  class="btn btn-primary float-left  btn-sm">
                          Back
                        </button>
                      </a>
              <button type="submit" class="btn btn-sm btn-primary">Submit</button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>


@endsection
