@extends('layouts/layoutMaster')

@section('content')
<div class="card">
<div class="card-header card-header-info">
<div class="dt-buttons btn-group flex-wrap">
     <div class="btn-group">
     </div>
     <a  href="{{route('cms-skill.create')}}">
       <button class="btn btn-secondary add-new btn-primary" tabindex="0"
        aria-controls="DataTables_Table_0" type="button">
        <span><i class="ti ti-plus me-0 me-sm-1 ti-xs"></i>
        <span class="d-none d-sm-inline-block">Add Skill</span>
      </span></button></a>
     </div>
</div>
  <div class="card-datatable table-responsive">
 
    <table id="example" class="datatables-users table border-top">
                <thead >
                  <th>ID</th>
                  <th>Category</th>
                  <th>Skill</th>
                  <th >Action</th>
                </thead>
                <tbody>  
                  @isset($CategorySkills)
                  @foreach($CategorySkills as $CategorySkill)
                  <tr>
                    <td>{{$loop->index + 1}}</td>
                    <td>{{$CategorySkill->category->category_name}}</td>
                    <td>{{$CategorySkill->skill_name}}</td>
                    <td  class="text-center">
                <div class="d-flex align-items-center">
            <div class="dropdown">
              <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown"><i class="ti ti-dots-vertical"></i></button>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="{{ route('cms-skill.edit', $CategorySkill->id) }}"><i class="ti ti-pencil me-1"></i> Edit</a>
                <a class="dropdown-item" href="{{route('cms-skill.delete', $CategorySkill->id)}}"><i class="ti ti-trash me-1"></i> Delete</a>
              </div>
            </div>

</div>
           
          </td>

                  
                  </tr>
                  @endforeach
                  @endisset
                </tbody>
              </table>
            </div>
          </div>
       
@endsection
