@php
    $formAction = isset($skill) ? route('cms-skill.update', $skill->id) : route('cms-skill.store');
    $issetSkill = isset($skill) ? 1 : 0;
@endphp
@extends('layouts/layoutMaster')

@section('content')

<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header card-header-info">
            <h4 class="card-title">
              @isset($skill)
                  Edit Skill Details
              @else
                  Add New Skill<Tutor></Tutor>
              @endisset
            </h4>
            {{--<p class="card-skill">Complete your profile</p>--}}
          </div>
          <div class="card-body">
            <form action="{{ $formAction }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @isset($skill) @method('POST') @endisset
              @csrf
              <div class="mb-3">
              <label class="form-label" for="category_type">Category Type<span class="text-danger">*</span></label>
                <select   data-placeholder="Select a category type"  name="category_type" id="category_type" class="select2 form-select form-select" data-allow-clear="true">
                    
                  
                    @isset($category)
                      @foreach($category as $value)
                      <!-- <option value="{{$value->id}}">{{strtoupper($value->category_name)}}</option> -->
                      <option value="{{ $value['id'] }}"  {{  ($issetSkill && $skill->category_id == $value['id']) || (old('category_type')==$value['id'])  ? 'selected' : ''}}>{{ $value['category_name'] }}</option>
                      @endforeach
                      @endisset
                    </select>
                    @error ('category_type')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                
</div>
<div class="mb-3">
                    <label class="bmd-label-floating">Skill Name<span class="text-danger">*</span></label>
                    <input id="skill_name" type="text" name="skill_name" class="form-control"
                        value="{{ old('skill_name') ?? ($issetSkill ? $skill->skill_name : '')  }}">
                    @error ('skill_name')
                        <p class="text-danger">Skill is required</p>
                    @enderror
                  </div>
                  <div class="row">
            <div class="col-12">
              <a href="{{ route('cms-skill.index') }}"  class= "pull-left">
                        <button type="button"  class="btn btn-primary float-left  btn-sm">
                          Back
                        </button>
                      </a>
              <button type="submit" class="btn btn-primary btn-sm float-right">Submit</button>
</div>
</div>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
      <!-- <div class="col-md-4">
        <div class="card card-profile">
          <div class="card-avatar">
            <a href="javascript:;">
              <img class="img" src="../assets/img/faces/marc.jpg" />
            </a>
          </div>
          <div class="card-body">
            <h6 class="card-skill text-gray">CEO / Co-Founder</h6>
            <h4 class="card-title">Alec Thompson</h4>
            <p class="card-description">
              Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
            </p>
            <a href="javascript:;" class="btn btn-primary btn-round">Follow</a>
          </div>
        </div>
      </div> -->
    </div>
   
  </div>
</div>
@endsection
