@php
    $formAction =  route('user.update',$user->id);
    $issetUser = isset($user) ? 1 : 0;
    $options=$countries;

@endphp

@extends('layouts/layoutMaster')

@section('title', 'Account settings - Account')

@section('vendor-style')
<link rel="stylesheet" href="{{asset('assets/vendor/libs/bs-stepper/bs-stepper.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/bootstrap-select/bootstrap-select.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/select2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/formvalidation/dist/css/formValidation.min.css')}}" />
@endsection

@section('vendor-script')
<script src="{{asset('assets/vendor/libs/bs-stepper/bs-stepper.js')}}"></script>
<script src="{{asset('assets/vendor/libs/bootstrap-select/bootstrap-select.js')}}"></script>
<script src="{{asset('assets/vendor/libs/select2/select2.js')}}"></script>
<script src="{{asset('assets/vendor/libs/formvalidation/dist/js/FormValidation.min.js')}}"></script>
<script src="{{asset('assets/vendor/libs/formvalidation/dist/js/plugins/Bootstrap5.min.js')}}"></script>
<script src="{{asset('assets/vendor/libs/formvalidation/dist/js/plugins/AutoFocus.min.js')}}"></script>
@endsection

@section('page-script')
<script src="{{asset('assets/js/forms-selects.js')}}"></script>
@endsection

@section('content')
<h4 class="fw-bold py-3 mb-4">
  <span class="text-muted fw-light">Buddy Profile /</span> Edit
</h4>


    <div class="card mb-4">
      <h5 class="card-header">Profile Details</h5>
     
     
        <form id="formAccountSettings" action="{{ $formAction }}"  method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
        
        <!-- Account -->
       <div class="card-body">
        <div class="d-flex align-items-start align-items-sm-center gap-4">
          <img src="{{  asset($user->profile_image) }}" alt="user-avatar" class="d-block w-px-100 h-px-100 rounded" id="uploadedAvatar" />
          <div class="button-wrapper">
            <label for="profile_image" class="btn btn-primary me-2 mb-3" tabindex="0">
              <span class="d-none d-sm-block">Upload New Photo</span>
              <i class="ti ti-upload d-block d-sm-none"></i>
              <input type="file" id="profile_image" name="profile_image" class="account-file-input" hidden accept="image/png, image/jpeg" />
            </label>
            <!-- <button type="button" class="btn btn-label-secondary account-image-reset mb-3">
              <i class="ti ti-refresh-dot d-block d-sm-none"></i>
              <span class="d-none d-sm-block">Reset</span>
            </button> -->

            <div id="valid_profile" style="font-size: 13px;display: block;" class="text-muted"><b> JPG or PNG. Max size of 800K</b></div>
          </div>
        </div>
      </div>
      <hr class="my-0">
      <div class="card-body">
       
              @csrf 
        <div class="row">
            <div class="mb-3 col-md-6">
            <label class="form-label" for="name">Name<span class="text-danger">*</span></label>
                <input type="text" name="name"  value="{{ old('name') ?? ($issetUser ? $user->name : '')  }}" {{($issetUser) ? '' : 'required'}} id="name" class="form-control" placeholder="johndoe" />
            </div>
            <div class="mb-3 col-md-6">
            <label class="bmd-label-floating">Email <span class="text-danger">*</span></label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') ?? ($issetUser ? $user->email : '')  }}" required autocomplete="email">

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
            </div>
            <div class="mb-3 col-md-6 form-password-toggle">
            <label class="form-label" for="formValidationPass">Password<span class="text-danger">*</span></label>
                <div class="input-group input-group-merge">
                  <input type="password" id="password"class="form-control @error('password') is-invalid @enderror" name="password" {{($issetUser) ? '' : 'required'}} autocomplete="new-password" placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;" aria-describedby="formValidationPass2" />
                  <span class="input-group-text cursor-pointer" id="password"><i class="ti ti-eye-off"></i></span>
                </div>
            </div>
            <div class="mb-3 col-md-6">
            <label class="form-label" for="formValidationConfirmPass">Confirm Password<span class="text-danger">*</span></label>
                <div class="input-group input-group-merge">
                  <input type="password" id="formValidationConfirmPass" {{($issetUser) ? '' : 'required'}} autocomplete="new-password" name="formValidationConfirmPass" class="form-control" placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;" aria-describedby="formValidationConfirmPass2" />
                  <span class="input-group-text cursor-pointer" id="formValidationConfirmPass2"><i class="ti ti-eye-off"></i></span>
                </div>
            </div>
            <div class="mb-3 col-md-6">
              <label class="form-label" for="phone_number">Mobile Number<span class="text-danger">*</span></label>
              <div class="input-group input-group-merge">
                <span class="input-group-text"></span>
                <input type="text" id="phone_number" name="phone_number"class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" value="{{ old('phone_number') ?? ($issetUser ? $user->phone_number : '')  }}" required autocomplete="phone_number" placeholder="202 555 0111" />
              </div>
            </div>
            <div class="mb-3 col-md-6">
            <label  class="bmd-label-floating">Grade <span class="text-danger">*</span></label>
                    <select data-placeholder="Select a Grade"  name="grade" id="grade" class="select2 form-select form-select @error('grade') is-invalid @enderror" data-allow-clear="true" >
                  
                        <!-- <option value=""></option> -->
                       
                        @foreach ($grades as $key => $value)
                        <option value="{{ $value['id'] }}"  {{  ($issetUser && $StudentDetail->grade == $value['id']) || (old('grade')==$value['id'])  ? 'selected' : ''}}>{{ $value['grade_name'] }}</option>
                      @endforeach
                    </select>
                    @error('location')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
            </div>
            <div class="mb-3 col-md-6">
            <label class="form-label" for="category">Category<span class="text-danger">*</span></label>
                  <select name="category[]" id="category" class="select2 form-select" multiple data-dropdown-css-class="select2-purple" data-placeholder="Select Category" >
                  @if(isset($user))
                        @foreach ($category_subset as $category)
        
                       
                        <option value="{{ $category->id }}" {{ (collect(old('category'))->contains($category->id)) ? 'selected':'' }}  @foreach($studentcategorys as $sublist){{$sublist == $category->id ? 'selected': ''}}   @endforeach  > {{ $category->category_name }}</option>
                        @endforeach
                      
        @endif
                  </select>
            </div>

            <div class="mb-3 col-md-6">
            <label class="form-label" for="commision">Skill<span class="text-danger">*</span></label>
                  <select name="skills[]" id="skills" class="select2 form-select" multiple data-dropdown-css-class="select2-purple" data-placeholder="Select Skill" >
                  @if(isset($user))
                        @foreach ($skill_subset as $skill)
        
                       
                        <option value="{{ $skill->id }}" {{ (collect(old('skills'))->contains($skill->id)) ? 'selected':'' }}  @foreach($studentskill as $sublist){{$sublist == $skill->id ? 'selected': ''}}   @endforeach  > {{ $skill->skill_name }}</option>
                        @endforeach
                      
        @endif
                  </select>
            </div>
            <div class="mb-3 col-md-6">
            <label  class="bmd-label-floating">DOB <span class="text-danger">*</span></label>
                        <input id="dob" type="date" name="dob" class="form-control"
                               value="{{  ($user->dob)  }}" required>
                        @error ('dob')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
            </div>
            <div class="mb-3 col-md-6">
            <label  class="bmd-label-floating">Country <span class="text-danger">*</span></label>
                    <select data-placeholder="Select a country"  name="location" id="location" class="select2 form-select form-select @error('location') is-invalid @enderror" data-allow-clear="true" >
                  
                        <!-- <option value=""></option> -->
                       
                        @foreach ($options as $key => $value)
                        <option value="{{ $value['id'] }}"  {{  ($issetUser && $user->location == $value['id']) || (old('location')==$value['id'])  ? 'selected' : ''}}>{{ $value['country_name'] }}</option>
                      @endforeach
                    </select>
                    @error('location')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
            </div>
            <div class="mb-3 col-md-12">
            <label class="bmd-label-floating">Description <span class="text-danger">*</span></label>
                    <textarea id="description" cols="70" rows="5"  class="form-control @error('description') is-invalid @enderror" 
                    name="description" value="{{ old('description') ?? ($issetUser ? $user->description : '')  }}" required autocomplete="description">{{ old('description') ?? ($issetUser ? $user->description : '')  }}</textarea>
                    <!-- <textarea id="description"  rows="4" cols="50" name="description" class="form-control"
                        value="{{ old('description') ?? ($issetUser ? $user->description : '')  }}"{{($issetUser) ? '' : 'required'}} ></textarea> -->
                    @error ('description')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
            </div>
          
  
          </div>
          <div class="mt-2">
            <button id ="submit" type="submit" class="btn btn-primary me-2">Save changes</button>
            <a href="{{ route('user.index') }}"  class= "float-left">
            <button type="button" class="btn btn-label-secondary">Cancel</button>
                      </a>
          </div>
        </form>
      </div>
      <!-- /Account -->
   
    
  </div>
</div>
 
@endsection
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>
    $( document ).ready(function() {

      var selected_categories = {!! json_encode($studentcategorys) !!};
        var selected_skills = {!! json_encode($studentskill) !!};

        // Populate the city dropdown with the cities corresponding to the selected countries
        if (selected_categories.length > 0) {
            $.ajax({
                url: '{{ route('skills') }}',
                type: 'GET',
                data: {
                    'category_ids': selected_categories
                },
                success: function (response) {
                    var options = '<option value="">Select one or more skills</option>';

                    $.each(response, function (key, skill) {
                      options += '<option value="' + skill.id + '">' + skill.skill_name + '</option>';
                    });

                    $('#skills').html(options);

                    // Pre-select the previously selected city values
                    if (selected_skills.length > 0) {
                        $('#skills').val(selected_skills);
                    }
                }
            });
        }

      $('#category').change(function () {
    
    var category_ids = $(this).val();

    $.ajax({
        url: '{{ route('skills') }}',
        type: 'GET',
        data: {
            'category_ids': category_ids
        },
        success: function (response) {
          console.log(response);
            var options = '<option value="">Select one or more skills</option>';

            $.each(response, function (key, skill) {
                options += '<option value="' + skill.id + '">' + skill.skill_name + '</option>';
            });

            $('#skills').html(options);
        }
    });
});

  

      });
  </script>
