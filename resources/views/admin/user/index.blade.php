@extends('layouts/layoutMaster')

@section('title', 'Student List ')

@section('content')

<div class="card">
<div class="card-header card-header-info">
<div class="dt-buttons btn-group flex-wrap">
     <div class="btn-group">
     </div>
     <a  href="{{route('user.create')}}">
       <button class="btn btn-secondary add-new btn-primary" tabindex="0"
        aria-controls="DataTables_Table_0" type="button">
        <span><i class="ti ti-plus me-0 me-sm-1 ti-xs"></i>
        <span class="d-none d-sm-inline-block">Add </span>
      </span></button></a>
     </div>
</div>
  <div class="card-datatable table-responsive">
 
    <table id="example" class="datatables-users table border-top">
                <thead>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th class="text-center">Action</th>
                </thead>
                <tbody>
                  @isset($users)
                  @foreach($users as $user)
                  <tr>
                    <td>{{$loop->index + 1}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->phone_number}}</td>
                    <td>
                    <div class="d-flex align-items-center">
            <div class="dropdown">
              <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown"><i class="ti ti-dots-vertical"></i></button>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="{{ route('user.edit', $user->id) }}"><i class="ti ti-pencil me-1"></i> Edit</a>
                <a class="dropdown-item" href="{{ route('user.delete', $user->id) }}"><i class="ti ti-trash me-1"></i> Delete</a>
              </div>
            </div>
<a href="{{ route('user.show', $user->id) }}" class="text-body"><i class="ti ti-eye ti-sm me-2"></i></a>
</div>
                    </td>
                  </tr>
                  @endforeach
                  @endisset
                </tbody>
              </table>
            </div>
          </div>
        
@endsection
