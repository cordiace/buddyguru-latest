@php
    $formAction = isset($user) ? route('user.update', $user->id) : route('user.store');
    $issetUser = isset($user) ? 1 : 0;
        $options=$countries;
    $grades=$grade;

@endphp
@extends('layouts/layoutMaster')
@section('vendor-style')
<link rel="stylesheet" href="{{asset('assets/vendor/libs/bs-stepper/bs-stepper.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/bootstrap-select/bootstrap-select.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/select2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/formvalidation/dist/css/formValidation.min.css')}}" />
@endsection

@section('vendor-script')
<script src="{{asset('assets/vendor/libs/bs-stepper/bs-stepper.js')}}"></script>
<script src="{{asset('assets/vendor/libs/bootstrap-select/bootstrap-select.js')}}"></script>
<script src="{{asset('assets/vendor/libs/select2/select2.js')}}"></script>
<script src="{{asset('assets/vendor/libs/formvalidation/dist/js/FormValidation.min.js')}}"></script>
<script src="{{asset('assets/vendor/libs/formvalidation/dist/js/plugins/Bootstrap5.min.js')}}"></script>
<script src="{{asset('assets/vendor/libs/formvalidation/dist/js/plugins/AutoFocus.min.js')}}"></script>
@endsection



@section('page-script')
<script src="{{asset('assets/js/forms-selects.js')}}"></script>
<script src="{{asset('assets/js/user/form-validation.js')}}"></script>
@endsection
@section('content')

<div class="content">
  <div class="container-fluid">
    <div class="row d-flex justify-content-center">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              @isset($user)
                  Edit Details
              @else
                  Add New User
              @endisset
            </h4>
            <p class="card-category">Complete your profile</p>
          </div>
          <div class="card-body">
            <form novalidate action="{{ $formAction }}" class="form" id="myForm" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @isset($user) @method('PUT') @endisset
              @csrf
              <div class="row g-3">
              <div class="col-sm-6">
                    <label class="form-label">Name</label>
                    <input id="name" type="text" name="name" class="form-control"
                        value="{{ old('name') ?? ($issetUser ? $user->name : '')  }}" {{($issetUser) ? '' : 'required'}}>
                        <div class="invalid-feedback">Name is required </div>
                  </div>
              
                  <div class="col-sm-6">
                          <label class="form-label">Dob</label>
                          <input id="dob" type="date" name="dob" class="form-control"
                                 value=" "{{($issetUser) ? '' : 'required'}}>
                                 <div class="invalid-feedback">DOB is required </div>
                      </div>
                
                      <div class="col-sm-6">
                    <label class="form-label">Email</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') ?? ($issetUser ? $user->email : '')  }}" {{($issetUser) ? '' : 'required'}} autocomplete="email">

                    <div class="invalid-feedback">Email is required </div>
                  </div>
               
                  <div class="col-sm-6">
                    <label class="form-label">Phone Number</label>
                    <input id="phone_number" type="number" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" value="{{ old('phone_number') ?? ($issetUser ? $user->phone_number : '')  }}" {{($issetUser) ? '' : 'required'}} autocomplete="phone_number">

                    <div class="invalid-feedback">Phone Number is required </div>
                  </div>
              
                  <div class="col-sm-6">
                    <label class="form-label">Password</label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" {{($issetUser) ? '' : 'required'}} autocomplete="new-password">

                    <div class="invalid-feedback">Password is required </div>
                  </div>
               
                  <div class="col-sm-6">
                    <label class="form-label">Confirm Password</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" {{($issetUser) ? '' : 'required'}} autocomplete="new-password">
                    <div class="invalid-feedback">Confirm Password is required </div>
                  </div>
                

              <!-- <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Parent Name</label>
                    <input id="parent_name" type="text" name="parent_name" class="form-control"
                           value="{{ old('parent_name') ?? ($issetUser ? $user->student_detail->parent_name : '')  }}">
                    @error ('parent_name')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Parent Email</label>
                    <input id="parent_email" type="text" name="parent_email" class="form-control"
                           value="{{ old('parent_email') ?? ($issetUser ? $user->student_detail->parent_email : '')  }}">
                    @error ('parent_email')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div> -->

              <!-- <div class="row"> -->
                <!-- <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Parent Mobile</label>
                    <input id="parent_mobile" type="text" name="parent_mobile" class="form-control"
                           value="{{ old('parent_mobile') ?? ($issetUser ? $user->student_detail->parent_mobile : '')  }}">
                    @error ('parent_mobile')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div> -->
                 
                <div class="col-sm-6">
                <label class="form-label" for="category[]">Category<span class="text-danger">*</span></label>
                <select name="category[]" id="category" class="select2 form-select" multiple data-dropdown-css-class="select2-purple" data-placeholder="Select Category" required>
                
                        @foreach ($categories as $categorie)

                        <option  value="{{ $categorie->id }}"> {{ $categorie->category_name }}</option>
                        @endforeach
                    
                  </select>
                  <div class="invalid-feedback">Category is required </div>
              </div>
              <div class="col-sm-6">
                <label class="form-label" for="skills[]">Skill<span class="text-danger">*</span></label>
                <select name="skills[]" id="skills" class="select2 form-select" multiple data-dropdown-css-class="select2-purple" data-placeholder="Select one or more Skills" required>
                
                        <!-- @foreach ($skill_subset as $skill)
        
                       
                        <option  value="{{ $skill->id }}"> {{ $skill->skill_name }}</option>
                        @endforeach -->
                    
                  </select>
                  <div class="invalid-feedback">Skill is required </div>
              </div>
              <div class="col-sm-6">
                <label class="form-label" for="location">Country<span class="text-danger">*</span></label>
                <select   data-placeholder="Select a country"  name="location" id="location" class="select2 form-select form-select" data-allow-clear="true">
                                      
                        @foreach ($options as $key => $value)
                        <option value="{{ $value['id'] }}"  {{ $value['country_name'] == 'India' ? 'selected' : '' }}>{{ $value['country_name'] }}</option>
                      @endforeach
</select>
</div>
                  
              
                      <div class="col-sm-6">
                          <label class="form-label">Grade</label>
                          <select name="grade" id="grade" class="select2 form-select  @error('grade') is-invalid @enderror" required>
                              <option value=""></option>
                              @foreach ($grades as $key => $value)
                                  <option value="{{ $value['id'] }}" {{($issetUser && $user->student_detail->grade == $value['id']) ? 'selected' : ''}}>{{ $value['grade_name'] }}</option>
                              @endforeach
                          </select>
                          <div class="invalid-feedback">Grade is required </div>
                      </div>
                      <div class="col-sm-6">
                <label class="form-label" for="profile_image">Profile Picture<span class="text-danger">*</span></label>
                <input type="file" name="profile_image" id="profile_image" class="form-control" />
                <div id="valid_profile" style="font-size: 13px;display: block;"><b>.PNG .JPG </b></div>
              </div>
              <div class="col-sm-6">
                <label class="form-label" for="description">Description<span class="text-danger">*</span></label>
                <textarea name="description" id="description" class="form-control"  ></textarea>
              </div>

                      <div class="row g-3">
<div class="col-12 d-flex justify-content-between">
              <a href="{{ route('user.index') }}"  class= "pull-right">
                        <button type="button"  class="btn btn-primary  btn-sm">
                          Back
                        </button>
                      </a>
                     <button type="submit" id ="submit" name="submit_button" class="btn btn-primary btn-sm pull-right" >Submit</button>
         </div>
         </div>
         </div>  
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- @isset($user)
    <div class="row d-flex justify-content-center">
      <div class="col-lg-8">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              Delete this User
            </h4>
          </div>
          <div class="card-body">
            <form action="{{ route('user.destroy', $user->id) }}" method="POST">
                @csrf
                @method('DELETE')
              <div class="row">
                <div class="col-md-12">
                  Do you really want to do this ? Make sure you are selected the right item. This action is irreversible.
                  All data associated with this item will be erased permanantly.
                </div>
              </div>
              <button type="submit" class="btn btn-primary pull-right">Delete</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
    </div>
    @endisset -->
  </div>
</div>
@endsection
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
  <script type="text/javascript">
    
 $( document ).ready(function() {
  
  $('#category').change(function () {
    
        var category_ids = $(this).val();

        $.ajax({
            url: '{{ route('skills') }}',
            type: 'GET',
            data: {
                'category_ids': category_ids
            },
            success: function (response) {
              console.log(response);
                var options = '<option value="">Select one or more skills</option>';

                $.each(response, function (key, skill) {
                    options += '<option value="' + skill.id + '">' + skill.skill_name + '</option>';
                });

                $('#skills').html(options);
            }
        });
    });

  });
    </script>