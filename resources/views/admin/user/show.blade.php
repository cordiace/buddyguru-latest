@extends('layouts.dashboard')

@section('content')
    @include('partials.nav')
    <div class="content">
        <div class="container-fluid">
            <div class="row d-flex justify-content-center">
                <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">
                                Buddy Profile
                            </h4>
                            <p class="card-category">Buddy profile details</p>
                        </div>
                        <div class="card-body">

                            <div class="row justify-content-md-center">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <img src="{{ asset($user->profile_image) }}" class="img-thumbnail" />
                                    </div>
                                </div>
                            </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Name</label><br>
                                            {{ $user->name }}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">DOB</label><br>
                                            {{ $user->dob->format('d/m/Y') }}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Email</label><br>
                                            {{ $user->email }}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Phone Number</label><br>
                                            {{ $user->phone_number }}
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Parent Name</label><br>
                                            {{ $user->student_detail->parent_name }}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Parent Email</label><br>
                                            {{ $user->student_detail->parent_email }}
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Parent Mobile</label><br>
                                            {{ $user->student_detail->parent_mobile }}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Country</label><br>
                                            {{ $user->country->country_name }}
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Skills</label><br>
                                            
                                            {{ $user->student_detail->getSkill->skill_name }}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Grade</label><br>
                                            {{ $user->student_detail->getGrade->grade_name }}
                                        </div>
                                    </div>

                                </div>
                                

                                <div class="clearfix"></div>
                            
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
@endsection
