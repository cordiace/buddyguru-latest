@php
  $formAction = isset($subcategory) ? route('subcategory.update', $subcategory->id) : route('subcategory.store');
  $issetSubCategory = isset($subcategory) ? 1 : 0;
@endphp
@extends('layouts.dashboard')

@section('content')
  @include('partials.nav')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-8">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title">
                @isset($subcategory)
                  Edit Sub Category Details
                  @else
                    Add New Sub Category<Tutor></Tutor>
                    @endisset
              </h4>
              {{--<p class="card-category">Complete your profile</p>--}}
            </div>
            <div class="card-body">
              <form action="{{ $formAction }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
                @isset($subcategory) @method('PUT') @endisset
                @csrf
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="bmd-label-floating">Category</label>
                      <select name="category_id" id="category_id" class="form-control @error('category_id') is-invalid @enderror">
                        @foreach ($category_list as $key => $value)
                          <option value="{{ $value['id'] }}"
                                  @if ($value['id'] == old('category_id', ($issetSubCategory?$subcategory->category_id:'')))
                                  selected="selected"
                                  @endif
                          >{{ $value['category_name'] }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="bmd-label-floating">Sub Category Name</label>
                      <input id="sub_category_name" type="text" name="sub_category_name" class="form-control"
                             value="{{ old('sub_category_name') ?? ($issetSubCategory ? $subcategory->sub_category_name : '')  }}">
                      @error ('sub_category_name')
                      <p class="text-danger">{{ $message }}</p>
                      @enderror
                    </div>
                  </div>
                </div>

                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                <div class="clearfix"></div>
              </form>
            </div>
          </div>
        </div>
        <!-- <div class="col-md-4">
          <div class="card card-profile">
            <div class="card-avatar">
              <a href="javascript:;">
                <img class="img" src="../assets/img/faces/marc.jpg" />
              </a>
            </div>
            <div class="card-body">
              <h6 class="card-category text-gray">CEO / Co-Founder</h6>
              <h4 class="card-title">Alec Thompson</h4>
              <p class="card-description">
                Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
              </p>
              <a href="javascript:;" class="btn btn-primary btn-round">Follow</a>
            </div>
          </div>
        </div> -->
      </div>
      @isset($subcategory)
        <div class="row">
          <div class="col-md-8">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title">
                  Delete this Category
                </h4>
              </div>
              <div class="card-body">
                <form action="{{ route('subcategory.destroy', $subcategory->id) }}" method="POST">
                  @csrf
                  @method('DELETE')
                  <div class="row">
                    <div class="col-md-12">
                      Do you really want to do this ? Make sure you are selected the right item. This action is irreversible.
                      All data associated with this item will be erased permanantly.
                    </div>
                  </div>
                  <button type="submit" class="btn btn-primary pull-right">Delete</button>
                  <div class="clearfix"></div>
                </form>
              </div>
            </div>
          </div>
        </div>
      @endisset
    </div>
  </div>
@endsection
