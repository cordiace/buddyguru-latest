@extends('layouts/layoutMaster')

@section('content')
<div class="card">
<div class="card-header card-header-info">
<div class="dt-buttons btn-group flex-wrap">
     <div class="btn-group">
     </div>
     <a  href="{{route('cms-applied_gurus.create')}}">
       <button class="btn btn-secondary add-new btn-primary" tabindex="0"
        aria-controls="DataTables_Table_0" type="button">
        <span><i class="ti ti-plus me-0 me-sm-1 ti-xs"></i>
        <span class="d-none d-sm-inline-block">Apply For Guru</span>
      </span></button></a>
     </div>
</div>
  <div class="card-datatable table-responsive">
 
    <table id="example" class="datatables-users table border-top">
                <thead >
                  <th>ID</th>
                  <th>Phone Number</th>
                  <!-- <th>Image</th> -->
                  <th >Action</th>
                </thead>
                <tbody>
                  @isset($AppliedGurus)
                  @foreach($AppliedGurus as $AppliedGuru)
                  <tr>
                    <td>{{$loop->index + 1}}</td>
                    <td>{{$AppliedGuru->phone_number}}</td>
                    
                   
                    <td  class="text-center">
                <div class="d-flex align-items-center">
            <div class="dropdown">
              <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown"><i class="ti ti-dots-vertical"></i></button>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="{{ route('cms-applied_gurus.edit', $AppliedGuru->id) }}"><i class="ti ti-pencil me-1"></i> Edit</a>
                <a class="dropdown-item" href="{{ route('cms-applied_gurus.delete', $AppliedGuru->id) }}"><i class="ti ti-trash me-1"></i> Delete</a>
              </div>
            </div>
            <a class="d-flex align-items-center" href="{{ route('cms-applied_gurus.approve',['id'=>$AppliedGuru->id,'val'=>1]) }}">
            <button type="button" class="btn btn-primary float-left  btn-sm waves-effect waves-light">
                          Approve
                        </button>
</a>
<a class="d-flex align-items-center" href="{{ route('cms-applied_gurus.approve', ['id'=>$AppliedGuru->id,'val'=>2]) }}">
            <button type="button" class="btn btn-danger float-left  btn-sm waves-effect waves-light">
                          Reject
                        </button>
</a>
</div>
           
          </td>
                   
                  
                  </tr>
                  @endforeach
                  @endisset
                </tbody>
              </table>
            </div>
          </div>
       
@endsection
