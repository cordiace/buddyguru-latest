@php
    $formAction = isset($AppliedGuru) ? route('cms-applied_gurus.update', $AppliedGuru->id) : route('cms-applied_gurus.store');
    $issetGuru = isset($AppliedGuru) ? 1 : 0;
@endphp
@extends('layouts/layoutMaster')

@section('vendor-style')
<link rel="stylesheet" href="{{asset('assets/vendor/libs/bs-stepper/bs-stepper.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/bootstrap-select/bootstrap-select.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/select2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/formvalidation/dist/css/formValidation.min.css')}}" />
@endsection

@section('vendor-script')
<script src="{{asset('assets/vendor/libs/bs-stepper/bs-stepper.js')}}"></script>
<script src="{{asset('assets/vendor/libs/bootstrap-select/bootstrap-select.js')}}"></script>
<script src="{{asset('assets/vendor/libs/select2/select2.js')}}"></script>
<script src="{{asset('assets/vendor/libs/formvalidation/dist/js/FormValidation.min.js')}}"></script>
<script src="{{asset('assets/vendor/libs/formvalidation/dist/js/plugins/Bootstrap5.min.js')}}"></script>
<script src="{{asset('assets/vendor/libs/formvalidation/dist/js/plugins/AutoFocus.min.js')}}"></script>
@endsection

@section('page-script')
<script src="{{asset('assets/js/applied-guru/form-validation.js')}}"></script>
<script src="{{asset('assets/js/forms-selects.js')}}"></script>
@endsection

@section('content')
<h4 class="fw-bold py-3 mb-4">
  <span class="text-muted fw-light">Applied Guru/</span> Add
</h4>

<div class="col-md">
    <div class="card">
      <h5 class="card-header">
      @isset($AppliedGuru)
                  Edit Guru Details
              @else
                  Add New Guru<Tutor></Tutor>
              @endisset
      </h5>
      <div class="card-body">
        <form   action="{{ $formAction }}" class="Guruform" id="Guruform" method="POST" accept-charset="UTF-8" enctype="multipart/form-data" >
        @isset($AppliedGuru) @method('POST') @endisset  
              @csrf 
              <div class="row">
           
            <div class="mb-3 col-md-6">
              <label class="form-label" for="phone_number">Mobile Number<span class="text-danger">*</span></label>
             
                <input type="text" id="phone_number" name="phone_number"class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" value="{{ old('phone_number') ?? ($issetGuru ? $AppliedGuru->phone_number : '')  }}"  autocomplete="phone_number" placeholder="202 555 0111" />
               
            
            </div>
            <div class="mb-3 col-md-6">
              
            <label class="form-label" for="cover_letter">Cover Letter<span class="text-danger">*</span></label>
                <input type="file" name="cover_letter" id="v" class="form-control" required/>
               
                <div id ="valid" style="font-size: 13px;display: block;"><b>.PDF .DOC</b></div>
                @isset($AppliedGuru)
                    <a href="" class="view-portfolio" id="viewVideo" data-video_url="{{asset($TutorDetail->video_url)}}">
                      <button type="button" rel="tooltip" title="Play Video" class="btn float-right btn-primary btn-link btn-sm">
                      <i class="fa fa-play" aria-hidden="true"></i>
                      </button>
                  </a>
                  @endif

</div>
            <div class="mb-3 col-md-6">
            <label class="form-label" for="category">Category<span class="text-danger">*</span></label>
                  <select name="category[]" id="category" class="select2 form-select" multiple data-dropdown-css-class="select2-purple" data-placeholder="Select Category" required>
                  
                  @if(isset($category_subset))
                  @foreach ($category_subset as $category)

<option  value="{{ $category->id }}"> {{ $category->category_name }}</option>
@endforeach
                      
        @endif
                  </select>
                 
            </div>

            <div class="mb-3 col-md-6">
            <label class="form-label" for="skills">Skill<span class="text-danger">*</span></label>
                  <select name="skills[]" id="skills" class="select2 form-select" multiple data-dropdown-css-class="select2-purple" data-placeholder="Select Skill" required >
                  
                  <!-- @if(isset($skill_subset))
                        @foreach ($skill_subset as $skill)
        
                       
                        <option value="{{ $skill->id }}" {{ (collect(old('skills'))->contains($skill->id)) ? 'selected':'' }}   > {{ $skill->skill_name }}</option>
                        @endforeach
                      
        @endif -->
                  </select>
                 
            </div>
            <div class="mb-3 col-md-6">
                <label class="form-label" for="video_url">Intro Video<span class="text-danger">*</span></label>
                <input type="file" name="video_url" id="video_url" class="form-control" />
                <div id="valid_video_url" style="font-size: 13px;display: block;"><b>.MP4 .MKV .WEBM(not more than 1 GB)</b></div>
               
              
                    <div class="progress" id="progress_bar" style="display:none;height:20px; line-height: 20px;">

        <div class="progress-bar" id="progress_bar_process" role="progressbar" style="width:0%;">0%</div>

    </div>
   
    <div id="uploaded_image" class="alert  success-upload alert-dismissible fade show row mt-1" role="alert" style="display:none;height:20px; line-height: 20px;">  </div>
    </div>  
            <div class="mb-3 col-md-6">
            <label class="bmd-label-floating">Bio <span class="text-danger">*</span></label>
                    <textarea id="bio" cols="70" rows="5"  class="form-control @error('bio') is-invalid @enderror" 
                    name="bio" value="{{ old('bio') ?? ($issetGuru ? $AppliedGuru->bio : '')  }}" required autocomplete="bio">{{ old('bio') ?? ($issetGuru ? $AppliedGuru->bio : '')  }}</textarea>
                   
                    @error ('bio')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
            </div>
          
         
          <div class="row">
            <div class="col-12">
          
            <a href="{{ route('cms-applied_gurus.index') }}"  class= "pull-left">
                        <button type="button"  class="btn btn-primary float-left  btn-sm">
                          Back
                        </button>
                      </a>
              <button type="submit" class="btn btn-sm btn-primary">Submit</button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>


@endsection
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
  <script type="text/javascript">
    
 $( document ).ready(function() {
  
  $('#category').change(function () {
    
        var category_ids = $(this).val();

        $.ajax({
            url: '{{ route('skills') }}',
            type: 'GET',
            data: {
                'category_ids': category_ids
            },
            success: function (response) {
              console.log(response);
                var options = '<option value="">Select one or more skills</option>';

                $.each(response, function (key, skill) {
                    options += '<option value="' + skill.id + '">' + skill.skill_name + '</option>';
                });

                $('#skills').html(options);
            }
        });
    });

//intro video section
document.getElementById('video_url').addEventListener("change", function (e) {


var fileName = document.getElementById("video_url").value;
  var idxDot = fileName.lastIndexOf(".") + 1;
  var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
  if (extFile=="mp4" || extFile=="webm" || extFile=="mkv"){
    $('#valid_video_url').hide();

$('#submit').attr('disabled','disabled');
  }else{
      alert("Only mp4/webm/mkv  files are allowed!");
      document.getElementById("video_url").value = null;
      return false;
  }


// let file_element =  $('#video_url').val();
var file_element = document.getElementById('video_url');
console.log("files");
console.log(file_element);
var progress_bar = document.getElementById('progress_bar');
var progress_bar_process = document.getElementById('progress_bar_process');

var uploaded_image = document.getElementById('uploaded_image');
var form_data = new FormData();

form_data.append('videoUrl', file_element.files[0]);

form_data.append('_token', document.getElementsByName('_token')[0].value);

progress_bar.style.display = 'block';

var ajax_request = new XMLHttpRequest();

ajax_request.open("POST", "{{ route('applied_gurus.storeVideo') }}");   

ajax_request.upload.addEventListener('progress', function(event){

var percent_completed = Math.round((event.loaded / event.total) * 100);

progress_bar_process.style.width = percent_completed + '%';

progress_bar_process.innerHTML = percent_completed + '% completed';

});
ajax_request.addEventListener('load', function(event){
console.log("response");
var file_data = JSON.parse(event.target.response);

console.log(file_data);

uploaded_image.innerHTML = '<div class="demo-inline-spacing" ><span class="badge bg-label-primary">Files Uploaded Successfully</span></div>';

$('#submit').removeAttr('disabled');

progress_bar.style.display = 'none';
uploaded_image.style.display = 'block';
$('.success-upload').fadeIn().delay(10000).fadeOut();
});

ajax_request.send(form_data);

});
//end intro video section


  });

  </script>