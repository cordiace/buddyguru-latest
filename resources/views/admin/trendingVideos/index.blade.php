@extends('layouts/layoutMaster')

@section('content')

<div class="card">
<div class="card-header card-header-info">
<div class="dt-buttons btn-group flex-wrap">
     <div class="btn-group">
     </div>
     <a  href="{{route('cms-videos.create')}}">
       <button class="btn btn-secondary add-new btn-primary" tabindex="0"
        aria-controls="DataTables_Table_0" type="button">
        <span><i class="ti ti-plus me-0 me-sm-1 ti-xs"></i>
        <span class="d-none d-sm-inline-block">Add Videos</span>
      </span></button></a>
     </div>
</div>
  <div class="card-datatable table-responsive">
 
    <table id="example" class="datatables-users table border-top">
                <thead>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Video</th>
                  <th >Action</th>
                </thead>
                <tbody>
                  @isset($TrendingVideos)
                  @foreach($TrendingVideos as $TrendingVideo)
                  <tr>
                    <td>{{$loop->index + 1}}</td>
                    <td>{{$TrendingVideo->file_name}}</td>
                    <td>
                    <a href="" class="view-portfolio" id="viewVideo" data-video_url="{{asset($TrendingVideo->video_url)}}">
                      <button type="button" rel="tooltip" title="Play Video" class="btn float-right btn-primary btn-link btn-sm">
                      <i class="fa fa-play" aria-hidden="true"></i>
                      </button>
                  </a>
                    </td>
                    <td  class="text-center">
                <div class="d-flex align-items-center">
            <div class="dropdown">
              <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown"><i class="ti ti-dots-vertical"></i></button>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="{{ route('cms-videos.edit', $TrendingVideo->id)}}"><i class="ti ti-pencil me-1"></i> Edit</a>
                <a class="dropdown-item" href="{{route('cms-videos.delete', $TrendingVideo->id)}}"><i class="ti ti-trash me-1"></i> Delete</a>
              </div>
            </div>

</div>
           
          </td>

                   
                  </tr>
                  @endforeach
                  @endisset
                </tbody>
              </table>
            </div>
          </div>
     <!-- Modal content-->
<div id="playModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

       
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">View Video</h4>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body" id="divVideo">
            <video width="320" height="240" controls>
              <source src="" type="video/mp4">
              Your browser does not support the video tag.
            </video>
          </div>

        </div>

      </div>
    </div>  
@endsection

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>
      $( document ).ready(function() {
 $( ".view-portfolio" ).on( "click", function(e) {
          e.preventDefault();


          $('source').attr('src',$(this).attr('data-video_url'));
          $("#divVideo video")[0].load();
          $('#playModal').modal('show');

        });

      });
  </script>
