@php
    $formAction = isset($Video) ? route('cms-videos.update', $Video->id) : route('cms-videos.store');
    $issetVideo = isset($Video) ? 1 : 0;
@endphp
@extends('layouts/layoutMaster')
@section('vendor-style')
<link rel="stylesheet" href="{{asset('assets/vendor/libs/bs-stepper/bs-stepper.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/bootstrap-select/bootstrap-select.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/select2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/formvalidation/dist/css/formValidation.min.css')}}" />
@endsection

@section('vendor-script')
<script src="{{asset('assets/vendor/libs/bs-stepper/bs-stepper.js')}}"></script>
<script src="{{asset('assets/vendor/libs/bootstrap-select/bootstrap-select.js')}}"></script>
<script src="{{asset('assets/vendor/libs/select2/select2.js')}}"></script>
<script src="{{asset('assets/vendor/libs/formvalidation/dist/js/FormValidation.min.js')}}"></script>
<script src="{{asset('assets/vendor/libs/formvalidation/dist/js/plugins/Bootstrap5.min.js')}}"></script>
<script src="{{asset('assets/vendor/libs/formvalidation/dist/js/plugins/AutoFocus.min.js')}}"></script>
@endsection



@section('page-script')
<script src="{{asset('assets/js/forms-selects.js')}}"></script>
@endsection
@section('content')

<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header card-header-info">
            <h4 class="card-title">
              @isset($Video)
                  Edit Videos 
              @else
                  Add New Videos<Tutor></Tutor>
              @endisset
            </h4>
            {{--<p class="card-country">Complete your profile</p>--}}
          </div>
          <div class="card-body">
            <form action="{{ $formAction }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @isset($Video) @method('POST') @endisset
              @csrf
              <div class="mb-3">
              <label class="form-label" for="region_id">Select User<span class="text-danger">*</span></label>
                <select   data-placeholder="Select a user"  name="user_id" id="user_id" class="select2 form-select form-select" data-allow-clear="true">
                    
                @isset($Users)
                      @foreach($Users as $value)
                      <!-- <option value="{{$value->id}}">{{strtoupper($value->category_name)}}</option> -->
                      <option value="{{ $value['id'] }}"  {{  ($issetVideo && $Video->user_id == $value['id']) || (old('user_id')==$value['id'])  ? 'selected' : ''}}>{{ $value['name'] }}</option>
                      @endforeach
                      @endisset
                    </select>
                    @error ('user_id')
                        <p class="text-danger">User is required</p>
                    @enderror
                  </div>
              
                  <div class="mb-3 ">
              
              <label class="form-label" for="video_url"> Video<span class="text-danger">*</span></label>
                  <input type="file" name="video_url" id="video_url" class="form-control" />
                  @error ('video_url')
                        <p class="text-danger">Video is required</p>
                    @enderror
                  <div id ="valid" style="font-size: 13px;display: block;"><b>.MP4 .MKV .WEBM(not More than 1GB)</b></div>
                  @isset($Video)
                      <a href="" class="view-portfolio" id="viewVideo" data-video_url="{{asset($Video->video_url)}}">
                        <button type="button" rel="tooltip" title="Play Video" class="btn float-right btn-primary btn-link btn-sm">
                        <i class="fa fa-play" aria-hidden="true"></i>
                        </button>
                    </a>
                    @endif
                
                    <div class="progress" id="progress_bar2" style="display:none;height:20px; line-height: 20px;">
  
  <div class="progress-bar" id="progress_bar_process2" role="progressbar" style="width:0%;">0%</div>
  
  </div>
  
  <div id="uploaded_image2" class="alert  success-upload2 alert-dismissible fade show row mt-1" role="alert">  </div>
         
  
  </div>
              <div class="row">
            <div class="col-12">
              <a href="{{ route('cms-videos.index') }}"  class= "pull-left">
                        <button type="button"  class="btn btn-primary float-left  btn-sm">
                          Back
                        </button>
                      </a>
              <button id="submit" type="submit" class="btn btn-primary float-right btn-sm">Submit</button>
              <div class="clearfix"></div>
</div>
</div>
            </form>
          </div>
        </div>
      </div>
      <!-- <div class="col-md-4">
        <div class="card card-profile">
          <div class="card-avatar">
            <a href="javascript:;">
              <img class="img" src="../assets/img/faces/marc.jpg" />
            </a>
          </div>
          <div class="card-body">
            <h6 class="card-country text-gray">CEO / Co-Founder</h6>
            <h4 class="card-title">Alec Thompson</h4>
            <p class="card-description">
              Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
            </p>
            <a href="javascript:;" class="btn btn-primary btn-round">Follow</a>
          </div>
        </div>
      </div> -->
    </div>
    <!-- @isset($country)
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              Delete this Country
            </h4>
          </div>
          <div class="card-body">
            <form action="" method="POST">
                @csrf
                @method('DELETE')
              <div class="row">
                <div class="col-md-12">
                  Do you really want to do this ? Make sure you are selected the right item. This action is irreversible.
                  All data associated with this item will be erased permanantly.
                </div>
              </div>
              <button type="submit" class="btn btn-primary pull-right">Delete</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
    </div>
    @endisset -->
  </div>
</div>
<!-- Modal content-->
<div id="playModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

       
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">View Video</h4>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body" id="divVideo">
            <video width="320" height="240" controls>
              <source src="" type="video/mp4">
              Your browser does not support the video tag.
            </video>
          </div>

        </div>

      </div>
    </div>
@endsection
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>
      $( document ).ready(function() {
 $( ".view-portfolio" ).on( "click", function(e) {
          e.preventDefault();


          $('source').attr('src',$(this).attr('data-video_url'));
          $("#divVideo video")[0].load();
          $('#playModal').modal('show');

        });

        document.getElementById('video_url').addEventListener("change", function (e) {
          var fileName = document.getElementById("video_url").value;
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
        if (extFile=="mp4" || extFile=="webm" || extFile=="mkv"){
          $('#valid').hide();

$('#viewVideo').hide();
$('#submit').attr('disabled','disabled');
        }else{
            alert("Only mp4/mkv/webm  files are allowed!");
            document.getElementById("video_url").value = null;
          return false;
        }

  var file_element = document.getElementById('video_url');
console.log("files");
console.log(file_element);
var progress_bar = document.getElementById('progress_bar2');
var progress_bar_process = document.getElementById('progress_bar_process2');

var uploaded_image = document.getElementById('uploaded_image2');
var form_data = new FormData();

form_data.append('videoUrl', file_element.files[0]);

form_data.append('_token', document.getElementsByName('_token')[0].value);

progress_bar.style.display = 'block';

var ajax_request = new XMLHttpRequest();

ajax_request.open("POST", "{{ route('trendingVideo.save') }}");   

ajax_request.upload.addEventListener('progress', function(event){

  var percent_completed = Math.round((event.loaded / event.total) * 100);

  progress_bar_process.style.width = percent_completed + '%';

  progress_bar_process.innerHTML = percent_completed + '% completed';

});
ajax_request.addEventListener('load', function(event){
console.log("response");
var file_data = JSON.parse(event.target.response);

console.log(file_data);

uploaded_image.innerHTML = '<div class="demo-inline-spacing" ><span class="badge bg-label-primary">Files Uploaded Successfully</span></div>';

$('#submit').removeAttr('disabled');

progress_bar.style.display = 'none';
$('.success-upload2').fadeIn().delay(10000).fadeOut();
});

ajax_request.send(form_data);

});


      });
  </script>

