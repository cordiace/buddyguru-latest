@php
    $formAction = route('general-admin.tier.storeTier');
    $issetBanner = isset($TierManagements) ? 1 : 0;
@endphp
@extends('layouts/layoutMaster')

@section('content')

<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header card-header-info">
            <h4 class="card-title">
              @isset($banner)
                  Edit Tier Details
              @else
                  Add New Tier
              @endisset
            </h4>
          </div>
          <div class="card-body">
            <form action="{{ $formAction }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @isset($banner) @method('POST') @endisset
              @csrf
              <div class="mb-3">
                    <label class="bmd-label-floating">Tier<span class="text-danger">*</span></label>
                    <select name="tier" class="form-control">
                    @foreach ($Tiers as $key => $value)
                          <option value="{{ $value['id'] }}" >{{ $value['name'] }}</option>
                          @endforeach
                    </select>
                    @error ('location')
                        <p class="text-danger">Tier is required</p>
                    @enderror
                
              </div>

              <div class="mb-3">
                    <label class="bmd-label-floating">Country<span class="text-danger">*</span></label>
                    <input name="country" id="heading" class="form-control" value="{{ old('banner') ?? ($issetBanner ? $banner->heading : '')  }}" required>
                    @error ('country')
                        <p class="text-danger">Country is required</p>
                    @enderror
                  </div>
             

                  <div class="mb-3">
                    <label class="bmd-label-floating">Currency Type<span class="text-danger">*</span></label>
                    <select name="currency_type" class="form-control">
                    @isset($currency)
                      @foreach($currency as $value)
                      <option value="{{$value->id}}">{{strtoupper($value->symbol)}}</option>
                      @endforeach
                      @endisset
                    </select>
                    @error ('currency_type')
                        <p class="text-danger">Currency is required</p>
                    @enderror
                  </div>
                  <div class="row">
            <div class="col-12">
              <a href="{{ route('general-admin.tier.index') }}"  class= "float-left">
                        <button type="button"  class="btn btn-primary float-left  btn-sm">
                          Back
                        </button>
                      </a>

              <button type="submit" class="btn btn-primary btn-sm float-right">Submit</button>
              <div class="clearfix"></div>
</div>
</div>
            </form>
          </div>
        </div>
      </div>
    </div>
    @isset($banner)
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              Delete this banner
            </h4>
          </div>
          <div class="card-body">
            <form action="{{ route('banner.destroy', $banner->id) }}" method="POST">
                @csrf
                @method('DELETE')
              <div class="row">
                <div class="col-md-12">
                  Do you really want to do this ? Make sure you are selected the right item. This action is irreversible.
                  All data associated with this item will be erased permanantly.
                </div>
              </div>
              <button type="submit" class="btn btn-primary pull-right">Delete</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
    </div>
    @endisset
  </div>
</div>
@endsection
