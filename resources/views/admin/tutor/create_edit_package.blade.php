@php
$issetUser = isset($user) ? 1 : 0;
$issetpackageDetails = count($packageDetails) >0 ? 1 : 0;
    $formAction = route('tutor.package.store',$id);
@endphp
@extends('layouts/layoutMaster')
@section('vendor-style')
<link rel="stylesheet" href="{{asset('assets/vendor/libs/bootstrap-select/bootstrap-select.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/select2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/flatpickr/flatpickr.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/typeahead-js/typeahead.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/tagify/tagify.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/formvalidation/dist/css/formValidation.min.css')}}" />
@endsection

@section('vendor-script')
<script src="{{asset('assets/vendor/libs/select2/select2.js')}}"></script>
<script src="{{asset('assets/vendor/libs/bootstrap-select/bootstrap-select.js')}}"></script>
<script src="{{asset('assets/vendor/libs/moment/moment.js')}}"></script>
<script src="{{asset('assets/vendor/libs/flatpickr/flatpickr.js')}}"></script>
<script src="{{asset('assets/vendor/libs/typeahead-js/typeahead.js')}}"></script>
<script src="{{asset('assets/vendor/libs/tagify/tagify.js')}}"></script>
<script src="{{asset('assets/vendor/libs/formvalidation/dist/js/FormValidation.min.js')}}"></script>
<script src="{{asset('assets/vendor/libs/formvalidation/dist/js/plugins/Bootstrap5.min.js')}}"></script>
<script src="{{asset('assets/vendor/libs/formvalidation/dist/js/plugins/AutoFocus.min.js')}}"></script>
@endsection

@section('page-script')
<script src="{{asset('assets/js/guru-class/form-validation.js')}}"></script>
<script src="{{asset('assets/js/forms-selects.js')}}"></script>
@endsection
@section('content')
<h4 class="fw-bold py-3 mb-4">
  <span class="text-muted fw-light">Guru Class /</span> Add
</h4>
<div class="content">
  <div class="container-fluid">
    <div class="row d-flex justify-content-center">
      <div class="col-lg-12">
     
      

        <div class="card">
          <div class="card-header card-header-info">
            <h4 class="card-title">
                  Guru Class
            </h4>
            <p class="card-category">Manage Guru Class</p>
          </div>
          @if (session()->has('message'))
    <div class="col-lg-3 " style="margin: 0 auto;">
        <div class="alert  alert-success alert-dismissible fade show" role="alert">
        {{ session()->get('message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
    </div>
@endif
          <div class="card-body">
            <form novalidate action="{{ $formAction }}" class="form" id="myForm" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @csrf
              <div class="row">
              <div class="col-md-6 class_type">
                  <div class="form-group">
                    <!-- <label class="bmd-label-floating">Select Class</label> -->
                    
                    <select id="batch_id"  type="hidden" name="batch_id" class="form-control" hidden>
                      @isset($userClasses)
                      @foreach($userClasses as $value)
                      <option value="{{$value->class_id}}">{{$value->class_name}}</option>
                      @endforeach
                      @endisset
                    </select>
                    
                    
                    @error('batch_id')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                  </div>
                  <!-- <div style="font-size: 12px;"> Not required,You can add it, Manage batch is in Guru Actions (Previous Menu).</div> -->
                </div>
              </div>
              <div class="row">
            
              <div class="mb-3 col-md-6">
            <label class="form-label" for="category">Category<span class="text-danger">*</span></label>
                  <select name="category[]" id="category" class="select2 form-select" multiple data-dropdown-css-class="select2-purple" data-placeholder="Select Category" >
                  @if(isset($category_subset))
                        @foreach ($category_subset as $category)
        
                       
                        <option value="{{ $category->id }}" {{ (collect(old('category'))->contains($category->id)) ? 'selected':'' }}  @foreach($tutorcategorys as $sublist){{$sublist == $category->id ? 'selected': ''}}   @endforeach  > {{ $category->category_name }}</option>
                        @endforeach
                      
        @endif
                  </select>
            </div>

            <div class="mb-3 col-md-6">
            <label class="form-label" for="skills">Skill<span class="text-danger">*</span></label>
                  <select name="skills[]" id="skills" class="select2 form-select" multiple data-dropdown-css-class="select2-purple" data-placeholder="Select Skill" >
                  @if(isset($skill_subset))
                        @foreach ($skill_subset as $skill)
        
                       
                        <option value="{{ $skill->id }}" {{ (collect(old('skills'))->contains($skill->id)) ? 'selected':'' }}  @foreach($tutorskill as $sublist){{$sublist == $skill->id ? 'selected': ''}}   @endforeach  > {{ $skill->skill_name }}</option>
                        @endforeach
                      
        @endif
                  </select>
            </div>
            
                <div class="col-md-4">
                  <div class="form-group">
                  
                    <label class="form-label">Intro Title <span class="text-danger">*</span></label>
                    <input id="intro_title" type="text" name="intro_title" class="form-control"
                        value=""required>
                        <!-- <textarea id="intro_title"  rows="2"  name="intro_title" class="form-control-sm"
                        value="" required></textarea> -->
                        <div class="invalid-feedback">Intro Title is required </div>
                  </div>
                </div>  
                
                <div class="col-md-5">
                  <div class="form-group">
                    <label class="form-label">Intro Description</label>
                    <textarea id="intro_description" cols="70" rows="5"  name="intro_description" class="form-control"
                        value="" required></textarea>
                        <div class="invalid-feedback">Intro Description is required </div>
                  </div>
                </div>
             
                <div class="col-md-3" id="introVideo" style="display:block">
                  <div class="form-group">
                  <label class="form-label">Trailer <span class="text-danger">*</span></label>
                 
                        <input type="file"  name="intro_video" class="form-control" id="intro_video" required>
                        
                     
                  <!-- <input id="intro_video" type="file" name="intro_video" class="form-control"
                        value="" required> -->

                        <div class="invalid-feedback">Trailer is required </div>
                  </div>
                  <div id="valid_intro_video" style="font-size: 13px;display: block;"><b>.MP4 .MKV .WEBM(not more than 1 GB)</b></div>
                 
              
                        <div class="progress" id="progress_bar_trailer" style="display:none;height:20px; line-height: 20px;">

            <div class="progress-bar" id="progress_bar_process_trailer" role="progressbar" style="width:0%;">0%</div>

        </div>

        <div id="uploaded_image_trailer" class="alert  success-upload_trailer alert-dismissible fade show row mt-1" role="alert">  </div>
                </div>
</div>


              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <input id="user_id" type="hidden" name="user_id" class="form-control" value="{{$id}}" required> 
                    <label class="form-label">Title <span class="text-danger">*</span></label>
                    <input id="title" type="text" name="title[]" class="form-control"
                        value=""required>
                        <div class="invalid-feedback">Title is required </div>
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="form-group">
                    <label class="form-label">Description <span class="text-danger">*</span></label>
                    <textarea id="description" cols="70" rows="5"  name="description[]" class="form-control"
                        value="" required ></textarea>
                        <div class="invalid-feedback">Description is required </div>
                  </div>
                </div>
                    
                <div class="col-md-3" id="video1" style="display:block">
                  <div class="form-group">
                    <label class="form-label">Video <span class="text-danger">*</span></label>
                  
                        <input type="file"  name="video_url[]" class="form-control" id="video_url" required>
                      
                    <!-- <input id="video_url" type="file" name="video_url[]" class="form-control"
                        value="" required> -->
                        <div class="invalid-feedback">Video is required </div>
                    <div id="valid_video_url" style="font-size: 13px;display: block;"><b>.MP4 .MKV .WEBM(not more than 1 GB)</p></div>
                    
              
                        <div class="progress" id="progress_bar" style="display:none;height:20px; line-height: 20px;">

            <div class="progress-bar" id="progress_bar_process" role="progressbar" style="width:0%;">0%</div>

        </div>

        <div id="uploaded_image" class="alert  success-upload alert-dismissible fade show row mt-1" role="alert">  </div>
                 
      </div>
                </div>
                            
              
</div>
<div id="guru_newinput"></div>
<div class="row pb-4">
            <div class="col-12">
<button type="button" name="add" id="add-btn" class="btn btn-primary  btn-sm">
Add More</button>
</div>
</div>

<div class="row g-3">
              <div class="col-sm-4">
                  <div class="form-group">
                    <label class="form-label"> Total Price(USD) <span class="text-danger">*</span></label>
                    <input id="total_price" type="text" name="total_price" class="form-control"
                    value=""required>
                    <div class="invalid-feedback">Total Price is required </div>
                  </div>
                </div>
<div class="row g-3">
<div class="col-12 d-flex justify-content-between">
              <a href="{{ route('tutor.show', $id) }}"  class= "pull-right">
                        <button type="button"  class="btn btn-primary  btn-sm">
                          Back
                        </button>
                      </a>
                     <button type="submit" id ="submit" name="submit_button" class="btn btn-primary btn-sm pull-right" >Submit</button>
         </div>
         </div>
  
            </form>
            
            </div>
          </div>
        </div>
      </div>
    </div>
    
  
   
    
  </div>
</div>


@endsection
<style>
    .dynamic-border {
    /* padding:20px; */
    /* display: inline-block; */
    /* vertical-align: top; */
    /* background: lightblue; */
    /* border: 1px solid #999; */
    /* border-radius: 10px; */
    margin: 10px auto;
   
    /* font-size: 12px; */
}
    </style>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script type="text/javascript">
//    window.addEventListener("DOMContentLoaded", function(e) {

// var form_being_submitted = false;

// var checkForm = function(e) {
//   var form = e.target;
//   if(form_being_submitted) {
//     alert("The form is being submitted, please wait a moment...");
//     form.submit_button.disabled = true;
//     e.preventDefault();
//     return;
//   }
//   form.submit_button.value = "Submitting form...";
//       form_being_submitted = true;
//     };
//     document.getElementById("myForm").addEventListener("submit", checkForm, false);

//   }, false);
      $( document ).ready(function() {
        var selected_categories = {!! json_encode($tutorcategorys) !!};
        var selected_skills = {!! json_encode($tutorskill) !!};

        // Populate the city dropdown with the cities corresponding to the selected countries
        if (selected_categories.length > 0) {
            $.ajax({
                url: '{{ route('skills') }}',
                type: 'GET',
                data: {
                    'category_ids': selected_categories
                },
                success: function (response) {
                    var options = '<option value="">Select one or more skills</option>';

                    $.each(response, function (key, skill) {
                      options += '<option value="' + skill.id + '">' + skill.skill_name + '</option>';
                    });

                    $('#skills').html(options);

                    // Pre-select the previously selected city values
                    if (selected_skills.length > 0) {
                        $('#skills').val(selected_skills);
                    }
                }
            });
        }

      $('#category').change(function () {
    
    var category_ids = $(this).val();

    $.ajax({
        url: '{{ route('skills') }}',
        type: 'GET',
        data: {
            'category_ids': category_ids
        },
        success: function (response) {
          console.log(response);
            var options = '<option value="">Select one or more skills</option>';

            $.each(response, function (key, skill) {
                options += '<option value="' + skill.id + '">' + skill.skill_name + '</option>';
            });

            $('#skills').html(options);
        }
    });
});

        $('.alert-success').fadeIn().delay(10000).fadeOut();
      
          document.getElementById('intro_video').addEventListener("change", function (e) {
            var fileName = document.getElementById("intro_video").value;
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
        if (extFile=="mp4" || extFile=="webm" || extFile=="mkv"){
          $('#valid_intro_video').hide();
         
$('#submit').attr('disabled','disabled');
        }else{
            alert("Only mp4/mkv/webm  files are allowed!");
            document.getElementById("intro_video").value = null;
          
        }



  var file_element = document.getElementById('intro_video');
console.log("files");
console.log(file_element);
var progress_bar = document.getElementById('progress_bar_trailer');
var progress_bar_process = document.getElementById('progress_bar_process_trailer');

var uploaded_image = document.getElementById('uploaded_image_trailer');
var form_data = new FormData();

form_data.append('videoUrl', file_element.files[0]);

form_data.append('_token', document.getElementsByName('_token')[0].value);

progress_bar.style.display = 'block';

var ajax_request = new XMLHttpRequest();

ajax_request.open("POST", "{{ route('package.StoreVideo') }}");   

ajax_request.upload.addEventListener('progress', function(event){

  var percent_completed = Math.round((event.loaded / event.total) * 100);

  progress_bar_process.style.width = percent_completed + '%';

  progress_bar_process.innerHTML = percent_completed + '% completed';

});
ajax_request.addEventListener('load', function(event){
console.log("response");
var file_data = JSON.parse(event.target.response);

console.log(file_data);

uploaded_image.innerHTML = '<div class="demo-inline-spacing" ><span class="badge bg-label-primary">Files Uploaded Successfully</span></div>';

$('#submit').removeAttr('disabled');

progress_bar.style.display = 'none';
$('.success-upload_trailer').fadeIn().delay(10000).fadeOut();
});

ajax_request.send(form_data);
});
// document.getElementById('intro_video').addEventListener("change", function (e) {

// $('#valid_intro_video').hide();
// });

    document.getElementById('video_url').addEventListener("change", function (e) {


      var fileName = document.getElementById("video_url").value;
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
        if (extFile=="mp4" || extFile=="webm" || extFile=="mkv"){
          $('#valid_video_url').hide();

$('#submit').attr('disabled','disabled');
        }else{
            alert("Only mp4/webm/mkv  files are allowed!");
            document.getElementById("video_url").value = null;
          
        }

  
// let file_element =  $('#video_url').val();
var file_element = document.getElementById('video_url');
console.log("files");
console.log(file_element);
var progress_bar = document.getElementById('progress_bar');
var progress_bar_process = document.getElementById('progress_bar_process');

var uploaded_image = document.getElementById('uploaded_image');
var form_data = new FormData();

form_data.append('videoUrl', file_element.files[0]);

form_data.append('_token', document.getElementsByName('_token')[0].value);

progress_bar.style.display = 'block';

var ajax_request = new XMLHttpRequest();

ajax_request.open("POST", "{{ route('package.StoreVideo') }}");   

ajax_request.upload.addEventListener('progress', function(event){

  var percent_completed = Math.round((event.loaded / event.total) * 100);

  progress_bar_process.style.width = percent_completed + '%';

  progress_bar_process.innerHTML = percent_completed + '% completed';

});
ajax_request.addEventListener('load', function(event){
console.log("response");
var file_data = JSON.parse(event.target.response);

console.log(file_data);

uploaded_image.innerHTML = '<div class="demo-inline-spacing" ><span class="badge bg-label-primary">Files Uploaded Successfully</span></div>';

$('#submit').removeAttr('disabled');

progress_bar.style.display = 'none';
$('.success-upload').fadeIn().delay(10000).fadeOut();
});

ajax_request.send(form_data);

});
   
      });
      var doc= '';


// $(document).ready(function() {
// $(document).on('click', '#upload'+count+'', function() {


// });
// }); 
var count ='';
      function getFile(val,getId){
        var a = document.getElementById(getId);
     
    count = getId;
    var fileName = document.getElementById(getId).value;
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
        if (extFile=="mp4" || extFile=="webm" || extFile=="mkv"){
          $('#multiplevideo'+count+'').hide();
 
$('#submit').attr('disabled','disabled');
        }else{
            alert("Only mp4/mkv/webm  files are allowed!");
            document.getElementById(getId).value = null;
            label.innerHTML = "Choose file";
          
        }

  console.log(getId);


// let file_element =  $('#video_url').val();
var file_element = document.getElementById(count);
console.log("files");
console.log(file_element);
var progress_bar = document.getElementById('progress_bar'+count+'');
var progress_bar_process = document.getElementById('progress_bar_process'+count+'');

var uploaded_image = document.getElementById('uploaded_image'+count+'');

console.log("uploaded_image");
console.log(uploaded_image);
var form_data = new FormData();

form_data.append('videoUrl', file_element.files[0]);

form_data.append('_token', document.getElementsByName('_token')[0].value);

progress_bar.style.display = 'block';

var ajax_request = new XMLHttpRequest();

ajax_request.open("POST", "{{ route('package.StoreVideo') }}");   

ajax_request.upload.addEventListener('progress', function(event){

  var percent_completed = Math.round((event.loaded / event.total) * 100);

  progress_bar_process.style.width = percent_completed + '%';

  progress_bar_process.innerHTML = percent_completed + '% completed';

});
ajax_request.addEventListener('load', function(event){
console.log("response");
var file_data = JSON.parse(event.target.response);

console.log(file_data);

uploaded_image.innerHTML = '<div id="success'+count+'"  class="demo-inline-spacing" ><span class="badge bg-label-primary">Files Uploaded Successfully</span></div>';

$('#submit').removeAttr('disabled');

progress_bar.style.display = 'none';
$('#success'+count+'').fadeIn().delay(10000).fadeOut();
});

ajax_request.send(form_data);
}
      
     


      
$(document).on('click', '#add-btn', function() {
addrow();

      });
     
      var i = 0;
      function addrow(){
       
        i++;

        var tr ='<div class="guru_sub">'+
        '<div class="col-md-12 col-xl-12 dynamic-border" >'+
    '<div class="card bg-label-primary text-white mb-3">'+
      '<div class="card-body">'+
        '<div class="row">'+
                '<div class="col-md-4">'+
                  '<div class="form-group">'+
                    '<input id="user_id" type="hidden" name="user_id" class="form-control" value="{{$id}}">'+
                    '<label class="form-label">Title <span class="text-danger">*</span></label>'+
                    '<input id="title" type="text" name="title[]" class="form-control"'+
                        'value=""required>'+
                   '<div class="invalid-feedback"> Title is required </div>'+
                  '</div>'+
                '</div>'+

                '<div class="col-md-5">'+
                  '<div class="form-group">'+
                    '<label class="form-label">Description <span class="text-danger">*</span></label>'+
                    '   <textarea id="description"  cols="70" rows="5" name="description[]" class="form-control"'+
                       ' value="" required></textarea>'+
                    ' <div class="invalid-feedback">Description  is required </div>'+
                  '</div>'+
                '</div>'+     

                '<div class="col-md-3"  style="display:block">'+
                  '<div class="form-group">'+
                    '<label class="form-label">Video <span class="text-danger">*</span></label>'+
                 
                        '<input id="'+i+'" type="file" onchange="return getFile(this.value,this.id);" name="video_url[]" class="form-control" value="" required>'+
                                            
                    ' <div class="invalid-feedback">Video is required </div>'+
                    '</div>'+
                    ' <div class="mb-3 col-md-12">'+
                    '<div id="multiplevideo'+i+'" style="font-size:13px;display:block;"><b>.MP4 .MKV .WEBM(not more than 1 GB)</b></div>'+
                    '</div>'+ 
                   
              
                       '<div class="progress" id="progress_bar'+i+'" style="display:none;height:20px; line-height: 20px;">'+

            '<div class="progress-bar" id="progress_bar_process'+i+'" role="progressbar" style="width:0%;">0%</div>'+

        '</div>'+

       

        '<div id="uploaded_image'+i+'" class="alert  success-upload alert-dismissible fade show row mt-1" role="alert"></div>'+
                  
                  '</div>'+
                  '</div>'+

                  '<div class="card-footer" style="text-align: right;" id="btn_remove'+i+'">'+
       
                  '<label > </label>'+
                  '<button type="button"  rel="tooltip" value="'+i+'" title="Delete"  class="btn btn-danger btn-sm guru_remove">'+
                 '<i class="ti ti-trash me-1"></i>'+
                        '</button>'+
                '</div >'+
            


                
                '</div >'+
                '</div >'+
                '</div >'+

                '</div >'  
                $('#guru_newinput').append(tr);
              }
              $(document).on('click', '.guru_remove', function(e) {
    e.preventDefault();
    $(this).closest('.guru_sub').remove();
});
  </script>

