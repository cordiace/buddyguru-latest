@php
    $formAction =route('tutor.store');
   

@endphp
@extends('layouts/layoutMaster')

@section('title', ' Create - Tutor')

@section('vendor-style')
<link rel="stylesheet" href="{{asset('assets/vendor/libs/bs-stepper/bs-stepper.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/bootstrap-select/bootstrap-select.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/select2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/formvalidation/dist/css/formValidation.min.css')}}" />
@endsection

@section('vendor-script')
<script src="{{asset('assets/vendor/libs/bs-stepper/bs-stepper.js')}}"></script>
<script src="{{asset('assets/vendor/libs/bootstrap-select/bootstrap-select.js')}}"></script>
<script src="{{asset('assets/vendor/libs/select2/select2.js')}}"></script>
<script src="{{asset('assets/vendor/libs/formvalidation/dist/js/FormValidation.min.js')}}"></script>
<script src="{{asset('assets/vendor/libs/formvalidation/dist/js/plugins/Bootstrap5.min.js')}}"></script>
<script src="{{asset('assets/vendor/libs/formvalidation/dist/js/plugins/AutoFocus.min.js')}}"></script>
@endsection

@section('page-script')
<script src="{{asset('assets/js/ui-popover.js')}}"></script>
<script src="{{asset('assets/js/guru-profile/custom-form-wizard-numbered.js')}}"></script>
<script src="{{asset('assets/js/guru-profile/custom-form-wizard-validation.js')}}"></script>
@endsection

@section('content')
<h4 class="fw-bold py-3 mb-4">
  <span class="text-muted fw-light">Guru/</span> Add
</h4>
<!-- Default -->
<div class="row">
  <div class="col-12">
    <h5>Guru Profile</h5>
  </div>
</div>

  <!-- Validation Wizard -->
  <div class="col-12 mb-4">
    <!-- <small class="text-light fw-semibold">Add</small> -->
    <div id="wizard-validation" class="bs-stepper mt-2">
      <div class="bs-stepper-header">
        <div class="step" data-target="#account-details-validation">
          <button type="button" class="step-trigger">
            <span class="bs-stepper-circle">1</span>
            <span class="bs-stepper-label mt-1">
              <span class="bs-stepper-title">Account Details</span>
              <span class="bs-stepper-subtitle">Setup Account Details</span>
            </span>
          </button>
        </div>
        <div class="line">
          <i class="ti ti-chevron-right"></i>
        </div>
        <div class="step" data-target="#guru-class-validation">
          <button type="button" class="step-trigger">
            <span class="bs-stepper-circle">2</span>
            <span class="bs-stepper-label">
              <span class="bs-stepper-title">Guru Class</span>
              <span class="bs-stepper-subtitle">Add Guru Class info</span>
            </span>
          </button>
        </div>
        <div class="line">
          <i class="ti ti-chevron-right"></i>
        </div>
        <div class="step" data-target="#module-class-validation">
          <button type="button" class="step-trigger">
            <span class="bs-stepper-circle">3</span>
            <span class="bs-stepper-label">
              <span class="bs-stepper-title">Module Class</span>
              <span class="bs-stepper-subtitle">Add Module Class Info</span>
            </span>
          </button>
        </div>
        <div class="line">
          <i class="ti ti-chevron-right"></i>
        </div>
        <div class="step" data-target="#live-class-validation">
          <button type="button" class="step-trigger">
            <span class="bs-stepper-circle">4</span>
            <span class="bs-stepper-label">
              <span class="bs-stepper-title">Live Class</span>
              <span class="bs-stepper-subtitle">Add Live Class info</span>
            </span>
          </button>
        </div>

      </div>
      <div class="bs-stepper-content">
      <form id="wizard-validation-form" onSubmit="return false" action="{{ $formAction }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
          <!-- Account Details -->
          @csrf
          <div id="account-details-validation" class="content">
            <div class="content-header mb-3">
              <h6 class="mb-0">Account Details</h6>
              <small>Enter Your Account Details.</small>
            </div>
            <div class="row g-3">
              <div class="col-sm-6">
                <label class="form-label" for="name">Name<span class="text-danger">*</span></label>
                <input type="text" name="name" id="name" class="form-control"  />
              </div>
              <div class="col-sm-6">
                <label class="form-label" for="email">Email<span class="text-danger">*</span></label>
                <input type="email" name="email" id="email" class="form-control"  aria-label="john.doe" />
              </div>
              <div class="col-sm-6 form-password-toggle">
                <label class="form-label" for="formValidationPass">Password<span class="text-danger">*</span></label>
                <div class="input-group input-group-merge">
                  <input type="password" id="password" name="password" class="form-control" placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;" aria-describedby="formValidationPass2" />
                  <span class="input-group-text cursor-pointer" id="password"><i class="ti ti-eye-off"></i></span>
                </div>
                @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
              </div>
              <div class="col-sm-6 form-password-toggle">
                <label class="form-label" for="formValidationConfirmPass">Confirm Password<span class="text-danger">*</span></label>
                <div class="input-group input-group-merge">
                  <input type="password" id="formValidationConfirmPass" name="formValidationConfirmPass" class="form-control" placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;" aria-describedby="formValidationConfirmPass2" />
                  <span class="input-group-text cursor-pointer" id="formValidationConfirmPass2"><i class="ti ti-eye-off"></i></span>
                </div>
              </div>
              <div class="col-sm-6">
                <label class="form-label" for="phone_number">Mobile Number<span class="text-danger">*</span></label>
                <input type="text" name="phone_number" id="phone_number" class="form-control"  />
              </div>
              <div class="col-sm-6">
                <label class="form-label"  for="commision">Commision in Percentage(%)<span class="text-danger">*</span><i class="fa fa-info-circle" aria-hidden="true" data-bs-toggle="tooltip" data-bs-placement="right" data-bs-custom-class="tooltip-warning" title="Tooltip on top"></i></label>
                <input type="text" name="commision" id="commision" class="form-control" value="{{$Commision->commision}}"  />
              </div>
            
              <div class="col-sm-6">
                <label class="form-label" for="category[]">Category<span class="text-danger">*</span></label>
                <select name="category[]" id="category" class="select2 form-select" multiple data-dropdown-css-class="select2-purple" data-placeholder="Select Category">
                
                        @foreach ($categories as $categorie)

                        <option  value="{{ $categorie->id }}"> {{ $categorie->category_name }}</option>
                        @endforeach
                    
                  </select>
              </div>
              <div class="col-sm-6">
                <label class="form-label" for="skills[]">Skill<span class="text-danger">*</span></label>
                <select name="skills[]" id="skills" class="select2 form-select" multiple data-dropdown-css-class="select2-purple" data-placeholder="Select one or more Skills">
                
                        <!-- @foreach ($skill_subset as $skill)
        
                       
                        <option  value="{{ $skill->id }}"> {{ $skill->skill_name }}</option>
                        @endforeach -->
                    
                  </select>
              </div>
              <div class="col-sm-6">
                <label class="form-label" for="location">Country<span class="text-danger">*</span></label>
                <select   data-placeholder="Select a country"  name="location" id="location" class="select2 form-select form-select-lg" data-allow-clear="true">
                                      
                        @foreach ($countries as $key => $value)
                        <option value="{{ $value['id'] }}"  {{ $value['country_name'] == 'India' ? 'selected' : '' }}>{{ $value['country_name'] }}</option>
                      @endforeach
                    </select>
            </div>
              <div class="col-sm-6">
                <label class="form-label" for="dob">DOB<span class="text-danger">*</span></label>
                <input type="date" name="dob" id="dob" class="form-control"  />
              </div>
              <div class="col-sm-6">
                <label class="form-label" for="description">Description<span class="text-danger">*</span></label>
                <textarea name="description" id="description" class="form-control"  ></textarea>
              </div>
              <div class="col-sm-6">
                <label class="form-label" for="profile_image">Profile Picture<span class="text-danger">*</span></label>
                <input type="file" name="profile_image" id="profile_image" class="form-control" />
                <div id="valid_profile" style="font-size: 13px;display: block;"><b>.PNG .JPG </b></div>
              </div>
              <div class="col-sm-6">
                <label class="form-label" for="video_url">Intro Video<span class="text-danger">*</span></label>
                <input type="file" name="video_url" id="video_url" class="form-control" />
                <div id="valid_video_url" style="font-size: 13px;display: block;"><b>.MP4 .MKV .WEBM(not more than 1 GB)</b></div>
               
              
                    <div class="progress" id="progress_bar" style="display:none;height:20px; line-height: 20px;">

        <div class="progress-bar" id="progress_bar_process" role="progressbar" style="width:0%;">0%</div>

    </div>
   
    <div id="uploaded_image" class="alert  success-upload alert-dismissible fade show row mt-1" role="alert" style="display:none;height:20px; line-height: 20px;">  </div>
    </div>          
              <div class="col-12 d-flex justify-content-between">
              <!-- <a href="{{ route('tutor.index') }}" >
                <button class="btn btn-primary " > <i class="ti ti-arrow-left "></i>
                  <span class="align-middle d-sm-inline-block d-none">Back</span>
                </button>
</a> -->
<a href="{{ route('tutor.index') }}"  class= "pull-left">
                        <button type="button"  class="btn btn-primary"><i class="ti ti-arrow-left "></i>
                          Back
                        </button>
                      </a>
                <button id="next1" class="btn btn-primary btn-next"> <span class="align-middle d-sm-inline-block d-none me-sm-1">Next</span> <i class="ti ti-arrow-right"></i></button>
              </div>
            </div>
          </div>
          <!-- Guru Class -->
          <div id="guru-class-validation" class="content">
            <div class="content-header mb-3">
              <h6 class="mb-0">Guru Class</h6>
              <small>Enter Your Class Info.</small>
            </div>
            <div class="row g-3">
              <div class="col-sm-4">
                <label class="form-label" for="guru_intro_title">Intro Title<span class="text-danger">*</span></label>
                <input type="text" id="guru_intro_title" name="guru_intro_title" class="form-control"  />
              </div>
              <div class="col-sm-4">
                <label class="form-label" for="guru_intro_description">Intro Description<span class="text-danger">*</span></label>
                <textarea id="guru_intro_description"  name="guru_intro_description" class="form-control"
                        value="" ></textarea>
              
              </div>
              <div class="col-sm-4">
                <label class="form-label" for="guru_trailer">Trailer<span class="text-danger">*</span></label>
                <input type="file" id="guru_trailer" name="guru_trailer" class="form-control"  />
                <div id="guru_valid_intro_video" style="font-size: 13px;display: block;"><b>.MP4 .MKV .WEBM(not more than 1 GB)</b></div>
                 
              
                        <div class="progress" id="guru_progress_bar_trailer" style="display:none;height:20px; line-height: 20px;">

            <div class="progress-bar" id="guru_progress_bar_process_trailer" role="progressbar" style="width:0%;">0%</div>

        </div>
       
        <div id="guru_uploaded_image_trailer" class="alert  success-upload_trailer alert-dismissible fade show row mt-1" role="alert" style="display:none;">  </div>
           
        </div> 
</div>      
    
    <div class="row g-3">
              <div class="col-sm-4">
                    
                    <label class="form-label " for="guru_title[]">Title <span class="text-danger">*</span></label>
                    <input id="guru_title" type="text" name="guru_title[]" class="form-control"
                        value="">
                      
                 
                </div>
                <div class="col-sm-4">
                 
                    <label class="form-label" for="guru_description[]">Description <span class="text-danger">*</span></label>
                    <textarea id="guru_description"  name="guru_description[]" class="form-control"
                        value="" ></textarea>
                   
                  </div>
             
                    
                
                <div class="col-sm-4" style="display:block">
                    <label class="form-label" for="guru_video">Video <span class="text-danger">*</span></label>
                  
                    <input type="file" id="guru_video" name="guru_video[]" class="form-control"  />
                  
                    <div id="guru_valid_video_url" style="font-size: 13px;display: block;"><b>.MP4 .MKV .WEBM(not more than 1 GB)</b></div>
                   
              
                        <div class="progress" id="guru_progress_bar" style="display:none;height:20px; line-height: 20px;">

            <div class="progress-bar" id="guru_progress_bar_process" role="progressbar" style="width:0%;">0%</div>

        </div>

        <div id="guru_uploaded_image" class="alert  success-upload alert-dismissible fade show row mt-1" role="alert" style="display:none">  </div>
                  </div>
              
                            
              
</div>
<div id="guru_newinput"></div>
<div class="row pb-4">
            <div class="col-12">
              <button type="button"  id="guru_add-btn" class="btn btn-primary" >Add More</button>
            </div>
          </div>
         
          <div class="row g-3">
              <div class="col-sm-4">
                    <label class="bmd-label-floating">Total Price(USD) <span class="text-danger">*</span></label>
                    <input id="guru_total_price" type="text" name="guru_total_price" class="form-control"
                    value="">
                  
                  </div>
          

              <div class="col-12 d-flex justify-content-between">
                <button class="btn btn-primary btn-prev"> <i class="ti ti-arrow-left "></i>
                  <span class="align-middle d-sm-inline-block d-none">Previous</span>
                </button>
                <button class="btn btn-primary btn-skip"> Skip <i class="ti ti-arrow-right"></i></button>
                <input type="text" id="guruIn" name="guru_class" value="false" hidden/>
                <button id="next2" class="btn btn-primary btn-next"> <span class="align-middle d-sm-inline-block d-none me-sm-1">Next</span> <i class="ti ti-arrow-right"></i></button>
              </div>
            </div>
            </div>

           <!-- Module Class -->
           <div id="module-class-validation" class="content">
            <div class="content-header mb-3">
              <h6 class="mb-0">Module Class</h6>
              <small>Enter Your Class Info.</small>
            </div>
            <div class="row g-3">
              <div class="col-sm-4">
                <label class="form-label" for="module_intro_title">Intro Title<span class="text-danger">*</span></label>
                <input type="text" id="module_intro_title" name="module_intro_title" class="form-control"  />
              </div>
              <div class="col-sm-4">
                <label class="form-label" for="module_intro_description">Intro Description<span class="text-danger">*</span></label>
                <textarea id="module_intro_description"  name="module_intro_description" class="form-control"
                        value="" ></textarea>
              
              </div>
              <div class="col-sm-4">
                <label class="form-label" for="module_trailer">Trailer<span class="text-danger">*</span></label>
                <input type="file" id="module_trailer" name="module_trailer" class="form-control"  />
                <div id="module_valid_intro_video" style="font-size: 13px;display: block;"><b>.MP4 .MKV .WEBM(not more than 1 GB)</b></div>
                 
                        <div class="progress" id="module_progress_bar_trailer" style="display:none;height:20px; line-height: 20px;">

            <div class="progress-bar" id="module_progress_bar_process_trailer" role="progressbar" style="width:0%;">0%</div>

        </div>

        <div id="module_uploaded_image_trailer" class="alert  success-upload_trailer alert-dismissible fade show row mt-1" role="alert" style="display:none">  </div>
           
    </div>
             
    
    <div class="col-sm-4">
                    
                    <label class="form-label" for="module_title[]">Title <span class="text-danger">*</span></label>
                    <input id="module_title" type="text" name="module_title[]" class="form-control"
                        value="">
                      
                  </div>
               
                  <div class="col-sm-4">
                
                    <label class="form-label" for="module_description[]">Description <span class="text-danger">*</span></label>
                    <textarea id="module_description"  name="module_description[]" class="form-control"
                        value="" ></textarea>
                   
                  </div>
               
                    
                
                <div class="col-sm-4"  style="display:block">
                    <label class="form-label" for="module_video[]">Video <span class="text-danger">*</span></label>
                  
                    <input type="file" id="module_video" name="module_video[]" class="form-control" />
                  
                    <div id="module_valid_video_url" style="font-size: 13px;display: block;"><b>.MP4 .MKV .WEBM(not more than 1 GB)</b></div>
                   
              
                        <div class="progress" id="module_progress_bar" style="display:none;height:20px; line-height: 20px;">

            <div class="progress-bar" id="module_progress_bar_process" role="progressbar" style="width:0%;">0%</div>

        </div>

        <div id="module_uploaded_image" class="alert  success-upload alert-dismissible fade show row mt-1" role="alert" style="display:none">  </div>
                  </div>
                </div>
                            
              
<div id="module_newinput"></div>
<div class="row pb-4">
            <div class="col-12">
              <button type="button"  id="module_add-btn" class="btn btn-primary" >Add More</button>
            </div>
          </div>
         
          <div class="row g-3">
              <div class="col-sm-4">
                    <label class="form-label" for="module_total_price">Total Price(USD) <span class="text-danger">*</span></label>
                    <input id="module_total_price" type="text" name="module_total_price" class="form-control"
                    value="">
                  
                  </div>
                

              <div class="col-12 d-flex justify-content-between">
                <button class="btn btn-primary btn-prev"> <i class="ti ti-arrow-left "></i>
                  <span class="align-middle d-sm-inline-block d-none">Previous</span>
                </button>
                <button class="btn btn-primary btn-skip"> <span class="align-right d-sm-inline-block d-none me-sm-1">Skip</span> <i class="ti ti-arrow-right"></i></button>
                <input type="text" id="moduleIn" name="module_class" value="false" hidden/>
                <button id="next3" class="btn btn-primary btn-next"> <span class="align-middle d-sm-inline-block d-none me-sm-1">Next</span> <i class="ti ti-arrow-right"></i></button>
              </div>
            </div>
            </div>
          <!-- Live Class -->
          <div id="live-class-validation" class="content">
            <div class="content-header mb-3">
              <h6 class="mb-0">Live Class</h6>
              <small>Enter Your class Info.</small>
            </div>
            <div class="row g-3">
           
            <div class="col-md-4">
                   
                    <label class="form-label">Intro Title <span class="text-danger">*</span></label>
                    <input id="live_intro_title" type="text" name="live_intro_title" class="form-control "
                        value="" >
                   
                </div>
                <div class="col-md-5">
                    <label class="form-label">Intro Description <span class="text-danger">*</span></label>
                    <textarea id="live_intro_description" rows="5" cols="70"  name="live_intro_description" class="form-control"
                        value="" ></textarea>
                  
                </div>
                <div class="col-md-3">
                
                    <label class="form-label">Trailer <span class="text-danger">*</span></label>
               <input type="file"  name="live_video" class="form-control" id="live_video">
                       
                    <div id="live_valid_video_url" style="font-size: 13px;display:block;"><b>.MP4 .MKV .WEBM(not More than 1GB)</b></div>
                    
                        <div class="progress" id="live_progress_bar" style="display:none;height:20px; line-height: 20px;">

            <div class="progress-bar" id="live_progress_bar_process" role="progressbar" style="width:0%;">0%</div>

        </div>

        <div id="live_uploaded_image" class="alert  live_success-upload alert-dismissible fade show row mt-1" role="alert" style="display:none">  </div>
                  </div>
  
                <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
      
                    <label class="form-label">Title <span class="text-danger">*</span></label>
                    <input id="live_title" type="text" name="live_title" class="form-control "
                        value="{{old('intro_description')}}" >
                
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="form-group">
                    <label class="form-label">Description <span class="text-danger">*</span></label>
                    <textarea id="live_description" rows="5" cols="70"  name="live_description" class="form-control"
                        value="" ></textarea>
                   
                  </div>
                </div>
                <div id="classTypes" class="col-md-3">
                  <div class="form-group">
                    <label class="form-label">Select Class Type <span class="text-danger">*</span></label>
                    
                    <select id="class_type" name="class_type" class="form-control class_type">
                      
                      <option value="1" @if (old('class_type') == "1") {{ 'selected' }} @endif>One To One</option>
                      <option value="2" @if (old('class_type') == "2") {{ 'selected' }} @endif>Group Class</option>
                    
                    </select>
                  
                  
        </div>
        </div>       
                      
</div>

<div class="row">
             
        <div id="startDate" class="col-md-3" >
                    <div class="form-group">
                        <label class="form-label">Start Date </label>
                        <input id="start_date" type="date" name="start_date" class="form-control"
                               value="{{ old('start_date')}}" >
                     
                    </div>
                </div>
                <div id="endDate" class="col-md-3">
                    <div class="form-group">
                        <label class="form-label">End Date</label>
                        <input id="end_date" type="date" name="end_date" class="form-control"
                               value="{{ old('end_date') }}" >
                      
                    </div>
                </div>
                <div id="startTime" class="col-md-3">
                    <div class="form-group">
                        <label class="form-label">Start Time</label>
                        <input id="start_time" type="time" name="start_time"  class="form-control timeSet"
                               value="{{old('start_time')}}">
                      
                    </div>
                </div>
                <div id="endTime" class="col-md-3" >
                    <div class="form-group">
                        <label class="form-label">End Time</label>
                        <input id="end_time" type="time" name="end_time" class="form-control timeSet"
                               value="{{old('end_time')}}">
                      
                    </div>
                </div>
</div>

<div class="row">

               
                <div id="Duration" class="col-md-3">
                    <div class="form-group">
                        <label class="form-label">Duration</label>
                        <input id="duration" type="text" name="duration" class="form-control duration"
                               value="{{old('duration')}}">
                      
                    </div>
                </div>
                <div id="particpents" class="col-md-3 particpents" style="display:none">
                    <div class="form-group">
                        <label class="form-label">No.of Participants <span class="text-danger">*</span></label>
                        <input id="particpents" type="text" name="no_of_participents" class="form-control timeSet"
                               value="{{old('no_of_participents')}}" >
                      
                    </div>
                </div>
                <div id="noOfClass" class="col-md-3">
                    <div class="form-group">
                        <label class="form-label">No.of Classes <span class="text-danger">*</span></label>
                        <input id="no_of_class" type="text" name="no_of_class" class="form-control timeSet"
                               value="{{old('no_of_class')}}">
                       
                    </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="form-label">Price(USD) <span class="text-danger">*</span></label>
                    <input id="price" type="text" name="price" class="form-control"
                    value="{{old('price')}}" >
                   
                  </div>
                </div>
</div>

<div class="row">
<div id ="meetingLinks" class="col-md-4 meetingLinks"style="display:none" >
                    <div class="form-group">
                        <label class="form-label">Meeting Link</label>
                        <input id="meeting_link" type="text" name="meeting_link"  class="form-control timeSet"
                               value="">
                       
                    </div>
                </div>
</div>
               
               
              
              <div class="col-12 d-flex justify-content-between">
                <button class="btn btn-primary btn-prev"> <i class="ti ti-arrow-left "></i>
                  <span class="align-middle d-sm-inline-block d-none">Previous</span>
                </button>
                <button class="btn btn-primary btn-skip" id="skip-submit-btn"> <span class="align-right d-sm-inline-block d-none me-sm-1">Skip/Submit</span> <i class="ti ti-arrow-right"></i></button>
                <input type="text" id="liveIn" name="live_class" value="false" hidden/>
                <input type="text" id="live_check_validation" name="live_check_validation" value="false" hidden/>
                <button  id ="final-submit-btn" class="btn btn-success btn-next btn-submit">Submit</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- /Validation Wizard -->

  
</div>
@endsection
<style>
    .dynamic-border {
    /* padding:20px;
    display: inline-block;
    vertical-align: top; */
    /* background: lightblue; */
    /* border: 1px solid #999;
    border-radius: 10px; */
    margin: 10px auto;
   
    /* font-size: 12px; */
}
    </style>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
  <script type="text/javascript">
    
 $( document ).ready(function() {
  
  $('#category').change(function () {
    
        var category_ids = $(this).val();

        $.ajax({
            url: '{{ route('skills') }}',
            type: 'GET',
            data: {
                'category_ids': category_ids
            },
            success: function (response) {
              console.log(response);
                var options = '<option value="">Select one or more skills</option>';

                $.each(response, function (key, skill) {
                    options += '<option value="' + skill.id + '">' + skill.skill_name + '</option>';
                });

                $('#skills').html(options);
            }
        });
    });
  $(document).on('click', '#skip-submit-btn', function(event) {
  
        event.preventDefault();
        var formElement = document.getElementById("wizard-validation-form");
var formData = new FormData(formElement);
      

        $.ajax({
            url: '/admin/tutor/store',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function(response) {
                console.log(response);
                 window.location.href = "http://13.233.6.137/admin/tutor";
            },
            error: function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    });
    $(document).on('click', '#final-submit-btn', function(event) {
      var isValid = document.getElementById("live_check_validation").value;

        event.preventDefault();
        if(isValid =='true'){
  var formElement = document.getElementById("wizard-validation-form");
var formData = new FormData(formElement);


  $.ajax({
      url: '/admin/tutor/store',
      type: 'POST',
      data: formData,
      processData: false,
      contentType: false,
      success: function(response) {
          console.log(response);
        window.location.href = "http://13.233.6.137/admin/tutor";
      },
      error: function(xhr, status, error) {
          console.log(xhr.responseText);
      }
  });


      }
    });
    });      
$( document ).ready(function() {
//profile picture
document.getElementById('profile_image').addEventListener("change", function (e) {


var fileName = document.getElementById("profile_image").value;
  var idxDot = fileName.lastIndexOf(".") + 1;
  var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
  if (extFile=="png" || extFile=="jpg"){
    $('#valid_profile').hide();


  }else{
      alert("Only png/jpg files are allowed!");
      document.getElementById("profile_image").value = null;
      return false;
  }

});


//intro video section
document.getElementById('video_url').addEventListener("change", function (e) {


var fileName = document.getElementById("video_url").value;
  var idxDot = fileName.lastIndexOf(".") + 1;
  var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
  if (extFile=="mp4" || extFile=="webm" || extFile=="mkv"){
    $('#valid_video_url').hide();

$('#submit').attr('disabled','disabled');
  }else{
      alert("Only mp4/webm/mkv  files are allowed!");
      document.getElementById("video_url").value = null;
      return false;
  }


// let file_element =  $('#video_url').val();
var file_element = document.getElementById('video_url');
console.log("files");
console.log(file_element);
var progress_bar = document.getElementById('progress_bar');
var progress_bar_process = document.getElementById('progress_bar_process');

var uploaded_image = document.getElementById('uploaded_image');
var form_data = new FormData();

form_data.append('videoUrl', file_element.files[0]);

form_data.append('_token', document.getElementsByName('_token')[0].value);

progress_bar.style.display = 'block';

var ajax_request = new XMLHttpRequest();

ajax_request.open("POST", "{{ route('profile.StoreVideo') }}");   

ajax_request.upload.addEventListener('progress', function(event){

var percent_completed = Math.round((event.loaded / event.total) * 100);

progress_bar_process.style.width = percent_completed + '%';

progress_bar_process.innerHTML = percent_completed + '% completed';

});
ajax_request.addEventListener('load', function(event){
console.log("response");
var file_data = JSON.parse(event.target.response);

console.log(file_data);

uploaded_image.innerHTML = '<div class="demo-inline-spacing" ><span class="badge bg-label-primary">Files Uploaded Successfully</span></div>';

$('#submit').removeAttr('disabled');

progress_bar.style.display = 'none';
uploaded_image.style.display = 'block';
$('.success-upload').fadeIn().delay(10000).fadeOut();
});

ajax_request.send(form_data);

});
//end intro video section

  //guru video section
     document.getElementById('guru_trailer').addEventListener("change", function (e) {
            var fileName = document.getElementById("guru_trailer").value;
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
        if (extFile=="mp4" || extFile=="webm" || extFile=="mkv"){
          $('#guru_valid_intro_video').hide();
         
$('#submit').attr('disabled','disabled');
        }else{
            alert("Only mp4/mkv/webm  files are allowed!");
            document.getElementById("guru_trailer").value = null;
          return false;
        }



  var file_element = document.getElementById('guru_trailer');
console.log("files");
console.log(file_element);
var progress_bar = document.getElementById('guru_progress_bar_trailer');
var progress_bar_process = document.getElementById('guru_progress_bar_process_trailer');

var uploaded_image = document.getElementById('guru_uploaded_image_trailer');
var form_data = new FormData();

form_data.append('videoUrl', file_element.files[0]);

form_data.append('_token', document.getElementsByName('_token')[0].value);

progress_bar.style.display = 'block';

var ajax_request = new XMLHttpRequest();

ajax_request.open("POST", "{{ route('package.StoreVideo') }}");   

ajax_request.upload.addEventListener('progress', function(event){

  var percent_completed = Math.round((event.loaded / event.total) * 100);

  progress_bar_process.style.width = percent_completed + '%';

  progress_bar_process.innerHTML = percent_completed + '% completed';

});
ajax_request.addEventListener('load', function(event){
console.log("response");
var file_data = JSON.parse(event.target.response);

console.log(file_data);

uploaded_image.innerHTML = '<div class="demo-inline-spacing" ><span class="badge bg-label-primary">Files Uploaded Successfully</span></div>';

$('#submit').removeAttr('disabled');

progress_bar.style.display = 'none';
uploaded_image.style.display = 'block';

$('.success-upload_trailer').fadeIn().delay(10000).fadeOut();
});

ajax_request.send(form_data);
});

document.getElementById('guru_video').addEventListener("change", function (e) {


var fileName = document.getElementById("guru_video").value;

  var idxDot = fileName.lastIndexOf(".") + 1;
  var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
  if (extFile=="mp4" || extFile=="webm" || extFile=="mkv"){
    $('#guru_valid_video_url').hide();

$('#submit').attr('disabled','disabled');
  }else{
      alert("Only mp4/webm/mkv  files are allowed!");
      document.getElementById("guru_video").value = null;
      return false;
  }


// let file_element =  $('#video_url').val();
var file_element = document.getElementById('guru_video');
console.log("files");
console.log(file_element);
var progress_bar = document.getElementById('guru_progress_bar');
var progress_bar_process = document.getElementById('guru_progress_bar_process');

var uploaded_image = document.getElementById('guru_uploaded_image');
var form_data = new FormData();

form_data.append('videoUrl', file_element.files[0]);

form_data.append('_token', document.getElementsByName('_token')[0].value);

progress_bar.style.display = 'block';

var ajax_request = new XMLHttpRequest();

ajax_request.open("POST", "{{ route('package.StoreVideo') }}");   

ajax_request.upload.addEventListener('progress', function(event){

var percent_completed = Math.round((event.loaded / event.total) * 100);

progress_bar_process.style.width = percent_completed + '%';

progress_bar_process.innerHTML = percent_completed + '% completed';

});
ajax_request.addEventListener('load', function(event){
console.log("response");
var file_data = JSON.parse(event.target.response);

console.log(file_data);

uploaded_image.innerHTML = '<div class="demo-inline-spacing" ><span class="badge bg-label-primary">Files Uploaded Successfully</span></div>';

$('#submit').removeAttr('disabled');

progress_bar.style.display = 'none';
uploaded_image.style.display = 'block';
$('.success-upload').fadeIn().delay(10000).fadeOut();
});

ajax_request.send(form_data);

});

//end guru video section

//module video section

document.getElementById('module_trailer').addEventListener("change", function (e) {
            var fileName = document.getElementById("module_trailer").value;
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
        if (extFile=="mp4" || extFile=="webm" || extFile=="mkv"){
          $('#module_valid_intro_video').hide();
         
$('#submit').attr('disabled','disabled');
        }else{
            alert("Only mp4/mkv/webm  files are allowed!");
            document.getElementById("module_trailer").value = null;
            return false;
        }



  var file_element = document.getElementById('module_trailer');
console.log("files");
console.log(file_element);
var progress_bar = document.getElementById('module_progress_bar_trailer');
var progress_bar_process = document.getElementById('module_progress_bar_process_trailer');

var uploaded_image = document.getElementById('module_uploaded_image_trailer');
var form_data = new FormData();

form_data.append('videoUrl', file_element.files[0]);

form_data.append('_token', document.getElementsByName('_token')[0].value);

progress_bar.style.display = 'block';

var ajax_request = new XMLHttpRequest();

ajax_request.open("POST", "{{ route('module.StoreVideo') }}");   

ajax_request.upload.addEventListener('progress', function(event){

  var percent_completed = Math.round((event.loaded / event.total) * 100);

  progress_bar_process.style.width = percent_completed + '%';

  progress_bar_process.innerHTML = percent_completed + '% completed';

});
ajax_request.addEventListener('load', function(event){
console.log("response");
var file_data = JSON.parse(event.target.response);

console.log(file_data);

uploaded_image.innerHTML = '<div class="demo-inline-spacing" ><span class="badge bg-label-primary">Files Uploaded Successfully</span></div>';

$('#submit').removeAttr('disabled');

progress_bar.style.display = 'none';
uploaded_image.style.display = 'block';
$('.success-upload_trailer').fadeIn().delay(10000).fadeOut();
});

ajax_request.send(form_data);
});

document.getElementById('module_video').addEventListener("change", function (e) {


var fileName = document.getElementById("module_video").value;
  var idxDot = fileName.lastIndexOf(".") + 1;
  var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
  if (extFile=="mp4" || extFile=="webm" || extFile=="mkv"){
    $('#module_valid_video_url').hide();

$('#submit').attr('disabled','disabled');
  }else{
      alert("Only mp4/webm/mkv  files are allowed!");
      document.getElementById("module_video").value = null;
      return false;
  }


// let file_element =  $('#video_url').val();
var file_element = document.getElementById('module_video');
console.log("files");
console.log(file_element);
var progress_bar = document.getElementById('module_progress_bar');
var progress_bar_process = document.getElementById('module_progress_bar_process');

var uploaded_image = document.getElementById('module_uploaded_image');
var form_data = new FormData();

form_data.append('videoUrl', file_element.files[0]);

form_data.append('_token', document.getElementsByName('_token')[0].value);

progress_bar.style.display = 'block';

var ajax_request = new XMLHttpRequest();

ajax_request.open("POST", "{{ route('module.StoreVideo') }}");   

ajax_request.upload.addEventListener('progress', function(event){

var percent_completed = Math.round((event.loaded / event.total) * 100);

progress_bar_process.style.width = percent_completed + '%';

progress_bar_process.innerHTML = percent_completed + '% completed';

});
ajax_request.addEventListener('load', function(event){
console.log("response");
var file_data = JSON.parse(event.target.response);

console.log(file_data);

uploaded_image.innerHTML = '<div class="demo-inline-spacing" ><span class="badge bg-label-primary">Files Uploaded Successfully</span></div>';

$('#submit').removeAttr('disabled');

progress_bar.style.display = 'none';
uploaded_image.style.display = 'block';
$('.success-upload').fadeIn().delay(10000).fadeOut();
});

ajax_request.send(form_data);

});


//end module video secion

//start live video section
document.getElementById('live_video').addEventListener("change", function (e) {


var fileName = document.getElementById("live_video").value;
  var idxDot = fileName.lastIndexOf(".") + 1;
  var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
  if (extFile=="mp4" || extFile=="webm" || extFile=="mkv"){
    $('#live_valid_video_url').hide();

$('#submit').attr('disabled','disabled');
  }else{
      alert("Only mp4/webm/mkv  files are allowed!");
      document.getElementById("live_video").value = null;
      return false;
  }


// let file_element =  $('#video_url').val();
var file_element = document.getElementById('live_video');
console.log("files");
console.log(file_element);
var progress_bar = document.getElementById('live_progress_bar');
var progress_bar_process = document.getElementById('live_progress_bar_process');

var uploaded_image = document.getElementById('live_uploaded_image');
var form_data = new FormData();

form_data.append('videoUrl', file_element.files[0]);

form_data.append('_token', document.getElementsByName('_token')[0].value);

progress_bar.style.display = 'block';

var ajax_request = new XMLHttpRequest();

ajax_request.open("POST", "{{ route('live.StoreVideo') }}");   

ajax_request.upload.addEventListener('progress', function(event){

var percent_completed = Math.round((event.loaded / event.total) * 100);

progress_bar_process.style.width = percent_completed + '%';

progress_bar_process.innerHTML = percent_completed + '% completed';

});
ajax_request.addEventListener('load', function(event){
console.log("response");
var file_data = JSON.parse(event.target.response);

console.log(file_data);

uploaded_image.innerHTML = '<div class="demo-inline-spacing" ><span class="badge bg-label-primary">Files Uploaded Successfully</span></div>';

$('#submit').removeAttr('disabled');

progress_bar.style.display = 'none';
uploaded_image.style.display = 'block';
$('.success-upload').fadeIn().delay(10000).fadeOut();
});

ajax_request.send(form_data);

});

//end live video section
});
var count ='';
      function getFileModule(val,getId){
        var a = document.getElementById(getId);
     
    count = getId;
    var fileName = document.getElementById(getId).value;
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
        if (extFile=="mp4" || extFile=="webm" || extFile=="mkv"){
          $('#module_multiplevideo'+count+'').hide();
 
$('#submit').attr('disabled','disabled');
        }else{
            alert("Only mp4/mkv/webm  files are allowed!");
            document.getElementById(getId).value = null;
            return false;
          
        }

  console.log(getId);


// let file_element =  $('#video_url').val();
var file_element = document.getElementById(count);
console.log("files");
console.log(file_element);
var progress_bar = document.getElementById('module_progress_bar'+count+'');
var progress_bar_process = document.getElementById('module_progress_bar_process'+count+'');

var uploaded_image = document.getElementById('module_uploaded_image'+count+'');

console.log("uploaded_image");
console.log(uploaded_image);
var form_data = new FormData();

form_data.append('videoUrl', file_element.files[0]);

form_data.append('_token', document.getElementsByName('_token')[0].value);

progress_bar.style.display = 'block';

var ajax_request = new XMLHttpRequest();

ajax_request.open("POST", "{{ route('module.StoreVideo') }}");   

ajax_request.upload.addEventListener('progress', function(event){

  var percent_completed = Math.round((event.loaded / event.total) * 100);

  progress_bar_process.style.width = percent_completed + '%';

  progress_bar_process.innerHTML = percent_completed + '% completed';

});
ajax_request.addEventListener('load', function(event){
console.log("response");
var file_data = JSON.parse(event.target.response);

console.log(file_data);

uploaded_image.innerHTML = '<div class="demo-inline-spacing" ><span class="badge bg-label-primary">Files Uploaded Successfully</span></div>';

$('#submit').removeAttr('disabled');

progress_bar.style.display = 'none';
uploaded_image.style.display = 'block';
$('#module_uploaded_image'+count+'').fadeIn().delay(10000).fadeOut();
});

ajax_request.send(form_data);
}

var guruCount ='';
      function getFileGuru(val,getId){
        var a = document.getElementById(getId);
     
        guruCount = getId;
    var fileName = document.getElementById(getId).value;
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
        if (extFile=="mp4" || extFile=="webm" || extFile=="mkv"){
          $('#guru_multiplevideo'+guruCount+'').hide();
 
$('#submit').attr('disabled','disabled');
        }else{
            alert("Only mp4/mkv/webm  files are allowed!");
            document.getElementById(getId).value = null;
           
            return false;
        }

  console.log(getId);


// let file_element =  $('#video_url').val();
var file_element = document.getElementById(guruCount);
console.log("files");
console.log(file_element);
var progress_bar = document.getElementById('guru_progress_bar'+guruCount+'');
var progress_bar_process = document.getElementById('guru_progress_bar_process'+guruCount+'');

var uploaded_image = document.getElementById('guru_uploaded_image'+guruCount+'');

console.log("uploaded_image");
console.log(uploaded_image);
var form_data = new FormData();

form_data.append('videoUrl', file_element.files[0]);

form_data.append('_token', document.getElementsByName('_token')[0].value);

progress_bar.style.display = 'block';

var ajax_request = new XMLHttpRequest();

ajax_request.open("POST", "{{ route('package.StoreVideo') }}");   

ajax_request.upload.addEventListener('progress', function(event){

  var percent_completed = Math.round((event.loaded / event.total) * 100);

  progress_bar_process.style.width = percent_completed + '%';

  progress_bar_process.innerHTML = percent_completed + '% completed';

});
ajax_request.addEventListener('load', function(event){
console.log("response");
var file_data = JSON.parse(event.target.response);

console.log(file_data);

uploaded_image.innerHTML = '<div class="demo-inline-spacing" ><span class="badge bg-label-primary">Files Uploaded Successfully</span></div>';

$('#submit').removeAttr('disabled');

progress_bar.style.display = 'none';
uploaded_image.style.display = 'block';
$('#guru_uploaded_image'+guruCount+'').fadeIn().delay(10000).fadeOut();
});

ajax_request.send(form_data);
}
        $( document ).ready(function() {
          $(document).on('click', '#module_add-btn', function() {
      
      moduleAddrow();

      });
    });      
     
      var i = 0;
      function moduleAddrow(){
       
        i++;

        var tr ='<div class="module_sub">'+
        '<div class="col-md-12 col-xl-12 dynamic-border" >'+
    '<div class="card bg-label-primary text-white mb-3">'+
      '<div class="card-body">'+
        '<div class="row">'+
                '<div class="col-md-4">'+
                  '<div class="form-group">'+
                    '<label class="form-label for="module_title[]">Title <span class="text-danger">*</span></label>'+
                    '<input id="module_title" type="text" name="module_title[]" class="form-control"'+
                        'value=""required>'+
                                  '</div>'+
                '</div>'+

                '<div class="col-md-4">'+
                  '<div class="form-group">'+
                    '<label class="form-label" for="module_description[]">Description <span class="text-danger">*</span></label>'+
                    '   <textarea id="module_description"   name="module_description[]" class="form-control"'+
                       ' value="" ></textarea>'+
                                      '</div>'+
                '</div>'+     

                '<div class="col-md-4"  style="display:block">'+
                  '<div class="form-group">'+
                    '<label class="form-label" for="module_video[]">Video <span class="text-danger">*</span></label>'+
                   
                        '<input id="'+i+'" type="file" onchange="return getFileModule(this.value,this.id);" name="module_video[]" class="form-control" value="">'+
                       
                     
                      
                   
                    '</div>'+
                   

                ' <div class="mb-3 col-md-12">'+
                '<div id="module_multiplevideo'+i+'" style="font-size:13px;display:block;"><b>.MP4 .MKV .WEBM(not more than 1 GB)</b></div>'+
                   '</div>'+ 
               
              
                    '<div class="progress" id="module_progress_bar'+i+'" style="display:none;height:20px; line-height: 20px;">'+

         '<div class="progress-bar" id="module_progress_bar_process'+i+'" role="progressbar" style="width:0%;">0%</div>'+

     '</div>'+
        '<div id="module_uploaded_image'+i+'" class="alert  success-upload alert-dismissible fade show row mt-1" role="alert" style="display:none"></div>'+
                  
                  '</div>'+
                  '</div >'+

                  '<div class="card-footer " style="text-align: right;" id="btn_remove'+i+'">'+
       
                  
                  '<label > </label>'+
                  '<button type="button"  rel="tooltip" value="'+i+'" title="Delete"  class="btn btn-danger btn-sm module_remove">'+
                 '<i class="ti ti-trash me-1"></i>'+
                        '</button>'+
                 
                '</div >'+


                
                '</div >'+
                '</div >'+
                '</div >'+

                '</div >'  
                $('#module_newinput').append(tr);
              }
              $(document).on('click', '.module_remove', function(e) {
    e.preventDefault();
    $(this).closest('.module_sub').remove();
});
        
    $(document).on('click', '#guru_add-btn', function() {
      
      guruAddrow();

      });
    
     
      var i = 0;
      function guruAddrow(){
       
        i++;

        var tr ='<div class="guru_sub">'+
        '<div class="col-md-12 col-xl-12 dynamic-border" >'+
    '<div class="card bg-label-primary text-white mb-3">'+
      '<div class="card-body">'+
        '<div class="row">'+
                '<div class="col-md-4">'+
                  '<div class="form-group">'+
                    '<label class="form-label" for="guru_title[]">Title <span class="text-danger">*</span></label>'+
                    '<input id="guru_title" type="text" name="guru_title[]" class="form-control"'+
                        'value="">'+
                                  '</div>'+
                '</div>'+

                '<div class="col-md-4">'+
                  '<div class="form-group">'+
                    '<label class="form-label" for="guru_description[]">Description <span class="text-danger">*</span></label>'+
                    '   <textarea id="guru_description"   name="guru_description[]" class="form-control"'+
                       ' value="" ></textarea>'+
                                      '</div>'+
                '</div>'+     

                '<div class="col-md-4"  style="display:block">'+
                  '<div class="form-group">'+
                    '<label class="form-label" for="guru_video[]">Video <span class="text-danger">*</span></label>'+
                  
                        '<input id="'+i+'" type="file" onchange="return getFileGuru(this.value,this.id);" name="guru_video[]" class="form-control" value="">'+
                   
                      
                   
                    '</div>'+
                    ' <div class="mb-3 col-md-12">'+
                    '<div id="guru_multiplevideo'+i+'" style="font-size:13px;display:block;"><b>.MP4 .MKV .WEBM(not more than 1 GB)</b></div>'+
                    '</div>'+
                                         

                '<div class="progress" id="guru_progress_bar'+i+'" style="display:none;height:20px; line-height: 20px;">'+

'<div class="progress-bar" id="guru_progress_bar_process'+i+'" role="progressbar" style="width:0%;">0%</div>'+

'</div>'+
        '<div id="guru_uploaded_image'+i+'" class="alert  success-upload alert-dismissible fade show row mt-1" role="alert" style="display:none"></div>'+
                  
                  '</div>'+
                  '</div>'+


                
                  '<div class="card-footer " style="text-align: right;" id="btn_remove'+i+'">'+
                 
                  '<label > </label>'+
                  '<button type="button"  rel="tooltip" value="'+i+'" title="Delete"  class="btn btn-danger btn-sm guru_remove">'+
                 '<i class="ti ti-trash me-1"></i>'+
                        '</button>'+
                '</div >'+
                
                
                '</div >'+
                '</div >'+
                '</div >'+

                '</div >'  
                $('#guru_newinput').append(tr);
              }
              $(document).on('click', '.guru_remove', function(e) {
    e.preventDefault();
    $(this).closest('.guru_sub').remove();
});
             
const type =  $('#class_type').val();
            // clearFields();
           if(type == 1){
            // $('#meetingLinks').show();
            $('#particpents').hide();
           }
           else{
            // $('#meetingLinks').hide();
            $('#particpents').show();
           }  
           $( document ).ready(function() {  
      document.getElementById('class_type').addEventListener("change", function (e) {
       
            const type =  $('#class_type').val();
            // clearFields();
           if(type == 1){
            // $('#meetingLinks').show();
            $('#particpents').hide();
           }
           else{
            // $('#meetingLinks').hide();
            $('#particpents').show();
           }
          
          });  

          document.getElementById('live_video').addEventListener("change", function (e) {

var fileName = document.getElementById("live_video").value;
console.log(fileName);
      var idxDot = fileName.lastIndexOf(".") + 1;
      var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
      if (extFile=="mp4" || extFile=="webm" || extFile=="mkv"){
        $('#live_valid_video_url').hide();
$('#submit').attr('disabled','disabled');
var file_element = document.getElementById('live_video');
console.log("files");
console.log(file_element.files[0]);
var progress_bar = document.getElementById('live_progress_bar');
var progress_bar_process = document.getElementById('live_progress_bar_process');

var uploaded_image = document.getElementById('live_uploaded_image');
var form_data = new FormData();

form_data.append('videoUrl', file_element.files[0]);

form_data.append('_token', document.getElementsByName('_token')[0].value);

progress_bar.style.display = 'block';

var ajax_request = new XMLHttpRequest();

ajax_request.open("POST", "{{ route('live.StoreVideo') }}");   

ajax_request.upload.addEventListener('progress', function(event){

  var percent_completed = Math.round((event.loaded / event.total) * 100);

  progress_bar_process.style.width = percent_completed + '%';

  progress_bar_process.innerHTML = percent_completed + '% completed';

});
ajax_request.addEventListener('load', function(event){
console.log("response");
var file_data = JSON.parse(event.target.response);

console.log(file_data);

uploaded_image.innerHTML = '<div class="demo-inline-spacing" ><span class="badge bg-label-primary">Files Uploaded Successfully</span></div>';

$('#submit').removeAttr('disabled');

progress_bar.style.display = 'none';
uploaded_image.style.display = 'block';
$('.live_success-upload').fadeIn().delay(10000).fadeOut();
});

ajax_request.send(form_data);
}
else{
          alert("Only mp4/webm/mkv  files are allowed!");
          document.getElementById("live_video").value = null;
        return false;
      }
});





});     

    </script>

