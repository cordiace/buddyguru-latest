@php
    $formAction = route('tutor.portfolio.update',$userPortfolio->id);
  
@endphp
@extends('layouts.dashboard')

@section('content')
@include('partials.nav')


<div class="content">
  <div class="container-fluid">
    <div class="row d-flex justify-content-center">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
                  Edit Portfolio
            </h4>
            <p class="card-category">Manage guru portfolio</p>
          </div>
          @if (session()->has('message'))
    <div class="col-sm-12">
        <div class="alert  alert-success alert-dismissible fade show" role="alert">
        {{ session()->get('message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
    </div>
@endif
          <div class="card-body">
            <form action="{{ $formAction }}" id="form" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @csrf
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                  <input id="user_id" type="hidden" name="user_id" class="form-control" value="{{$userPortfolio->user_id}}">
                    <label class="bmd-label-floating">Title</label>
                    <input id="title" type="text" name="title" class="form-control"
                        value="{{$userPortfolio->title}}" required>
                    @error ('title')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Description</label>
                    <input id="description" type="text" name="description" class="form-control"
                    value="{{$userPortfolio->description}}" required>
                    @error('description')
                   
                    <p class="text-danger">{{ $message }}</p>
                       
                    @enderror
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Video</label>
                    <input id="video_url" type="file" name="video_url" class="form-control"
                    value="" >
                    @error('video_url')
                    <p class="text-danger">Required and size less than 1GB</p>
                    @enderror
                    <a href="" class="view-portfolio" id="view-portfolio" data-video_url="{{asset($userPortfolio->video_url)}}">
                      <button type="button" rel="tooltip" title="Play Video" class="btn btn-primary btn-link btn-sm">
                        <i class="material-icons">play_arrow</i>
                      </button>
                  </a>
                  </div>
                  <button type="button" id= "upload" style="display:none;"class=" btn btn-primary btn-sm pull-right"  rel="tooltip" title="upload video" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">upload</i>
</button>
              
                        <div class="progress" id="progress_bar" style="display:none;height:20px; line-height: 20px;">

            <div class="progress-bar" id="progress_bar_process" role="progressbar" style="width:0%;">0%</div>

        </div>

        <div id="uploaded_image" class="alert  success-upload alert-dismissible fade show row mt-1" role="alert">  </div>

       
                </div>
                

              </div>

              <!-- @if($ShowNext =='true')
              <button id ="submit" type="submit" name="submit_button" class="btn btn-primary btn-sm pull-right">Next</button>
              @else  -->
              <button id ="submit" type="submit" name="submit_button" class="btn btn-primary btn-sm pull-right">submit</button>
             <!-- @endif -->
                 
                
                   
           
            </form>
         
          </div>
        </div>
      </div>
    </div>
   

    <div id="playModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">View Video</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body" id="divVideo">
            <video width="320" height="240" controls>
              <source src="" type="video/mp4">
              Your browser does not support the video tag.
            </video>
          </div>

        </div>

      </div>
    </div>
   
  </div>
</div>
@endsection

@section('custom-scripts')

  <script type="text/javascript">
       window.addEventListener("DOMContentLoaded", function(e) {

var form_being_submitted = false;

var checkForm = function(e) {
  var form = e.target;
  if(form_being_submitted) {
    alert("The form is being submitted, please wait a moment...");
    form.submit_button.disabled = true;
    e.preventDefault();
    return;
  }
  form.submit_button.value = "Submitting form...";
      form_being_submitted = true;
    };
    document.getElementById("form").addEventListener("submit", checkForm, false);

  }, false);
      $( document ).ready(function() {
        $( ".view-portfolio" ).on( "click", function(e) {
          e.preventDefault();


          $('source').attr('src',$(this).attr('data-video_url'));
          $("#divVideo video")[0].load();
          $('#playModal').modal('show');
        });
      

        document.getElementById('video_url').addEventListener("change", function (e) {

$('#upload').show();
$('#view-portfolio').hide();
$('#submit').attr('disabled','disabled');
});

$(document).on('click', '#upload', function() {

$('#upload').hide();
// let file_element =  $('#video_url').val();
var file_element = document.getElementById('video_url');
var progress_bar = document.getElementById('progress_bar');
var progress_bar_process = document.getElementById('progress_bar_process');

var uploaded_image = document.getElementById('uploaded_image');
var form_data = new FormData();

form_data.append('videoUrl', file_element.files[0]);

form_data.append('_token', document.getElementsByName('_token')[0].value);

progress_bar.style.display = 'block';

var ajax_request = new XMLHttpRequest();

ajax_request.open("POST", "{{ route('portfolio.StoreVideo',$userPortfolio->user_id) }}");   

ajax_request.upload.addEventListener('progress', function(event){

  var percent_completed = Math.round((event.loaded / event.total) * 100);

  progress_bar_process.style.width = percent_completed + '%';

  progress_bar_process.innerHTML = percent_completed + '% completed';

});
ajax_request.addEventListener('load', function(event){
console.log("response");
var file_data = JSON.parse(event.target.response);

console.log(file_data);

uploaded_image.innerHTML = '<div class="alert alert-success">Files Uploaded Successfully</div>';

$('#submit').removeAttr('disabled');

progress_bar.style.display = 'none';
$('.success-upload').fadeIn().delay(10000).fadeOut();
});

ajax_request.send(form_data);

});

      });

  
  </script>
@endsection
