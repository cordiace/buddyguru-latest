@extends('layouts.dashboard')

@section('content')
    @include('partials.nav')
    <div class="content">
        <div class="container-fluid">
            <div class="row d-flex justify-content-center">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header card-header-info">
                            <h4 class="card-title">
                                Guru Profile
                            </h4>
                            <p class="card-category">Guru Profile Details</p>
                        </div>
                      
       

            <div class="col-md-12 justify-content-md-center"  d-flex align-items-stretch flex-column " style=" margin-left: 170px!important; padding-right: 344px;">
              <div class="card bg-light d-flex flex-fill">
                <div class="card-header text-muted border-bottom-0">

                            <!-- <div class="row justify-content-md-center">
                                <div class="col-md-3">
                                @if(!empty($user->profile_image)) 
                                    <div class="form-group">
                                        <img src="{{ asset($user->profile_image) }}" class="img-thumbnail" />
                                    </div>
                                    @else
                                    <div class="form-group">
                                       
                                    </div>
                                    @endif
                                </div>
                            </div> -->
                            <!-- <div class="col-5 text-center"> -->
                            <div class="card-body box-profile">
                            <div class="row ">
                <img  alt="User profile picture" class="profile-user-img img-fluid img-circle" src="{{ asset($user->profile_image) }}" class="img-thumbnail" />
</div>
                </div>
<!-- </div> -->
                            <div class="row justify-content-md-center">
        
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Name</label><br>
                                        {{ $user->name }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Email</label><br>
                                        {{ $user->email }}
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-md-center" >
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Phone Number</label><br>
                                        {{ $user->phone_number }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Country</label><br>
                                        {{ $user->country->country_name }}
                                    </div>
                                </div>
                            </div>

                            <div class="row justify-content-md-center" >
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Skill</label><br>
                                        @foreach ($TutorSkills as $TutorSkill)
                                        <!-- {{ $user->tutor_detail->getCategory->category_name}} -->
                                        {{$TutorSkill->CategorySkills->skill_name}}
                                        @endforeach
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Date of birth</label><br>
                                        <!-- {{ substr($user->dob,0,-9)}} -->
                                        {{ $user->dob->format('j F, Y')}}
                                    </div>
                                </div>
</div>
<div class="row justify-content-md-center"  >
                                <div class="col-md-4">
                                    <div class="form-group">
                                    <label class="bmd-label-floating">Video</label><br>
<a href="" class="view-portfolio" data-video_url="{{asset($TutorDetail->video_url)}}">
                      <button type="button" rel="tooltip" title="Play Video" class="btn btn-primary btn-link btn-sm">
                        <i class="material-icons">play_arrow</i>
                      </button>
                    </a>
</div>
</div>
<div class="col-md-4">
                                    <div class="form-group">
</div>
</div>
</div>
<!-- <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Cover Letter</label><br>
                                        @isset($TutorDetail)
                    @if(!empty($TutorDetail->cover_letter)) 
                                    <div class="form-group">
                                        <a href="{{  asset($TutorDetail->cover_letter) }}" target="_blank" class="img-thumbnail">view cover letter</a>
                                    </div>
                                    @else
                                    <div class="form-group">
                                       
                                    </div>
                                    @endif
                    @endif
                                    </div>
                                </div> -->
                                <div class="card-footer">
                                <div class="row col-12 justify-content-md-center">
                                <div class="form-group mx-auto">
                              
                                <a href="{{ route('tutor.index') }}" >
                        <button type="button" rel="tooltip" title="Back to list" class="btn btn-primary btn-link btn-sm" style=" background-color: #0c82be !important;
    border-color: #0c82be !important;">
                        <i class="material-icons">
keyboard_return</i> back
                  </button></a>
 

                      <a href="{{ route('admin.tutor.edit', ['id' =>$id,'showNext'=>'false']) }}">
                        <button type="button" rel="tooltip" title="Edit Guru" class="btn btn-primary btn-link btn-sm" style=" background-color: #0c82be !important;
    border-color: #0c82be !important;">
                          <i class="material-icons">edit</i>edit
                        </button>
                      </a>

                      <!-- <a href="{{ route('tutor.portfolio.create',  ['id' =>$id,'showNext'=>'false']) }}">
                        <button type="button" rel="tooltip" title="Manage Intro Video" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">play_circle</i>
                        </button>
                      </a> -->
                     
                      <a href="{{ route('tutor.package.view', $id) }}">
                        <button type="button" rel="tooltip" title="Manage Guru Class" class="btn btn-primary btn-link btn-sm" style=" background-color: #0c82be !important;
    border-color: #0c82be !important;">
                          <i class="material-icons">school</i> Guru
                        </button>
                      </a>

                      <a href="{{ route('tutor.module.view',$id) }}">
                        <button type="button" rel="tooltip" title="Manage Module Class" class="btn btn-primary btn-link btn-sm" style=" background-color: #0c82be !important;
    border-color: #0c82be !important;">
                          <i class="material-icons">hotel_class</i> Module
                        </button>
                      </a>


                      <a href="{{ route('tutor.live_class.view',$id) }}">
                        <button type="button" rel="tooltip" title="Manage Live Class" class="btn btn-primary btn-link btn-sm" style=" background-color: #0c82be !important;
    border-color: #0c82be !important;">
                          <i class="material-icons">live_tv</i> Live
                        </button>
                      </a>

</div>
</div>
</div>

</div>
<div>

                                {{-- <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Sub Category</label><br>
                                        {{ $user->tutor_detail->getSubCategory->sub_category_name}}
                                    </div>
                                </div> --}}
                            </div>

                            {{-- <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Skills</label><br>
                                        {{ $user->tutor_detail->getSkill->skill_name}}
                                    </div>
                                </div>


                            </div> --}}

                            {{-- <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">About Us</label><br>
                                        {{ $user->tutor_detail->about}}
                                    </div>
                                </div>


                            </div> --}}

                            {{-- <div class="row">
                                <div class="col-md-12">
                                    <label class="bmd-label-floating">Experience</label>
                                    <div class="form-group add_another">
                                        @isset($user->tutor_detail->experience)
                                            @php
                                                $exp = json_decode($user->tutor_detail->experience, true);
                                            @endphp
                                            @foreach ($exp as $experience)
                                                <div class="row">
                                                    <div class="col-md-5">
                                                            <label class="">Name</label><br>
                                                            {{ $experience['name'] }}
                                                    </div>
                                                    <div class="col-md-5">
                                                       <label class="">Designation</label><br>
                                                        {{ $experience['designation'] }}
                                                    </div>

                                                    
                                                </div>
                                            @endforeach
                                        @endisset
                                        
                                    </div>
                                </div>
                            </div> --}}




                            <div class="clearfix"></div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div id="playModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">View Video</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body" id="divVideo">
            <video width="320" height="240" controls>
              <source src="" type="video/mp4">
              Your browser does not support the video tag.
            </video>
          </div>

        </div>

      </div>
    </div>
@endsection
@section('custom-scripts')
<script type="text/javascript">
 $( document ).ready(function() {
    $( ".view-portfolio" ).on( "click", function(e) {
          e.preventDefault();
          $('source').attr('src',$(this).attr('data-video_url'));
         
          $("#divVideo video")[0].load();
        
          $('#playModal').modal('show');
        

        });
 });
    </script>
@endsection