@extends('layouts/layoutMaster')

@section('content')
<h4 class="fw-bold py-3 mb-4">
  <span class="text-muted fw-light">Tutor / View /</span> Module
</h4>
    <div class="content">
        <div class="container-fluid">
            <div class="row d-flex justify-content-center">
                <div class="col-lg-12">
                <!-- <div class="row">
                <div class="col-md-12">
         
      <a href="{{ route('tutor.show', $package->user_id) }}">
                        <button type="button"  class="btn btn-primary btn-link btn-sm">
                          Back
                  </button></a>
</div>
</div> -->

                    <div class="card">
                        <div class="card-header card-header-info">
                            <h4 class="card-title">
                               Module Class
                            </h4>
                            <p class="card-category">Module Class Details</p>
                        </div>

                        <div class="col-md-12   d-flex align-items-stretch flex-column " style=" margin-left: 143px!important; padding-right: 344px;">
              <div class="card bg-light d-flex flex-fill">
                <div class="card-header text-muted border-bottom-0">
                        <!-- <div class="card-body"> -->
                        <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Intro Title</label><br>
                                        {{ $package->intro_title }}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Intro Description</label><br>
                                        {{ $package->intro_description }}
                                    </div>
                                </div>
                            </div>
                        <div class="row">
                        <div class="col-md-6">
                                    <div class="form-group">
                                    <label class="bmd-label-floating">Trailer</label><br>
<a href="" class="view-portfolio" data-video_url="{{asset($package->video_url)}}">
                      <button type="button" rel="tooltip" title="Play Video" class="btn btn-primary btn-link btn-sm">
                        <i class="material-icons">play_arrow</i>
                      </button>
                    </a>
</div>
</div>

                        <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Total Price</label><br>
                                        USD ${{ $package->total_price }}
                                    </div>
                                </div>

</div>
<div class=" parent ">

                        @foreach($packageDetails as $packageDetail)
                     
                        <div class=" col-md-6 view-border ">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Title</label><br>
                                        {{ $packageDetail->title }}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Description</label><br>
                                        {{ $packageDetail->description }}
                                    </div>
                                </div>
                                
                            </div>
                            
                            <div class="row">
        
<div class="col-md-6">
                                    <div class="form-group">
                                    <label class="bmd-label-floating">Video</label><br>
<a href="" class="view-portfolio" data-video_url="{{asset($packageDetail->video_url)}}">
                      <button type="button" rel="tooltip" title="Play Video" class="btn btn-primary btn-link btn-sm">
                        <i class="material-icons">play_arrow</i>
                      </button>
                    </a>
</div>
</div>
</div>
                        
</div>

                      
                        @endforeach
                        </div>                

</div>
</div>
                        <!-- <div class="row">
          <div class="col-md-12">
          <div class="form-group">
      <a href="{{ route('tutor.show', $package->user_id) }}">
                        <button type="button"  class="btn btn-primary btn-link btn-sm">
                          Back
                  </button></a>
</div>
</div>
</div> -->
</div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div id="playModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">View Video</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body" id="divVideo">
            <video width="320" height="240" controls>
              <source src="" type="video/mp4">
              Your browser does not support the video tag.
            </video>
          </div>

        </div>

      </div>
    </div>
@endsection
@section('custom-scripts')
<script type="text/javascript">
 $( document ).ready(function() {
    $( ".view-portfolio" ).on( "click", function(e) {
          e.preventDefault();


          $('source').attr('src',$(this).attr('data-video_url'));
          $("#divVideo video")[0].load();
          $('#playModal').modal('show');

        });
 });
    </script>
@endsection