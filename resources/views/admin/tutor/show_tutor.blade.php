@php
$issetpackage = count($guruPackages) >0 ? 1 : 0;
@endphp
@extends('layouts/layoutMaster')

@section('title', 'Tutor View ')


@section('content')
<h4 class="fw-bold py-3 mb-4">
  <span class="text-muted fw-light">Tutor / View /</span> Profile
</h4>
<div class="row">
  <!-- User Sidebar -->
  <div class="col-xl-4 col-lg-5 col-md-5 order-1 order-md-0">
    <!-- User Card -->
    <div class="card mb-4">
      <div class="card-body">
        <div class="user-avatar-section">
          <div class=" d-flex align-items-center flex-column">
            <img class="img-fluid rounded mb-3 pt-1 mt-4" src="{{ ($user->profile_image)  ?  asset($user->profile_image)  : asset('assets/img/avatars/avatar.png') }}" height="100" width="100" alt="User avatar" />
            <div class="user-info text-center">
              <h4 class="mb-2">{{$user->name}}</h4>
              <span class="badge bg-label-secondary mt-1">Tutor</span>
            </div>
          </div>
        </div>
        <div class="d-flex justify-content-around flex-wrap mt-3 pt-3 pb-4 border-bottom">
          <div class="d-flex align-items-start me-4 mt-3 gap-2">
         Intro Video
            <div>
              <p class="mb-0 fw-semibold"> <a href="" class="view-portfolio" data-video_url="{{asset($TutorDetail->video_url)}}">
                      <button type="button" rel="tooltip" title="Play Video" class="btn btn-primary btn-link btn-sm">
                      <i class='fa fa-play' aria-hidden='true'></i>
                      </button>
                    </a></p>
              
            </div>
          </div>
          <div class="d-flex align-items-start mt-3 gap-2">
           Description
            <div>
              <p class="mb-0 fw-semibold"> {{Str::limit($user->description,10)}}</p>
              <small></small>
            </div>
          </div>
        </div>
        <p class="mt-4 small text-uppercase text-muted">Details</p>
        <div class="info-container">
          <ul class="list-unstyled">
            <li class="mb-2">
              <span class="fw-semibold me-1">Name:</span>
              <span>{{$user->name}}</span>
            </li>
            <li class="mb-2 pt-1">
              <span class="fw-semibold me-1">Email:</span>
              <span>{{$user->email}}</span>
            </li>
            <li class="mb-2 pt-1">
              <span class="fw-semibold me-1">Skill:</span>
              @foreach ($TutorSkills as $TutorSkill)
                                      
              <span class="badge bg-label-success">{{$TutorSkill->CategorySkills->skill_name}}</span>
                                        @endforeach
            
            </li>
            <li class="mb-2 pt-1">
              <span class="fw-semibold me-1">Role:</span>
              <span>Admin</span>
            </li>
            <li class="mb-2 pt-1">
              <span class="fw-semibold me-1">Country:</span>
              <span>  {{ $user->country->country_name }}</span>
            </li>
            <li class="mb-2 pt-1">
              <span class="fw-semibold me-1">Contact:</span>
              <span>{{$user->phone_number}}</span>
            </li>
            <li class="mb-2 pt-1">
              <span class="fw-semibold me-1">DOB:</span>
              <span> {{ Carbon\Carbon::parse($user->dob)->format('j F, Y') }}</span>
            </li>
            <li class="pt-1">
              <span class="fw-semibold me-1">Commision:</span>
              <span>{{$user->commision}}</span>
            </li>
          </ul>
          <div class="d-flex justify-content-center">
            <a href="{{ route('admin.tutor.edit', $user->id) }}" class="btn btn-primary me-3" >Edit</a>
            <!-- <a href="javascript:;" class="btn btn-label-danger suspend-user">Suspended</a> -->
          </div>
        </div>
      </div>
    </div>
    <!-- /User Card -->
   
  </div>
  <!--/ User Sidebar -->


  <div class="col-xl-8 col-lg-7 col-md-7 order-0 order-md-1">
    <!-- <h6 class="text-muted">Classes</h6> -->
    <div class="nav-align-top mb-4">
      <ul class="nav nav-pills mb-3 nav-fill" role="tablist">
        <li class="nav-item">
          <button type="button" class="nav-link active" role="tab" data-bs-toggle="tab" data-bs-target="#guru-class" aria-controls="navs-pills-justified-home" aria-selected="true"><i class="tf-icons ti ti-home ti-xs me-1"></i> Guru Class</button>
        </li>
        <li class="nav-item">
          <button type="button" class="nav-link" role="tab" data-bs-toggle="tab" data-bs-target="#module-class" aria-controls="navs-pills-justified-profile" aria-selected="false"><i class="tf-icons ti ti-user ti-xs me-1"></i> Module Class</button>
        </li>
        <li class="nav-item">
          <button type="button" class="nav-link" role="tab" data-bs-toggle="tab" data-bs-target="#live-class" aria-controls="navs-pills-justified-messages" aria-selected="false"><i class="fa fa-video-camera ti-xs me-1" aria-hidden="true"></i>Live Class</button>
        </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane fade show active" id="guru-class" role="tabpanel">
        @if($issetpackage ==0)
        <div class="card-header card-header-info">
<div class="dt-buttons btn-group flex-wrap">
     <div class="btn-group">
     </div>
     <a  href="{{route('tutor.package.create',$id)}}">
       <button class="btn btn-secondary add-new btn-primary" tabindex="0"
        aria-controls="DataTables_Table_0" type="button">
        <span><i class="ti ti-plus me-0 me-sm-1 ti-xs"></i>
        <span class="d-none d-sm-inline-block">Add Guru Class</span>
      </span></button></a>
     </div>
</div>
@endisset
  <div class="card-datatable table-responsive">
 
    <table id="guru" class="datatables-users table border-top">
    
        <thead>
            <tr>
            <th>ID</th>
                  <th>Intro Title</th>
                  <th>Intro Description</th>
                  <th >Action</th>
            </tr>
        </thead>
        <tbody>
           
        @isset($guruPackages)
                  @foreach($guruPackages as $userPackage)
                  <tr> 
                    <td>{{$loop->index + 1}}</td>
                    <td>{{$userPackage->intro_title}}</td>

                      <td>{{Str::limit($userPackage->intro_description,40)}} </td>
                <td>
                <div class="d-flex align-items-center">
            <div class="dropdown">
              <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown"><i class="ti ti-dots-vertical"></i></button>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="{{ route('tutor.package.edit', $userPackage->id) }}"><i class="ti ti-pencil me-1"></i> Edit</a>
                <a class="dropdown-item" href="{{ route('tutor.package.delete', $userPackage->id) }}"><i class="ti ti-trash me-1"></i> Delete</a>
              </div>
            </div>
            <a style="cursor: pointer;" class="text-body viewdetails" data-id='{{ $userPackage->id }}'><i class="ti ti-eye ti-sm me-2"></i></a>
</div>
           
          </td>
            </tr>
            @endforeach
                  @endisset
        </tbody>
       
    </table>
</div>
        </div>
        <div class="tab-pane fade" id="module-class" role="tabpanel">
        <div class="card-header card-header-info">
<div class="dt-buttons btn-group flex-wrap">
     <div class="btn-group">
     </div>
     <a  href="{{route('tutor.module.create',$id)}}">
       <button class="btn btn-secondary add-new btn-primary" tabindex="0"
        aria-controls="DataTables_Table_0" type="button">
        <span><i class="ti ti-plus me-0 me-sm-1 ti-xs"></i>
        <span class="d-none d-sm-inline-block">Add Module Class</span>
      </span></button></a>
     </div>
</div>
  <div class="card-datatable table-responsive">
 
    <table id="module" class="datatables-users table border-top">
    
        <thead>
            <tr>
            <th>ID</th>
                  <th>Intro Title</th>
                  <th>Intro Description</th>
                  <th >Action</th>
            </tr>
        </thead>
        <tbody>
           
        @isset($modulePackages)
                  @foreach($modulePackages as $userPackage)
                  <tr> 
                    <td>{{$loop->index + 1}}</td>
                    <td>{{$userPackage->intro_title}}</td>

                      <td>{{Str::limit($userPackage->intro_description,40)}} </td>
                <td>
                <div class="d-flex align-items-center">
            <div class="dropdown">
              <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown"><i class="ti ti-dots-vertical"></i></button>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="{{ route('tutor.module.edit', $userPackage->id) }}"><i class="ti ti-pencil me-1"></i> Edit</a>
                <a class="dropdown-item" href="{{ route('tutor.module.delete', $userPackage->id) }}"><i class="ti ti-trash me-1"></i> Delete</a>
              </div>
            </div>
            <a style="cursor: pointer;" class="text-body viewdetails" data-id='{{ $userPackage->id }}'><i class="ti ti-eye ti-sm me-2"></i></a>
</div>
           
          </td>
            </tr>
            @endforeach
                  @endisset
        </tbody>
       
    </table>
</div>
        </div>
        <div class="tab-pane fade" id="live-class" role="tabpanel">
        <div class="card-header card-header-info">
<div class="dt-buttons btn-group flex-wrap">
     <div class="btn-group">
     </div>
     <a  href="{{route('tutor.live_class.create',$id)}}">
       <button class="btn btn-secondary add-new btn-primary" tabindex="0"
        aria-controls="DataTables_Table_0" type="button">
        <span><i class="ti ti-plus me-0 me-sm-1 ti-xs"></i>
        <span class="d-none d-sm-inline-block">Add Live Class</span>
      </span></button></a>
     </div>
</div>
  <div class="card-datatable table-responsive">
 
    <table id="live" class="datatables-users table border-top">
    
    <thead>
            <tr>
            <th>ID</th>
                  <th>Intro Title</th>
                  <th>Intro Description</th>
                  <th >Action</th>
            </tr>
        </thead>
        <tbody>
           
        @isset($LiveClassPackages)
                  @foreach($LiveClassPackages as $userPackage)
                  <tr> 
                    <td>{{$loop->index + 1}}</td>
                    <td>{{$userPackage->intro_title}}</td>

                      <td>{{Str::limit($userPackage->intro_description,40)}} </td>
                <td>
                <div class="d-flex align-items-center">
            <div class="dropdown">
              <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown"><i class="ti ti-dots-vertical"></i></button>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="{{ route('tutor.live_class.edit', $userPackage->id) }}"><i class="ti ti-pencil me-1"></i> Edit</a>
                <a class="dropdown-item" href="{{ route('tutor.live_class.delete', $userPackage->id) }}"><i class="ti ti-trash me-1"></i> Delete</a>
              </div>
            </div>
            <a style="cursor: pointer;" class="text-body viewdetails" data-id='{{ $userPackage->id }}'><i class="ti ti-eye ti-sm me-2"></i></a>
</div>
           
          </td>
            </tr>
            @endforeach
                  @endisset
        </tbody>
       
    </table>
</div>
        </div>
      </div>
    </div>
  </div>

<!-- Modal -->
<div class="modal fade" id="guruModal" >
         <div class="modal-dialog ">

            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title">Guru Class Info</h4>
                  <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
               </div>
               <div class="modal-body">
                   <table class="w-100" id="tblempinfo">
                      <tbody></tbody>
                   </table>
               </div>
               <div class="modal-footer">
                   <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>

      <div class="modal fade" id="moduleModal" >
         <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title">Module Class Info</h4>
                  <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
               </div>
               <div class="modal-body">
                   <table class="w-100" id="tblempinfo">
                      <tbody></tbody>
                   </table>
               </div>
               <div class="modal-footer">
                   <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>

      <div class="modal fade" id="liveModal" >
         <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title">Live Class Info</h4>
                  <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
               </div>
               <div class="modal-body">
                   <table class="w-100" id="tblempinfo">
                      <tbody></tbody>
                   </table>
               </div>
               <div class="modal-footer">
                   <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>

      <div id="playModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">View Video</h4>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body" id="divVideo">
            <video width="320" height="240" controls>
              <source src="" type="video/mp4">
              Your browser does not support the video tag.
            </video>
          </div>

        </div>

      </div>
    </div>

    <div id="modalToggle2" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">View Video</h4>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body" id="divVideo">
            <video width="320" height="240" controls>
              <source src="" type="video/mp4">
              Your browser does not support the video tag.
            </video>
          </div>
          <div class="modal-footer">
                  <button class="btn btn-primary" data-bs-target="#guruModal" data-bs-toggle="modal" data-bs-dismiss="modal">Back </button>
                </div>
        </div>

      </div>
    </div>
      
    <div id="modalToggleModule2" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">View Video</h4>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body" id="divVideo">
            <video width="320" height="240" controls>
              <source src="" type="video/mp4">
              Your browser does not support the video tag.
            </video>
          </div>
          <div class="modal-footer">
                  <button class="btn btn-primary" data-bs-target="#moduleModal" data-bs-toggle="modal" data-bs-dismiss="modal">Back </button>
                </div>
        </div>

      </div>
    </div>

    <div id="modalToggleLive2" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">View Video</h4>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body" id="divVideo">
            <video width="320" height="240" controls>
              <source src="" type="video/mp4">
              Your browser does not support the video tag.
            </video>
          </div>
          <div class="modal-footer">
                  <button class="btn btn-primary" data-bs-target="#liveModal" data-bs-toggle="modal" data-bs-dismiss="modal">Back </button>
                </div>
        </div>

      </div>
    </div>
@endsection

<style>
    .dynamic-border {
    padding:20px;
    display: inline-block;
    vertical-align: top;
    /* background: lightblue; */
    border: 1px solid #999;
    border-radius: 10px;
    margin: 10px auto;
   
    font-size: 12px;
}
    </style>

<!-- Script -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

<script type='text/javascript'>
  function Myfun(element){
    // event.preventDefault();
    console.log("this:", element);
    var videoUrl = $(element).attr("data-video_url");
    console.log(videoUrl);
  if (videoUrl) {
    $('source').attr('src', videoUrl);
          // $('source').attr('src',$(this).attr('data-video_url'));
          $("#divVideo video")[0].load();
       
  }
  else {
    console.error("Missing or invalid video URL");
  }
  }
   $(document).ready(function(){
   
   
    $( ".view-portfolio" ).on( "click", function(e) {
    
          e.preventDefault();
          console.log("this:", this);
          $('source').attr('src',$(this).attr('data-video_url'));
          $("#divVideo video")[0].load();

          $('#playModal').modal('show');

        });
        $('#guru').on('click','.viewdetails',function(e){
       
       var empid = $(this).attr('data-id');

       if(empid > 0){
         $.ajaxSetup({
         headers: {
             'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
         }
     });
     e.preventDefault();
          // AJAX request
          var url = "{{ route('tutor.guru.show',[':empid']) }}";
          url = url.replace(':empid',empid);

          // Empty modal data
          $('#tblempinfo tbody').empty();

          $.ajax({
              url: url,
              type: "GET",
            
              success: (result) => {
        // Add employee details
                  $('#tblempinfo tbody').html(result.html);

                  // Display Modal
                  $('#guruModal').modal('show'); 
     },
               
          });
       }
   });
      $('#module').on('click','.viewdetails',function(e){
       
          var empid = $(this).attr('data-id');

          if(empid > 0){
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });
        e.preventDefault();
             // AJAX request
             var url = "{{ route('tutor.module.show',[':empid']) }}";
             url = url.replace(':empid',empid);

             // Empty modal data
             $('#tblempinfo tbody').empty();

             $.ajax({
                 url: url,
                 type: "GET",
               
                 success: (result) => {
           // Add employee details
                     $('#tblempinfo tbody').html(result.html);

                     // Display Modal
                     $('#moduleModal').modal('show'); 
        },
                
             });
          }
      });

      $('#live').on('click','.viewdetails',function(e){
       
       var empid = $(this).attr('data-id');

       if(empid > 0){
         $.ajaxSetup({
         headers: {
             'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
         }
     });
     e.preventDefault();
          // AJAX request
          var url = "{{ route('tutor.live_class.show',[':empid']) }}";
          url = url.replace(':empid',empid);

          // Empty modal data
          $('#tblempinfo tbody').empty();

          $.ajax({
              url: url,
              type: "GET",
            
              success: (result) => {
        // Add employee details
                  $('#tblempinfo tbody').html(result.html);

                  // Display Modal
                  $('#liveModal').modal('show'); 
     },
             
          });
       }
   });

   });

  </script>