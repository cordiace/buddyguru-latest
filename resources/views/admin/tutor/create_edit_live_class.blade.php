@php
$issetUser = isset($user) ? 1 : 0;
$issetpackageDetails =count($LiveClassDetails) >0 ? 1 : 0;
$formAction = route('tutor.live_class.store',$id);
@endphp
@extends('layouts/layoutMaster')

@section('vendor-style')
<link rel="stylesheet" href="{{asset('assets/vendor/libs/bootstrap-select/bootstrap-select.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/select2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/flatpickr/flatpickr.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/typeahead-js/typeahead.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/tagify/tagify.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/formvalidation/dist/css/formValidation.min.css')}}" />
@endsection

@section('vendor-script')
<script src="{{asset('assets/vendor/libs/select2/select2.js')}}"></script>
<script src="{{asset('assets/vendor/libs/bootstrap-select/bootstrap-select.js')}}"></script>
<script src="{{asset('assets/vendor/libs/moment/moment.js')}}"></script>
<script src="{{asset('assets/vendor/libs/flatpickr/flatpickr.js')}}"></script>
<script src="{{asset('assets/vendor/libs/typeahead-js/typeahead.js')}}"></script>
<script src="{{asset('assets/vendor/libs/tagify/tagify.js')}}"></script>
<script src="{{asset('assets/vendor/libs/formvalidation/dist/js/FormValidation.min.js')}}"></script>
<script src="{{asset('assets/vendor/libs/formvalidation/dist/js/plugins/Bootstrap5.min.js')}}"></script>
<script src="{{asset('assets/vendor/libs/formvalidation/dist/js/plugins/AutoFocus.min.js')}}"></script>
@endsection

@section('page-script')
<script src="{{asset('assets/js/live-class/form-validation.js')}}"></script>
@endsection

@section('content')
<h4 class="fw-bold py-3 mb-4">
  <span class="text-muted fw-light">Live Class /</span> Add
</h4>
<div class="content">
  <div class="container-fluid">
    <div class="row d-flex justify-content-center">
      <div class="col-lg-12">
     
        <div class="card">
          <div class="card-header card-header-info">
            <h4 class="card-title">
                  Live Class
            </h4>
            <p class="card-category">Manage Live Class</p>
          </div>
          @if (session()->has('message'))
          <div class="col-lg-3 " style="margin: 0 auto;">
        <div class="alert  alert-success alert-dismissible fade show" role="alert">
        {{ session()->get('message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
    </div>
@endif
          <div class="card-body">
            <form novalidate action="{{ $formAction }}" class="form" id="form" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @csrf
              <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                    <!-- <label class="bmd-label-floating">Select Class</label> -->
                    
                    <select id="batch_id" name="batch_id" class="form-control" hidden>
                      @isset($userClasses)
                      @foreach($userClasses as $value)
                      <option value="{{$value->class_id}}">{{$value->class_name}}</option>
                      @endforeach
                      @endisset
                    </select>
                    
                    
                    @error('batch_id')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                  </div>
                  <!-- <div style="font-size: 12px;"> Not required,You can add it, Manage batch is in Guru Actions (Previous Menu).</div> -->
                </div>
              </div>
              
 <div class="main" id="main">
          
<div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <input id="user_id" type="hidden" name="user_id" class="form-control " value="{{$id}}"> 
                    <label class="bmd-label-floating">Intro Title <span class="text-danger">*</span></label>
                    <input id="intro_title" type="text" name="intro_title" class="form-control @error('intro_title') is-invalid @enderror"
                        value="{{old('intro_title')}}" required>
                        <div class="invalid-feedback">Intro Title is required </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="bmd-label-floating">Intro Description <span class="text-danger">*</span></label>
                    <textarea id="intro_description" rows="5" cols="70"  name="intro_description" class="form-control"
                        value="" required>{{old('intro_description')}}</textarea>
                        <div class="invalid-feedback">Intro Description is required </div>
                  </div>
                </div>
                <div class="col-md-4" id="video1" style="display:block">
                  <div class="form-group">
                    <label class="bmd-label-floating">Trailer <span class="text-danger">*</span></label>
                   
                        <input type="file"  name="video_url" class="form-control" id="video_url" required>
                       
                    <!-- <input id="video_url" type="file" name="video_url" class="form-control"
                        value="" required> -->
                        <div class="invalid-feedback">Trailer is required </div>
                    <div id="valid_video_url" style="font-size: 13px;display:block;"><b>.MP4,.MKV,.WEBM(not more than 1 GB)</b></div>
                    
              
                        <div class="progress" id="progress_bar2" style="display:none;height:20px; line-height: 20px;">

            <div class="progress-bar" id="progress_bar_process2" role="progressbar" style="width:0%;">0%</div>

        </div>

        <div id="uploaded_image2" class="alert  success-upload2 alert-dismissible fade show row mt-1" role="alert">  </div>
                  </div>
                </div>
                      
</div>
                <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <input id="user_id" type="hidden" name="user_id" class="form-control " value="{{$id}}"> 
                    <label class="bmd-label-floating">Title <span class="text-danger">*</span></label>
                    <input id="title" type="text" name="title" class="form-control @error('title') is-invalid @enderror"
                        value="{{old('intro_description')}}" required>
                        <div class="invalid-feedback">Title is required </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="bmd-label-floating">Description <span class="text-danger">*</span></label>
                    <textarea id="description" rows="5" cols="70"  name="description" class="form-control"
                        value="" required >{{old('description')}}</textarea>
                        <div class="invalid-feedback">Description is required </div>
                  </div>
                </div>
                <div id="classTypes" class="col-md-4">
                  <div class="form-group">
                    <label class="bmd-label-floating">Select Class Type <span class="text-danger">*</span></label>
                    
                    <select id="class_type" name="class_type" class="form-control class_type" required>
                      
                      <option value="1" @if (old('class_type') == "1") {{ 'selected' }} @endif>One To One</option>
                      <option value="2" @if (old('class_type') == "2") {{ 'selected' }} @endif>Group Class</option>
                    
                    </select>
                  
                    <div class="invalid-feedback">Class Type is required </div>
        </div>
        </div>       
                      
</div>

<div class="row">
             
        <div id="startDate" class="col-md-3" >
                    <div class="form-group">
                        <label class="bmd-label-floating">Start Date </label>
                        <input id="start_date" type="date" name="start_date" class="form-control"
                               value="{{ old('start_date')}}" required>
                               <div class="invalid-feedback">Start Date is required </div>
                    </div>
                </div>
                <div id="endDate" class="col-md-3">
                    <div class="form-group">
                        <label class="bmd-label-floating">End Date</label>
                        <input id="end_date" type="date" name="end_date" class="form-control"
                               value="{{ old('end_date') }}" required>
                               <div class="invalid-feedback">End date is required </div>
                    </div>
                </div>
                <div id="startTime" class="col-md-3">
                    <div class="form-group">
                        <label class="bmd-label-floating">Start Time</label>
                        <input id="start_time" type="time" name="start_time"  class="form-control timeSet"
                               value="{{old('start_time')}}" required>
                               <div class="invalid-feedback">Start Time is required </div>
                    </div>
                </div>
                <div id="endTime" class="col-md-3" >
                    <div class="form-group">
                        <label class="bmd-label-floating">End Time</label>
                        <input id="end_time" type="time" name="end_time" class="form-control timeSet"
                               value="{{old('end_time')}}" required>
                               <div class="invalid-feedback">End Time is required </div>
                    </div>
                </div>
</div>

<div class="row">

               
                <div id="Duration" class="col-md-3">
                    <div class="form-group">
                        <label class="bmd-label-floating">Duration</label>
                        <input id="duration" type="text" name="duration" class="form-control duration"
                               value="{{old('duration')}}" required>
                               <div class="invalid-feedback">Duration is required </div>
                    </div>
                </div>
                <div id="particpents" class="col-md-3 particpents" style="display:none">
                    <div class="form-group">
                        <label class="bmd-label-floating">No.of Participants <span class="text-danger">*</span></label>
                        <input id="particpents" type="text" name="no_of_participents" class="form-control timeSet"
                               value="{{old('no_of_participents')}}" >
                               <div class="invalid-feedback">No.Of Participants is required </div>
                    </div>
                </div>
                <div id="noOfClass" class="col-md-3">
                    <div class="form-group">
                        <label class="bmd-label-floating">No.of Classes <span class="text-danger">*</span></label>
                        <input id="no_of_class" type="text" name="no_of_class" class="form-control timeSet"
                               value="{{old('no_of_class')}}" required>
                               <div class="invalid-feedback">No.Of Class is required </div>
                    </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="bmd-label-floating">Price(USD) <span class="text-danger">*</span></label>
                    <input id="price" type="text" name="price" class="form-control"
                    value="{{old('price')}}" required>
                    <div class="invalid-feedback">Price is required </div>
                  </div>
                </div>
</div>

<div class="row g-3" >
              <div class="col-sm-4 meetingLinks" id ="meetingLinks"style="display:none">
              <div class="form-group">
                    
                        <label class="bmd-label-floating">Meeting Link</label>
                        <input id="meeting_link" type="text" name="meeting_link"  class="form-control timeSet"
                               value="">
                        @error ('meeting_link')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                </div>


 
              <!-- <button type="submit" class="btn btn-primary pull-right">{{($issetpackageDetails) ? 'Submit' : 'Next'}}</button> -->
              <div class="row g-3">
              
<div class="col-12 d-flex justify-content-between">
              <a href="{{ route('tutor.show', $id) }}"  class= "pull-left">
                        <button type="button"  class="btn btn-primary btn-link btn-sm">
                          Back
                        </button>
                      </a>
                     <button type="submit" id ="submit" name="submit_button" class="btn btn-primary btn-sm pull-right" >Submit</button>
                     </div>
          </div>
</div>
            </form>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->
  
@endsection

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script type="text/javascript">
   window.addEventListener("DOMContentLoaded", function(e) {

var form_being_submitted = false;

var checkForm = function(e) {
  var form = e.target;
  if(form_being_submitted) {
    alert("The form is being submitted, please wait a moment...");
    form.submit_button.disabled = true;
    e.preventDefault();
    return;
  }
  form.submit_button.value = "Submitting form...";
      form_being_submitted = true;
    };
    document.getElementById("form").addEventListener("submit", checkForm, false);

  }, false);
      $( document ).ready(function() {
        const type =  $('#class_type').val();
            // clearFields();
           if(type == 1){
            // $('#meetingLinks').show();
            $('#particpents').hide();
           }
           else{
            // $('#meetingLinks').hide();
            $('#particpents').show();
           }  
        $('.alert-success').fadeIn().delay(10000).fadeOut();
      document.getElementById('class_type').addEventListener("change", function (e) {
       
            const type =  $('#class_type').val();
            // clearFields();
           if(type == 1){
            // $('#meetingLinks').show();
            $('#particpents').hide();
           }
           else{
            // $('#meetingLinks').hide();
            $('#particpents').show();
           }
          
});  

document.getElementById('video_url').addEventListener("change", function (e) {

  var fileName = document.getElementById("video_url").value;
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
        if (extFile=="mp4" || extFile=="webm" || extFile=="mkv"){
          $('#valid_video_url').hide();

$('#submit').attr('disabled','disabled');
        }else{
            alert("Only mp4/webm/mkv  files are allowed!");
            document.getElementById("video_url").value = null;
          
        }

  var file_element = document.getElementById('video_url');
console.log("files");
console.log(file_element);
var progress_bar = document.getElementById('progress_bar2');
var progress_bar_process = document.getElementById('progress_bar_process2');

var uploaded_image = document.getElementById('uploaded_image2');
var form_data = new FormData();

form_data.append('videoUrl', file_element.files[0]);

form_data.append('_token', document.getElementsByName('_token')[0].value);

progress_bar.style.display = 'block';

var ajax_request = new XMLHttpRequest();

ajax_request.open("POST", "{{ route('live.StoreVideo',$id) }}");   

ajax_request.upload.addEventListener('progress', function(event){

  var percent_completed = Math.round((event.loaded / event.total) * 100);

  progress_bar_process.style.width = percent_completed + '%';

  progress_bar_process.innerHTML = percent_completed + '% completed';

});
ajax_request.addEventListener('load', function(event){
console.log("response");
var file_data = JSON.parse(event.target.response);

console.log(file_data);

uploaded_image.innerHTML = '<div class="demo-inline-spacing" ><span class="badge bg-label-primary">Files Uploaded Successfully</span></div>';

$('#submit').removeAttr('disabled');

progress_bar.style.display = 'none';
$('.success-upload2').fadeIn().delay(10000).fadeOut();
});

ajax_request.send(form_data);
});
});
  </script>

