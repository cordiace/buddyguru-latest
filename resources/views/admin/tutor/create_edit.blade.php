@php
    $formAction = isset($user) ? route('admin.tutor.update',['id'=>$user->id,'showNext'=>$showNext]) : route('tutor.store');
    $issetUser = isset($user) ? 1 : 0;
    $options=$countries;

@endphp
@php
$configData = Helper::appClasses();
@endphp

@extends('layouts/layoutMaster')

@section('content')

<div class="content">
  <div class="container-fluid">
    <div class="row d-flex justify-content-center">
      <div class="col-lg-12">
      <div class="row">
          <div class="col-md-12">
          @isset($user)
                <a href="{{ route('tutor.show', $user->id) }}" class= "pull-left">
                        <button type="button"  class="btn btn-primary btn-link btn-sm" style=" background-color: #0c82be !important;
    border-color: #0c82be !important;">
                          Back
                  </button></a>
                  @else
                  <a href="{{ route('tutor.index') }}" class= "pull-left">
                        <button type="button"  class="btn btn-primary btn-link btn-sm" style=" background-color: #0c82be !important;
    border-color: #0c82be !important;">
                          Back
                  </button></a>
                  @endif
           
</div>
</div>
        <div class="card">
          <div class="card-header card-header-info">
            <h4 class="card-title" id="content1">
              @isset($user)
                  Edit Guru Details
              @else
                  Add New Guru
              @endisset
            </h4>
            <p class="card-category">Complete Your Profile</p>
          </div>
          <div class="card-body">
            <form action="{{ $formAction }}" id="form" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @isset($user) @method('POST') @endisset
              @csrf

              <div class="row">
                <div class="col-md-6">


                  <div class="form-group">
                    <label class="bmd-label-floating">Profile Picture <span class="text-danger">*</span></label>
                    @isset($user)
                    <div class="row justify-content-md-center">
                                <div class="col-md-3">
                    @if(!empty($user->profile_image)) 
                                    <div class="form-group" >
                                        <img src="{{  asset($user->profile_image) }}"  class="profile-user-img img-fluid img-circle" width="70px" height="70px"/>
                                    </div>
                                    @else
                                    <div class="form-group">
                                       
                                    </div>
                                    @endif
</div></div>
@endif
<!-- @isset($user)
                    <div class="row justify-content-md-center">
                                <div class="col-md-3">
                    @if(!empty($user->profile_image)) 
                                    <div class="form-group" >
                                        <img src="{{  asset('profile_image') }}"  class="profile-user-img img-fluid img-circle" width="70px" height="70px"/>
                                    </div>
                                    @else
                                    <div class="form-group">
                                       
                                    </div>
                                    @endif
</div></div>
@endif -->
<div class="custom-file">
                        <input type="file" accept=".jpg,.jpeg,.png" onchange="validateFileType()"  class="custom-file-input" id="profile_image" @error('profile_image') is-invalid @enderror"  value="{{ old('profile_image') ?? ($issetUser ? $user->image_file_name : '')  }}" autocomplete="profile_image" name="profile_image" {{($issetUser) ? '' : 'required'}}>
                        <label class="custom-file-label" for="profile_image">Choose file</label>
                      </div>
                    <!-- <input id="profile_image" type="file" class="form-control @error('profile_image') is-invalid @enderror"  value="{{ old('profile_image') ?? ($issetUser ? $user->image_file_name : '')  }}" autocomplete="profile_image" name="profile_image" {{($issetUser) ? '' : 'required'}}> -->

                    @error('profile_image')
                    <p class="text-danger">Required png/jpg file</p>
                    @enderror
                    <div id ="valid_profile_image" style="font-size: 13px; display: block;"><p class="text-danger">.jpg,.png</p></div>
                  </div>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                    <label class="bmd-label-floating">Intro Video <span class="text-danger">*</span></label>
                    <div class="custom-file">
                        <input type="file"  name="video_url" class="custom-file-input" id="video_url" value="" {{($issetUser) ? '' : 'required'}}>
                        <label class="custom-file-label" for="video_url">Choose file</label>
                      </div> 
                    <!-- <input id="video_url" type="file" name="video_url" class="form-control file" value="" {{($issetUser) ? '' : 'required'}}> -->
                    @error('video_url')
                    <p class="text-danger">Required and size less than 1 GB</p>
                    @enderror
                    <div  style="display: none" class="progress mt-3" style="height: 25px">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%; height: 100%">75%</div>
                    </div>
                    <div id ="valid" style="font-size: 13px; display: block;"><p class="text-danger">.mp4,.mkv,.webm(not More than 1GB)</p></div>
                </div>
                    @isset($user)
                    <a href="" class="view-portfolio" id="viewVideo" data-video_url="{{asset($TutorDetail->video_url)}}">
                      <button type="button" rel="tooltip" title="Play Video" class="btn float-right btn-primary btn-link btn-sm">
                        <i class="material-icons">play_arrow</i>
                      </button>
                  </a>
                  @endif
                  <button type="button" id= "TrailerUpload" style="display:none;"class=" btn btn-primary btn-sm pull-right"  rel="tooltip" title="upload video" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">upload</i>
</button>
              
                        <div class="progress" id="progress_bar2" style="display:none;height:20px; line-height: 20px;">

            <div class="progress-bar" id="progress_bar_process2" role="progressbar" style="width:0%;">0%</div>

        </div>

        <div id="uploaded_image2" class="alert  success-upload2 alert-dismissible fade show row mt-1" role="alert">  </div>
                  </div>    
</div>
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="bmd-label-floating">Name <span class="text-danger">*</span></label>
                    <input id="name" type="text" name="name" class="form-control"
                        value="{{ old('name') ?? ($issetUser ? $user->name : '')  }}" {{($issetUser) ? '' : 'required'}}>
                    @error ('name')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="bmd-label-floating">Email <span class="text-danger">*</span></label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') ?? ($issetUser ? $user->email : '')  }}" required autocomplete="email">

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
</div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="bmd-label-floating">Commision in Percentage(%) <span class="text-danger">*</span></label>
                    <input id="commision" type="number" name="commision" class="form-control"
                        value="{{ old('commision') ?? ($issetUser ? $user->commision : '')  }}" {{($issetUser) ? '' : 'required'}}>
                    @error ('commision')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
</div>
<div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="bmd-label-floating">Password <span class="text-danger">*</span></label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" {{($issetUser) ? '' : 'required'}} autocomplete="new-password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="bmd-label-floating">Confirm Password <span class="text-danger">*</span></label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" {{($issetUser) ? '' : 'required'}} autocomplete="new-password">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="bmd-label-floating">Mobile Number <span class="text-danger">*</span></label>
                    <input id="phone_number" type="number" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" value="{{ old('phone_number') ?? ($issetUser ? $user->phone_number : '')  }}" required autocomplete="phone_number">

                    @error('phone_number')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                </div>
              
              </div>
              <div class="row">
              <div class="col-md-4">
                  <div class="form-group">
                    <label  class="bmd-label-floating">Country <span class="text-danger">*</span></label>
                    <select style="width: 100%;" data-dropdown-css-class="select2-purple" data-placeholder="Select a country"  name="location" id="location" class="form-control  @error('location') is-invalid @enderror">
                  
                        <!-- <option value=""></option> -->
                       
                        @foreach ($options as $key => $value)
                        <option value="{{ $value['id'] }}"  {{  ($issetUser && $user->location == $value['id']) || (old('location')==$value['id'])  ? 'selected' : ''}}>{{ $value['country_name'] }}</option>
                      @endforeach
                    </select>
                    @error('location')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                  </div>
                </div>
               
                <div class="col-md-4">
                <div class="form-group">
                  <label  class="bmd-label-floating">Select Your Skill <span class="text-danger">*</span></label>
                  <select name="skills[]" id="skills" class="select2 form-control" multiple="multiple" data-dropdown-css-class="select2-purple" data-placeholder="Select Skill" style="width: 100%;">
                  @if(isset($user))
                        @foreach ($skill_subset as $skill)
        
                       
                        <option style="
    background-color: #cd713f;" value="{{ $skill->id }}" {{ (collect(old('skills'))->contains($skill->id)) ? 'selected':'' }}  @foreach($tutorskill as $sublist){{$sublist == $skill->id ? 'selected': ''}}   @endforeach  > {{ $skill->skill_name }}</option>
                        @endforeach
                       @else
                        @foreach ($skill_subset as $skill)
          
        <option value="{{ $skill->id }}" {{ (collect(old('skills'))->contains($skill->id)) ? 'selected':'' }} > {{ $skill->skill_name }}</option>
        @endforeach
        @endif
                  </select>
                </div>    
</div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label  class="bmd-label-floating">DOB <span class="text-danger">*</span></label>
                        <input id="dob" type="date" name="dob" class="form-control"
                               value="{{ old('dob') ?? ($issetUser ? substr($user->dob,0,-9) : '')  }}" required>
                        @error ('dob')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
              </div>
<div class="row">
               
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Description <span class="text-danger">*</span></label>
                    <textarea id="description" cols="70" rows="5"  class="form-control @error('description') is-invalid @enderror" 
                    name="description" value="{{ old('description') ?? ($issetUser ? $user->description : '')  }}" required autocomplete="description">{{ old('description') ?? ($issetUser ? $user->description : '')  }}</textarea>
                    <!-- <textarea id="description"  rows="4" cols="50" name="description" class="form-control"
                        value="{{ old('description') ?? ($issetUser ? $user->description : '')  }}"{{($issetUser) ? '' : 'required'}} ></textarea> -->
                    @error ('description')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>
             
             
             

              

                  <!-- <div class="col-md-6">
                    <div class="form-group">
                      <label class="bmd-label-floating">Select Your Skill <span class="text-danger">*</span></label>
                      <select name="skills[]" id="skills" class="select2-multiple form-control @error('skills') is-invalid @enderror" multiple= "multiple">
                       
                        @if(isset($user))
                        @foreach ($skill_subset as $skill)
        
                       
                        <option value="{{ $skill->id }}" {{ (collect(old('skills'))->contains($skill->id)) ? 'selected':'' }}  @foreach($tutorskill as $sublist){{$sublist == $skill->id ? 'selected': ''}}   @endforeach  > {{ $skill->skill_name }}</option>
                        @endforeach
                       @else
                        @foreach ($skill_subset as $skill)
          
        <option value="{{ $skill->id }}" {{ (collect(old('skills'))->contains($skill->id)) ? 'selected':'' }} > {{ $skill->skill_name }}</option>
        @endforeach
        @endif
                      </select>
                    </div>
                  </div> -->

               

                  {{-- <div class="col-md-6">
                    <div class="form-group">
                      <label class="bmd-label-floating">Sub Category </label>
                      <select name="sub_category" id="sub_category" class="form-control @error('sub_category') is-invalid @enderror">
                          <option value=""></option>
                          @isset($user)
                          @foreach ($sub_category as $key => $value)
                          <option value="{{ $value['id'] }}" {{($issetUser && $user->tutor_detail->sub_category == $value['id']) ? 'selected' : ''}} >{{ $value['sub_category_name'] }}</option>
                        @endforeach
                        @endisset
                      </select>
                    </div>
                  </div> --}}
              
                {{-- <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="bmd-label-floating">Skills</label>
                      <select name="skills" id="skills"  class="select2 form-control @error('skills') is-invalid @enderror" multiple>
                          <option value=""></option>
                          
                          @foreach ($skills as $key => $value)
                          <option value="{{ $value['id'] }}" {{($issetUser && $user->tutor_detail->skills == $value['id']) ? 'selected' : ''}}>{{ $value['skill_name'] }}</option>
                        @endforeach
                        
                      </select>
                    </div>
                  </div>
                </div> --}}
                  {{-- <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="bmd-label-floating">About</label>
                      <input id="about" type="text" name="about" class="form-control"
                             value="{{ old('about') ?? ($issetUser ? $user->tutor_detail->about : '')  }}">
                      @error ('about')
                      <p class="text-danger">{{ $message }}</p>
                      @enderror
                    </div>
                  </div>
                  </div>
                  <div class="row">
                  <div class="col-md-12">
                      <label class="bmd-label-floating">Experience</label>
                      <div class="form-group add_another">
                      @isset($user->tutor_detail->experience)
                        @php
                          $i = 0;
                          $exp=json_decode($user->tutor_detail->experience, true);
                          $len = count($exp);
                        @endphp
                        @foreach ( $exp as $experience)
                          <div class="row">
                          <div class="col-md-5">
                              @if($i == 0)
                              <label class="">Name</label>
                              @endif
                              <input id="experience" type="text" name="college_name[]" class="form-control"
                             value="{{ old('experience') ?? ($issetUser ? $experience['name'] : '')  }}">
                      @error ('experience')
                          <p class="text-danger">{{ $message }}</p>
                          @enderror
                          </div>
                          <div class="col-md-5">
                              @if($i == 0)
                              <label class="">Designation</label>
                              @endif
                              <input id="experience" type="text" name="designation[]" class="form-control"
                             value="{{ old('experience') ?? ($issetUser ? $experience['designation'] : '')  }}">
                      @error ('experience')
                      <p class="text-danger">{{ $message }}</p>
                      @enderror
                          </div>

                          @if($i != 0)
                              <div class="col-md-2">
                                <div class="form-group">
                                  <button type="button" rel="tooltip" title="" class="remove btn btn-primary btn-link btn-sm">
                                    <i class="material-icons">remove</i>
                                    <div class="ripple-container"></div></button>
                                </div>
                              </div>
                            @endif
                          </div>
                          @php
                            $i++;
                          @endphp
                        @endforeach
                        @endisset
                      <div class="row">
                        <div class="col-md-5">
                          <div class="form-group">
                            <input type="text" {{(isset($exp)) ? '':'required' }} name="college_name[]" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-5">
                          <div class="form-group">
                            <input type="text" {{(isset($exp)) ? '':'required' }} name="designation[]" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <button type="button" rel="tooltip" title="" class="add btn btn-primary btn-link btn-sm">
                              <i class="material-icons">add</i>
                              <div class="ripple-container"></div></button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                    </div>
                  </div>
                </div> --}}

                <div class="row">
                <!-- <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Cover Letter <span class="text-danger">*</span></label>
                    <input id="cover_letter" type="file" value="" class="form-control @error('cover_letter') is-invalid @enderror" name="cover_letter" {{($issetUser) ? '' : 'required'}}>
                    @isset($TutorDetail)
                    @if(!empty($TutorDetail->cover_letter)) 
                                    <div class="form-group">
                                        <a href="{{  asset($TutorDetail->cover_letter) }}" class="img-thumbnail">view cover letter</a>
                                    </div>
                                    @else
                                    <div class="form-group">
                                       
                                    </div>
                                    @endif
                    @endif
                    @error('cover_letter')  
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <div id ="valid_cover_letter" style="font-size: 13px; display: block;"><p class="text-danger">.DOC,.PDF</p></div>
                  </div>
                </div> -->
               
                </div>
                <!-- @isset($user)
                <a href="{{ route('tutor.show', $user->id) }}" class= "pull-left">
                        <button type="button"  class="btn btn-primary btn-link btn-sm">
                          Back
                  </button></a>
                  @else
                  <a href="{{ route('tutor.index') }}" class= "pull-left">
                        <button type="button"  class="btn btn-primary btn-link btn-sm">
                          Back
                  </button></a>
                  @endif
            -->
                  <div class="col-md-12" >
                  <div class="form-group" style="float: right;">
                 @isset($showNext)
                  @if($showNext =='false')
              <button id ="submit" style=" background-color: #0c82be !important;
    border-color: #0c82be !important;" type="submit" name="submit_button" class="btn btn-primary btn-sm">Submit</button>
             
             @endif
             @endisset
             @if($issetUser == 0)
             <button id ="submit" type="submit" style=" background-color: #0c82be !important;
    border-color: #0c82be !important;" name="submit_button" class="btn btn-primary btn-sm">Next</button>
             @endif
             
             <div class="clearfix"></div>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- @isset($user)
    <div class="row d-flex justify-content-center">
      <div class="col-lg-8">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              Delete this Guru
            </h4>
          </div>
          <div class="card-body">
            <form action="{{ route('tutor.destroy', $user->id) }}" method="POST">
                @csrf
                @method('DELETE')
              <div class="row">
                <div class="col-md-12">
                  Do you really want to do this ? Make sure you are selected the right item. This action is irreversible.
                  All data associated with this item will be erased permanantly.
                </div>
              </div>
              <button type="submit" class="btn btn-primary pull-right">Delete</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
    </div>
    @endisset -->
  </div>
</div>
<div id="playModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">View Video</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body" id="divVideo">
            <video width="320" height="240" controls>
              <source src="" type="video/mp4">
              Your browser does not support the video tag.
            </video>
          </div>

        </div>

      </div>
    </div>
@endsection

@section('custom-scripts')

 <script type="text/javascript">
  function validateFileType(){
        var fileName = document.getElementById("profile_image").value;
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
        if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){
          $('#valid_profile_image').hide();
        }else{
            alert("Only jpg/jpeg and png files are allowed!");
            document.getElementById("profile_image").value = null;
            $('#valid_profile_image').show();
        }
      }
      window.addEventListener("DOMContentLoaded", function(e) {

var form_being_submitted = false;

var checkForm = function(e) {
  var form = e.target;  
  if(form_being_submitted) {
    alert("The form is being submitted, please wait a moment...");
    form.submit_button.disabled = true;
    e.preventDefault();
    return;
  }
  form.submit_button.value = "Submitting form...";
      form_being_submitted = true;
    };
    document.getElementById("form").addEventListener("submit", checkForm, false);

  }, false);
      $( document ).ready(function() {
       

          $( ".add" ).on( "click", function(e) {
              e.preventDefault();
              // $('.add_another').append('<div class="row"> <div class="col-md-5"> <div class="form-group"> <input type="text" {{(isset($exp)) ? '':'required' }} name="college_name[]" class="form-control"> </div> </div> <div class="col-md-5"> <div class="form-group"> <input type="text" {{(isset($exp)) ? '':'required' }} name="designation[]" class="form-control"> </div> </div> <div class="col-md-2"> <div class="form-group"> <button type="button" rel="tooltip" title="" class="remove btn btn-primary btn-link btn-sm"><i class="material-icons">remove</i><div class="ripple-container"></div></button> </div> </div> </div>');
          });
          $( "body" ).on( "click",".remove", function(e) {
              e.preventDefault();
              $(this).parent().parent().parent().remove();
          });

          $( ".view-portfolio" ).on( "click", function(e) {
          e.preventDefault();


          $('source').attr('src',$(this).attr('data-video_url'));
          $("#divVideo video")[0].load();
          $('#playModal').modal('show');

        });
        // document.getElementById('profile_image').addEventListener("change", function (e) {
        //   $('#valid_profile_image').hide();
        // });
       

        document.getElementById('video_url').addEventListener("change", function (e) {
          var fileName = document.getElementById("video_url").value;
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
        if (extFile=="mp4" || extFile=="webm" || extFile=="mkv"){
          $('#valid').hide();
$('#TrailerUpload').show();
$('#viewVideo').hide();
$('#submit').attr('disabled','disabled');
        }else{
            alert("Only mp4/mkv/webm  files are allowed!");
            document.getElementById("video_url").value = null;
          
        }
        
});
$(document).on('click', '#TrailerUpload', function() {
  $('#TrailerUpload').hide();
  var file_element = document.getElementById('video_url');
console.log("files");
console.log(file_element);
var progress_bar = document.getElementById('progress_bar2');
var progress_bar_process = document.getElementById('progress_bar_process2');

var uploaded_image = document.getElementById('uploaded_image2');
var form_data = new FormData();

form_data.append('videoUrl', file_element.files[0]);

form_data.append('_token', document.getElementsByName('_token')[0].value);

progress_bar.style.display = 'block';

var ajax_request = new XMLHttpRequest();

ajax_request.open("POST", "{{ route('profile.StoreVideo') }}");   

ajax_request.upload.addEventListener('progress', function(event){

  var percent_completed = Math.round((event.loaded / event.total) * 100);

  progress_bar_process.style.width = percent_completed + '%';

  progress_bar_process.innerHTML = percent_completed + '% completed';

});
ajax_request.addEventListener('load', function(event){
console.log("response");
var file_data = JSON.parse(event.target.response);

console.log(file_data);

uploaded_image.innerHTML = '<div class="alert col-xs-2  alert-success" style="width: 203px;height: 54px">Files Uploaded Successfully</div>';

$('#submit').removeAttr('disabled');

progress_bar.style.display = 'none';
$('.success-upload2').fadeIn().delay(10000).fadeOut();
});

ajax_request.send(form_data);
});

      });
  </script>
  <script type="text/javascript">
   
  $(document).ready(function() { 
      
           $("#category").on('change', function(e) {
             e.preventDefault();
             var category = $('#category').val();
             console.log(category)
          //    $.ajax({
          //        url: "",
          //        type:"GET",
          //        data:{
          //            "_token": "{{ csrf_token() }}",
          //            "category": category,
          //        },
          //        success:function(data){
          //            $('#sub_category').empty();

          //            $.each(data,function(index,subcatObj){
          //                $('#sub_category').append('<option value ="'+subcatObj.id+'">'+subcatObj.sub_category_name+'</option>');
          //            });

          //        },
          //  });

      });
      });
  </script>
@endsection
