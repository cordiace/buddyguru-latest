@php
$issetpackage = count($userPackages) >0 ? 1 : 0;
@endphp
@extends('layouts.dashboard')
@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
      <div class="row">
          <div class="col-md-12">
      <a href="{{ route('tutor.show', $id) }}" class= "pull-left">
                        <button type="button"  class="btn btn-primary btn-link btn-sm" style=" background-color: #0c82be !important;
    border-color: #0c82be !important;">
                     Back
                  </button></a>
</div>
</div>
        <div class="card">
          <div class="card-header card-header-info">
          @if($issetpackage ==0)
            <h4 class="card-title " style="display:inline-flex"><span style="padding-left: 337px;padding-top: 8px;">Guru Classes</span></h4>
           @else
           <h4 class="card-title " style="display:inline-flex"><span style="padding-left: 294px;padding-top: 8px;">Guru Classes</span></h4>
           @endif
            <!-- <p class="card-category"> Guru Class List</p> -->
            @if($issetpackage ==0)
            <ul class="nav nav-tabs"style=" display:inline-block;float: right;" data-tabs="tabs">
                       
                         <li class="nav-item" >
                          <a class="nav-link active" style=" height: 38px;
    line-height: 0;" href="{{route('tutor.package.create',['id' =>$id,'showNext'=>'false'])}}" >
                            <i class="material-icons">add</i> Add
                            </a>
                        </li>
                      </ul>
@endif
          </div>
          <div class="card-body">
          <table id="example1" class="table table-bordered table-striped">
                <thead class="text-center">
                  <th>ID</th>
                  <th>Intro Title</th>
                  <th>Intro Description</th>
                  <th class="text-center">Action</th>
                </thead>
                <tbody>
                @isset($userPackages)
                  @foreach($userPackages as $userPackage)
                  <tr> 
                    <td>{{$loop->index + 1}}</td>
                    <td>{{$userPackage->intro_title}}</td>

                      <td>{{Str::limit($userPackage->intro_description,40)}} </td>
                      <td class="text-center py-0 align-middle">
                      <div class="btn-group btn-group-sm">
                      <a href="{{ route('tutor.package.delete', $userPackage->id) }}" rel="tooltip" title="Delete" class="btn btn-danger btn-sm">
                        <!-- <button type="button" rel="tooltip" title="Delete" class="btn btn-primary btn-link btn-sm"> -->
                        <i class="fas fa-trash"></i>
                        <!-- </button> -->
                      </a>
                      <a href="{{ route('tutor.package.edit', $userPackage->id) }}"rel="tooltip" title="Edit" class="btn btn-info  btn-sm">
                        <!-- <button type="button" rel="tooltip" title="Edit" class="btn btn-primary btn-link btn-sm"> -->
                        <i class="fas fa-edit"></i>
                        <!-- </button> -->
                      </a>
                      <a href="{{ route('tutor.package.show', $userPackage->id) }}" rel="tooltip" title="View Guru Class" class="btn btn-primary btn-sm">
                        <!-- <button type="button" rel="tooltip" title="View Guru Class" class="btn btn-primary btn-link btn-sm"> -->
                        <i class="fas fa-eye"></i>
                        <!-- </button> -->
                      </a>
</div>
                    </td>
                  </tr>
                  @endforeach 
                @endisset
                </tbody>
              </table>

            </div>
         
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('custom-scripts')
<script type="text/javascript">
$( document ).ready(function() {
  $( ".delete-banner" ).on( "click", function(e) {
    e.preventDefault();
    $('#deleteBanner').attr('action',$(this).data("route"));
    $('#deleteBanner').attr('method','POST');
    $('#delete-banner').modal('show');
    });
});
</script>
@endsection
