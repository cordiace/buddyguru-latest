@php
    $formAction = isset($banner) ? route('general-generalsetting.update', $banner->id) : route('generalsetting.store');
    $issetBanner = isset($banner) ? 1 : 0;
@endphp
@extends('layouts/layoutMaster')

@section('content')

<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header card-header-info">
            <h4 class="card-title">
              @isset($banner)
                  Edit Details
              @else
                  Add New
              @endisset
            </h4>
          </div>
          <div class="card-body">
            <form action="{{ $formAction }}" method="POST" accept-charset="UTF-8">
              @isset($banner) @method('POST') @endisset
              @csrf



              <div class="mb-3">
                    <label class="bmd-label-floating">Commision in Percentage(%)<span class="text-danger">*</span></label>
                    <input name="commision" id="commision" class="form-control"  value="{{ old('banner') ?? ($issetBanner ? $banner->commision: '')  }}">  
                    @error ('commision')
                        <p class="text-danger">Commision is required</p>
                    @enderror
               </div>

               <div class="row">
            <div class="col-12">
           
              <button type="submit" class="btn btn-primary btn-sm float-right">Submit</button>
              <div class="clearfix"></div>
</div>
</div>
            </form>
          </div>
        </div>
      </div>
    </div>
   
  </div>
</div>
@endsection
