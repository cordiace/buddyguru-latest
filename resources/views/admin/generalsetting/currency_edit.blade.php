@php
    $formAction = isset($getCurrency) ? route('admin.settings.updateCurrency', $getCurrency->id) : route('admin.settings.storeCurrency');
    $issetCurrency = isset($getCurrency) ? 1 : 0;
@endphp
@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header card-header-info">  
            <h4 class="card-title">
              @isset($getCurrency)
                  Update Currency Details
              @else
                  Add New
              @endisset
            </h4>
          </div>
          <div class="card-body">
            <form action="{{ $formAction }}" method="POST" accept-charset="UTF-8">
              <!-- @isset($getCurrency) @method('PUT') @endisset -->
              @csrf

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">USD (Base)</label>
                    <input name="currency_usd" id="currency_usd" class="form-control"  value="{{ old('getCurrency') ?? ($issetCurrency ? $getCurrency->usd: '')  }}">  
                    @error ('currency_usd')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                   <div class="form-group">
                    <label class="bmd-label-floating">SG $ (Based on USD)</label>
                    <input name="currency_sg" id="currency_sg" class="form-control"  value="{{ old('getCurrency') ?? ($issetCurrency ? $getCurrency->sg: '')  }}">  
                    @error ('currency_sg')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label class="bmd-label-floating">INR(Based on USD)</label>
                    <input name="currency_inr" id="currency_inr" class="form-control"  value="{{ old('getCurrency') ?? ($issetCurrency ? $getCurrency->inr: '')  }}">  
                    @error ('currency_inr')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                   <div class="form-group">
                    <label class="bmd-label-floating">AED(Based on USD)</label>
                    <input name="currency_aed" id="currency_aed" class="form-control"  value="{{ old('getCurrency') ?? ($issetCurrency ? $getCurrency->aed: '')  }}">  
                    @error ('currency_aed')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label class="bmd-label-floating">POUND(Based on USD)</label>
                    <input name="currency_pound" id="currency_pound" class="form-control"  value="{{ old('getCurrency') ?? ($issetCurrency ? $getCurrency->pound: '')  }}">  
                    @error ('currency_pound')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label class="bmd-label-floating">AUD(Based on USD)</label>
                    <input name="currency_aud" id="currency_aud" class="form-control"  value="{{ old('getCurrency') ?? ($issetCurrency ? $getCurrency->aud: '')  }}">  
                    @error ('currency_aud')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                 <div class="form-group">
                    <label class="pull-right">Last Updated by <span style="color:#656363">{{ old('getCurrency') ?? ($issetCurrency ? date('d F Y H:i A', strtotime($getCurrency->update_date)): '')  }}</span></label>
                  </div>
                </div>
              </div>

              <button type="submit" class="btn btn-primary pull-right">Update</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
    </div>
   
  </div>
</div>
@endsection