@php
    $formAction = route('general-admin.currency_settings.storeCurrency');
   
@endphp
@extends('layouts/layoutMaster')

@section('content')

<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-info">
            <h4 class="card-title">
             
                  Add New Currency
              
            </h4>
          </div>
          <div class="card-body">
            <form action="{{ $formAction }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @isset($banner) @method('POST') @endisset
              @csrf
             

              <div class="mb-3">
                    <label class="bmd-label-floating">Currency<span class="text-danger">*</span></label>
                    <input name="currency" type="text" id="Currency" class="form-control" value="{{ old('currency')}}">
                    @error ('currency')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>

                  <div class="mb-3">
                    <label class="bmd-label-floating">price<span class="text-danger">*</span></label>
                    <input name="price" type="number" id="price" class="form-control" value="{{ old('price')}}">
                    @error ('price')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                  <div class="row">
            <div class="col-12">
              <a href="{{ route('general-admin.settings.viewCurrency') }}"  class= "float-left">
                        <button type="button"  class="btn btn-primary float-left  btn-sm">
                          Back
                        </button>
                      </a>
              <button type="submit" class="btn btn-primary btn-sm float-right">Submit</button>
              <div class="clearfix"></div>
</div>
</div>
            </form>
          </div>
        </div>
      </div>
    </div>
    @isset($banner)
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              Delete this banner
            </h4>
          </div>
          <div class="card-body">
            <form action="{{ route('banner.destroy', $banner->id) }}" method="POST">
                @csrf
                @method('DELETE')
              <div class="row">
                <div class="col-md-12">
                  Do you really want to do this ? Make sure you are selected the right item. This action is irreversible.
                  All data associated with this item will be erased permanantly.
                </div>
              </div>
              
              <button type="submit" class="btn btn-primary float-right">Delete</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
    </div>
    @endisset
  </div>
</div>
@endsection
