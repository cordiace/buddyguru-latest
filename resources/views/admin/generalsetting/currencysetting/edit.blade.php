@php
    $formAction = route('admin.currency_settings.updateCurrency',$Currencysetting->id);
@endphp
@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              
                  Edit Details
            </h4>
          </div>
          <div class="card-body">
            <form action="{{ $formAction }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @isset($banner) @method('PUT') @endisset
              @csrf
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Currency</label>
                    <input name="currency" type="text" id="currency" class="form-control" value="{{$Currencysetting->symbol}}" required>
                    @error ('currency')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">price</label>
                    <input name="price" type="number" id="Currency" class="form-control" value="{{$Currencysetting->value}}" required>
                    @error ('price')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>

              <a href="{{ route('admin.settings.viewCurrency') }}"  class= "float-left">
                        <button type="button"  class="btn btn-primary float-left btn-link btn-sm">
                          Back
                        </button>
                      </a>
              <button type="submit" class="btn btn-primary float-right">Submit</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
@endsection
