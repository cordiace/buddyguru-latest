@php
    $formAction =  route('general-settings.currencyUpdate');
    $CurrencySetting = isset($CurrencySettings) ? 1 : 0;
@endphp
@extends('layouts/layoutMaster')

@section('content')

<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">

        <div class="card-header card-header-info ">
<div class="dt-buttons btn-group flex-wrap">
     <div class="btn-group">
     </div>
     <a  href="{{route('general-admin.currency_settings.createCurrency')}}">
       <button class="btn btn-secondary add-new btn-primary" tabindex="0"
        aria-controls="DataTables_Table_0" type="button">
        <span><i class="ti ti-plus me-0 me-sm-1 ti-xs"></i>
        <span class="d-none d-sm-inline-block">Add</span>
      </span></button></a>
     </div>
</div>

        
          @isset($CurrencySetting)
          <div class="card-body">
            <form action="{{ $formAction }}" id="myform" method="POST" accept-charset="UTF-8">
             
              @csrf

              @foreach ($CurrencySettings as $key => $CurrencySetting)

              <div class="mb-3">
                    <label class="bmd-label-floating">{{strtoupper($CurrencySetting->symbol)}}$(Based on USD)</label>  
                  <label  class="d-flex flex-row justify-content-end"><span class="badge bg-label-warning">Live Rate - {{$convertedAmounts[$key]}}</span></label>
                    <input name="commision[{{$CurrencySetting->id}}]" id="commision" class="form-control"  value="{{ old('commision') ?? ($CurrencySetting ? $CurrencySetting->value: '')  }}">  
                  
                    @error ('commision')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                 
@endforeach
<div class="row">
            <div class="col-12">       
<button type="button" id="clear-btn" onclick= "clearInput()" class="btn btn-primary btn-sm float-right">Clear All</button>
              <button type="submit" class="btn btn-primary btn-sm float-right">Submit</button>
              <div class="clearfix"></div>
</div>
</div>
            </form>
          </div>
@endisset



        </div>
      </div>
    </div>
  </div>
</div>
@endsection
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript">
$( document ).ready(function() {
  $( ".delete-banner" ).on( "click", function(e) {
    e.preventDefault();
    $('#deleteBanner').attr('action',$(this).data("route"));
    $('#deleteBanner').attr('method','POST');
    $('#delete-banner').modal('show');
    });
  });


function clearInput(){

  $("input[name^='commision']").val("");
 }
</script>

