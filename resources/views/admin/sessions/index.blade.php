@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <!-- <h4 class="card-title ">Users</h4>
            <p class="card-category"> Here is a subtitle for this table</p> -->
            <ul class="nav nav-tabs" data-tabs="tabs">
                        <li class="nav-item">
                          <a class="nav-link active" href="{{route('sessions.create')}}" >
                            <i class="material-icons">supervisor_account</i> Add Session
                            <div class="ripple-container"></div>
                          <div class="ripple-container"></div></a>
                        </li>
                      </ul>

          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th width="">ID</th>
                  <th width="">Guru</th>
                  <th width="">Batch</th>
                  <th width="">Name</th>
                  <th width="">Date</th>
                  <th width="">Time</th>
                  <th class="text-right" width="">Action</th>
                </thead>
                <tbody>
                  @isset($sessions)
                  @foreach($sessions as $teacher_session)
                  <tr>
                    <td width="5%">{{$loop->index + 1}}</td>
                    <td width="">{{$teacher_session->user->name}}</td>
                    <td width="">{{$teacher_session->batch->batch_name}}</td>
                    <td>{{$teacher_session->session_name}}</td>
                    <td>{{$teacher_session->date}}</td>
                    <td>{{$teacher_session->time}}</td>
                    <td class="td-actions" width="5%">
                      <a href="{{ route('sessions.edit', $teacher_session->id) }}">
                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">edit</i>
                        </button>
                      </a>
                    </td>
                  </tr>
                  @endforeach
                  @endisset
                </tbody>
              </table>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('custom-scripts')
<script type="text/javascript">
$( document ).ready(function() {
  $( ".delete-banner" ).on( "click", function(e) {
    e.preventDefault();
    $('#deleteBanner').attr('action',$(this).data("route"));
    $('#deleteBanner').attr('method','POST');
    $('#delete-banner').modal('show');
    });
});
</script>
@endsection
