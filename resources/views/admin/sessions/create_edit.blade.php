@php
$formAction = isset($sessions) ? route('sessions.update', $sessions->id) : route('sessions.store');
$issetSessions = isset($sessions) ? 1 : 0;

@endphp
@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
    <div class="container-fluid">
        <div class="row d-flex justify-content-center">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title">
                            @isset($sessions)
                            Edit Session Details
                            @else
                            Add New Session
                            @endisset
                        </h4>
                    </div>
                    <div class="card-body">
                        <form action="{{ $formAction }}" method="POST" accept-charset="UTF-8"
                            enctype="multipart/form-data">
                            @isset($sessions) @method('PUT') @endisset
                            @csrf
                            {{-- <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Select Subject</label>
                                        <select name="category_id" id="category_id" class="form-control @error('category_id') is-invalid @enderror">
                                            @foreach ($category as $key => $value)
                                                <option value="{{ $value['id'] }}" {{($issetSessions && $sessions->category_id == $value['id']) ? 'selected' : ''}}>{{ $value['category_name'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div> --}}
                            {{-- <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Select Grade</label>
                                        <select name="grade_id" id="grade_id" class="form-control @error('grade') is-invalid @enderror">
                                            @foreach ($grades as $key => $value)
                                                <option value="{{ $value['id'] }}" {{($issetSessions && $sessions->grade_id == $value['id']) ? 'selected' : ''}}>{{ $value['grade_name'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div> --}}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Select Guru</label>
                                        <select name="user_id" id="user_id" class="form-control @error('user_id') is-invalid @enderror">
                                          <option  >select guru</option>
                                            @foreach ($gurus as $value)
                                            <option {{(isset($sessions) && $sessions->user_id == $value->id) ? 'selected' : ''}} value="{{ $value->id }}" >{{ $value->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Select Batch</label>
                                        <select name="batch_id" id="batch_id" class="form-control @error('batch_id') is-invalid @enderror">

                                          {{--  @foreach ($batches as $value)
                                            <option {{(isset($sessions) && $sessions->batch_id == $value->id) ? 'selected' : ''}} value="{{ $value->id }}" >{{ $value->batch_name }}</option>
                                            @endforeach --}}
                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Session Name</label>
                                        <input id="skill" type="text" name="session_name" class="form-control"
                                            value="{{ old('session_name') ?? ($issetSessions ? $sessions->session_name : '')  }}">
                                        @error ('session_name')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                {{--<div class="col-md-12">--}}
                                <div class="col-md-3">
                                    <label class="bmd-label-floating">Session Date</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input id="date" type="date" name="date" class="form-control"
                                            value="{{ old('date') ?? ($issetSessions ? $sessions->date : '')  }}">
                                        @error ('date')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            {{--</div>--}}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Session Time</label>
                                        <input id="time" type="time" name="time" class="form-control"
                                            value="{{ old('time') ?? ($issetSessions ? substr($sessions->time, 0,-3) : '')  }}">
                                        @error ('time')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            {{-- <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Meeting Link</label>
                                        <input id="meeting_link" type="text" name="meeting_link" class="form-control"
                                            value="{{ old('meeting_link') ?? ($issetSessions ? $sessions->meeting_link : '')  }}">
                                        @error ('meeting_link')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                            </div> --}}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Describe your session</label>
                                        <input id="description" type="text" name="description" class="form-control"
                                            value="{{ old('description') ?? ($issetSessions ? $sessions->description : '')  }}">
                                        @error ('description')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">What student will learnn</label>
                                        <input id="what_learn" type="text" name="what_learn[]" class="form-control"
                                            value="{{ old('what_learn') ?? ($issetSessions ? $sessions->what_learn : '')  }}">
                                        @error ('what_learn')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                            </div> --}}


                            {{--<div class="row">--}}
                            {{--<div class="col-md-12">--}}
                            {{--<div class="form-group">--}}
                            {{--<label class="bmd-label-floating">What student will learn</label>--}}
                            {{--<input id="learn" type="text" name="learn" class="form-control"--}}
                            {{--value="{{ old('learn') ?? ($issetSessions ? $sessions->learn : '')  }}">--}}
                            {{--@error ('learn')--}}
                            {{--<p class="text-danger">{{ $message }}</p>--}}
                            {{--@enderror--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
        @isset($sessions)
        <div class="row d-flex justify-content-center">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title">
                            Delete this Session
                        </h4>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('sessions.destroy', $sessions->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <div class="row">
                                <div class="col-md-12">
                                    Do you really want to do this ? Make sure you are selected the right item. This
                                    action is irreversible.
                                    All data associated with this item will be erased permanantly.
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Delete</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @endisset
    </div>
</div>
@endsection
@section('custom-scripts')
<script>
 $(document).ready(function() {
   if($('#user_id').val())
   {
     <?php
     if($issetSessions)
     {
       ?>
       var batch_id = <?= $sessions->batch_id ?>;
       <?php
     }
      ?>
     getBatch($('#user_id').val(),batch_id);
   }
   $(document).on('change', '#user_id', function () {
     var id = $(this).val();
     getBatch(id,0);
   });
   function getBatch(id,batch_id)
   {
     $.ajax({
        type:'GET',
        url:'{{ route("common.getBatch") }}',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data: {
          "_token": "{{ csrf_token() }}",
          "id": id
       },
        success:function(data){
          $('#batch_id').empty();
          $.each(data, function(key, value) {
            if(value['id'] == batch_id)
            {
              $('#batch_id').append($("<option selected></option>").attr("value", value['id']).text(value['batch_name']));
            }
            else {
              $('#batch_id').append($("<option></option>").attr("value", value['id']).text(value['batch_name']));
            }

          });
        }
     });
   }

});
</script>
@endsection
